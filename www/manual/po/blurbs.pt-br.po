# Brazilian Portuguese translation of http://www.gnu.org/manual/blurbs.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Rafael Fontenelle <rafaelff@gnome.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: blurbs.html\n"
"POT-Creation-Date: 2022-01-07 09:28+0000\n"
"PO-Revision-Date: 2020-07-26 17:11-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2022-01-07 09:28+0000\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: Content of: <title>
msgid "GNU Package Blurbs - GNU Project - Free Software Foundation"
msgstr "Apresentação de pacotes GNU - Projeto GNU - Free Software Foundation"

#. type: Content of: <div><h2>
msgid "GNU Package Blurbs"
msgstr "Apresentação de pacotes GNU"

#. type: Content of: <div><div><p>
#, fuzzy
#| msgid ""
#| "This file gives short blurbs for all <a href=\"/software/software.html"
#| "\">official GNU packages</a> with links to their home pages.  <a href=\"/"
#| "manual/manual.html\">More documentation of GNU packages</a>."
msgid ""
"The following list gives short blurbs for <a href=\"/software/software.html"
"\">official GNU packages</a> with links to their home pages.  <a href=\"/"
"manual/manual.html\">More documentation of GNU packages</a>."
msgstr ""
"Este arquivo fornece uma apresentação breve de todos os <a href=\"/software/"
"software.html\">pacotes GNU oficiais</a> com links para suas páginas.  <a "
"href=\"/manual/manual.html\">Mais documentação de pacotes GNU</a>."

#. type: Content of: <div><p>
msgid "See <a href=\"#topinfo\">info about this list at top</a>."
msgstr "Veja <a href=\"#topinfo\">informações sobre esta lista no topo</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e a "
"contribuição com traduções das páginas deste site."

#. type: Content of: <div><p>
#, fuzzy
#| msgid "Copyright &copy; 2014, 2020 Free Software Foundation, Inc."
msgid "Copyright &copy; 2013, 2022 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2014, 2020 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Esta página está licenciada sob uma licença <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle <a href=\"mailto:rafaelff@gnome.org\">&lt;"
"rafaelff@gnome.org&gt;</a>, 2020."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"

#~ msgid "Copyright &copy; 2014, 2020, 2022 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014, 2020, 2022 Free Software Foundation, Inc."

#~ msgid "Copyright &copy; 2014 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014 Free Software Foundation, Inc."

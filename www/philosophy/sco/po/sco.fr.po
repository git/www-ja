# French translation of http://www.gnu.org/philosophy/sco/sco.html
# Copyright (C) 2004 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2004, 2008, 2009.
# Thérèse Godefroy <godef.th AT free.fr>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: sco.html\n"
"POT-Creation-Date: 2021-09-22 09:26+0000\n"
"PO-Revision-Date: 2024-03-03 16:18+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"FSF's Position regarding SCO's attacks on Free Software - GNU Project - Free "
"Software Foundation"
msgstr ""
"Position de la FSF concernant les attaques de SCO contre le logiciel libre - "
"Projet GNU - Free Software Foundation"

#. type: Content of: <div><h2>
msgid "FSF's Position regarding SCO's attacks on Free Software"
msgstr ""
"Position de la FSF concernant les attaques de SCO contre le logiciel libre"

#. type: Content of: <div><p>
msgid ""
"Much press coverage has been given in the last months to SCO's press and "
"legal attacks on Free Software and the GNU/Linux system.  Even though these "
"attacks have narrowed from a focus on the whole GNU/Linux system to the "
"kernel named Linux specifically, FSF, particularly through our General "
"Counsel Eben Moglen, remains deeply involved in strategic planning for the "
"community's responses to SCO."
msgstr ""
"Ces derniers mois, les campagnes de presse et les attaques judiciaires de "
"<abbr title=\"Santa Cruz Operation\">SCO</abbr> contre le logiciel libre et "
"le système GNU/Linux ont fait l'objet d'une grande couverture médiatique. "
"Même si ces attaques, dirigées à l'origine contre l'ensemble du système GNU/"
"Linux, se focalisent maintenant sur le noyau appelé Linux, la FSF, en "
"particulier par l'intermédiaire de notre avocat-conseil général Eben Moglen, "
"demeure profondément impliquée dans le calendrier stratégique des réponses "
"de la communauté à SCO."

#. type: Content of: <div><p>
msgid ""
"Professor Moglen has been coordinating with IBM's lawyers on the matter, "
"including both their inside and outside counsel.  He has coordinated with "
"OSDL, who employ Linus Torvalds, the original author and a key copyright "
"holder of the kernel named Linux.  Professor Moglen continues diplomatic "
"efforts throughout the Free Software and Open Source Movements, and "
"throughout the technology industry, to bring together a broad, coordinated "
"coalition to oppose SCO, both legally and in the media."
msgstr ""
"Le professeur Moglen s'est coordonné avec les avocats d'IBM à ce sujet, "
"qu'ils soient internes ou externes à l'entreprise. Il s'est coordonné "
"également avec <abbr title=\"Open Source Developpement Labs\">OSDL</abbr>, "
"qui emploie Linus Torvalds, auteur original et détenteur-clé du copyright "
"sur le noyau appelé Linux. Le professeur Moglen continue ses efforts "
"diplomatiques, écumant les mouvements du logiciel libre et de l'open source, "
"ainsi que l'industrie technologique, afin de rassembler une coalition vaste "
"et organisée pour s'opposer à SCO, juridiquement et médiatiquement."

#. type: Content of: <div><p>
msgid ""
"On this site, we will publish documents related to our work opposing SCO."
msgstr ""
"Dans cette section, nous publions des documents relatifs à notre travail "
"d'opposition à SCO."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/subpoena.html\">The SCO Subpoena of FSF</a>, by "
"Bradley M. Kuhn, released on Tuesday 18 May 2004."
msgstr ""
"<a href=\"/philosophy/sco/subpoena.html\">L'assignation de la FSF par SCO</"
"a>, par Bradley M. Kuhn, publiée le mardi 18 mai 2004."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/sco-without-fear.html\">SCO: Without Fear and "
"Without Research</a>, by Eben Moglen, released on Monday 24 November 2003."
msgstr ""
"<a href=\"/philosophy/sco/sco-without-fear.html\">SCO : Sans peur et sans "
"recherche</a>, par Eben Moglen, paru le lundi 24 novembre 2003."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/sco-preemption.html\">SCO Scuttles Sense, Claiming "
"GPL Invalidity</a>, by Eben Moglen, released on Monday 18 August 2003."
msgstr ""
"<a href=\"/philosophy/sco/sco-preemption.html\">SCO à contresens, "
"revendiquant l'invalidité de la GPL</a>, par Eben Moglen, paru le lundi "
"18 août 2003."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/questioning-sco.html\">Questioning SCO: A Hard "
"Look at Nebulous Claims</a>, by Eben Moglen, released on Friday 1 August "
"2003."
msgstr ""
"<a href=\"/philosophy/sco/questioning-sco.html\">Remise en cause de la "
"position de SCO : une analyse critique de leurs revendications nébuleuses</"
"a>, par Eben Moglen, paru le vendredi 1er août 2003."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/sco-gnu-linux.html\">SCO, GNU, and Linux</a>, by "
"Richard Stallman, released on Sunday 29 June 2003."
msgstr ""
"<a href=\"/philosophy/sco/sco-gnu-linux.html\">SCO, GNU et Linux</a>, par "
"Richard Stallman, paru le dimanche 29 juin 2003."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/sco/sco-v-ibm.html\">FSF Statement on <cite>SCO v. "
"IBM</cite></a>, by Eben Moglen, released on Friday 27 June 2003."
msgstr ""
"<a href=\"/philosophy/sco/sco-v-ibm.html\">Déclaration de la FSF sur "
"<cite>SCO contre IBM</cite></a>, par Eben Moglen, paru le vendredi "
"27 juin 2003."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2003, 2004, 2021 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2003, 2004, 2021 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza<br /> Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

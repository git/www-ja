<!--#set var="PO_FILE"
 value='<a href="/philosophy/po/free-hardware-designs.pt-br.po">
 https://www.gnu.org/philosophy/po/free-hardware-designs.pt-br.po</a>'
 --><!--#set var="ORIGINAL_FILE" value="/philosophy/free-hardware-designs.html"
 --><!--#set var="DIFF_FILE" value="/philosophy/po/free-hardware-designs.pt-br-diff.html"
 --><!--#set var="OUTDATED_SINCE" value="2021-09-14" -->

<!--#include virtual="/server/header.pt-br.html" -->
<!-- Parent-Version: 1.90 -->

<!-- This file is automatically generated by GNUnited Nations! -->
<title>Hardware livre e designs de hardware livre - Projeto GNU - Free Software
Foundation</title>

<!--#include virtual="/philosophy/po/free-hardware-designs.translist" -->
<!--#include virtual="/server/banner.pt-br.html" -->
<!--#include virtual="/server/outdated.pt-br.html" -->
<h2>Hardware livre e designs de hardware livre</h2>

<p>por <a href="http://www.stallman.org/">Richard M. Stallman</a></p>

<!-- rms: I deleted the links because of Wired's announced
     anti-ad-block system -->
<blockquote>
<p>A maior parte deste artigo foi publicada em duas partes na Wired em março de
2015.</p>
</blockquote>

<p>Até que ponto as ideias de software livre se estendem ao hardware? É uma
obrigação moral tornarmos nossos designs de hardware livres, assim como é
fazermos nosso software livre? Para manter nossa liberdade, é necessário
rejeitar o hardware feito com designs não livres?</p>

<h3 id="definitions">Definições</h3>

<p><em>Software livre</em> (<i lang="en">“free software”</i>, em inglês) é uma
questão de liberdade, não de preço; de um modo geral, significa que os
usuários são livres para usar o software e copiar e redistribuir o software,
com ou sem alterações. Mais precisamente, a definição é formulada em termos
das <a href="/philosophy/free-sw.html">quatro liberdades
essenciais</a>. Para enfatizar que o <i lang="en">“free”</i> de <i
lang="en">“free software”</i> se refere à liberdade, não ao preço,
geralmente usamos a palavra francesa ou espanhola <i lang="es">“libre”</i>;
junto com <i lang="en">“free”</i><sup><a id="TransNote1-rev"
href="#TransNote1">1</a></sup>.</p>

<p>Aplicando o mesmo conceito diretamente ao hardware, <em>hardware livre</em>
(<i lang="en">“free hardware”</i>, em inglês) significa hardware que os
usuários podem usar livremente, copiar e redistribuir com ou sem
alterações. No entanto, não há copiadoras para hardware, além das formas
externas de chaves, DNA e objetos de plástico. A maior parte do hardware é
fabricada a partir de algum tipo de design. O design vem antes do hardware.</p>

<p>Portanto, o conceito que realmente precisamos é o de <em>design de hardware
livre</em>. Isso é simples: significa um design que permite aos usuários
usar o design (ou seja, fabricar hardware a partir dele) e copiá-lo e
redistribuí-lo, com ou sem alterações. O design deve fornecer as mesmas
quatro liberdades que definem o software livre.</p>

<p>Então, podemos nos referir ao hardware feito a partir de um design livre
como “hardware livre”, mas “hardware de design livre” é um termo mais claro,
pois evita possíveis mal-entendidos.</p>

<p>As pessoas que se deparam com a ideia de software livre geralmente pensam
que isso significa que você pode obter uma cópia gratuitamente. Muitos
programas livres estão disponíveis a preço zero, pois não custa nada baixar
sua própria cópia, mas não é isso que significa <i lang="en">“free”</i>
aqui. (De fato, alguns programas spyware, como <a
href="/philosophy/proprietary/proprietary-surveillance.html">Flash Player e
Angry Birds</a>, são gratuitos, embora não sejam livres.) Dizer <i
lang="en">“libre”</i> junto com <i lang="en">“free”</i> ajuda a esclarecer o
ponto.</p>

<p>Para o hardware, essa confusão tende a ir na outra direção; o hardware custa
dinheiro para produzir; portanto, o hardware produzido comercialmente não
será gratuito (a menos que seja um líder de perdas ou um empate), mas isso
não impede que seu design seja <i lang="en">free</i>/<i
lang="en">libre</i>. As coisas que você faz em sua própria impressora 3D
podem ser muito baratas, mas não exatamente gratuitas, pois as
matérias-primas normalmente têm um custo. Em termos éticos, a questão da
liberdade supera totalmente a questão do preço, pois um dispositivo que nega
liberdade aos seus usuários vale menos que nada.</p>

<p>Podemos usar o termo “hardware livre” como um equivalente conciso para
“hardware feito a partir de um design livre (libre)”.</p>

<p>Os termos “hardware aberto” e “hardware de código aberto” (<i
lang="en">“open hardware”</i> e <i lang="en">“open source hardware”</i>, em
inglês, respectivamente) são usados por alguns com o mesmo significado
concreto que “hardware de design livre”, mas esses termos minimizam a
liberdade como um problema. Eles foram derivados do termo “software de
código aberto” (<i lang="en">“open source software”</i>), que se refere mais
ou menos a software livre, mas <a
href="/philosophy/open-source-misses-the-point.html">sem falar sobre
liberdade ou apresentar o problema por uma questão de certo ou
errado</a>. Para enfatizar a importância da liberdade, fazemos questão de
nos referir à liberdade sempre que pertinente; como “open” falha em fazer
isso, não vamos substituí-lo por “free”.</p>

<h3 id="hw-and-sw">Hardware e software</h3>

<p>Hardware e software são fundamentalmente diferentes. Um programa, mesmo na
forma executável compilada, é uma coleção de dados que pode ser interpretada
como instruções para um computador. Como qualquer outro trabalho digital,
ele pode ser copiado e alterado usando um computador. Uma cópia de um
programa não possui forma ou modalidade física preferida inerente.</p>

<p>Por outro lado, o hardware é uma estrutura física e sua fisicalidade é
crucial. Embora o design do hardware possa ser representado como dados, em
alguns casos, mesmo como um programa, o design não é o hardware. Um design
para uma CPU não pode executar um programa. Você não vai muito longe
tentando digitar um design para um teclado ou exibir pixels em um design
para uma tela.</p>

<p>Além disso, enquanto você pode usar um computador para modificar ou copiar o
design do hardware, um computador não pode converter o design na estrutura
física descrita. Isso requer equipamento de fabricação.</p>

<h3 id="boundary">A fronteira entre hardware e software</h3>

<p>Qual é o limite, em dispositivos digitais, entre hardware e software? Ele
segue as definições. Software é a parte operacional de um dispositivo que
pode ser copiado e alterado em um computador; hardware é a parte operacional
que não pode ser. Este é o caminho certo para fazer a distinção, porque se
refere às consequências práticas.</p>

<p>Há uma área cinza entre o hardware e o software que contém o firmware que
<em>pode</em> ser atualizado ou substituído, mas que nunca deve ser
atualizado ou substituído depois que o produto é vendido. Em termos
conceituais, a área cinza é bastante estreita. Na prática, é importante
porque muitos produtos caem nela. Podemos tratar esse firmware como hardware
com uma pequena extensão de seu significa.</p>

<p>Alguns disseram que programas de firmware pré-instalados e chips de
Field-Programmable Gate Array (FPGAs) “obscurecem a fronteira entre hardware
e software”, mas acho que isso é uma interpretação incorreta dos fatos. O
firmware que é instalado durante o uso é um software; O firmware que é
entregue dentro do dispositivo e não pode ser alterado é um software por
natureza, mas podemos tratá-lo como se fosse um circuito. Quanto aos FPGAs,
o próprio FPGA é hardware, mas o padrão de porta <i lang="en">gate</i>
carregada no FPGA é um tipo de firmware.</p>

<p>O uso de padrões livres de porta em FPGAs poderia ser um método útil para
criar dispositivos digitais livres no nível do circuito. No entanto, para
tornar os FPGAs utilizáveis no mundo livre, precisamos de ferramentas de
desenvolvimento livres para eles. O obstáculo é que o formato do arquivo de
padrão de porta carregado no FPGA é secreto. Por muitos anos, não houve
modelo de FPGA para o qual esses arquivos pudessem ser produzidos sem
ferramentas não livres (privativas).</p>

<p>A partir de 2015, as ferramentas de software livre estão disponíveis para <a
href="http://www.clifford.at/icestorm/">programação do Lattice iCE40</a>, um
modelo comum de FPGA, a partir da entrada gravada em uma linguagem de
descrição de hardware (HDL). Também é possível compilar programas C e
executá-los em FPGA de Xilinx Spartan 6 LX9 com <a
href="https://github.com/Wolfgang-Spraul/fpgatools">ferramentas livres</a>,
mas elas não o possuem suporte a entrada HDL. Recomendamos que você rejeite
outros modelos FPGA até que eles também tenham suporte em ferramentas
livres.</p>

<p>Quanto ao próprio código HDL, ele pode atuar como software (quando é
executado em um emulador ou carregado em um FPGA) ou como um design de
hardware (quando é realizado em silício imutável ou em uma placa de
circuito).</p>

<h3 id="ethical-3d-printers">A pergunta ética para impressoras 3D</h3>

<p>Eticamente, <a
href="/philosophy/free-software-even-more-important.html">software deve ser
livre</a>; um programa não livre é uma injustiça. Devemos adotar a mesma
visão para designs de hardware?</p>

<p>Com certeza demos, nos campos que a impressão 3D (ou, geralmente, qualquer
tipo de fabricação pessoal) possa lidar. Os padrões da impressora para criar
um objeto útil e prático (ou seja, funcional em vez de decorativo)
<em>devem</em> ser livres porque são trabalhos feitos para uso prático. Os
usuários merecem controle sobre esses trabalhos, assim como eles merecem
controle sobre o software que usam. Distribuir um design de objeto funcional
não livre é tão errado quanto distribuir um programa não livre.</p>

<p>Tenha cuidado ao escolher impressoras 3D que funcionam com software
exclusivamente livre; a Free Software Foundation <a
href="http://fsf.org/resources/hw/endorsement">endossa tais
impressoras</a>. Algumas impressoras 3D são feitas com designs de hardware
livre, mas <a
href="http://www.cnet.com/news/pulling-back-from-open-source-hardware-makerbot-angers-some-adherents/">os
designs de hardware da Makerbot não são livres</a>.</p>

<h3 id="reject-nonfree">Devemos rejeitar o hardware digital não livre?</h3>

<p>Um design de hardware digital <a href="#fn1">(*)</a> não livre é uma
injustiça? Devemos, por nossa liberdade, rejeitar todo o hardware digital
feito com designs não livres, assim como devemos rejeitar software não
livre?</p>

<p>Devido ao paralelo conceitual entre designs de hardware e código-fonte do
software, muitos hackers de hardware condenam rapidamente designs de
hardware não livres, assim como software não livre. Discordo porque as
circunstâncias para hardware e software são diferentes.</p>

<p>A tecnologia atual de fabricação de chips e placas se assemelha à
impressora: ela se presta à produção em massa em uma fábrica. É mais como
copiar livros em 1950 do que copiar software hoje.</p>

<p>A liberdade de copiar e alterar o software é um imperativo ético porque
essas atividades são viáveis para quem usa o software: o equipamento que
permite o uso do software (um computador) também é suficiente para copiá-lo
e alterá-lo. Os computadores móveis de hoje são fracos demais para serem
bons para isso, mas qualquer um pode encontrar um computador suficientemente
poderoso.</p>

<p>Além disso, um computador é suficiente para baixar e executar uma versão
alterada por outra pessoa que saiba como, mesmo se você não for um
programador. De fato, os não programadores baixam o software e o executam
todos os dias. É por isso que o software livre faz uma diferença real para
os não programadores.</p>

<p>Quanto disso se aplica ao hardware? Nem todo mundo que pode usar hardware
digital sabe como alterar um design de circuito ou de chip, mas qualquer
pessoa que possua um PC possui o equipamento necessário para isso. Até
agora, o hardware é paralelo ao software, mas a seguir vem a grande
diferença.</p>

<p>Você não pode criar e usar um design de circuito ou chip no seu
computador. Construir um grande circuito é muito trabalhoso, e é assim que
você tem a placa de circuito. Hoje, fabricar um chip não é viável para
indivíduos; somente a produção em massa pode torná-los baratos o
suficiente. Com a tecnologia de hardware de hoje, os usuários não podem
baixar e executar uma versão modificada de um design de hardware digital
amplamente utilizado, pois poderiam executar uma versão modificada de um
programa amplamente utilizado. Assim, as quatro liberdades não dão aos
usuários hoje controle coletivo sobre um design de hardware, como dão aos
usuários controle coletivo sobre um programa. É aí que o raciocínio que
mostra que todo software deve ser livre não se aplica à tecnologia de
hardware atual.</p>

<p>Em 1983, não havia sistema operacional livre, mas estava claro que, se
tivéssemos um, poderíamos usá-lo imediatamente e obter liberdade de
software. Tudo o que faltava era o código para um.</p>

<p>Em 2014, se tivéssemos um design livre para um chip de CPU adequado para um
PC, os chips produzidos em massa feitos com esse design não nos dariam a
mesma liberdade no domínio do hardware. Se vamos comprar um produto
produzido em massa em uma fábrica, essa dependência da fábrica causa quase
todos os mesmos problemas que um design não livre. Para designs livres para
nos dar liberdade de hardware, precisamos de tecnologia de fabricação
futura.</p>

<p>Podemos imaginar um futuro em que nossos fabricantes pessoais possam
fabricar chips, e nossos robôs possam montá-los e soldá-los juntamente com
transformadores, interruptores, chaves, telas, ventoinhas e assim por
diante. Nesse futuro, todos nós fabricaremos nossos próprios computadores (e
fabricantes e robôs) e todos poderemos tirar proveito dos designs
modificados feitos por quem conhece hardware. Os argumentos para rejeitar
software não livre também se aplicarão a designs de hardware não livres.</p>

<p>Esse futuro está a anos de distância, no mínimo. Enquanto isso, não há
necessidade de rejeitar hardware com designs não livres em princípio.</p>

<hr />

<p id="fn1">* Na forma usada aqui, “hardware digital” inclui hardware com alguns
circuitos analógicos e componentes em adição aos digitais.</p>

<h3 id="free-designs">Precisamos de designs de hardware digital livres</h3>

<p>Embora não seja necessário rejeitar o hardware digital feito a partir de
designs não livres nas circunstâncias atuais, precisamos desenvolver designs
livres e usá-los sempre que possível. Eles oferecem vantagens hoje e, no
futuro, podem ser a única maneira de usar o software livre.</p>

<p>Designs de hardware livre oferecem vantagens práticas. Várias empresas podem
fabricar um, o que reduz a dependência de um único fornecedor. Grupos podem
organizar para fabricá-los em quantidade. Ter diagramas de circuito ou
código HDL torna possível estudar o design para procurar erros ou
funcionalidades maliciosas (sabe-se que a NSA adquiriu deficiências
maliciosas em alguns hardwares de computação). Além disso, os designs livres
podem servir como blocos de construção para projetar computadores e outros
dispositivos complexos, cujas especificações serão publicadas e terão menos
peças que podem ser usadas contra nós.</p>

<p>Designs de hardware livre podem se tornar utilizáveis para algumas partes de
nossos computadores e redes e para sistemas embarcados, antes que possamos
fabricar computadores inteiros dessa maneira.</p>

<p>Designs de hardware livre podem se tornar essenciais mesmo antes de podermos
fabricar o hardware pessoalmente, se eles se tornarem a única maneira de
evitar software não livre. Como o hardware comercial comum é cada vez mais
projetado para subjugar os usuários, torna-se cada vez mais incompatível com
o software livre, devido a especificações e requisitos secretos de código a
ser assinado por alguém que não seja você. Os chips de modem para celular e
até alguns aceleradores gráficos já exigem que o firmware seja assinado pelo
fabricante. Qualquer programa em seu computador – que outra pessoa pode
mudar, mas você não – é um instrumento de poder injusto sobre você; O
hardware que impõe esse requisito é um hardware mal-intencionado. No caso de
chips de modem para celular, todos os modelos agora disponíveis são
maliciosos.</p>

<p>Algum dia, o hardware digital de design livre pode ser a única plataforma
que permite executar um sistema livre. Vamos procurar ter os designs
digitais livres necessários antes disso e esperar que tenhamos os meios para
fabricá-los de maneira barata o suficiente para todos os usuários.</p>

<p>Se você projetar hardware, faça seus designs livres. Se você usa hardware,
junte-se a empresas de pressão e pressão para liberar os designs de
hardware.</p>

<h3 id="levels-of-design">Níveis de design</h3>

<p>O software possui níveis de implementação; um pacote pode incluir
bibliotecas, comandos e scripts, por exemplo. Mas esses níveis não fazem uma
diferença significativa para a liberdade de software, porque é possível
tornar todos os níveis livres. Projetar componentes de um programa é o mesmo
tipo de trabalho que projetar o código que os combina; da mesma forma, criar
os componentes a partir do fonte é o mesmo tipo de operação que criar o
programa combinado a partir do fonte. Para libertar a coisa toda, basta
continuar o trabalho até que tenhamos feito todo o trabalho.</p>

<p>Portanto, insistimos que um programa seja livre em todos os níveis. Para que
um programa seja qualificado como livre, todas as linhas do código-fonte que
o compõem devem ser livres, para que você possa recompilar o programa apenas
com código-fonte livre.</p>

<p>Os objetos físicos, por outro lado, geralmente são construídos com
componentes projetados e construídos em um tipo diferente de fábrica. Por
exemplo, um computador é feito de chips, mas projetar (ou fabricar) chips é
muito diferente de projetar (ou fabricar) o computador a partir de chips.</p>

<p>Portanto, precisamos distinguir <em>níveis</em> no design de um produto
digital (e talvez em outros tipos de produtos). O circuito que conecta os
chips é de um nível; o design de cada chip é outro nível. Em um FPGA, a
interconexão de células primitivas é um nível, enquanto as próprias células
primitivas são outro nível. No futuro ideal, queremos que o design seja
livre em todos os níveis. Nas circunstâncias atuais, apenas liberar um nível
é um avanço significativo.</p>

<p>No entanto, se um design em um nível combina peças livres e não livres – por
exemplo, um circuito HDL “livre” que incorpora “núcleos flexíveis”
privativos – devemos concluir que o projeto como um todo não é livre nesse
nível. Da mesma forma, para “assistentes” ou “macros” não livres, se
especificarem parte das interconexões de chips ou partes de chips conectadas
de forma programática. As peças livres podem ser um passo em direção ao
objetivo futuro de um design livre, mas alcançá-lo implica substituir as
peças não livres. Eles nunca podem ser admissíveis no mundo livre.</p>

<h3 id="licenses">Licenças e direitos autorais para designs de hardware livres</h3>

<p>Você cria um design de hardware livre, lançando-o sob uma licença
livre. Recomendamos o uso da Licença Pública Geral GNU, versão 3 ou
posterior. Criamos a GPL versão 3 com esse objetivo.</p>

<p>Copyleft em circuitos e formas de objetos não decorativas não chega tão
longe quanto se pode supor. Os direitos autorais desses designs se aplicam
apenas à maneira como o design é desenhado ou escrito. O copyleft é uma
maneira de usar a lei de direitos autorais, portanto, seu efeito se aplica
apenas até a lei de direitos autorais.</p>

<p>Por exemplo, um circuito, como uma topologia, não pode ter direitos autorais
(e, portanto, não pode ter copyleft). As definições dos circuitos escritos
em HDL podem ter direitos autorais (e, portanto, com copyleft), mas o
copyleft cobre apenas os detalhes de expressão do código HDL, não a
topologia de circuito que ele gera. Da mesma forma, um desenho ou layout de
um circuito pode ter direitos autorais, portanto pode ser copyleft, mas isso
abrange apenas o desenho ou o layout, não a topologia do circuito. Qualquer
pessoa pode desenhar legalmente a mesma topologia de circuito de uma maneira
diferente ou escrever uma definição de HDL diferente que produz o mesmo
circuito.</p>

<p>Os direitos autorais não abrangem circuitos físicos; portanto, quando as
pessoas constroem instâncias do circuito, a licença do design não afeta
legalmente o que fazem com os dispositivos que construíram.</p>

<p>Para desenhos de objetos e modelos de impressoras 3D, os direitos autorais
não cobrem a criação de um desenho diferente da mesma forma de objeto
puramente funcional. Também não cobre os objetos físicos funcionais criados
a partir do desenho. No que diz respeito aos direitos autorais, todos são
livres para criá-los e usá-los (e é uma liberdade que precisamos muito). Nos
EUA, os direitos autorais não cobrem os aspectos funcionais descritos pelo
design, mas <a
href="http://www.copyright.gov/title17/92chap13.html#1301">abrange os
aspectos decorativos</a>. Quando um objeto tem aspectos decorativos e
funcionais, você entra em um terreno complicado <a href="#fn2">(*)</a>.</p>

<p>Tudo isso também pode ser verdade no seu país ou não. Antes de produzir
objetos comercialmente ou em quantidade, você deve consultar um advogado
local. O copyright não é o único problema com o qual você precisa se
preocupar. Você pode ser atacado por patentes, provavelmente por entidades
que não têm nada a ver com a criação do design que está usando, e também
pode haver outros problemas legais.</p>

<p>Lembre-se de que as leis de direitos autorais e de patentes são totalmente
diferentes. É um erro supor que eles tenham algo em comum. É por isso que o
termo “<a href="/philosophy/not-ipr.html">propriedade intelectual</a>” é
pura confusão e deve ser totalmente rejeitado.</p>

<hr />

<p id="fn2">* Um artigo do Public Knowledge fornece uma informação útil sobre essa <a
href="https://www.publicknowledge.org/assets/uploads/documents/3_Steps_for_Licensing_Your_3D_Printed_Stuff.pdf">
complexidade</a>, para os EUA, apesar de ele cair no erro comum de usar o
conceito falso de “propriedade intelectual” e o termo propaganda “<a
href="/philosophy/words-to-avoid.html#Protection">proteção</a>”.</p>

<h3 id="promoting">Promovendo designs de hardware livres por meio de repositórios</h3>

<p>A maneira mais eficaz de levar os designs de hardware publicados a serem
livres é através de regras nos repositórios em que são publicados. Os
operadores de repositório devem colocar a liberdade das pessoas que usarão
os designs acima das preferências das pessoas que os fazem. Isso significa
exigir que os designs de objetos úteis sejam livres, como condição para
publicá-los.</p>

<p>Para objetos decorativos, esse argumento não se aplica; portanto, não
precisamos insistir que eles devem ser livres. No entanto, devemos insistir
em que sejam compartilháveis. Portanto, um repositório que lide com modelos
de objetos decorativos e funcionais deve ter uma política de licença
apropriada para cada categoria.</p>

<p>Para projetos digitais, sugiro que o repositório insista em GNU GPL v3 ou
posterior, Apache 2.0 ou CC0. Para designs 3D funcionais, o repositório deve
solicitar ao autor do design que escolha uma das quatro licenças: GNU GPL v3
ou posterior, Apache 2.0, CC BY-SA, CC BY ou CC0. Para designs decorativos,
deve sugerir o GNU GPL v3 ou posterior, Apache 2.0, CC0 ou qualquer uma das
licenças CC.</p>

<p>O repositório deve exigir que todos os designs sejam publicados como
código-fonte, e o código-fonte em formatos secretos utilizáveis apenas por
programas privativos de design não é realmente adequado. Para um modelo 3D,
o <a href="http://en.wikipedia.org/wiki/STL_%28file_format%29">formato
STL</a> não é o formato preferido para alterar o design, não sendo um
código-fonte. Portanto, o repositório não deve aceitá-lo, exceto talvez
acompanhando o código-fonte real.</p>

<p>Não há razão para escolher um formato único para o código-fonte dos designs
de hardware, mas os formatos fonte que ainda não podem ser manipulados com
software livre devem ser aceitos com relutância, na melhor das hipóteses.</p>

<h3 id="warranties">Designs de hardware livre e garantias</h3>

<p>Em geral, os autores de designs de hardware livres não têm obrigação moral
de oferecer uma garantia àqueles que fabricam o design. Esse é um problema
diferente da venda de hardware físico, que deve vir com uma garantia do
vendedor e/ou do fabricante.</p>

<h3 id="conclusion">Conclusão</h3>

<p>Já temos licenças adequadas para tornar livres os nossos designs de
hardware. O que precisamos é reconhecer como comunidade que é isso que
devemos fazer e insistir em designs livres quando fabricamos objetos.</p>

<div class="translators-notes">

<!--TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.-->
<b>Nota do tradutor</b>:
<ol>
<li>
<a id="TransNote1" href="#TransNote1-rev" class="nounderline">&#8593;</a>
Em inglês, o termo “free” de “free software” pode significar tanto liberdade
quanto preço zero, sendo o termo “libre”, em substituição ou junto com
“free”, usado para se referir inequivocamente ao sentido de liberdade. Em
português, essa ambiguidade não ocorre, diferenciando-se com “livre” e
“gratuito”.
</li>
</ol></div>
</div>

<!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.pt-br.html" -->
<div id="footer">
<div class="unprintable">

<p>Envie perguntas em geral sobre a FSF e o GNU para <a
href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>. Também existem <a
href="/contact/">outros meios de contatar</a> a FSF. Links quebrados e
outras correções ou sugestões podem ser enviadas para <a
href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p>
<!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">

        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and submitting translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
A equipe de traduções para o português brasileiro se esforça para oferecer
traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por
favor, envie seus comentários e sugestões em geral sobre as traduções para
<a
href="mailto:web-translators@gnu.org">&lt;web-translators@gnu.org&gt;</a>.
</p><p>Consulte o <a href="/server/standards/README.translations.html">Guia
para as traduções</a> para mais informações sobre a coordenação e o envio de
traduções das páginas deste site.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->
<p>Copyright &copy; 2015, 2016, 2018, 2019, 2021 Richard Stallman</p>

<p>Esta página está licenciada sob uma licença <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/deed.pt_BR">Creative
Commons Atribuição-SemDerivações 4.0 Internacional</a>.</p>

<!--#include virtual="/server/bottom-notes.pt-br.html" -->
<div class="translators-credits">

<!--TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.-->
Traduzido por: Rafael Fontenelle <a
href="mailto:rafaelff@gnome.org">&lt;rafaelff@gnome.org&gt;</a>, 2021.</div>

<p class="unprintable"><!-- timestamp start -->
Última atualização:

$Date: 2022/01/01 17:34:45 $

<!-- timestamp end -->
</p>
</div>
</div>
<!-- for class="inner", starts in the banner include -->
</body>
</html>

# Catalan translation of http://www.gnu.org/philosophy/patent-reform-is-not-enough.html
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnu.org article.
# Miquel Puigpelat <mpuigpe1@xtec.cat>, 2008.
# May 2019, Sept 2021: unfuzzify (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: patent-reform-is-not-enough.ca\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-01 05:26+0000\n"
"PO-Revision-Date: 2013-08-29 08:22+0200\n"
"Last-Translator: Miquel Puigpelat <mpuigpe1@xtec.cat>\n"
"Language-Team: Catalan <www-ca-traductors@gnu.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2023-05-10 10:55+0000\n"

#. type: Content of: <title>
msgid "Patent Reform Is Not Enough - GNU Project - Free Software Foundation"
msgstr ""
"La reforma de les patents no és suficient - Projecte GNU - Free Software "
"Foundation"

#. type: Content of: <div><h2>
msgid "Patent Reform Is Not Enough"
msgstr "La reforma de les patents no és suficient"

#. type: Content of: <div><p>
msgid ""
"When people first learn about the problem of software patents, their "
"attention is often drawn to the egregious examples: patents that cover "
"techniques already widely known.  These techniques include sorting a "
"collection of formulae so that no variable is used before it is calculated "
"(called &ldquo;natural order recalculation&rdquo; in spreadsheets), and the "
"use of exclusive-or to modify the contents of a bit-map display."
msgstr ""
"Quan la gent comença a aprendre alguna cosa sobre el problema de les patents "
"del programari, la seva atenció se centra sovint en els casos més notoris: "
"per exemple, patents que cobreixen tècniques que ja són àmpliament "
"conegudes. Aquestes tècniques inclouen la d'ordenar una col·lecció de "
"fórmules de manera que no s'utilitzi cap variable abans de ser calculada "
"(anomenada «recàlcul d'ordre natural» en els fulls de càlcul) i l'ús de "
"l'operador OR exclusiu per modificar el contingut d'una visualització d'un "
"mapa de bits."

#. type: Content of: <div><p>
msgid ""
"Focusing on these examples can lead some people to ignore the rest of the "
"problem.  They are attracted to the position that the patent system is "
"basically correct and needs only &ldquo;reforms&rdquo; to carry out its own "
"rules properly."
msgstr ""
"Centrar-se en aquests exemples pot portar la gent a ignorar la resta del "
"problema. Poden arribar a pensar que el sistema de patents és bàsicament "
"correcte i que només necessita «reformes» per funcionar de manera eficient."

#. type: Content of: <div><p>
msgid ""
"But would correct implementation really solve the problem of software "
"patents? Let's consider an example."
msgstr ""
"Però, una correcta implementació resoldria realment el problema de les "
"patents de programari? Considerem un exemple."

#. type: Content of: <div><p>
msgid ""
"In the early 90s we desperately needed a new free program for compression, "
"because the old de-facto standard &ldquo;compress&rdquo; program had been "
"taken away from us by patents.  In April 1991, software developer Ross "
"Williams began publishing a series of data compression programs using new "
"algorithms of his own devising.  Their superior speed and compression "
"quality soon attracted users."
msgstr ""
"A principis dels 90 necessitàvem desesperadament un nou programa lliure de "
"compressió, perquè ens havien quedat sense l'antic programa estàndard "
"«compress» per culpa de les patents. A l'abril de 1991, el desenvolupador de "
"programari Ross Williams va començar a publicar una sèrie de programes de "
"compressió de dades utilitzant nous algoritmes de creació pròpia. La seva "
"major velocitat i qualitat de compressió van atraure aviat els usuaris."

#. type: Content of: <div><p>
msgid ""
"That September, when the FSF was about a week away from releasing one of "
"them as the new choice for compressing our distribution files, use of these "
"programs in the United States was halted by a newly issued patent, number "
"5,049,881."
msgstr ""
"Aquell setembre, quan la FSF estava a una setmana de publicar un d'aquests "
"nous programes de compressió com a eina escollida per comprimir els arxius "
"de la nostra distribució, l'ús d'aquests programes als Estats Units va ser "
"aturat pel registre d'una nova patent, la número 5,049,881."

#. type: Content of: <div><p>
msgid ""
"Under the patent system's rules, whether the public is allowed to use these "
"programs (i.e., whether the patent is invalid) depends on whether there is "
"&ldquo;prior art&rdquo;: whether the basic idea was published before the "
"patent application, which was on June 18, 1990.  Williams' publication in "
"April 1991 came after that date, so it does not count."
msgstr ""
"Sota les regles del sistema de patents, que el públic pugui usar aquests "
"programes (per exemple, perquè la patent no és vàlida) depèn de si hi ha "
"«treball anterior». O sigui, de si la idea bàsica es va publicar o no abans "
"del registre de la patent, que va ser el 18 de juny de 1990. La publicació "
"de Williams a l'abril de 1991 va arribar després, i per tant no es considera "
"treball anterior."

#. type: Content of: <div><p>
msgid ""
"A student described a similar algorithm in 1988-1989 in a class paper at the "
"University of San Francisco, but the paper was not published.  So it does "
"not count as prior art under the current rules."
msgstr ""
"Un estudiant va descriure un algoritme semblant el 1988-1989 en un treball "
"de classe a la Universitat de San Francisco, però no es va publicar. De "
"manera que tampoc no es considera treball anterior sota les regles actuals."

#. type: Content of: <div><p>
msgid ""
"Reforms to make the patent system work &ldquo;properly&rdquo; would not have "
"prevented this problem.  Under the rules of the patent system, this patent "
"seems valid.  There was no prior art for it.  It is not close to obvious, as "
"the patent system interprets the term.  (Like most patents, it is neither "
"worldshaking nor trivial, but somewhere in between.)  The fault is in the "
"rules themselves, not their execution."
msgstr ""
"Les reformes per fer que el sistema de patents funcioni «correctament» no "
"haurien evitat aquest problema. Sota les regles del sistema de patents, "
"aquesta patent sembla vàlida. No hi havia un treball anterior. No patenta "
"una idea òbvia en el sentit en què el sistema de patents interpreta el terme "
"(com la majoria de les patents, no patenta una idea ni revolucionària ni "
"trivial, sinó entre mig). La culpa és de les mateixes regles, no de la seva "
"aplicació."

#. type: Content of: <div><p>
msgid ""
"In the US legal system, patents are intended as a bargain between society "
"and individuals; society is supposed to gain through the disclosure of "
"techniques that would otherwise never be available.  It is clear that "
"society has gained nothing by issuing patent number 5,049,881.  This "
"technique was going to be available anyway.  It was easy enough to find that "
"several people did so at around the same time."
msgstr ""
"Al sistema legal dels Estats Units, les patents estan pensades com un tracte "
"entre la societat i els individus; se suposa que la societat hi surt "
"guanyant amb el descobriment de tècniques que d'una altra manera mai no "
"haurien estat disponibles. Sembla clar que la societat no hi ha guanyat res "
"amb el registre de la patent número 5,049,881. Aquesta tècnica hagués estat "
"disponible igualment. Era tan senzilla que unes quantes persones la van "
"descobrir al mateix temps."

#. type: Content of: <div><p>
msgid ""
"Under current rules, our ability to use Williams's programs depends on "
"whether anyone happened to publish the same idea before June 18, 1990.  That "
"is to say, it depends on luck.  This system is good for promoting the "
"practice of law, but not progress in software."
msgstr ""
"Sota les regles actuals, la possibilitat d'utilitzar els programes de "
"Williams depèn de si a algú se li ha acudit publicar la mateixa idea abans "
"del 18 de juny de 1990. És a dir, depèn de la sort. Aquest sistema és bo per "
"promoure la pràctica del dret, però no per al progrés del programari."

#. type: Content of: <div><p>
msgid ""
"Teaching the Patent Office to look at more of the existing prior art might "
"prevent some outrageous mistakes.  It will not cure the greater problem, "
"which is the patenting of every <em>new</em> wrinkle in the use of "
"computers, like the one that Williams and others independently developed."
msgstr ""
"Ensenyar a l'oficina de patents a indagar més sobre l'existència de treball "
"anterior pot prevenir alguns errors escandalosos. Però no resoldrà el "
"principal problema, que és la tendència a patentar cada una de les noves "
"idees en l'ús dels ordinadors, com la que Williams i altres van desenvolupar "
"de manera independent."

#. type: Content of: <div><p>
msgid ""
"This will turn software into a quagmire.  Even an innovative program "
"typically uses dozens of not-quite-new techniques and features, each of "
"which might have been patented.  Our ability to use each wrinkle will depend "
"on luck, and if we are unlucky half the time, few programs will escape "
"infringing a large number of patents.  Navigating the maze of patents will "
"be harder than writing software.  As <cite>The Economist</cite> says, "
"software patents are simply bad for business."
msgstr ""
"Això convertirà el programari en un fangar. Fins i tot un programa innovador "
"utilitza normalment dotzenes de tècniques i capacitats no gaire noves que "
"poden haver estat patentades. La nostra capacitat per utilitzar cada una "
"d'aquestes idees dependrà de la sort, i si tenim mala sort la meitat del "
"temps, pocs seran els programes que no infringeixin un bon nombre de "
"patents. Navegar en el laberint de les patents serà més difícil que escriure "
"programari. Com diu <cite>The Economist</cite>, les patents del programari "
"són simplement dolentes per als negocis."

#. type: Content of: <div><h3>
msgid "What you can do to help"
msgstr "El que podeu fer per ajudar"

#.  [Dead as of 2019-03-23] support <a href="http://stopsoftwarepatents.eu/">
#. this
#. petition</a> for a Europe free of software patents, and 
#. type: Content of: <div><p>
msgid ""
"There is a massive effort in Europe to stop software patents.  Please see <a "
"href=\"https://ffii.org/\"> the FFII web site</a> for full details of how "
"you can help."
msgstr ""
"Hi ha un important esforç a Europa per aturar les patents del programari. Si "
"us plau, visiteu <a href=\"https://ffii.org/\"> la pàgina web de la FFII</a> "
"per saber amb detall com podeu ajudar."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envieu les vostres preguntes sobre la FSF i GNU a <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>.  També hi ha <a href=\"/contact/contact.html"
"\">altres formes de contactar</a> amb la FSF. Envieu els enllaços trencats i "
"altres correccions o suggeriments a <a href=\"mailto:webmasters@gnu.org"
"\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Vegeu la <a href=\"/server/standards/README.translations.html\">Guia de "
"traducció</a> per informar-vos sobre la coordinació i publicació de les "
"traduccions d'aquest article. <br />Contacteu amb l'<a href=\"/server/"
"standards/translations/ca/\">Equip de traducció</a> per col·laborar en la "
"traducció al català del web de GNU."

#. type: Content of: <div><p>
# | Copyright &copy; 1996-1998, 2001, [-2021, 2023-] {+2023, 2024+} Free
# | Software Foundation, Inc.
#, fuzzy
#| msgid ""
#| "Copyright &copy; 1996-1998, 2001, 2021, 2023 Free Software Foundation, "
#| "Inc."
msgid ""
"Copyright &copy; 1996-1998, 2001, 2023, 2024 Free Software Foundation, Inc."
msgstr ""
"Copyright &copy; 1996-1998, 2001, 2021, 2023 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Aquesta pàgina es troba sota la <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.ca\">Llicència Creative Commons "
"Reconeixement-SenseObraDerivada 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Darrera revisió: <a href=\"http://www.puigpe.org/\">puigpe</a>, 28 de juny "
"de 2009."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Updated:"

#, fuzzy
#~| msgid ""
#~| "Copyright &copy; 1996-1998, 2001, 2021 Free Software Foundation, Inc."
#~ msgid ""
#~ "Copyright &copy; 1996-1998, 2001, 2023 Free Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 1996-1998, 2001, 2021 Free Software Foundation, Inc."

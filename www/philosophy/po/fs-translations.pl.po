# Polish translation for http://www.gnu.org/philosophy/fs-translations.html
# Copyright (C) 2016 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Jan Owoc <jsowoc AT gmail.com>, 2016.
# May 2019, April 2021: update (T. Godefroy).
# Sept 2021: unfuzzify (TG).
msgid ""
msgstr ""
"Project-Id-Version: fs-translations.html\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-04 17:55+0000\n"
"PO-Revision-Date: 2024-08-23 18:09-0600\n"
"Last-Translator: Jan Owoc <jsowoc AT gmail DOT com>\n"
"Language-Team: Polish <www-pl-trans@gnu.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.3\n"

#. type: Content of: <title>
msgid ""
"Translations of the term &ldquo;free software&rdquo; - GNU Project - Free "
"Software Foundation"
msgstr ""
"Tłumaczenia terminu &bdquo;free software&rdquo; - Projekt GNU - Free "
"Software Foundation"

#. type: Content of: <div><h2>
msgid "Translations of the term &ldquo;free software&rdquo;"
msgstr "Tłumaczenia terminu &bdquo;free software&rdquo;"

#. type: Content of: <div><p>
msgid ""
"This is a list of recommended unambiguous translations for the term &ldquo;"
"free software&rdquo; (<a href=\"/philosophy/open-source-misses-the-point.html"
"\">free as in freedom</a>) into various languages."
msgstr ""
"To jest lista zalecanych jednoznacznych tłumaczeń terminu &bdquo;free "
"software&rdquo; (<a href=\"/philosophy/open-source-misses-the-point.html"
"\">free jak wolność</a>) na&nbsp;rozmaite języki."

#. type: Content of: <div><p>
msgid ""
"We also provide translations of &ldquo;gratis software,&rdquo; &ldquo;"
"proprietary software,&rdquo; and &ldquo;commercial software&rdquo; to show "
"how to make the contrast in various languages."
msgstr ""
"Także mamy tłumaczenie &bdquo;gratis software&rdquo; i&nbsp;&bdquo;"
"commercial software&rdquo;, aby&nbsp;pokazać kontrast w&nbsp;różnych "
"językach."

#. type: Content of: <div><p>
msgid ""
"If you know a correction or addition to the list, please email it to <a href="
"\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>.  "
"Please stick to UTF-8 in your message.  Thanks."
msgstr ""
"Jeśli znacie poprawkę lub&nbsp;uzupełnienie do&nbsp;tej listy, napiszcie "
"na&nbsp;adres <a href=\"mailto:web-translators@gnu.org\">&lt;web-"
"translators@gnu.org&gt;</a>. Prosimy o używanie UTF-8 we wiadomości. Dzięki."

#. type: Content of: <div><p>
msgid ""
"The parenthesized phrases in Latin letters after some of the entries are "
"transliterations (with vowels added where relevant).  Please send any "
"corrections or additions to those, too."
msgstr ""
"Zwroty w&nbsp;nawiasach w&nbsp;alfabecie łacińskim po&nbsp;niektórych "
"wpisach to transliteracja (z dodanymi samogłoskami gdzie konieczne). Prosimy "
"o nadesłanie poprawek do&nbsp;tych także."

#. type: Content of: <div><h3>
msgid "Afrikaans"
msgstr "Afrikaans"

#. type: Content of: <div><table><tr><th>
msgid "Language Code"
msgstr "Kod języka"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "af"
msgstr "af"

#. type: Content of: <div><table><tr><th>
msgid "Free Software"
msgstr "Wolne oprogramowanie [<em>Free Software</em>]"

#. type: Content of: <div><table><tr><td>
msgid "vrye sagteware"
msgstr "vrye sagteware"

#. type: Content of: <div><table><tr><th>
msgid "Gratis Software"
msgstr "Darmowe oprogramowanie [<em>Gratis Software</em>]"

#. type: Content of: <div><table><tr><td>
msgid "gratis sagteware"
msgstr "gratis sagteware"

#. type: Content of: <div><h3>
msgid "Albanian"
msgstr "Albański"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sq"
msgstr "sq"

#. type: Content of: <div><table><tr><td>
msgid "software i lirë"
msgstr "software i&nbsp;lirë"

#. type: Content of: <div><table><tr><td>
msgid "software falas"
msgstr "software falas"

#. type: Content of: <div><h3>
msgid "Arabic"
msgstr "Arabski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ar"
msgstr "ar"

#. type: Content of: <div><table><tr><td>
msgid "برمجيات حرة (barmajiyat ḥorrah)"
msgstr "برمجيات حرة (barmajiyat ḥorrah)"

#. type: Content of: <div><h3>
msgid "Armenian"
msgstr "Ormiański"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "hy"
msgstr "hy"

#. type: Content of: <div><table><tr><td>
msgid "ազատ ծրագիր/ծրագրեր (azat tsragir/tsragrer)"
msgstr "ազատ ծրագիր/ծրագրեր (azat tsragir/tsragrer)"

#. type: Content of: <div><h3>
msgid "Basque"
msgstr "Baskijski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "eu"
msgstr "eu"

#. type: Content of: <div><table><tr><td>
msgid "software librea"
msgstr "software librea"

#. type: Content of: <div><table><tr><td>
msgid "doako softwarea"
msgstr "doako softwarea"

#. type: Content of: <div><h3>
msgid "Belarusian"
msgstr "Białoruski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "be"
msgstr "be"

#. type: Content of: <div><table><tr><td>
msgid ""
"свабоднае праграмнае забесьпячэньне (svabodnae pragramnae zabes'pjachen'ne)"
msgstr ""
"свабоднае праграмнае забесьпячэньне (svabodnae pragramnae zabes'pjachen'ne)"

#. type: Content of: <div><h3>
msgid "Bengali"
msgstr "Bengalski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "bn"
msgstr "bn"

#. type: Content of: <div><table><tr><td>
msgid "স্বাধীন সফটওয়্যার (swadhin software)"
msgstr "স্বাধীন সফটওয়্যার (swadhin software)"

#. type: Content of: <div><table><tr><td>
msgid "বিনামূল্যের সফটওয়্যার (binamuller software)"
msgstr "বিনামূল্যের সফটওয়্যার (binamuller software)"

#. type: Content of: <div><table><tr><th>
msgid "Proprietary Software"
msgstr "Proprietary Software"

#. type: Content of: <div><table><tr><td>
msgid "মালিকানা সফটওয়্যার (malikana software)"
msgstr "মালিকানা সফটওয়্যার (malikana software)"

#. type: Content of: <div><table><tr><th>
msgid "Commercial Software"
msgstr "Commercial Software"

#. type: Content of: <div><table><tr><td>
msgid "বাণিজ্যিক সফটওয়্যার (banijjik software)"
msgstr "বাণিজ্যিক সফটওয়্যার (banijjik software)"

#. type: Content of: <div><h3>
msgid "Bulgarian"
msgstr "Bułgarski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "bg"
msgstr "bg"

#. type: Content of: <div><table><tr><td>
msgid "свободен софтуер (svoboden softuer)"
msgstr "свободен софтуер (svoboden softuer)"

#. type: Content of: <div><table><tr><td>
msgid "безплатен софтуер (bezplaten softuer)"
msgstr "безплатен софтуер (bezplaten softuer)"

#. type: Content of: <div><h3>
msgid "Burmese"
msgstr "Birmański"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "my"
msgstr "my"

#. type: Content of: <div><table><tr><td>
msgid "လွတ်လပ်ဆော့ဖ်ဝဲ (lu' la' software)"
msgstr "လွတ်လပ်ဆော့ဖ်ဝဲ (lu' la' software)"

#. type: Content of: <div><table><tr><td>
msgid "အခမဲ့ဆော့ဖ်ဝဲ (a̱kha. me. software)"
msgstr "အခမဲ့ဆော့ဖ်ဝဲ (a̱kha. me. software)"

#. type: Content of: <div><h3>
msgid "Catalan"
msgstr "Kataloński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ca"
msgstr "ca"

#. type: Content of: <div><table><tr><td>
msgid "programari lliure"
msgstr "programari lliure"

#. type: Content of: <div><table><tr><td>
msgid "programari gratuït"
msgstr "programari gratuït"

#. type: Content of: <div><table><tr><td>
msgid "programari privatiu"
msgstr "programari privatiu"

#. type: Content of: <div><table><tr><td>
msgid "programari comercial"
msgstr "programari comercial"

#. type: Content of: <div><h3>
msgid "Chinese (Simplified)"
msgstr "Chiński (uproszczony)"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "zh-cn"
msgstr "zh-cn"

#. type: Content of: <div><table><tr><td>
msgid "自由软件 (zi-you ruan-jian)"
msgstr "自由软件 (zi-you ruan-jian)"

#. type: Content of: <div><table><tr><td>
msgid "免费软件 (mian-fei ruan-jian)"
msgstr "免费软件 (mian-fei ruan-jian)"

#. type: Content of: <div><h3>
msgid "Chinese (Traditional)"
msgstr "Chiński (tradycyjny)"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "zh-tw"
msgstr "zh-tw"

#. type: Content of: <div><table><tr><td>
msgid "自由軟體 (zih-yo ruan-ti)"
msgstr "自由軟體 (zih-yo ruan-ti)"

#. type: Content of: <div><table><tr><td>
msgid "免費軟體 (mien-fei ruan-ti)"
msgstr "免費軟體 (mien-fei ruan-ti)"

#. type: Content of: <div><h3>
msgid "Croatian"
msgstr "Chorwacki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "hr"
msgstr "hr"

#. type: Content of: <div><table><tr><td>
msgid "slobodan softver"
msgstr "slobodan softver"

#. type: Content of: <div><table><tr><td>
msgid "besplatan softver"
msgstr "besplatan softver"

#. type: Content of: <div><h3>
msgid "Czech"
msgstr "Czeski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "cs"
msgstr "cs"

#. type: Content of: <div><table><tr><td>
msgid "svobodný software"
msgstr "svobodný software"

#. type: Content of: <div><table><tr><td>
msgid "bezplatný software"
msgstr "bezplatný software"

#. type: Content of: <div><table><tr><td>
msgid "proprietární software"
msgstr "proprietární software"

#. type: Content of: <div><table><tr><td>
msgid "komerční software"
msgstr "komerční software"

#. type: Content of: <div><h3>
msgid "Danish"
msgstr "Duński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "da"
msgstr "da"

#. type: Content of: <div><table><tr><td>
msgid "fri software / frit programmel"
msgstr "fri software / frit programmel"

#. type: Content of: <div><table><tr><td>
msgid "gratis software"
msgstr "gratis software"

#. type: Content of: <div><h3>
msgid "Dutch"
msgstr "Niderlandzki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "nl"
msgstr "nl"

#. type: Content of: <div><table><tr><td>
msgid "vrije software"
msgstr "vrije software"

#. type: Content of: <div><table><tr><td>
msgid "private software"
msgstr "private software"

#. type: Content of: <div><table><tr><td>
msgid "commerci&euml;le software"
msgstr "commerci&euml;le software"

#. type: Content of: <div><h3>
msgid "Esperanto"
msgstr "Esperanto"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "eo"
msgstr "eo"

#. type: Content of: <div><table><tr><td>
msgid "libera programaro / programo"
msgstr "libera programaro / programo"

#. type: Content of: <div><table><tr><td>
msgid "senpaga programaro / programo"
msgstr "senpaga programaro / programo"

#. type: Content of: <div><table><tr><td>
msgid "mallibera programaro / programo"
msgstr "mallibera programaro / programo"

#. type: Content of: <div><table><tr><td>
msgid "komerca programaro / programo"
msgstr "komerca programaro / programo"

#. type: Content of: <div><h3>
msgid "Estonian"
msgstr "Estoński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "et"
msgstr "et"

#. type: Content of: <div><table><tr><td>
msgid "vaba tarkvara"
msgstr "vaba tarkvara"

#. type: Content of: <div><table><tr><td>
msgid "tasuta tarkvara"
msgstr "tasuta tarkvara"

#. type: Content of: <div><h3>
msgid "Finnish"
msgstr "Fiński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "fi"
msgstr "fi"

#. type: Content of: <div><table><tr><td>
msgid "vapaa ohjelmisto"
msgstr "vapaa ohjelmisto"

#. type: Content of: <div><table><tr><td>
msgid "ilmainen ohjelmisto"
msgstr "ilmainen ohjelmisto"

#. type: Content of: <div><h3>
msgid "French"
msgstr "Francuski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "fr"
msgstr "fr"

#. type: Content of: <div><table><tr><td>
msgid "logiciel libre"
msgstr "logiciel libre"

#. type: Content of: <div><table><tr><td>
msgid "logiciel gratuit"
msgstr "logiciel gratuit"

#. type: Content of: <div><table><tr><td>
msgid "logiciel privateur"
msgstr "logiciel privateur"

#. type: Content of: <div><table><tr><td>
msgid "logiciel commercial"
msgstr "logiciel commercial"

#. type: Content of: <div><h3>
msgid "Galician"
msgstr "Galicyjski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "gl"
msgstr "gl"

#. type: Content of: <div><table><tr><td>
msgid "software libre"
msgstr "software libre"

#. type: Content of: <div><table><tr><td>
msgid "software gratuíto"
msgstr "software gratuíto"

#. type: Content of: <div><table><tr><td>
msgid "software privativo"
msgstr "software privativo"

#. type: Content of: <div><table><tr><td>
msgid "software comercial"
msgstr "software comercial"

#. type: Content of: <div><h3>
msgid "Georgian"
msgstr "Gruziński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ka"
msgstr "ka"

#. type: Content of: <div><table><tr><td>
msgid "თავისუფალი პროგრამები (tavisupali programebi)"
msgstr "თავისუფალი პროგრამები (tavisupali programebi)"

#. type: Content of: <div><table><tr><td>
msgid "უფასო პროგრამები (upaso programebi)"
msgstr "უფასო პროგრამები (upaso programebi)"

#. type: Content of: <div><h3>
msgid "German"
msgstr "Niemiecki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "de"
msgstr "de"

#. type: Content of: <div><table><tr><td>
msgid "Freie Software"
msgstr "Freie Software"

#. type: Content of: <div><table><tr><td>
msgid "Gratis-Software / Kostenlose Software"
msgstr "Gratis-Software / Kostenlose Software"

#. type: Content of: <div><table><tr><td>
msgid "Proprietäre Software"
msgstr "Proprietäre Software"

#. type: Content of: <div><table><tr><td>
msgid "Kommerzielle Software"
msgstr "Kommerzielle Software"

#. type: Content of: <div><h3>
msgid "Greek"
msgstr "Grecki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "el"
msgstr "el"

#. type: Content of: <div><table><tr><td>
msgid "ελεύθερο λογισμικό (elefthero logismiko)"
msgstr "ελεύθερο λογισμικό (elefthero logismiko)"

#. type: Content of: <div><table><tr><td>
msgid "δωρεάν λογισμικό (dorean logismiko)"
msgstr "δωρεάν λογισμικό (dorean logismiko)"

#. type: Content of: <div><h3>
msgid "Hebrew"
msgstr "Hebrajski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "he"
msgstr "he"

#. type: Content of: <div><table><tr><td>
msgid "תוכנה חופשית (tochna chofshit)"
msgstr "תוכנה חופשית (tochna chofshit)"

#. type: Content of: <div><table><tr><td>
msgid "תוכנה חינמית (tochna chinamit)"
msgstr "תוכנה חינמית (tochna chinamit)"

#. type: Content of: <div><h3>
msgid "Hindi"
msgstr "Hindi"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "hi"
msgstr "hi"

#. type: Content of: <div><table><tr><td>
msgid "मुक्त सॉफ्टवेयर (mukt software)"
msgstr "मुक्त सॉफ्टवेयर (mukt software)"

#. type: Content of: <div><table><tr><td>
msgid "मुफ़्त सॉफ्टवेयर (muft software)"
msgstr "मुफ़्त सॉफ्टवेयर (muft software)"

#. type: Content of: <div><h3>
msgid "Hungarian"
msgstr "Węgierski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "hu"
msgstr "hu"

#. type: Content of: <div><table><tr><td>
msgid "szabad szoftver"
msgstr "szabad szoftver"

#. type: Content of: <div><table><tr><td>
msgid "ingyenes szoftver / ingyen szoftver"
msgstr "ingyenes szoftver / ingyen szoftver"

#. type: Content of: <div><h3>
msgid "Icelandic"
msgstr "Islandzki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "is"
msgstr "is"

#. type: Content of: <div><table><tr><td>
msgid "frjáls hugbúnaður"
msgstr "frjáls hugbúnaður"

#. type: Content of: <div><h3>
msgid "Ido"
msgstr "Ido"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "io"
msgstr "io"

#. type: Content of: <div><table><tr><td>
msgid "libera programaro"
msgstr "libera programaro"

#. type: Content of: <div><h3>
msgid "Indonesian"
msgstr "Indonezyjski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "id"
msgstr "id"

#. type: Content of: <div><table><tr><td>
msgid "perangkat lunak bebas"
msgstr "perangkat lunak bebas"

#. type: Content of: <div><table><tr><td>
msgid "perangkat lunak gratis"
msgstr "perangkat lunak gratis"

#. type: Content of: <div><h3>
msgid "Interlingua"
msgstr "Interlingua"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ia"
msgstr "ia"

#. type: Content of: <div><table><tr><td>
msgid "libere programmage / libere programmario"
msgstr "libere programmage / libere programmario"

#. type: Content of: <div><h3>
msgid "Irish"
msgstr "Irlandzki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ga"
msgstr "ga"

#. type: Content of: <div><table><tr><td>
msgid "Saorbhogearraí"
msgstr "Saorbhogearraí"

#. type: Content of: <div><table><tr><td>
msgid "Bogearraí saora in aisce"
msgstr "Bogearraí saora in aisce"

#. type: Content of: <div><h3>
msgid "Italian"
msgstr "Włoski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "it"
msgstr "it"

#. type: Content of: <div><table><tr><td>
msgid "software libero"
msgstr "software libero"

#. type: Content of: <div><table><tr><td>
msgid "software gratuito"
msgstr "software gratuito"

#. type: Content of: <div><table><tr><td>
msgid "software proprietario"
msgstr "software proprietario"

#. type: Content of: <div><table><tr><td>
msgid "software commerciale"
msgstr "software commerciale"

#. type: Content of: <div><h3>
msgid "Japanese"
msgstr "Japoński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ja"
msgstr "ja"

#. type: Content of: <div><table><tr><td>
msgid "自由ソフトウェア (jiyū sofutouea)"
msgstr "自由ソフトウェア (jiyū sofutouea)"

#. type: Content of: <div><table><tr><td>
msgid "無料ソフトウェア (muryō sofutouea)"
msgstr "無料ソフトウェア (muryō sofutouea)"

#. type: Content of: <div><table><tr><td>
msgid "プロプライエタリなソフトウェア (puropuraietari na sofutouea)"
msgstr "プロプライエタリなソフトウェア (puropuraietari na sofutouea)"

#. type: Content of: <div><table><tr><td>
msgid "商用ソフトウェア (shōyō sofutouea)"
msgstr "商用ソフトウェア (shōyō sofutouea)"

#. type: Content of: <div><h3>
msgid "Korean"
msgstr "Koreański"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ko"
msgstr "ko"

#. type: Content of: <div><table><tr><td>
msgid "자유 소프트웨어 (ja-yu software)"
msgstr "자유 소프트웨어 (ja-yu software)"

#. type: Content of: <div><h3>
msgid "Latvian"
msgstr "Łotewski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "lv"
msgstr "lv"

#. type: Content of: <div><table><tr><td>
msgid "brīva programmatūra"
msgstr "brīva programmatūra"

#. type: Content of: <div><table><tr><td>
msgid "bezmaksas programmatūra"
msgstr "bezmaksas programmatūra"

#. type: Content of: <div><h3>
msgid "Lithuanian"
msgstr "Litewski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "lt"
msgstr "lt"

#. type: Content of: <div><table><tr><td>
msgid "laisva programinė įranga"
msgstr "laisva programinė įranga"

#. type: Content of: <div><table><tr><td>
msgid "nemokama programinė įranga"
msgstr "nemokama programinė įranga"

#. type: Content of: <div><h3>
msgid "Macedonian"
msgstr "Macedoński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "mk"
msgstr "mk"

#. type: Content of: <div><table><tr><td>
msgid "слободен софтвер (sloboden softver)"
msgstr "слободен софтвер (sloboden softver)"

#. type: Content of: <div><table><tr><td>
msgid "бесплатен софтвер (besplaten softver)"
msgstr "бесплатен софтвер (besplaten softver)"

#. type: Content of: <div><h3>
msgid "Malay"
msgstr "Malajski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ms"
msgstr "ms"

#. type: Content of: <div><table><tr><td>
msgid "perisian bebas"
msgstr "perisian bebas"

#. type: Content of: <div><table><tr><td>
msgid "perisian percuma"
msgstr "perisian percuma"

#. type: Content of: <div><table><tr><td>
msgid "perisian hakmilik"
msgstr "perisian hakmilik"

#. type: Content of: <div><table><tr><td>
msgid "perisian dagangan"
msgstr "perisian dagangan"

#. type: Content of: <div><h3>
msgid "Malayalam"
msgstr "Malajalam"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ml"
msgstr "ml"

#. type: Content of: <div><table><tr><td>
msgid "സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ (svatantrasophṯṯveyar)"
msgstr "സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ (svatantrasophṯṯveyar)"

#. type: Content of: <div><table><tr><td>
msgid "സൗജന്യസോഫ്റ്റ്‌വെയര്‍ (soujanyasophṯṯveyar)"
msgstr "സൗജന്യസോഫ്റ്റ്‌വെയര്‍ (soujanyasophṯṯveyar)"

#. type: Content of: <div><h3>
msgid "Norwegian"
msgstr "Norweski"

#. type: Content of: <div><table><tr><td>
msgid "no, nb, nn"
msgstr "no, nb, nn"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "no"
msgstr "no"

#. type: Content of: <div><table><tr><td>
msgid "fri programvare"
msgstr "fri programvare"

#. type: Content of: <div><table><tr><td>
msgid "gratis programvare"
msgstr "gratis programvare"

#. type: Content of: <div><table><tr><td>
msgid "godseid programvare / proprietær programvare"
msgstr "godseid programvare / proprietær programvare"

#. type: Content of: <div><table><tr><td>
msgid "kommersiell programvare"
msgstr "kommersiell programvare"

#. type: Content of: <div><h3>
msgid "Persian (Farsi)"
msgstr "Perski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "fa"
msgstr "fa"

#. type: Content of: <div><table><tr><td>
msgid "نرم‌افزار آزاد (narmafzar azad)"
msgstr "نرم‌افزار آزاد (narmafzar azad)"

#. type: Content of: <div><table><tr><td>
msgid "نرم‌افزار رایگان (narmafzar raygan)"
msgstr "نرم‌افزار رایگان (narmafzar raygan)"

#. type: Content of: <div><h3>
msgid "Polish"
msgstr "Polski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "pl"
msgstr "pl"

#. type: Content of: <div><table><tr><td>
msgid "wolne oprogramowanie"
msgstr "wolne oprogramowanie"

#. type: Content of: <div><table><tr><td>
msgid "darmowe oprogramowanie"
msgstr "darmowe oprogramowanie"

#. type: Content of: <div><table><tr><td>
msgid "oprogramowanie własnościowe"
msgstr "oprogramowanie własnościowe"

#. type: Content of: <div><table><tr><td>
msgid "oprogramowanie komercyjne"
msgstr "oprogramowanie komercyjne"

#. type: Content of: <div><h3>
msgid "Portuguese"
msgstr "Portugalski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "pt"
msgstr "pt"

#. type: Content of: <div><table><tr><td>
msgid "software livre"
msgstr "software livre"

#. type: Content of: <div><h3>
msgid "Romanian"
msgstr "Rumuński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ro"
msgstr "ro"

#. type: Content of: <div><table><tr><td>
msgid "programe libere"
msgstr "programe libere"

#. type: Content of: <div><table><tr><td>
msgid "programe gratuite"
msgstr "programe gratuite"

#. type: Content of: <div><table><tr><td>
msgid "programe proprietărești"
msgstr "programe proprietărești"

#. type: Content of: <div><table><tr><td>
msgid "programe comerciale"
msgstr "programe comerciale"

#. type: Content of: <div><h3>
msgid "Russian"
msgstr "Ruski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ru"
msgstr "ru"

#. type: Content of: <div><table><tr><td>
msgid "свободные программы (svobodnie programmi)"
msgstr "свободные программы (svobodnie programmi)"

#. type: Content of: <div><table><tr><td>
msgid "бесплатные программы (besplatnie programmi)"
msgstr "бесплатные программы (besplatnie programmi)"

#. type: Content of: <div><table><tr><td>
msgid "несвободные программы (nesvobodnie programmi)"
msgstr "несвободные программы (nesvobodnie programmi)"

#. type: Content of: <div><table><tr><td>
msgid "коммерческие программы (kommercheskie programmi)"
msgstr "коммерческие программы (kommercheskie programmi)"

#. type: Content of: <div><h3>
msgid "Sardinian"
msgstr "Sardyński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sc"
msgstr "sc"

#. type: Content of: <div><table><tr><td>
msgid "software liberu"
msgstr "software liberu"

#. type: Content of: <div><h3>
msgid "Scottish Gaelic"
msgstr "Gaelicki szkocki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "gd"
msgstr "gd"

#. type: Content of: <div><table><tr><td>
msgid "bathar-bog saor"
msgstr "bathar-bog saor"

#. type: Content of: <div><table><tr><td>
msgid "bathar-bog an-asgaidh"
msgstr "bathar-bog an-asgaidh"

#. type: Content of: <div><h3>
msgid "Serbian"
msgstr "Serbski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sr"
msgstr "sr"

#. type: Content of: <div><table><tr><td>
msgid "слободни софтвер / slobodni softver"
msgstr "слободни софтвер / slobodni softver"

#. type: Content of: <div><table><tr><td>
msgid "бесплатни софтвер / besplatni softver"
msgstr "бесплатни софтвер / besplatni softver"

#. type: Content of: <div><h3>
msgid "Sinhala"
msgstr "Syngaleski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "si"
msgstr "si"

#. type: Content of: <div><table><tr><td>
msgid "නිදහස් මෘදුකාංග (nidahas mṛdukāṅga)"
msgstr "නිදහස් මෘදුකාංග (nidahas mṛdukāṅga)"

#. type: Content of: <div><h3>
msgid "Slovak"
msgstr "Słowacki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sk"
msgstr "sk"

#. type: Content of: <div><table><tr><td>
msgid "slobodný softvér"
msgstr "slobodný softvér"

#. type: Content of: <div><h3>
msgid "Slovenian"
msgstr "Słoweński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sl"
msgstr "sl"

#. type: Content of: <div><table><tr><td>
msgid "prosto programje"
msgstr "prosto programje"

#. type: Content of: <div><h3>
msgid "Spanish"
msgstr "Hiszpański"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "es"
msgstr "es"

#. type: Content of: <div><h3>
msgid "Swahili"
msgstr "Suahili"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sw"
msgstr "sw"

#. type: Content of: <div><table><tr><td>
msgid "Software huru / Programu huru za Kompyuta"
msgstr "Software huru / Programu huru za&nbsp;Kompyuta"

#. type: Content of: <div><h3>
msgid "Swedish"
msgstr "Szwedzki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "sv"
msgstr "sv"

#. type: Content of: <div><table><tr><td>
msgid "fri programvara / fri mjukvara"
msgstr "fri programvara / fri mjukvara"

#. type: Content of: <div><table><tr><td>
msgid "gratis programvara / gratis mjukvara"
msgstr "gratis programvara / gratis mjukvara"

#. type: Content of: <div><h3>
msgid "Tagalog / Filipino"
msgstr "Tagalski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "tl"
msgstr "tl"

#. type: Content of: <div><table><tr><td>
msgid "malayang software"
msgstr "malayang software"

#. type: Content of: <div><h3>
msgid "Tamil"
msgstr "Tamilski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ta"
msgstr "ta"

#. type: Content of: <div><table><tr><td>
msgid "கட்டற்ற மென்பொருள் (kaṭṭaṟṟa meṉpoñaḷ)"
msgstr "கட்டற்ற மென்பொருள் (kaṭṭaṟṟa meṉpoñaḷ)"

#. type: Content of: <div><table><tr><td>
msgid "இலவச மென்பொருள் (illavasa menporul)"
msgstr "இலவச மென்பொருள் (illavasa menporul)"

#. type: Content of: <div><h3>
msgid "Telugu"
msgstr "Telugu"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "te"
msgstr "te"

#. type: Content of: <div><table><tr><td>
msgid "స్వేచ్ఛ సాఫ్ట్‌వేర్ (swechcha software)"
msgstr "స్వేచ్ఛ సాఫ్ట్‌వేర్ (swechcha software)"

#. type: Content of: <div><table><tr><td>
msgid "ఉచిత సాఫ్ట్‌వేర్ (uchitha software)"
msgstr "ఉచిత సాఫ్ట్‌వేర్ (uchitha software)"

#. type: Content of: <div><table><tr><td>
msgid "యాజమాన్య సాఫ్ట్‌వేర్ (yaajamaanya software)"
msgstr "యాజమాన్య సాఫ్ట్‌వేర్ (yaajamaanya software)"

#. type: Content of: <div><table><tr><td>
msgid "వాణిజ్య సాఫ్ట్‌వేర్ (vaanijya software)"
msgstr "వాణిజ్య సాఫ్ట్‌వేర్ (vaanijya software)"

#. type: Content of: <div><h3>
msgid "Thai"
msgstr "Tajski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "th"
msgstr "th"

#. type: Content of: <div><table><tr><td>
msgid "ซอฟต์แวร์เสรี (sofotwerseri)"
msgstr "ซอฟต์แวร์เสรี (sofotwerseri)"

#. type: Content of: <div><h3>
msgid "Turkish"
msgstr "Turecki"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "tr"
msgstr "tr"

#. type: Content of: <div><table><tr><td>
msgid "özgür yazılım"
msgstr "özgür yazılım"

#. type: Content of: <div><table><tr><td>
msgid "bedava yazılım"
msgstr "bedava yazılım"

#. type: Content of: <div><table><tr><td>
msgid "özel mülk yazılım"
msgstr "özel mülk yazılım"

#. type: Content of: <div><table><tr><td>
msgid "ticari yazılım"
msgstr "ticari yazılım"

#. type: Content of: <div><h3>
msgid "Ukrainian"
msgstr "Ukraiński"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "uk"
msgstr "uk"

#. type: Content of: <div><table><tr><td>
msgid "вільне програмне забезпечення (vil'ne prohramne zabezpechennja)"
msgstr "вільне програмне забезпечення (vil'ne prohramne zabezpechennja)"

#. type: Content of: <div><table><tr><td>
msgid "безплатне програмне забезпечення (bezplatne programne zapezpechennja)"
msgstr "безплатне програмне забезпечення (bezplatne programne zapezpechennja)"

#. type: Content of: <div><table><tr><td>
msgid ""
"власницьке програмне забезпечення (vlasnits'ke programne zapezpechennja)"
msgstr ""
"власницьке програмне забезпечення (vlasnits'ke programne zapezpechennja)"

#. type: Content of: <div><table><tr><td>
msgid ""
"комерційне програмне забезпечення (kommertsijne programne zapezpechennja)"
msgstr ""
"комерційне програмне забезпечення (kommertsijne programne zapezpechennja)"

#. type: Content of: <div><h3>
msgid "Urdu"
msgstr "Urdu"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "ur"
msgstr "ur"

#. type: Content of: <div><table><tr><td>
msgid "آزاد سافٹ ویئر (azad software)"
msgstr "آزاد سافٹ ویئر (azad software)"

#. type: Content of: <div><table><tr><td>
msgid "مفت سافٹ ویئر (muft software)"
msgstr "مفت سافٹ ویئر (muft software)"

#. type: Content of: <div><h3>
msgid "Vietnamese"
msgstr "Wietnamski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "vi"
msgstr "vi"

#. type: Content of: <div><table><tr><td>
msgid "phần mềm tự do"
msgstr "phần mềm tự do"

#. type: Content of: <div><table><tr><td>
msgid "phần mềm miễn phí"
msgstr "phần mềm miễn phí"

#. type: Content of: <div><table><tr><td>
msgid "phần mềm sở hữu độc quyền"
msgstr "phần mềm sở hữu độc quyền"

#. type: Content of: <div><table><tr><td>
msgid "phần mềm thương mại"
msgstr "phần mềm thương mại"

#. type: Content of: <div><h3>
msgid "Welsh"
msgstr "Walijski"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "cy"
msgstr "cy"

#. type: Content of: <div><table><tr><td>
msgid "meddalwedd rydd"
msgstr "meddalwedd rydd"

#. type: Content of: <div><h3>
msgid "Zulu"
msgstr "Zulu"

#. type: Attribute 'lang' of: <div><table><tr><td>
msgid "zu"
msgstr "zu"

#. type: Content of: <div><table><tr><td>
msgid "Isoftware Ekhululekile"
msgstr "Isoftware Ekhululekile"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:web-translators@gnu.org\">&lt;web-"
"translators@gnu.org&gt;</a>."
msgstr ""
"Wszelkie pytania dotyczące GNU i&nbsp;FSF prosimy kierować na&nbsp;adres <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Inne metody kontaktu "
"z&nbsp;FSF można znaleźć na&nbsp;stronie <a href=\"/contact/contact.html"
"\">kontakt</a> <br /> Informacje o niedziałających odnośnikach oraz&nbsp;"
"inne poprawki (lub propozycje) prosimy wysyłać na&nbsp;adres <a href="
"\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Staramy się, aby&nbsp;tłumaczenia były wierne i&nbsp;wysokiej jakości, "
"ale&nbsp;nie jesteśmy zwolnieni z&nbsp;niedoskonałości. Komentarze odnośnie "
"tłumaczenia polskiego oraz&nbsp;zgłoszenia dotyczące chęci współpracy w&nbsp;"
"tłumaczeniu prosimy kierować na&nbsp;adres <a href=\"mailto:www-pl-trans@gnu."
"org\">www-pl-trans@gnu.org</a>. <br /> Więcej informacji na&nbsp;temat "
"koordynacji oraz&nbsp;zgłaszania propozycji tłumaczeń artykułów znajdziecie "
"na&nbsp;<a href=\"/server/standards/README.translations.html\">stronie "
"tłumaczeń</a>."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1999, 2001, 2003, 2004, 2006, 2007, 2009, 2010, 2013-2019, "
"2021, 2022 Free Software Foundation, Inc."
msgstr ""
"Copyright &copy; 1999, 2001, 2003, 2004, 2006, 2007, 2009, 2010, 2013-2019, "
"2021, 2022 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Ta strona jest dostępna na&nbsp;<a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.pl\">licencji Creative Commons "
"Uznanie autorstwa&nbsp;&ndash; Bez&nbsp;utworów zależnych 4.0 "
"Międzynarodowe</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr "Tłumaczenie: Jan Owoc 2016."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Aktualizowane:"

#~ msgid "Language Name"
#~ msgstr "Nazwa języka"

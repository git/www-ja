# Malayalam Translation of http://www.gnu.org/philosophy/microsoft-old.html
# Copyright (C) 2008, 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnu.org article
# Santhosh Thottingal <santhosh.thottingal@gmail.com>
# Shyam Karanatt <shyam@swathanthran.in>
# Navaneeth <navaneeth@gnu.org>, 2014
# April 2017: standardize links, footer & punctuation (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: microsoft-old.html\n"
"POT-Creation-Date: 2021-09-12 08:26+0000\n"
"PO-Revision-Date: 2014-05-24 16:55+0530\n"
"Last-Translator: Navaneeth <navaneeth@gnu.org>\n"
"Language-Team: Malayalam <web-translators@gnu.org>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2021-07-18 09:55+0000\n"

#. type: Content of: <title>
msgid ""
"Is Microsoft the Great Satan? (Old Version)  - GNU Project - Free Software "
"Foundation"
msgstr ""
"മൈക്രോസോഫ്റ്റാണോ വലിയ ചെകുത്താ? (പഴയ പതിപ്പ്) - ഗ്നു സംരംഭം - സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ പ്രസ്ഥാനം"

#. type: Content of: <div><h2>
msgid "Is Microsoft the Great Satan? (Old Version)"
msgstr "മൈക്രോസോഫ്റ്റാണോ വലിയ ചെകുത്താ? (പഴയ പതിപ്പ്)"

#. type: Content of: <div><div><p>
msgid ""
"There is an <a href=\"/philosophy/microsoft.html\"> updated version</a> of "
"this article."
msgstr ""
"ഈ ലേഖനത്തിന്റെ <a href=\"/philosophy/microsoft.html\">പുതുക്കിയ പതിപ്പു് ഇവിടെ</a> "
"ലഭ്യമാണു്."

#. type: Content of: <div><p>
msgid ""
"Many people think of Microsoft as the monster menace of the software "
"industry. There is even a campaign to boycott Microsoft. This feeling has "
"intensified since Microsoft expressed active hostility towards free software."
msgstr ""
"സോഫ്റ്റ്‌വെയര്‍ വ്യവസായത്തിനാകെ നാശം വരുത്തുന്ന ചെകുത്താനായിട്ടാണു് മൈക്രോസോഫ്റ്റിനെ പലരും "
"കരുതുന്നതു്. മൈക്രോസോഫ്റ്റിനെ ബഹിഷ്കരിയ്ക്കുക എന്നൊരു പ്രചരണവമുണ്ടു്. സ്വതന്ത്രസോഫ്റ്റ്‌വെയറിനോടു് "
"വിരോധം കാണിയ്ക്കുക വഴി മൈക്രോസോഫ്റ്റ് ഈ വിശ്വാസത്തെ ഊട്ടിയുറപ്പിയ്ക്കുകയും ചെയ്തു."

#. type: Content of: <div><p>
msgid ""
"In the free software movement, our perspective is different. We see that "
"Microsoft is doing something that is bad for software users: making software "
"<a href=\"/philosophy/categories.html#ProprietarySoftware\"> proprietary</a> "
"and thus denying users their rightful freedom."
msgstr ""
"സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ പ്രസ്ഥാനത്തിലെ ഞങ്ങളുടെ വീക്ഷണം പക്ഷേ വ്യത്യസ്തമാണു്. സോഫ്റ്റ്‌വെയര്‍ "
"ഉപയോക്താക്കള്‍ക്കാകമാനം മോശമായ രീതിയില്‍ മൈക്രോസോഫ്റ്റ് പലതും ചെയ്യുന്നതായാണു് ഞങ്ങള്‍ കാണുന്നത്: "
"സോഫ്റ്റ്‌വെയറിന്റെ <a href=\"/philosophy/categories.html#ProprietarySoftware"
"\">കുത്തകവത്കരണവും</a> അതുവഴി ഉപയോക്താക്കള്‍ക്കവകാശപ്പെട്ട സ്വാതന്ത്ര്യനിഷേധവും ആണതു്."

#. type: Content of: <div><p>
msgid ""
"But Microsoft is not alone in this; almost all software companies do the "
"same thing to the users. If other companies manage to dominate fewer users "
"than Microsoft, that is not for lack of trying."
msgstr ""
"പക്ഷേ മൈക്രോസോഫ്റ്റ് മാത്രമല്ല ഇതെല്ലാം ചെയ്യുന്നതു്. മിക്ക സോഫ്റ്റ്‌വെയര്‍ കമ്പനികളും "
"ഉപയോക്താക്കളോടു് ചെയ്യുന്നതിതു തന്നെയാണു്. മൈക്രോസോഫ്റ്റിനെക്കാള്‍ കുറച്ചു ഉപയോക്താക്കളുടെ മേല്‍ "
"ആധിപത്യം നേടാനേ മറ്റുള്ളവര്‍ക്കു് കഴിഞ്ഞുള്ളൂ  എന്നതു് അവര്‍ ശ്രമിയ്ക്കാഞ്ഞിട്ടല്ല."

#. type: Content of: <div><p>
msgid ""
"This is not meant to excuse Microsoft. Rather, it is meant as a reminder "
"that Microsoft is the natural development of a software industry based on <a "
"href=\"/philosophy/shouldbefree.html\">dividing users and taking away their "
"freedom</a>. When criticizing Microsoft, we must not exonerate the other "
"companies that also make proprietary software. At the FSF, we don't run any "
"proprietary software&mdash;not from Microsoft or anyone else."
msgstr ""
"മൈക്രോസോഫ്റ്റിനെ വെറുതെവിടാനല്ല ഇതു പറഞ്ഞതു്. <a href=\"/philosophy/shouldbefree.html"
"\">ഉപയോക്താക്കളെ വിഭജിയ്ക്കുകയും അവരുടെ സ്വതന്ത്ര്യത്തെ ഹനിയ്ക്കുകയും ചെയ്യുകയെന്ന</a> "
"സോഫ്റ്റ്‌വെയര്‍ ഇന്‍ഡസ്ട്രിയുടെ സ്വഭാവത്തില്‍ നിന്നുള്ള  സ്വാഭാവികമായ ആവിര്‍ഭാവമായിരുന്നു "
"മൈക്രോസോഫ്റ്റ്. മൈക്രോസോഫ്റ്റിനെ വിമര്‍ശിയ്ക്കുമ്പോള്‍ കുത്തക സോഫ്റ്റ്‌വെയര്‍ ഉണ്ടാക്കുന്ന മറ്റു "
"സോഫ്റ്റ്‌വെയര്‍ കമ്പനികളെ നാം മറന്നുകൂടാ. സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ പ്രസ്ഥാനത്തില്‍ നാം കുത്തക "
"സോഫ്റ്റ്‌വെയറുകള്‍ ഉപയോഗിയ്ക്കുന്നില്ല &ndash; മൈക്രോസോഫ്റ്റിന്റെ മാത്രമല്ല, മറ്റാരുടെയും."

#. type: Content of: <div><p>
# | In the &ldquo;Halloween [-documents&rdquo;,-] {+documents,&rdquo;+}
# | released at the end of October 1998, Microsoft executives stated an
# | intention to use various methods to obstruct the development of free
# | software: specifically, designing secret protocols and file formats, and
# | patenting algorithms and software features.
#, fuzzy
#| msgid ""
#| "In the &ldquo;Halloween documents&rdquo;, released at the end of October "
#| "1998, Microsoft executives stated an intention to use various methods to "
#| "obstruct the development of free software: specifically, designing secret "
#| "protocols and file formats, and patenting algorithms and software "
#| "features."
msgid ""
"In the &ldquo;Halloween documents,&rdquo; released at the end of October "
"1998, Microsoft executives stated an intention to use various methods to "
"obstruct the development of free software: specifically, designing secret "
"protocols and file formats, and patenting algorithms and software features."
msgstr ""
"1998 ഒക്ടോബറില്‍ പുറത്തുവിട്ട &ldquo;ഹാലോവീന്‍ രേഖകളില്‍&rdquo; സ്വതന്ത്രസോഫ്റ്റ്‌വെയര്‍ വികസനം "
"തടയാനുള്ള വിവിധ പദ്ധതികള്‍ മൈക്രോസോഫ്റ്റ് ഉദ്യോഗസ്ഥര്‍  വിശദീകരിച്ചിരുന്നു. പ്രത്യേകിച്ചും, രഹസ്യ "
"പ്രോട്ടോക്കോളുകളും രഹസ്യ ഫയല്‍ ഫോര്‍മാറ്റുകളും ഉണ്ടാക്കുകയും, സോഫ്റ്റ്‌വെയര്‍ അല്‍ഗോരിതങ്ങളും "
"സവിശേഷതകളും പേറ്റന്റ് ചെയ്യുകയും ചെയ്യുന്നതിനെപ്പറ്റി."

#. type: Content of: <div><p>
# | These obstructionist policies are nothing new: Microsoft, and many other
# | software companies, have been doing them for years now. In the past,
# | probably, their motivation was to attack each other; now, it seems, we are
# | among the intended targets. But that change in motivation has no practical
# | consequence, because secret conventions and software patents obstruct
# | everyone, regardless of the &ldquo;intended [-target&rdquo;.-]
# | {+target.&rdquo;+}
#, fuzzy
#| msgid ""
#| "These obstructionist policies are nothing new: Microsoft, and many other "
#| "software companies, have been doing them for years now. In the past, "
#| "probably, their motivation was to attack each other; now, it seems, we "
#| "are among the intended targets. But that change in motivation has no "
#| "practical consequence, because secret conventions and software patents "
#| "obstruct everyone, regardless of the &ldquo;intended target&rdquo;."
msgid ""
"These obstructionist policies are nothing new: Microsoft, and many other "
"software companies, have been doing them for years now. In the past, "
"probably, their motivation was to attack each other; now, it seems, we are "
"among the intended targets. But that change in motivation has no practical "
"consequence, because secret conventions and software patents obstruct "
"everyone, regardless of the &ldquo;intended target.&rdquo;"
msgstr ""
"ഇത്തരം പിന്തിരിപ്പന്‍ പദ്ധതികള്‍ പുത്തനല്ല; മൈക്രോസോഫ്റ്റും മറ്റു സോഫ്റ്റ്‌വെയര്‍ കമ്പനികളും "
"വര്‍ഷങ്ങളായി ചെയ്തു വരുന്നതാണിതു്. പക്ഷേ, പണ്ട് അവരുടെ പ്രചോദനം, ഏറെകുറെ പരസ്പരം "
"ആക്രമിക്കുന്നതിലായിരുന്നെങ്കില്‍, ഇപ്പോള്‍ നമ്മളാണു് ലക്ഷ്യമെന്നു തോന്നുന്നു.   പക്ഷേ പ്രചോദനത്തിലുള്ള "
"വ്യത്യാസം പ്രായോഗികമായ മാറ്റങ്ങളൊന്നുമുണ്ടാക്കില്ല. കാരണം, സോഫ്റ്റ്‌വെയര്‍ പേറ്റന്റുകളും രഹസ്യ "
"സങ്കേതങ്ങളും എല്ലാവരെയും ബാധിയ്ക്കുന്നതാണു്, &ldquo;ലക്ഷ്യത്തെ മാത്രമല്ല&rdquo;."

#. type: Content of: <div><p>
msgid ""
"Secrecy and patents do threaten free software. They have obstructed us "
"greatly in the past, and we must expect they will do so even more in the "
"future. But this is no different from what was going to happen even if "
"Microsoft had never noticed us. The only real significance of the &ldquo;"
"Halloween documents&rdquo; is that Microsoft seems to think that the <a href="
"\"/gnu/linux-and-gnu.html\">GNU/Linux system</a> has the potential for great "
"success."
msgstr ""
"രഹസ്യ സങ്കേതങ്ങളും പേറ്റന്റുകളും സ്വതന്ത്രസോഫ്റ്റ്‌വെയറിനു് ഭീഷണി തന്നെയാണു്. പണ്ടു് അവ വലിയതോതില്‍ "
"നമ്മുടെ വഴിമുടക്കിയിട്ടുണ്ടു്.  ഭാവിയിലും വര്‍ദ്ധിത വീര്യത്തോടെ അവരതു് ചെയ്യുമെന്നു് നാം "
"പ്രതീക്ഷിക്കണം. പക്ഷേ മൈക്രോസോഫ്റ്റ് നമ്മളെ ശ്രദ്ധിച്ചിരുന്നില്ലെങ്കിലും ഏറെക്കുറെ ഇതൊക്കെ തന്നെ "
"സംഭവിക്കുമായിരുന്നു. <a href=\"/gnu/linux-and-gnu.html\">ഗ്നു/ലിനക്സ് സിസ്റ്റ</a> ത്തിനു് "
"വന്‍ വിജയസാധ്യതയുണ്ടെന്നു് മൈക്രോസോഫ്റ്റ് കരുതിയിരുന്നിരിക്കാം എന്നാതാണു്  &ldquo;ഹാലോവീന്‍ "
"രേഖകളുടെ&rdquo; സാംഗത്യം."

#. type: Content of: <div><p>
msgid "Thank you, Microsoft, and please get out of the way."
msgstr "മൈക്രോസോഫ്റ്റേ നന്ദി, പക്ഷെ ദയവായി വഴിമുടക്കരുതു്."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"എഫ്.എസ്.എഫിനെ കുറിച്ചും ഗ്നുവിനെ കുറിച്ചുമുള്ള ചോദ്യങ്ങളും സംശയങ്ങളും ദയവായി <a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a> എന്ന വിലാസത്തിലേയ്ക്കു് അയയ്ക്കുക. എഫ്.എസ്.എഫുമായി "
"ബന്ധപ്പെടാന്‍ <a href=\"/contact/\">മറ്റു വഴികളും ഉണ്ടു്</a>. തെറ്റായ കണ്ണികളെകുറിച്ചും മറ്റു "
"നിര്‍ദ്ദേശങ്ങളും അഭിപ്രായങ്ങളും <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a> എന്ന വിലാസത്തിലേയ്ക്കു് എഴുതാവുന്നതാണു്."

# TODO: submitting -> contributing.
# type: Content of: <div><p>
#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
# || No change detected.  The change might only be in amounts of spaces.
#, fuzzy
#| msgid ""
#| "Please see the <a href=\"/server/standards/README.translations.html"
#| "\">Translations README</a> for information on coordinating and "
#| "contributing translations of this article."
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"ഗ്നു താളുകളുടെ മലയാളം പരിഭാഷകള്‍ കൃത്യവും നിലവാരമുള്ളതുമാക്കാൻ ഞങ്ങള്‍ പരമാവധി ശ്രമിക്കുന്നുണ്ടു്. "
"എന്നിരുന്നാലും അവ പൂര്‍ണമായും കുറ്റമറ്റതാണെന്നു പറയാന്‍ സാധിക്കില്ല. ഇതിനെകുറിച്ചുള്ള താങ്കളുടെ "
"അഭിപ്രായങ്ങളും നിർദ്ദേശങ്ങളും ദയവായി <a href=\"mailto:web-translators@gnu.org"
"\">&lt;web-translators@gnu.org&gt;</a> എന്ന വിലാസത്തിൽ അറിയിക്കുക.</p><p>വെബ് "
"താളുകളുടെ പരിഭാഷകൾ സമർപ്പിക്കാനും ബന്ധപ്പെട്ട വിവരങ്ങൾക്കും <a href=\"/server/standards/"
"README.translations.html\">Translations README</a> നോക്കുക."

# type: Content of: <div><p>
#. type: Content of: <div><p>
# | Copyright &copy; [-2010-] {+1997-2000, 2021+} Free Software Foundation,
# | Inc.
#, fuzzy
#| msgid "Copyright &copy; 2010 Free Software Foundation, Inc."
msgid "Copyright &copy; 1997-2000, 2021 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2010 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"ഈ താളു് <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-"
"nd/4.0/\">ക്രിയേറ്റീവ് കോമണ്‍സ് ആട്രിബ്യൂഷന്‍-നോഡെറിവേറ്റീവ്സ് 4.0 ഇന്റർനാഷണൽ ലൈസൻസ്</a> "
"അടിസ്ഥാനത്തില്‍ പ്രസിദ്ധീകരിച്ചതാണു്."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<b>പരിഭാഷ</b>: Santhosh Thottingal | സന്തോഷ് തോട്ടിങ്ങല്‍  &lt;santhosh."
"thottingal@gmail.com&gt;, Shyam Karanatt | ശ്യാം കാരനാട്ട് &lt;"
"shyam@swathanthran.in&gt;"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "പുതുക്കിയതു്:"

#~ msgid ""
#~ "Copyright &copy; 1997, 1998, 1999, 2000, 2007, 2008, 2009 Free Software "
#~ "Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 1996, 1997, 1998, 2001, 2007, 2008, 2009 Free Software "
#~ "Foundation, Inc."

#~ msgid ""
#~ "Copyright &copy; 1997, 1998, 1999, 2000, 2007, 2008, 2009, 2021 Free "
#~ "Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 1996, 1997, 1998, 2001, 2007, 2008, 2009, 2021 Free "
#~ "Software Foundation, Inc."

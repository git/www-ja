# French translation of http://www.gnu.org/philosophy/fire.html
# Copyright (C) 2000 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Benjamin Drieu, 2000.
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2008.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: fire.html\n"
"POT-Creation-Date: 2023-05-10 10:55+0000\n"
"PO-Revision-Date: 2024-03-03 16:18+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Copyrighting Fire! - GNU Project - Free Software Foundation"
msgstr "Le feu sous copyright ! - Projet GNU - Free Software Foundation"

#. type: Content of: <div><h2>
msgid "Copyrighting Fire!"
msgstr "Le feu sous copyright !"

#. type: Content of: <div><address>
msgid "by Ian Clarke"
msgstr "par Ian Clarke"

#. type: Content of: <div><p>
msgid ""
"I was in the pub last night, and a guy asked me for a light for his "
"cigarette. I suddenly realised that there was a demand here and money to be "
"made, and so I agreed to light his cigarette for 10 pence, but I didn't "
"actually give him a light, I sold him a license to burn his cigarette. My "
"fire-license restricted him from giving the light to anybody else, after "
"all, that fire was my property. He was drunk, and dismissing me as a loony, "
"but accepted my fire (and by implication the licence which governed its "
"use)  anyway. Of course in a matter of minutes I noticed a friend of his "
"asking him for a light and to my outrage he gave his cigarette to his friend "
"and pirated my fire! I was furious, I started to make my way over to that "
"side of the bar but to my added horror his friend then started to light "
"other people's cigarettes left, right, and centre! Before long that whole "
"side of the bar was enjoying <strong>my</strong> fire without paying me "
"anything. Enraged I went from person to person grabbing their cigarettes "
"from their hands, throwing them to the ground, and stamping on them."
msgstr ""
"J'étais au pub hier soir quand un type m'a demandé du feu pour sa cigarette. "
"J'ai soudainement réalisé qu'il y avait là une demande et de l'argent à se "
"faire et j'ai accepté d'allumer sa cigarette pour 10 pence, mais je ne lui "
"ai pas vraiment donné du feu, je lui ai vendu une licence pour fumer sa "
"cigarette. Ma licence de feu l'empêchait d'allumer celle de quelqu'un "
"d'autre ; après tout, ce feu était ma propriété. Il était soûl et me prenait "
"pour un dingue, mais il a tout de même accepté mon feu (et par implication "
"la licence qui régissait son utilisation). Bien sûr, au bout de quelques "
"minutes j'ai noté qu'un de ses amis lui avait demandé du feu et, à ma grande "
"indignation, il a donné sa cigarette à son ami et a piraté mon feu ! J'étais "
"furieux et j'ai commencé à tailler mon chemin vers ce côté du bar, mais à ma "
"grande horreur son ami a commencé à allumer les cigarettes d'autres "
"personnes, à gauche, à droite, au centre ! Sous peu, ce côté du bar tout "
"entier jouissait de <strong>mon</strong> feu sans rien me payer. Fou de "
"rage, j'allais d'une personne à l'autre pour lui arracher la cigarette des "
"mains, la jeter par terre et l'écraser."

#. type: Content of: <div><p>
msgid ""
"Strangely the door staff exhibited no respect for my property rights as they "
"threw me out the door."
msgstr ""
"Étrangement, les videurs ne montrèrent aucun respect pour mon droit de "
"propriété en me jetant dehors."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2000 Ian Clarke"
msgstr "Copyright &copy; 2000 Ian Clarke"

#. type: Content of: <div><p>
msgid ""
"Verbatim copying and distribution of this entire article is permitted in any "
"medium, provided this notice is preserved."
msgstr ""
"La reproduction exacte et la distribution intégrale de cet article sont "
"permises sur n'importe quel support d'archivage, pourvu que le présent avis "
"soit préservé."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Benjamin Drieu<br />Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

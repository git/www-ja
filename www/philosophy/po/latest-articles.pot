# LANGUAGE translation of https://www.gnu.org/philosophy/latest-articles.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: latest-articles.html\n"
"POT-Creation-Date: 2024-01-17 11:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Latest Articles - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <div><h2>
msgid "Philosophy of the GNU Project &mdash; Latest Articles"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Hot off the presses, here are the latest published articles on free software "
"and the GNU project."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/the-moral-and-the-legal.html\">The Moral and the "
"Legal</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/gnu/road-to-gnu.html\">The Road to GNU</a> &mdash; Richard "
"Stallman's experiences that prepared him to fight for a free software world."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/tivoization.html\">Tivoization</a> &mdash; When free "
"software isn't free in practice"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"/philosophy/wwworst-app-store.html\">The WWWorst app store</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/free-software-rocket.html\">Should Rockets Have Only "
"Free Software? Free Software and Appliances</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/saying-no-even-once.html\">Saying No to unjust "
"computing even once is help</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"/philosophy/posting-videos.html\">Posting Videos</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/install-fest-devil.html\">Install Fests: What to Do "
"about the Deal with the Devil</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/upgrade-windows.html\">What Is the Right Way to "
"Upgrade an Installation of Windows?</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/phone-anonymous-payment.html\">Anonymous Payment by "
"Phone</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/kind-communication.html\">GNU Kind Communications "
"Guidelines</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/surveillance-testimony.html\">Surveillance "
"Testimony</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/licenses/identify-licenses-clearly.html\"> For Clarity's Sake, "
"Please Don't Say &ldquo;Licensed under GNU GPL 2&rdquo;!</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/devils-advocate.html\"> Why the Devil's Advocate "
"Doesn't Help Reach the Truth</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/hackathons.html\"> Why Hackathons Should Insist on "
"Free Software</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/contradictory-support.html\"> Beware of Contradictory "
"&ldquo;Support&rdquo;</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/when-free-depends-on-nonfree.html\"> When Free "
"Software Depends on Nonfree</a>"
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1996-2010, 2013-2021, 2023, 2024 Free Software Foundation, "
"Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

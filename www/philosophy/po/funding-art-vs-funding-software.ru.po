# Russian translation of http://www.gnu.org/philosophy/funding-art-vs-funding-software.html
# Copyright (C) 2013, 2021 Richard Stallman
# Copyright (C) 2013, 2021 Free Software Foundation, Inc. (translation)
# This file is distributed under the same license as the original article.
# Ineiev <ineiev@gnu.org>, 2013, 2017, 2021
# this translation lacks appropriate review
#
msgid ""
msgstr ""
"Project-Id-Version: funding-art-vs-funding-software.html\n"
"POT-Creation-Date: 2021-09-16 16:58+0000\n"
"PO-Revision-Date: 2021-09-16 17:51+0000\n"
"Last-Translator: Ineiev <ineiev@gnu.org>\n"
"Language-Team: Russian <www-ru-list@gnu.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Funding Art vs Funding Software - GNU Project - Free Software Foundation"
msgstr ""
"Финансирование искусства и финансирование программ - Проект GNU - Фонд "
"свободного программного обеспечения"

#. type: Content of: <div><h2>
msgid "Funding Art vs Funding Software"
msgstr "Финансирование искусства и финансирование программ"

# type: Content of: <p>
#. type: Content of: <div><address>
msgid "by <a href=\"https://www.stallman.org/\">Richard Stallman</a>"
msgstr "<a href=\"https://www.stallman.org/\">Ричард Столмен</a>"

#. type: Content of: <div><p>
msgid ""
"I've proposed two new systems to fund artists in a world where we have "
"legalized sharing (noncommercial redistribution of exact copies) of "
"published works.  One is for the state to collect taxes for the purpose, and "
"divide the money among artists in proportion to the cube root of the "
"popularity of each one (as measured by surveying samples of the "
"population).  The other is for each player to have a &ldquo;donate&rdquo; "
"button to anonymously send a small sum (perhaps 50 cents, in the US) to the "
"artists who made the last work played.  These funds would go to artists, not "
"to their publishers."
msgstr ""
"Я предложил две новых системы финансирования деятелей искусства в мире, в "
"котором мы легализовали обмен опубликованными работами, т.е. некоммерческое "
"перераспространение точных копий. Один способ&nbsp;&mdash; сбор государством "
"налогов на эти нужды и распределение денег между деятелями искусства "
"пропорционально кубическому корню популярности каждого (измеряемой опросами "
"населения). Другой&nbsp;&mdash; наличие в каждом проигрывателе кнопки &ldquo;"
"пожертвовать&rdquo;, чтобы анонимно посылать небольшие суммы (например, по "
"50&nbsp;центов для США) деятелям искусства, создавшим последнее из "
"проигранных произведений. Эти средства переводились бы деятелям искусства, а "
"не их издателям."

#. type: Content of: <div><p>
msgid ""
"People often wonder why I don't propose these methods for free software.  "
"There's a reason for that: it is hard to adapt them to works that are free."
msgstr ""
"Часто меня спрашивают, почему я не предлагаю эти методы для свободных "
"программ. На это есть причина: эти методы трудно приспособить к работам, "
"которые свободны."

#. type: Content of: <div><p>
msgid ""
"In my view, works designed to be used to do practical jobs must be free.  "
"The people who use them deserve to have control over the jobs they do, which "
"requires control over the works they use to do them, which requires <a href="
"\"/philosophy/free-sw.html\">the four freedoms</a>.  Works to do practical "
"jobs include educational resources, reference works, recipes, text fonts "
"and, of course, software; these works must be free."
msgstr ""
"По моим представлениям, произведения, составленные для решения практических "
"задач, должны быть свободны. Люди, применяющие их, заслуживают того, чтобы у "
"них был контроль над работой, которую они выполняют, что требует контроля "
"над произведениями, которыми они для этого пользуются, что требует <a href="
"\"/philosophy/free-sw.html\">четырех свобод</a>. К произведениям для решения "
"практических задач относятся образовательные материалы, справочные работы, "
"рецепты, шрифты и, конечно, программы; эти произведения должны быть свободны."

#. type: Content of: <div><p>
msgid ""
"That argument does not apply to works of opinion (such as this one) or art, "
"because they are not designed for the users to do practical jobs with.  "
"Thus, I don't believe those works must be free.  We must legalize sharing "
"them, and using pieces in remix to make totally different new works, but "
"that doesn't include in publishing modified versions of them.  It follows "
"that, for these works, we can tell who the authors are.  Each published work "
"can specify who its authors are, and changing that information can be "
"illegal."
msgstr ""
"Это рассуждение неприменимо к работам, выражающим мнение (таким, как эта) "
"или к произведениям искусства, потому что они не составлены для того, чтобы "
"с их помощью пользователи решали практические задачи. Таким образом, я не "
"убежден, что эти работы должны быть свободны. Мы должны легализовать обмен "
"ими и применение отрывков для создания совершенно новых произведений, но "
"сюда не входит публикация их измененных версий. Отсюда следует, что для этих "
"произведений мы можем назвать авторов. В каждом опубликованном произведении "
"могут быть указаны авторы, а изменение этих сведений может быть незаконным."

#. type: Content of: <div><p>
msgid ""
"That crucial point enables my proposed funding systems to work.  It means "
"that if you play a song and push the &ldquo;donate&rdquo; button, the system "
"can be sure who should get your donation.  Likewise, if you participate in "
"the survey that calculates popularities, the system will know who to credit "
"with a little more popularity because you listened to that song or made a "
"copy of it."
msgstr ""
"Благодаря этому жизненно важному моменту предложенные мной системы "
"финансирования становятся работоспособными. Это значит, что если вы "
"проигрываете песню и нажимаете кнопку &ldquo;внести пожертвование&rdquo;, "
"система может точно определить, кто должен получить ваше пожертвование. "
"Подобным же образом, если вы участвуете в опросе, в котором определяется "
"популярность, система будет знать, чью популярность нужно чуть-чуть "
"увеличить, когда вы прослушали или скопировали эту песню."

#. type: Content of: <div><p>
msgid ""
"When one song is made by multiple artists (for instance, several musicians "
"and a songwriter), that doesn't happen by accident.  They know they are "
"working together, and they can decide in advance how to divide up the "
"popularity that song later develops&mdash;or use the standard default rules "
"for this division.  This case creates no problem for those two funding "
"proposals because the work, once made, is not changed by others."
msgstr ""
"Когда над одной песней работает несколько человек (например, несколько "
"музыкантов и сочинитель), это происходит не случайно. Они знают, что они "
"работают вместе, и они могут заранее решить, как поделить популярность, "
"которую им впоследствии принесет эта песня&nbsp;&mdash; или воспользоваться "
"обычными стандартными правилами. Этот случай не создает проблемы для тех "
"двух предложений по финансированию, потому что однажды созданное "
"произведение не изменяется другими людьми."

#. type: Content of: <div><p>
msgid ""
"However, in a field of free works, one large work can have hundreds, even "
"thousands of authors.  There can be various versions with different, "
"overlapping sets of authors.  Moreover, the contributions of those authors "
"will differ in kind as well as in magnitude.  This makes it impossible to "
"divide the work's popularity among the contributors in a way that can be "
"justified as correct.  It's not just hard work; it's not merely complex.  "
"The problem raises philosophical questions that have no good answers."
msgstr ""
"Однако в области свободных произведений у одной крупной работы могут быть "
"сотни и даже тысячи авторов. Могут быть разные версии с различными "
"перекрывающимися множествами авторов. Более того, вклады этих авторов "
"различаются как по своему характеру, так и по величине. Из-за этого "
"становится невозможным разделить популярность работы между авторами так, "
"чтобы это можно было считать справедливым. Это не просто тяжелая работа; это "
"не просто сложно. При решении этой проблемы возникают философские вопросы, "
"на которые нет хороших ответов."

#. type: Content of: <div><p>
msgid ""
"Consider, for example, the free program GNU Emacs.  Our records of "
"contributions to the code of GNU Emacs are incomplete in the period before "
"we started using version control&mdash;before that we have only the change "
"logs.  But let's imagine we still had every version and could determine "
"precisely what code contribution is due to each of the hundreds of "
"contributors.  We'd still be stuck."
msgstr ""
"Рассмотрим, например, свободную программу GNU Emacs. Наши записи о вкладах в "
"исходный текст GNU Emacs имеют пробелы в период, предшествующий началу "
"применения нами системы контроля версий&nbsp;&mdash; до этого у нас были "
"только записи об изменениях. Но представим себе, что у нас все-таки была бы "
"каждая версия и мы могли бы точно определить, каков вклад в исходный текст "
"каждого из сотен соразработчиков. Мы все равно были бы в тупике."

#. type: Content of: <div><p>
msgid ""
"If we wanted to give credit in proportion to lines of code (or should it be "
"characters?), then it would be straightforward, once we decide how to handle "
"a line that was written by A and then changed by B.  But that assumes each "
"line as important as every other line.  I am sure that is wrong&mdash;some "
"pieces of the code do more important jobs and others less; some code is "
"harder to write and other code is easier.  But I see no way to quantify "
"these distinctions, and the developers could argue about them forever.  I "
"might deserve some additional credit for having initially written the "
"program, and certain others might deserve additional credit for having "
"initially written certain later important additions, but I see no objective "
"way to decide how much.  I can't propose a justifiable rule for dividing up "
"the popularity credit of a program like GNU Emacs."
msgstr ""
"Если бы мы хотели измерять заслуги пропорционально числу строк программы "
"(или это должны быть символы?), то это стало бы просто, как только мы решили "
"бы, что делать со строкой, которая была написана разработчиком А, а потом "
"изменена разработчиком Б. Но это предполагает, что каждая строка так же "
"важна, как любая другая. Я уверен, что это не так&nbsp;&mdash; некоторые "
"части текста выполняют более важные задачи, а другие&nbsp;&mdash; менее "
"важные; некоторые части писать труднее, а другие легче. Но я не вижу способа "
"дать количественную оценку этим различиям, и разработчики могли бы об этом "
"спорить до бесконечности. Я, возможно, заслуживаю некоторой дополнительной "
"благодарности за то, что написал первоначальную программу, а некоторые "
"другие, возможно, заслуживают дополнительной благодарности за то, что "
"написали после этого определенные важные дополнения, но я не вижу "
"объективного способа дать количественную оценку. Я не могу предложить "
"оправданного правила распределения популярности такой программы, как GNU "
"Emacs."

#. type: Content of: <div><p>
msgid ""
"As for asking all the contributors to negotiate an agreement, we can't even "
"try.  There have been hundreds of contributors, and we could not find them "
"all today.  They contributed across a span of 26 years, and never at any "
"time did all those people decide to work together."
msgstr ""
"Что касается того, чтобы попросить всех соразработчиков заключить "
"соглашение, то тут нечего даже и пытаться. Их были сотни, и сегодня мы не "
"смогли бы даже разыскать их всех. Они вносили вклад на протяжении 26&nbsp;"
"лет, и никогда не было момента, в который все эти люди решили бы работать "
"вместе."

#. type: Content of: <div><p>
msgid ""
"We might not even know the names of all the authors.  If some code was "
"donated by companies, we did not need to ask which persons wrote that code."
msgstr ""
"Мы, возможно, даже не знаем имен всех авторов. Если мы получали какой-то "
"текст в дар от компаний, нам не нужно было спрашивать, какие лица писали "
"этот текст."

#. type: Content of: <div><p>
msgid ""
"Then what about the forked or modified variants of GNU Emacs? Each one is an "
"additional case, equally complex but different.  How much of the credit for "
"such a variant should go to those who worked on that variant, and how much "
"to the original authors of the code they got from other GNU Emacs versions, "
"other programs, and so on?"
msgstr ""
"А потом, как быть с ответвлениями и измененными вариантами GNU Emacs? Каждый "
"представляет собой дополнительный случай, такой же сложный, но другой. "
"Сколько заслуг такого варианта должно переходить к тем, кто работал над этим "
"вариантом, а сколько первоначальным авторам программ, которые они взяли из "
"других версий GNU Emacs, из других программ и так далее? "

#. type: Content of: <div><p>
msgid ""
"The conclusion is that there is no way we could come up with a division of "
"the credit for GNU Emacs and justify it as anything but arbitrary.  But "
"Emacs is not a special case; it is a typical example.  The same problems "
"would arise for many important free programs, and other free works such as "
"Wikipedia pages."
msgstr ""
"Вывод таков: нет способа, которым мы могли бы разделить заслуги за GNU Emacs "
"и оправдать это чем-либо, кроме произвола. Но Emacs&nbsp;&mdash; не особый "
"случай; это типичный случай. Те же самые проблемы встали бы для многих "
"важных свободных программ и других свободных работ, таких как страницы "
"Википедии."

#. type: Content of: <div><p>
msgid ""
"These problems are the reasons I don't propose using those two funding "
"systems in fields such as software, encyclopedias or education, where all "
"works ought to be free."
msgstr ""
"По причине этих проблем я не предлагаю пользоваться этими двумя системами "
"финансирования в таких сферах, как программы, энциклопедии и образование, "
"где все произведения должны быть свободны."

#. type: Content of: <div><p>
msgid ""
"What makes sense for these areas is to ask people to donate to <em>projects</"
"em> for the work <em>they propose to do</em>.  That system is simple."
msgstr ""
"Что имеет смысл в этих областях&nbsp;&mdash; это просить людей вносить "
"пожертвования в <em>проекты</em> за работу, которую <em>предлагают "
"выполнить</em>. Эта система проста."

#. type: Content of: <div><p>
msgid ""
"The Free Software Foundation asks for donations in two ways.  We ask for <a "
"href=\"https://my.fsf.org/donate/\"> general donations to support the "
"foundation's work</a>, and we invite <a href=\"https://my.fsf.org/donate/"
"directed-donations\"> targeted donations for certain specific projects</a>.  "
"Other free software organizations do this too."
msgstr ""
"Фонд свободного программного обеспечения собирает пожертвования двумя "
"способами. Мы собираем <a href=\"https://my.fsf.org/donate/\">общие "
"пожертвования в поддержку работы фонда</a> и приглашаем вносить <a href="
"\"https://my.fsf.org/donate/directed-donations\"> целевые пожертвования на "
"конкретные проекты</a>. Так делают и другие организации по поддержке "
"свободных программ."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Пожалуйста, присылайте общие запросы фонду и GNU по адресу <a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Есть также <a href=\"/contact/"
"\">другие способы связаться</a> с фондом. Отчеты о неработающих ссылках и "
"другие поправки или предложения можно присылать по адресу <a href=\"mailto:"
"webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Мы старались сделать этот перевод точным и качественным, но исключить "
"возможность ошибки мы не можем. Присылайте, пожалуйста, свои замечания и "
"предложения по переводу по адресу <a href=\"mailto:web-translators@gnu.org"
"\">&lt;web-translators@gnu.org&gt;</a>. </p><p>Сведения по координации и "
"предложениям переводов наших статей см. в <a href=\"/server/standards/README."
"translations.html\">&ldquo;Руководстве по переводам&rdquo;</a>."

# type: Content of: <div><p>
#. type: Content of: <div><p>
msgid "Copyright &copy; 2013, 2021 Richard Stallman"
msgstr ""
"Copyright &copy; 2013, 2021 Richard Stallman<br />Copyright &copy; 2013, "
"2021 Free Software Foundation, Inc. (translation)"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Это произведение доступно по <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.ru\">лицензии Creative Commons "
"Attribution-NoDerivs (<em>Атрибуция&nbsp;&mdash; Без производных "
"произведений</em>) 4.0 Всемирная</a>."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<em>Внимание! В подготовке этого перевода участвовал только один человек. Вы "
"можете существенно улучшить перевод, если проверите его и расскажете о "
"найденных ошибках в <a href=\"http://savannah.gnu.org/projects/www-ru"
"\">русской группе переводов gnu.org</a>.</em>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Обновлено:"

# type: Content of: <div><p>
#~ msgid "Copyright &copy; 2013, 2017, 2021 Richard Stallman"
#~ msgstr ""
#~ "Copyright &copy; 2013, 2017, 2021 Richard Stallman<br />Copyright &copy; "
#~ "2013, 2017, 2021 Free Software Foundation, Inc. (translation)"

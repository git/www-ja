# Italian translation for why-audio-format-matters.html
# Copyright (C) 2008, 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnu.org article.
# Luca Padrin <luca@innurindi.net>, 2008.
# Revisioni di Paolo Melchiorre e Andrea Pescetti, 2008.
# Dora Scilipoti, 2021.
# Oct 2021: unfuzzify, clean up (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: why-audio-format-matters.html\n"
"POT-Creation-Date: 2021-09-22 08:26+0000\n"
"PO-Revision-Date: 2017-07-06 22:00+0100\n"
"Last-Translator: Andrea Pescetti <pescetti@gnu.org>\n"
"Language-Team: Italian\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: <title>
msgid "Why Audio Format Matters - GNU Project - Free Software Foundation"
msgstr ""
"Perché il formato audio è importante - Progetto GNU - Free Software "
"Foundation"

#. type: Content of: <div><h2>
msgid "Why Audio Format Matters"
msgstr "Perché il formato audio è importante"

#. type: Content of: <div><h3>
msgid "An invitation to audio producers to use Ogg Vorbis alongside MP3"
msgstr "Un invito a chi produce file audio ad affiancare Ogg Vorbis all'MP3"

#. type: Content of: <div><address>
msgid "by Karl Fogel"
msgstr "di Karl Fogel"

#. type: Content of: <div><div><p>
msgid ""
"The patents covering MP3 will reportedly all have expired by 2018, but "
"similar problems will continue to arise as long as patents are permitted to "
"restrict software development."
msgstr ""
"I brevetti sul formato MP3, a quanto pare, scadono tutti entro il 2018, ma "
"problemi del genere continueranno a verificarsi finché sarà possibile che i "
"brevetti restringano lo sviluppo del software."

#. type: Content of: <div><p>
msgid ""
"If you produce audio for general distribution, you probably spend 99.9% of "
"your time thinking about form, content, and production quality, and 0.1% "
"thinking about what audio format to distribute your recordings in."
msgstr ""
"Se producete file audio per la distribuzione pubblica, passerete "
"probabilmente il 99.9% del tempo a pensare alla forma, al contenuto ed alla "
"qualità della produzione, e lo 0.1% al formato audio da usare per "
"distribuire le vostre registrazioni."

#. type: Content of: <div><p>
msgid ""
"And in an ideal world, this would be fine.  Audio formats would be like the "
"conventions of laying out a book, or like pitches and other building-blocks "
"of music: containers of meaning, available for anyone to use, free of "
"restrictions.  You wouldn't have to worry about the consequences of "
"distributing your material in MP3 format, any more than you would worry "
"about putting a page number at the top of a page, or starting a book with a "
"table of contents."
msgstr ""
"Ed in un mondo ideale questo andrebbe bene. I formati audio dovrebbero "
"essere come le convenzioni per creare un libro, od i toni e gli altri "
"blocchi costitutivi della musica: contenitori del significato, disponibili "
"per l'uso da parte di tutti, liberi da restrizioni. Non dovreste "
"preoccuparvi delle conseguenze di distribuire materiale in formato MP3 più "
"di quanto fareste per mettere un numero di pagina in cima a quest'ultima, o "
"cominciare un libro con un indice."

#. type: Content of: <div><p>
msgid ""
"Unfortunately, that is not the world we live in.  MP3 is a patented format.  "
"What this means is that various companies have government-granted monopolies "
"over certain aspects of the MP3 standard, such that whenever someone creates "
"or listens to an MP3 file, <em>even with software not written by one of "
"those companies</em>, the companies have the right to decide whether or not "
"to permit that use of MP3.  Typically what they do is demand money, of "
"course.  But the terms are entirely up to them: they can forbid you from "
"using MP3 at all, if they want.  If you've been using MP3 files and didn't "
"know about this situation, then either a) someone else, usually a software "
"maker, has been paying the royalties for you, or b)  you've been unknowingly "
"infringing on patents, and in theory could be sued for it."
msgstr ""
"Sfortunatamente questo non è il mondo in cui viviamo. L'MP3 è un formato "
"brevettato. Ciò significa che varie aziende detengono monopoli legali su "
"alcuni aspetti dello standard MP3, così ogni volta che qualcuno crea od "
"ascolta un file MP3, <em>anche se con software non scritto da queste "
"aziende</em>, queste ultime hanno il diritto di decidere se permettere o no "
"quest'uso. Solitamente quello che fanno è chiedere soldi, come ci si può "
"aspettare. Ma la scelta sulle condizioni d'utilizzo è demandata interamente "
"a loro: possono proibirvi completamente di usare l'MP3 se vogliono. Se state "
"già usando questi file e non eravate a conoscenza di questa situazione, "
"allora o a) qualcun altro, di solito un produttore di software, ha pagato i "
"diritti per voi, o b) state violando senza saperlo i brevetti, ed in teoria "
"potreste essere perseguiti per questo."

#. type: Content of: <div><p>
msgid ""
"The harm here goes deeper than just the danger to you.  A software patent "
"grants one party the exclusive right to use a certain mathematical fact.  "
"This right can then be bought and sold, even litigated over like a piece of "
"property, and you can never predict what a new owner might do with it.  This "
"is not just an abstract possibility: MP3 patents have been the subject of "
"multiple lawsuits, with damages totalling more than a billion dollars."
msgstr ""
"Il danno, tuttavia, va ben oltre al rischio per voi. Il brevetto software "
"garantisce ad una parte il diritto esclusivo all'uso di una certa formula "
"matematica. Questo diritto può quindi essere acquistato o venduto, può "
"persino essere oggetto di contestazioni legali come un documento di "
"proprietà, e voi non potete mai prevedere ciò che un nuovo proprietario "
"potrebbe farci. Questa non è una possibilità solo teorica: i brevetti "
"sull'MP3 sono stati oggetto di numerose cause legali, con danni che "
"ammontano a più di un miliardo di dollari."

#. type: Content of: <div><p>
msgid ""
"The most important issue here is not about the fees, it's about the freedom "
"to communicate and to develop communications tools.  Distribution formats "
"such as MP3 are the containers of information exchange on the Internet.  "
"Imagine for a moment that someone had a patent on the modulated vibration of "
"air molecules: you would need a license just to hold a conversation or play "
"guitar for an audience.  Fortunately, our government has long held that old, "
"familiar methods of communication, like vibrating air molecules or writing "
"symbols on pieces of paper, are not patentable: no one can own them, they "
"are free for everyone to use.  But until those same liberties are extended "
"to newer, less familiar methods (like particular standards for representing "
"sounds via digital encoding), we who generate audio works must take care "
"what format we use&mdash;and require our listeners to use."
msgstr ""
"Il problema più importante non è il pagamento dei diritti, ma la libertà di "
"comunicare e sviluppare strumenti di comunicazione. I formati di "
"distribuzione come l'MP3 sono i contenitori dello scambio d'informazioni su "
"Internet. Pensate per un momento se qualcuno avesse un brevetto sulla "
"vibrazione modulata delle molecole d'aria: vi servirebbe una licenza solo "
"per tenere una conversazione o suonare la chitarra per un pubblico. "
"Fortunatamente il nostro governo da lungo tempo ritiene che questi vecchi e "
"familiari metodi di comunicazione, come la vibrazione delle molecole d'aria "
"o la scrittura di simboli su un pezzo di carta, non siano brevettabili: "
"nessuno può averne la proprietà, il loro uso è libero per tutti. Ma finché "
"queste stesse libertà non siano estese ai metodi nuovi e meno familiari "
"(come i particolari standard per rappresentare il suono con la codifica "
"digitale), noi che produciamo lavori audio dobbiamo fare attenzione al "
"formato che usiamo, e che richiediamo ai nostri ascoltatori di usare."

#. type: Content of: <div><h4>
msgid "A way out: Ogg Vorbis format"
msgstr "Una soluzione: il formato Ogg Vorbis"

#. type: Content of: <div><p>
msgid ""
"Ogg Vorbis is an alternative to MP3.  It gets high sound quality, can "
"compress down to a smaller size than MP3 while still sounding good (thus "
"saving you time and bandwidth costs), and best of all, is designed to be "
"completely free of patents."
msgstr ""
"Ogg Vorbis è un'alternativa all'MP3. Offre un'alta qualità del suono, può "
"comprimere fino ad una dimensione minore rispetto all'MP3 a parità di "
"qualità (facendo così risparmiare tempo e costi di banda), e, quel che più "
"conta, è progettato per essere completamente libero da brevetti."

#. type: Content of: <div><p>
msgid ""
"You won't sacrifice any technical quality by encoding your audio in Ogg "
"Vorbis.  The files sound fine, and most players know how to play them.  But "
"you will increase the total number of people who can listen to your tracks, "
"and at the same time help the push for patent-free standards in distribution "
"formats."
msgstr ""
"Codificando i vostri lavori in Ogg Vorbis, non dovrete fare alcuna rinuncia "
"sulla qualità tecnica. I file suoneranno bene, e verranno letti dalla "
"maggior parte dei lettori multimediali. Ma voi incrementerete il numero di "
"persone che potranno ascoltare le vostre tracce, ed allo stesso tempo "
"aiuterete a spingere per l'uso di standard liberi da brevetti nei formati di "
"distribuzione."

# TODO: the old translation was kept (just as good).
#. type: Content of: <div><div><p>
msgid ""
"More information <a href=\"https://xiph.org/about/\">about Xiph.org</a> (the "
"organization that created Ogg Vorbis) and the importance of free "
"distribution formats."
msgstr ""
"Sono disponibili <a href=\"http://xiph.org/about/\">ulteriori informazioni</"
"a> su Xiph.org (l'organizzazione che ha creato Ogg Vorbis) e sull'importanza "
"dei formati liberi di distribuzione."

#. type: Content of: <div><div><p>
msgid ""
"The Free Software Foundation has produced a user-friendly <a href=\"https://"
"www.fsf.org/campaigns/playogg/how\">guide to installing Ogg Vorbis support "
"in Microsoft Windows and Apple Mac OS X</a>."
msgstr ""
"La Free Software Foundation ha prodotto anche una <a href=\"https://www.fsf."
"org/campaigns/playogg/how\">guida semplice per installare il supporto ad Ogg "
"Vorbis in Microsoft Windows ed Apple Mac OS X</a>."

#. type: Content of: <div><p>
msgid ""
"The <a href=\"https://xiph.org/vorbis/\">Ogg Vorbis home page</a> has all "
"the information you need to both listen to and produce Vorbis-encoded "
"files.  The safest thing, for you and your listeners, would be to offer Ogg "
"Vorbis files exclusively.  But since there are still some players that can "
"only handle MP3, and you don't want to lose audience, a first step is to "
"offer both Ogg Vorbis and MP3, while explaining to your downloaders (perhaps "
"by linking to this article) exactly why you support Ogg Vorbis."
msgstr ""
"<a href=\"https://xiph.org/vorbis/\">L'home page di Ogg Vorbis</a> ha tutte "
"le informazioni che vi servono sia per ascoltare che per produrre file "
"codificati in questo formato. La cosa più sicura, per voi ed i vostri "
"ascoltatori, sarebbe offrire esclusivamente file Ogg Vorbis. Ma dato che ci "
"sono ancora alcuni lettori che riescono a gestire solo gli MP3, e non volete "
"perdere pubblico, un primo passo può essere quello di offrire sia Ogg Vorbis "
"che MP3, spiegando con precisione nello stesso tempo a chi scarica i vostri "
"file perché supportate l'Ogg Vorbis (magari con un collegamento a "
"quest'articolo)."

#. type: Content of: <div><p>
msgid ""
"And with Ogg Vorbis, you'll even <em>gain</em> some audience.  Here's how:"
msgstr ""
"Inoltre, con l'Ogg Vorbis <em>guadagnerete</em> altro pubblico. Ecco come."

#. type: Content of: <div><p>
msgid ""
"Up till now, the MP3 patent owners have been clever enough not to harass "
"individual users with demands for payment.  They know that would stimulate "
"popular awareness of (and eventually opposition to)  the patents.  Instead, "
"they go after the makers of products that implement the MP3 format.  The "
"victims of these shakedowns shrug wearily and pay up, viewing it as just "
"another cost of doing business, which is then passed on invisibly to users.  "
"However, not everyone is in a position to pay: some of your listeners use "
"free software programs to play audio files.  Because this software is freely "
"copied and downloaded, there is no practical way for either the authors or "
"the users to pay a patent fee&mdash;that is, to pay for the right to use the "
"mathematical facts that underly the MP3 format.  As a result, these programs "
"cannot legally implement MP3, even though the tracks the users want to "
"listen to may themselves be perfectly free! Because of this situation, some "
"distributors of the GNU/Linux computer operating system&mdash;which has "
"millions of users worldwide&mdash;have been unable to include MP3 players in "
"their software distributions."
msgstr ""
"Finora, i proprietari dei brevetti sugli MP3 sono stati abbastanza "
"intelligenti da non molestare i singoli utenti con richieste di pagamento. "
"Sanno che questo avrebbe stimolato una pubblica presa di coscienza (ed "
"infine un'opposizione) sui brevetti. Invece, si sono occupati degli "
"sviluppatori di prodotti che implementano il formato MP3. Le vittime di "
"queste estorsioni alzano stancamente le spalle e pagano, vedendolo solo come "
"un altro costo per l'attività economica, da trasferire quindi invisibilmente "
"agli utenti. Comunque, non tutti hanno la possibilità di pagare: alcuni dei "
"vostri ascoltatori usano software libero per leggere file musicali. Dato che "
"questo software è copiato o scaricato dalla rete liberamente, non c'è nessun "
"modo pratico per gli autori o gli utenti per pagare un diritto di brevetto, "
"cioè pagare il diritto all'uso delle formule matematiche sottostanti il "
"formato MP3. Come risultato, questi programmi non possono implementare "
"legalmente l'MP3, perfino se le tracce che l'utente vuol sentire sono per sé "
"stesse perfettamente libere! A causa di questa situazione, alcuni "
"distributori di sistemi operativi GNU/Linux, che ha milioni di utenti in "
"giro per il mondo, non possono includere lettori MP3 nelle loro "
"distribuzioni software."

#. type: Content of: <div><p>
msgid ""
"Luckily, you don't have to require such users to engage in civil "
"disobedience every time they want to listen to your works.  By offering Ogg "
"Vorbis, you ensure that no listeners have to get involved with a patented "
"distribution format unless they choose to, and that your audio works will "
"never be hampered by unforseen licensing requirements.  Eventually, the "
"growing acceptance of Ogg Vorbis as a standard, coupled with increasingly "
"unpredictable behavior by some of the MP3 patent holders, may make it "
"impractical to offer MP3 files at all.  But even before that day comes, Ogg "
"Vorbis remains the only portable, royalty-free audio format on the Internet, "
"and it's worth a little extra effort to support."
msgstr ""
"Fortunatamente, non dovete richiedere a questi utenti di impegnarsi in una "
"disobbedienza civile ogni volta che vogliono ascoltare i vostri lavori. "
"Offrendo file Ogg Vorbis, vi assicurate che nessun utente debba ricorrere ad "
"un formato brevettato a meno che non lo voglia, e che le vostre creazioni "
"non saranno mai ostacolate da requisiti di licenza non previsti. Prima o "
"poi, l'accettazione crescente di Ogg Vorbis come standard, unita al "
"comportamento via via sempre più imprevedibile di alcuni dei detentori di "
"brevetti sull'MP3, potrebbe rendere impraticabile del tutto l'offerta di "
"file in quest'ultimo formato. Ma fino a quel momento, Ogg Vorbis resta "
"l'unico formato portabile e libero da brevetti disponibile su Internet, e "
"vale la pena fare un piccolo sforzo supplementare per supportarlo."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Per informazioni su FSF e GNU rivolgetevi, possibilmente in inglese, a <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Ci sono anche <a href="
"\"/contact/\">altri modi di contattare</a> la FSF. Inviate segnalazioni di "
"link non funzionanti e altri suggerimenti relativi alle pagine web a <a href="
"\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Le traduzioni italiane sono effettuate ponendo la massima attenzione ai "
"dettagli e alla qualità, ma a volte potrebbero contenere imperfezioni. Se ne "
"riscontrate, inviate i vostri commenti e suggerimenti riguardo le traduzioni "
"a <a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;"
"</a> oppure contattate direttamente il <a href=\"http://savannah.gnu.org/"
"projects/www-it/\">gruppo dei traduttori italiani</a>.<br/>Per informazioni "
"su come gestire e inviare traduzioni delle nostre pagine web consultate la "
"<a href=\"/server/standards/README.translations.html\">Guida alle "
"traduzioni</a>."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2007 Karl Fogel"
msgstr "Copyright &copy; 2007 Karl Fogel"

#. type: Content of: <div><p>
msgid ""
"Verbatim copying and distribution of this entire article are permitted "
"worldwide, without royalty, in any medium, provided this notice, and the "
"copyright notice, are preserved."
msgstr ""
"La copia letterale e la distribuzione di questo articolo nella sua integrità "
"sono permesse con qualsiasi mezzo, a condizione che questa nota sia "
"riprodotta."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Tradotto da Luca Padrin; revisioni successive di Paolo Melchiorre, Andrea "
"Pescetti."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Ultimo aggiornamento:"

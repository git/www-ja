# Croatian translation of http://www.gnu.org/philosophy/compromise.html
# Copyright (C) 2013 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Marin Rameša <marin.ramesa@gmail.com>, 2013.
# T. Godefroy 2017, 2020, 2021 (partially unfuzzify).
#
msgid ""
msgstr ""
"Project-Id-Version: compromise.html\n"
"POT-Creation-Date: 2021-09-11 09:55+0000\n"
"PO-Revision-Date: 2013-05-06 21:44+0100\n"
"Last-Translator: Marin Rameša <marin.ramesa@gmail.com>\n"
"Language-Team: www-hr <www-hr-lista@gnu.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2014-04-05 00:02+0000\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Content of: <title>
msgid "Avoiding Ruinous Compromises - GNU Project - Free Software Foundation"
msgstr ""
"Izbjegavanje pogubnih kompromisa - GNU projekt - Zaklada za slobodan softver"

#. type: Content of: <div><h2>
msgid "Avoiding Ruinous Compromises"
msgstr "Izbjegavanje pogubnih kompromisa"

#. type: Content of: <div><address>
msgid "by Richard Stallman"
msgstr "napisao Richard Stallman"

#. type: Content of: <div><p>
# | [-&ldquo;Twenty-five-]{+Twenty-five+} years ago <a
# | href=\"/gnu/initial-announcement.html\">on September 27, 1983, I announced
# | a plan</a> to create a completely free operating system called
# | GNU&mdash;for &l[-s-]{+d+}quo;GNU's Not [-Unix&rsquo;.-] {+Unix.&rdquo;+}
# | As part of the 25th anniversary of the GNU system, I have written this
# | article on how our community can avoid ruinous compromises.  In addition
# | to avoiding such compromises, there are many ways you can <a
# | href=\"/help/help.html\">{+ +}help GNU</a> and free software.  One
# | [-basic-] way is to [-<a
# | href=\"https://www.fsf.org/associate/support_freedom/join_fsf?referrer=4052\">
# | join-] {+say no to+} the [-Free Software Foundation</a> as-] {+use of a
# | nonfree program or+} an [-Associate Member.&rdquo;&mdash;<b>Richard
# | Stallman</b>-] {+online disservice as often as you can or <a
# | href=\"/philosophy/saying-no-even-once.html\"> even once</a>.+}
#, fuzzy
#| msgid ""
#| "&ldquo;Twenty-five years ago <a href=\"/gnu/initial-announcement.html"
#| "\">on September 27, 1983, I announced a plan</a> to create a completely "
#| "free operating system called GNU&mdash;for &lsquo;GNU's Not Unix&rsquo;.  "
#| "As part of the 25th anniversary of the GNU system, I have written this "
#| "article on how our community can avoid ruinous compromises.  In addition "
#| "to avoiding such compromises, there are many ways you can <a href=\"/help/"
#| "help.html\">help GNU</a> and free software.  One basic way is to <a href="
#| "\"https://www.fsf.org/associate/support_freedom/join_fsf?referrer=4052\"> "
#| "join the Free Software Foundation</a> as an Associate Member.&rdquo;"
#| "&mdash;<b>Richard Stallman</b>"
msgid ""
"Twenty-five years ago <a href=\"/gnu/initial-announcement.html\">on "
"September 27, 1983, I announced a plan</a> to create a completely free "
"operating system called GNU&mdash;for &ldquo;GNU's Not Unix.&rdquo; As part "
"of the 25th anniversary of the GNU system, I have written this article on "
"how our community can avoid ruinous compromises.  In addition to avoiding "
"such compromises, there are many ways you can <a href=\"/help/help.html\"> "
"help GNU</a> and free software.  One way is to say no to the use of a "
"nonfree program or an online disservice as often as you can or <a href=\"/"
"philosophy/saying-no-even-once.html\"> even once</a>."
msgstr ""
"&ldquo;Prije dvadeset i pet godina <a href=\"/gnu/initial-announcement.html"
"\">27. rujna 1983., najavio sam plan</a> za stvaranje potpuno slobodnog "
"operativnog sustava zvanog GNU&mdash;za &lsquo;GNU Nije Unix&rsquo;. Kao dio "
"25. godišnjice GNU sustava, napisao sam ovaj članak o načinu na koji naša "
"zajednica može izbjeći pogubne kompromise. Dodatno izbjegavanju takvih "
"kompromisa, postoje mnogi načini na koje možete <a href=\"/help/help.html"
"\">pomoći GNU</a> i slobodnom softveru. Jedan osnovni način je da se <a href="
"\"https://www.fsf.org/associate/support_freedom/join_fsf?"
"referrer=4052\">pridružite Zakladi za slobodan softver</a> kao vanredan član."
"&rdquo;&mdash;<b>Richard Stallman</b>"

#. type: Content of: <div><p>
msgid ""
"The free software movement aims for a social change: <a href=\"/philosophy/"
"free-sw.html\">to make all software free</a> so that all software users are "
"free and can be part of a community of cooperation.  Every nonfree program "
"gives its developer unjust power over the users.  Our goal is to put an end "
"to that injustice."
msgstr ""
"Pokret slobodnog softvera cilja na društvenu promjenu: <a href=\"/philosophy/"
"free-sw.html\">napraviti sav softver slobodnim</a> tako da svi korisnici "
"softvera budu slobodni i da mogu biti dio zajednice temeljene na suradnji. "
"Svaki neslobodni program daje svome developeru nepravednu moć nad "
"korisnicima. Naš cilj je zaustaviti tu nepravdu."

#. type: Content of: <div><p>
msgid ""
"The road to freedom is <a href=\"https://www.fsf.org/bulletin/2008/spring/"
"the-last-mile-is-always-the-hardest/\"> a long road</a>.  It will take many "
"steps and many years to reach a world in which it is normal for software "
"users to have freedom.  Some of these steps are hard, and require "
"sacrifice.  Some of them become easier if we make compromises with people "
"that have different goals."
msgstr ""
"Put do slobode je <a href=\"https://www.fsf.org/bulletin/2008/spring/the-"
"last-mile-is-always-the-hardest/\">dugi put</a>. Biti će potrebno mnogo "
"koraka i mnogo godina da se dostigne svijet u kojem je normalno da korisnici "
"softvera imaju slobodu. Neki od tih koraka su teški, i zahtijevaju "
"žrtvovanje. Neki od njih postaju lakšima ako radimo kompromise sa ljudima "
"koji imaju drugačije ciljeve."

#. type: Attribute 'alt' of: <div><img>
msgid "&nbsp;[GPL Logo]&nbsp;"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Thus, the <a href=\"https://www.fsf.org/\">Free Software Foundation</a> "
"makes compromises&mdash;even major ones.  For instance, we made compromises "
"in the patent provisions of version 3 of the <a href=\"/licenses/gpl.html"
"\">GNU General Public License</a> (GNU GPL) so that major companies would "
"contribute to and distribute GPLv3-covered software and thus bring some "
"patents under the effect of these provisions."
msgstr ""
"Dakle, <a href=\"https://www.fsf.org/\">Zaklada za slobodan softver</a> radi "
"kompromise&mdash;čak i one velike. Na primjer, napravili smo kompromise u "
"patentnim odredbama inačice 3 <a href=\"/licenses/gpl.html\">GNU opće javne "
"licence</a> (GNU GPL) tako da bi velike kompanije pridonijele i "
"distribuirale softver pod GPLv3 i time dovele neke od patenata pod efekt tih "
"odredbi."

#. type: Attribute 'alt' of: <div><img>
msgid "&nbsp;[LGPL Logo]&nbsp;"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/licenses/lgpl.html\">The Lesser GPL</a>'s purpose is a "
"compromise: we use it on certain chosen free libraries to permit their use "
"in nonfree programs because we think that legally prohibiting this would "
"only drive developers to proprietary libraries instead.  We accept and "
"install code in GNU programs to make them work together with common nonfree "
"programs, and we document and publicize this in ways that encourage users of "
"the latter to install the former, but not vice versa.  We support specific "
"campaigns we agree with, even when we don't fully agree with the groups "
"behind them."
msgstr ""
"Svrha <a href=\"/licenses/lgpl.html\">nižeg GPL-a</a> je kompromis: "
"koristimo tu licencu na određenim izabranim slobodnim bibliotekama da bi "
"dozvolili njihovo korištenje u neslobodnim programima zato jer mislimo da bi "
"zakonska zabrana toga samo otjerala developere prema vlasničkim "
"(<i>proprietary</i>) bibliotekama. Prihvaćamo i instaliramo kod u GNU "
"programe da bi radili zajedno s čestim neslobodnim programima, i "
"dokumentiramo i objavljujemo to na način koji ohrabruje korisnike potonjih "
"da instaliraju prijašnje, ali ne obrnuto. Podržavamo određene kampanje s "
"kojima se slažemo, čak i kada se u potpunosti ne slažemo s grupama iza njih."

#. type: Content of: <div><p>
msgid ""
"But we reject certain compromises even though many others in our community "
"are willing to make them.  For instance, we <a href=\"/distros/free-system-"
"distribution-guidelines.html\"> endorse only the GNU/Linux distributions</a> "
"that have policies not to include nonfree software or lead users to install "
"it.  To endorse nonfree distributions would be a <abbr title=\"ruinous "
"(r&#363;'&#601;-n&#601;s) adj. 1. Causing or apt to cause ruin; "
"destructive.  2. Falling to ruin; dilapidated or decayed.\">ruinous</abbr> "
"compromise."
msgstr ""
"Ali odbijamo određene kompromise iako su ih mnogi drugi u našoj zajednici "
"voljni napraviti. Na primjer, <a href=\"/distros/free-system-distribution-"
"guidelines.html\">odobravamo samo GNU/Linux distribucije</a> koje imaju "
"politiku da ne uključuju neslobodni softver i da ne navode korisnike da ga "
"instaliraju. Odobravanje neslobodnih distribucija bi bio poguban (<i><abbr "
"title=\"ruinous (r&#363;'&#601;-n&#601;s) pridjev 1. Koji uzrokuje ili "
"pogoduje propasti; destruktivan.  2. Koji pada u propast; razoren ili "
"istrunuo.\">ruinous</abbr></i>) kompromis."

#. type: Content of: <div><p>
msgid ""
"Compromises are ruinous if they would work against our aims in the long "
"term.  That can occur either at the level of ideas or at the level of "
"actions."
msgstr ""
"Kompromisi su pogubni ako bi na duge staze radili protiv naših ciljeva. To "
"se može dogoditi ili na razini ideja ili na razini djelovanja. "

#. type: Content of: <div><p>
msgid ""
"At the level of ideas, ruinous compromises are those that reinforce the "
"premises we seek to change.  Our goal is a world in which software users are "
"free, but as yet most computer users do not even recognize freedom as an "
"issue.  They have taken up &ldquo;consumer&rdquo; values, which means they "
"judge any program only on practical characteristics such as price and "
"convenience."
msgstr ""
"Na razini ideja, pogubni kompromisi su oni koji pojačavaju premise koje "
"želimo promijeniti. Naš cilj je svijet u kojem su korisnici softvera "
"slobodni, ali do sada, većina korisnika računala čak i ne prepoznaju slobodu "
"kao pitanje. Oni su prihvatili &ldquo;potrošačke&rdquo; vrijednosti, što "
"znači da prosuđuju neki program samo praktičnim karakteristikama kao što su "
"cijena i pogodnost."

#. type: Content of: <div><p>
# | Dale Carnegie's classic self-help book, <cite>How to Win Friends and
# | Influence People</cite>, advises that the most effective way to persuade
# | someone to do something is to present arguments that appeal to [-his-]
# | {+per+} values.  There are ways we can appeal to the consumer values
# | typical in our society.  For instance, free software obtained gratis can
# | save the user money.  Many free programs are convenient and reliable, too.
# |  Citing those practical benefits has succeeded in persuading many users to
# | adopt various free programs, some of which are now quite successful.
#, fuzzy
#| msgid ""
#| "Dale Carnegie's classic self-help book, <cite>How to Win Friends and "
#| "Influence People</cite>, advises that the most effective way to persuade "
#| "someone to do something is to present arguments that appeal to his "
#| "values.  There are ways we can appeal to the consumer values typical in "
#| "our society.  For instance, free software obtained gratis can save the "
#| "user money.  Many free programs are convenient and reliable, too.  Citing "
#| "those practical benefits has succeeded in persuading many users to adopt "
#| "various free programs, some of which are now quite successful."
msgid ""
"Dale Carnegie's classic self-help book, <cite>How to Win Friends and "
"Influence People</cite>, advises that the most effective way to persuade "
"someone to do something is to present arguments that appeal to per values.  "
"There are ways we can appeal to the consumer values typical in our society.  "
"For instance, free software obtained gratis can save the user money.  Many "
"free programs are convenient and reliable, too.  Citing those practical "
"benefits has succeeded in persuading many users to adopt various free "
"programs, some of which are now quite successful."
msgstr ""
"Klasična knjiga samo-pomoći, čiji autor je Dale Carnegie: <cite>Kako "
"pridobiti prijatelje i utjecati na ljude</cite>, savjetuje da je "
"najefektniji način da uvjerite nekoga da napravi nešto je da prezentirate "
"argumente koji odgovaraju njegovim ili njezinim vrijednostima. Postoje "
"načini na koje možemo vršiti priziv na potrošačke vrijednosti tipične u "
"našem društvu. Na primjer, slobodan softver dobiven besplatno može uštedjeti "
"novac korisniku. Mnogi slobodni programi su isto tako prikladni i pouzdani. "
"Navođenje tih praktičnih koristi je bilo uspješno u uvjeravanju mnogih "
"korisnika da prihvate razne slobodne programe, neki od kojih su sada vrlo "
"uspješni."

#. type: Content of: <div><p>
msgid ""
"If getting more people to use some free programs is as far as you aim to go, "
"you might decide to keep quiet about the concept of freedom, and focus only "
"on the practical advantages that make sense in terms of consumer values.  "
"That's what the term &ldquo;open source&rdquo; and its associated rhetoric "
"do."
msgstr ""
"Ako je navođenje većeg broja ljudi na korištenje nekih slobodnih programa "
"vaš doseg i cilj, možete se odlučiti na šutnju o konceptu slobode, i "
"fokusirati se samo na praktične vrijednosti koje imaju smisla u terminima "
"potrošačkih vrijednosti. To je ono što termin &ldquo;otvoreni izvorni "
"kod&rdquo; i njegova asocirana retorika rade."

#. type: Content of: <div><p>
# | That approach can get us only part way to the goal of freedom.  People who
# | use free software only because it is convenient will stick with it only as
# | long as it is {+more+} convenient.  And they will see no reason not to use
# | convenient proprietary programs along with it.
#, fuzzy
#| msgid ""
#| "That approach can get us only part way to the goal of freedom.  People "
#| "who use free software only because it is convenient will stick with it "
#| "only as long as it is convenient.  And they will see no reason not to use "
#| "convenient proprietary programs along with it."
msgid ""
"That approach can get us only part way to the goal of freedom.  People who "
"use free software only because it is convenient will stick with it only as "
"long as it is more convenient.  And they will see no reason not to use "
"convenient proprietary programs along with it."
msgstr ""
"Taj pristup nas može provesti samo dijelom puta do cilja slobode. Ljudi koji "
"koriste slobodan softver samo zato jer je pogodan će ga se držati samo "
"utoliko dok je pogodan. I neće vidjeti razlog da ne koriste pogodne "
"vlasničke programe uz njega."

#. type: Content of: <div><p>
# | The philosophy of open source presupposes and appeals to consumer values,
# | and this affirms and reinforces them.  That's why we <a
# | href=\"/philosophy/open-source-misses-the-point.html\">do not [-support-]
# | {+advocate+} open source.</a>
#, fuzzy
#| msgid ""
#| "The philosophy of open source presupposes and appeals to consumer values, "
#| "and this affirms and reinforces them.  That's why we <a href=\"/"
#| "philosophy/open-source-misses-the-point.html\">do not support open source."
#| "</a>"
msgid ""
"The philosophy of open source presupposes and appeals to consumer values, "
"and this affirms and reinforces them.  That's why we <a href=\"/philosophy/"
"open-source-misses-the-point.html\">do not advocate open source.</a>"
msgstr ""
"Filozofija otvorenog izvornog koda pretpostavlja i radi priziv na potrošačke "
"vrijednosti, i to ih potvrđuje i pojačava. Zato mi <a href=\"/philosophy/"
"open-source-misses-the-point.html\">ne podržavamo otvoreni izvorni kod.</a>"

#. type: Attribute 'alt' of: <div><div><img>
msgid "[Levitating Gnu with a laptop]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"To establish a free community fully and lastingly, we need to do more than "
"get people to use some free software.  We need to spread the idea of judging "
"software (and other things) on &ldquo;citizen values,&rdquo; based on "
"whether it respects users' freedom and community, not just in terms of "
"convenience.  Then people will not fall into the trap of a proprietary "
"program baited by an attractive, convenient feature."
msgstr ""
"Da bi se osnovala slobodna zajednica potpuno i trajno, moramo napraviti više "
"od navođenja ljudi da koriste neki slobodan softver. Moramo raširiti ideju "
"prosuđivanja softvera (i drugih stvari) temeljem &ldquo;građanskih "
"vrijednosti&rdquo;, bazirano na tome da li softver poštuje korisničke "
"slobode i zajednicu, ne samo u terminima pogodnosti. Tada ljudi neće pasti u "
"zamku vlasničkoga programa namamljeni privlačnom, pogodnom značajkom."

#. type: Content of: <div><p>
msgid ""
"To promote citizen values, we have to talk about them and show how they are "
"the basis of our actions.  We must reject the Dale Carnegie compromise that "
"would influence their actions by endorsing their consumer values."
msgstr ""
"Da bi se promovirale građanske vrijednosti, moramo govoriti o njima i "
"pokazati kako su baza sa naše djelovanje. Moramo odbiti kompromis Dalea "
"Carnegiea koji bi utjecao na njihova djela odobravajući njihove potrošačke "
"vrijednosti."

#. type: Content of: <div><p>
msgid ""
"This is not to say we cannot cite practical advantage at all&mdash;we can "
"and we do.  It becomes a problem only when the practical advantage steals "
"the scene and pushes freedom into the background.  Therefore, when we cite "
"the practical advantages of free software, we reiterate frequently that "
"those are just <em>additional, secondary</em> reasons to prefer it."
msgstr ""
"To ne znači da ne možemo uopće navoditi praktične prednosti&mdash;možemo i "
"radimo to. Problem se pojavi samo kada praktična prednost stane u prvi plan "
"i gurne slobodu u pozadinu. Dakle, kada navodimo praktične prednosti "
"slobodnog softvera, često ponavljamo da su to samo <em>dodatni, sekundarni</"
"em> razlozi da se slobodan softver preferira. "

#. type: Content of: <div><p>
msgid ""
"It's not enough to make our words accord with our ideals; our actions have "
"to accord with them too.  So we must also avoid compromises that involve "
"doing or legitimizing the things we aim to stamp out."
msgstr ""
"Nije dovoljno da se naše riječi slažu s našim idealima; naše djelovanje se "
"mora isto tako slagati s njima. Prema tome moramo također izbjegavati "
"kompromise koji uključuju radnje ili legitimiziranje stvari koje ciljamo "
"suzbiti. "

#. type: Content of: <div><p>
msgid ""
"For instance, experience shows that you can attract some users to <a href=\"/"
"gnu/why-gnu-linux.html\">GNU/Linux</a> if you include some nonfree "
"programs.  This could mean a cute nonfree application that will catch some "
"user's eye, or a nonfree programming platform such as <a href=\"/philosophy/"
"java-trap.html\">Java</a> (formerly) or the Flash runtime (still), or a "
"nonfree device driver that enables support for certain hardware models."
msgstr ""
"Na primjer, iskustvo pokazuje da možete privući neke korisnike da koriste <a "
"href=\"/gnu/why-gnu-linux.html\">GNU/Linux</a> ako uključite neke neslobodne "
"programe. To može značiti neku slatku neslobodnu aplikaciju koja će uhvatiti "
"pažnju nekog korisnika, ili neslobodnu platformu za programiranje kao što je "
"<a href=\"/philosophy/java-trap.html\">Java</a> (ranije) ili softver za "
"izvođenje (<i>runtime</i>) Flash aplikacija (još uvijek), ili neslobodni "
"upravljački program koji omogućuje podršku za određene hardverske modele. "

#. type: Content of: <div><p>
msgid ""
"These compromises are tempting, but they undermine the goal.  If you "
"distribute nonfree software, or steer people towards it, you will find it "
"hard to say, &ldquo;Nonfree software is an injustice, a social problem, and "
"we must put an end to it.&rdquo; And even if you do continue to say those "
"words, your actions will undermine them."
msgstr ""
"Ti kompromisi su iskušenje, ali potkopavaju cilj. Ako distribuirate "
"neslobodni softver, ili navodite ljude prema njemu, biti će vam teško reći: "
"&ldquo;Neslobodni softver je nepravda, društveni problem i moramo ga "
"privesti kraju.&rdquo; Čak iako i nastavite govoriti te riječi, vaše "
"djelovanje će ih potkopati. "

#. type: Content of: <div><p>
msgid ""
"The issue here is not whether people should be <em>able</em> or <em>allowed</"
"em> to install nonfree software; a general-purpose system enables and allows "
"users to do whatever they wish.  The issue is whether we guide users towards "
"nonfree software.  What they do on their own is their responsibility; what "
"we do for them, and what we direct them towards, is ours.  We must not "
"direct the users towards proprietary software as if it were a solution, "
"because proprietary software is the problem."
msgstr ""
"Problem ovdje nije da li bi ljudi trebali biti <em>u mogućnosti</em> ili "
"<em>imati dopuštenje</em> instalirati neslobodni softver; sustav opće "
"namjene omogućuje i dopušta korisnicima da rade što žele. Problem je da li "
"vodimo korisnike prema neslobodnom softveru. Što oni rade sami po sebi je "
"njihova odgovornost; što mi radimo za njih i prema čemu ih usmjerujemo, je "
"naša. Ne smijemo usmjeravati korisnike prema vlasničkom softveru kao da je "
"vlasnički softver rješenje, jer vlasnički softver je problem. "

#. type: Content of: <div><p>
msgid ""
"A ruinous compromise is not just a bad influence on others.  It can distort "
"your own values, too, through cognitive dissonance.  If you have certain "
"values, but your actions imply other, conflicting values, you are likely to "
"change your values or your actions so as to resolve the contradiction.  "
"Thus, projects that argue only from practical advantages, or direct people "
"toward some nonfree software, nearly always shy away from even "
"<em>suggesting</em> that nonfree software is unethical.  For their "
"participants, as well as for the public, they reinforce consumer values.  We "
"must reject these compromises if we wish to keep our values straight."
msgstr ""
"Pogubni kompromis nije samo loš utjecaj na druge. Može isto tako i iskriviti "
"vaše vlastite vrijednosti, kroz kognitivnu disonancu. Ako imate određene "
"vrijednosti, ali vaša djela impliciraju neke druge vrijednosti koje su "
"suprotne, izvjesno je da ćete promijeniti vaše vrijednosti ili vaša djela da "
"biste razriješili proturječje. Dakle, projekti koji argumentiraju samo iz "
"praktičnih prednosti, ili usmjeravaju ljude prema neslobodnom softveru, "
"skoro uvijek se posramljuju od <em>predlaganja</em> da je neslobodni softver "
"ne-etičan. Za njegove sudionike, kao i za javnost, on pojačava potrošačke "
"vrijednosti. Moramo odbiti te kompromise ako želimo držati naše vrijednosti "
"ispravnima. "

#. type: Content of: <div><p>
msgid ""
"If you want to move to free software without compromising the goal of "
"freedom, look at <a href=\"https://www.fsf.org/resources\">the FSF's "
"resources area</a>.  It lists hardware and machine configurations that work "
"with free software, <a href=\"/distros/distros.html\"> totally free GNU/"
"Linux distros</a> to install, and <a href=\"https://directory.fsf.org/\"> "
"thousands of free software packages</a> that work in a 100 percent free "
"software environment.  If you want to help the community stay on the road to "
"freedom, one important way is to publicly uphold citizen values.  When "
"people are discussing what is good or bad, or what to do, cite the values of "
"freedom and community and argue from them."
msgstr ""
"Ako želite prijeći na korištenje slobodnog softvera bez kompromitiranja "
"cilja slobode, pogledajte <a href=\"https://www.fsf.org/resources\">FSF dio "
"o resursima</a>. Tamo su popisane hardverske i strojne konfiguracije koje "
"rade sa slobodnim softverom, <a href=\"/distros/distros.html\">potpuno "
"slobodne GNU/Linux distribucije</a> za instalaciju, i <a href=\"https://"
"directory.fsf.org/\">tisuće paketa slobodnog softvera</a> koji rade u sto-"
"postotnom okruženju slobodnog softvera. Ako želite pomoći da zajednica "
"ostane na putu do slobode, jedan važan način je da javno podržavate "
"građanske vrijednosti. Kada ljudi raspravljaju o tome što je dobro, a što "
"loše, ili što napraviti, navodite vrijednosti slobode i zajednice i "
"argumentirajte iz tih vrijednosti. "

#. type: Content of: <div><p>
msgid ""
"A road that lets you go faster is not better if it leads to the wrong "
"place.  Compromise is essential to achieve an ambitious goal, but beware of "
"compromises that lead away from the goal."
msgstr ""
"Put koji vam dopušta da idete brže nije bolji ako vodi na krivo mjesto. "
"Kompromis je esencijalan za postizanje ambicioznog cilja, ali čuvajte se "
"kompromisa koji vode dalje od cilja. "

#. type: Content of: <div><p>
# | For a similar point in a different area of life, see <a
# | [-href=\"https://www.guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-behaviour-change\">\"'Nudge'-]
# | {+href=\"https://www.guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-behaviour-change\">
# | &ldquo;Nudge&rdquo;+} is not [-enough\"</a>.-] {+enough</a>.+}
#, fuzzy
#| msgid ""
#| "For a similar point in a different area of life, see <a href=\"https://"
#| "www.guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-"
#| "behaviour-change\">\"'Nudge' is not enough\"</a>."
msgid ""
"For a similar point in a different area of life, see <a href=\"https://www."
"guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-behaviour-change"
"\"> &ldquo;Nudge&rdquo; is not enough</a>."
msgstr ""
"Za sličan zaključak u drugačijem području života, pogledajte <a href="
"\"https://www.guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-"
"behaviour-change\">'Gurkanje' nije dovoljno</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Molim vas šaljite općenite FSF &amp; GNU upite na <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Postoje isto i <a href=\"/contact/\">drugi "
"načini kontaktiranja</a> FSF-a. Prekinute poveznice i drugi ispravci ili "
"prijedlozi mogu biti poslani na <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
# | Please see the <a
# | href=\"/server/standards/README.translations.html\">Translations
# | README</a> for information on coordinating and [-submitting-]
# | {+contributing+} translations of this article.
#, fuzzy
#| msgid ""
#| "Please see the <a href=\"/server/standards/README.translations.html"
#| "\">Translations README</a> for information on coordinating and submitting "
#| "translations of this article."
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Radimo naporno i dajemo sve od sebe da bi pružili točne, visoko kvalitetne "
"prijevode. Međutim, nismo oslobođeni od nesavršenosti. Molim vas šaljite "
"vaše komentare i općenite prijedloge u tom smislu na <a href=\"mailto:web-"
"translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>.</p> <p>Za "
"informacije o koordiniranju i slanju prijevoda naših mrežnih stranica, "
"pogledajte <a href=\"/server/standards/README.translations.html\">README za "
"prijevode</a>."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2008, 2021 Richard Stallman"
msgstr "Copyright &copy; 2008, 2021 Richard Stallman"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Ovo djelo je dano na korištenje pod licencom <a rel=\"license\" href="
"\"http://creativecommons.org/licenses/by-nd/4.0/deed.hr\">Creative Commons "
"Imenovanje-Bez prerada 4.0 međunarodna</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr "<b>Prijevod</b>: Marin Rameša, 2013."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Zadnji put promijenjeno:"

#~ msgid ""
#~ "Please see the <a href=\"/server/standards/README.translations.html"
#~ "\">Translations README</a> for information on coordinating and submitting "
#~ "translations of this article."
#~ msgstr ""
#~ "Radimo naporno i dajemo sve od sebe da bi pružili točne, visoko "
#~ "kvalitetne prijevode. Međutim, nismo oslobođeni od nesavršenosti. Molim "
#~ "vas šaljite vaše komentare i općenite prijedloge u tom smislu na <a href="
#~ "\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>.</"
#~ "p> <p>Za informacije o koordiniranju i slanju prijevoda naših mrežnih "
#~ "stranica, pogledajte <a href=\"/server/standards/README.translations.html"
#~ "\">README za prijevode</a>."

#, fuzzy
#~| msgid ""
#~| "Copyright &copy; 2008, 2009, 2014, 2015, 2017 <a href=\"http://www."
#~| "stallman.org/\">Richard Stallman</a>."
#~ msgid ""
#~ "Copyright &copy; 2008, 2009, 2014, 2015, 2017, 2018, 2019, 2020 <a href="
#~ "\"http://www.stallman.org/\">Richard Stallman</a>."
#~ msgstr ""
#~ "Copyright &copy; 2008, 2009, 2014, 2015, 2017 <a href=\"http://www."
#~ "stallman.org/\">Richard Stallman</a>."

#~ msgid "by <strong>Richard Stallman</strong>"
#~ msgstr "<strong>Richard Stallman</strong>"

#~ msgid ""
#~ "Please send FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org\">&lt;"
#~ "gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways to "
#~ "contact</a> the FSF."
#~ msgstr ""
#~ "Molim vas šaljite općenite FSF &amp; GNU upite na <a href=\"mailto:"
#~ "gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Postoje isto i <a href=\"/contact/"
#~ "\">drugi načini kontaktiranja</a> FSF-a. "

#~ msgid ""
#~ "Please send broken links and other corrections or suggestions to <a href="
#~ "\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
#~ msgstr ""
#~ "Prekinute poveznice i drugi ispravci ili prijedlozi mogu biti poslani na "
#~ "<a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>. "

#~ msgid ""
#~ "Richard Stallman is the founder of the Free Software Foundation.  You can "
#~ "copy and redistribute this article under the <a rel=\"license\" href="
#~ "\"http://creativecommons.org/licenses/by-nd/3.0/us/\">Creative Commons "
#~ "Attribution Noderivs 3.0 license</a>."
#~ msgstr ""
#~ "Richard Stallman je osnivač Zaklade za slobodan softver. Možete kopirati "
#~ "i ponovno distribuirati ovaj članak pod <a rel=\"license\" href=\"http://"
#~ "creativecommons.org/licenses/by-nd/3.0/us/deed.hr\"> Creative Commons "
#~ "Imenovanje-Bez prerada 3.0 SAD</a>."

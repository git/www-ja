# Spanish translation of http://www.gnu.org/philosophy/pirate-party.html
# Copyright (C) 2009-2013, 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnu.org article.
# Rafa Pereira <rptv2003@yahoo.com>, 2009.
# Xavier Reina <xreina@fsfe.org>, 2010, 2011.
# Dora Scilipoti <dora AT gnu DOT org>, 2012, 2013.
# Javier Fdez. Retenaga <jfrtnaga@gnu.org>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: pirate-party.html\n"
"POT-Creation-Date: 2021-09-19 16:26+0000\n"
"PO-Revision-Date: 2021-09-20 12:03+0200\n"
"Last-Translator: Javier Fdez. Retenaga <jfrtnaga@gnu.org>\n"
"Language-Team: Spanish <www-es-general@gnu.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

# type: Content of: <h2>
#. type: Content of: <title>
msgid ""
"How the Swedish Pirate Party Platform Backfires on Free Software - GNU "
"Project - Free Software Foundation"
msgstr ""
"Las propuestas del Partido Pirata sueco y el software libre: el tiro por la "
"culata - Proyecto GNU - Free Software Foundation"

# type: Content of: <h2>
#. type: Content of: <div><h2>
msgid "How the Swedish Pirate Party Platform Backfires on Free Software"
msgstr ""
"Las propuestas del Partido Pirata sueco y el software libre: el tiro por la "
"culata"

# type: Content of: <p>
#. type: Content of: <div><address>
msgid "by <a href=\"https://www.stallman.org/\">Richard Stallman</a>"
msgstr "por <a href=\"https://www.stallman.org/\">Richard Stallman</a>"

#. type: Content of: <div><div><p>
msgid ""
"Note: each Pirate Party has its own platform.  They all call for reducing "
"copyright power, but the specifics vary.  This issue may not apply to the "
"other parties' positions."
msgstr ""
"Nota: cada Partido Pirata tiene su propia plataforma. Todos solicitan la "
"reducción de los poderes del copyright, pero cambian los detalles. Las "
"cuestiones que aquí se discuten podrían no aplicarse a la posición de otros "
"partidos pirata."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"The bullying of the copyright industry in Sweden inspired the launch of the "
"first political party whose platform is to reduce copyright restrictions: "
"the Pirate Party.  Its platform includes the prohibition of Digital "
"Restrictions Management, legalization of noncommercial sharing of published "
"works, and shortening of copyright for commercial use to a five-year "
"period.  Five years after publication, any published work would go into the "
"public domain."
msgstr ""
"El comportamiento abusivo de la industria del copyright en Suecia inspiró el "
"lanzamiento del primer partido político cuyo ideario es la reducción de las "
"restricciones del copyright: el Partido Pirata. Su programa incluye la "
"prohibición de la gestión digital de restricciones [DRM por sus siglas en "
"inglés, <cite>Digital Restrictions Management</cite>], la legalización de la "
"compartición no comercial de obras publicadas, y la reducción del copyright "
"para uso comercial a un periodo de cinco años. Cinco años después de su "
"publicación, toda obra pasaría al dominio público."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"I support these changes, in general; but the specific combination chosen by "
"the Swedish Pirate Party backfires ironically in the special case of free "
"software.  I'm sure that they did not intend to hurt free software, but "
"that's what would happen."
msgstr ""
"En términos generales yo secundo estos cambios, pero la combinación "
"específica elegida por el Partido Pirata sueco, irónicamente, consigue el "
"efecto contrario al deseado en el caso particular del software libre. Estoy "
"seguro de que no pretendían perjudicar al software libre, pero eso es lo que "
"ocurriría."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"The GNU General Public License and other copyleft licenses use copyright law "
"to defend freedom for every user.  The GPL permits everyone to publish "
"modified works, but only under the same license.  Redistribution of the "
"unmodified work must also preserve the license.  And all redistributors must "
"give users access to the software's source code."
msgstr ""
"La Licencia Pública General de GNU [GPL por sus siglas en inglés, "
"<cite>General Public License</cite>] y otras licencias de copyleft usan la "
"ley de copyright para defender la libertad de todos los usuarios. La GPL "
"permite a todo el mundo publicar obras modificadas, pero sólo bajo la misma "
"licencia. También la redistribución de la obra sin modificar debe mantener "
"la licencia. Y todo el que redistribuye debe dar a los usuarios acceso al "
"código fuente del software."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"How would the Swedish Pirate Party's platform affect copylefted free "
"software? After five years, its source code would go into the public domain, "
"and proprietary software developers would be able to include it in their "
"programs.  But what about the reverse case?"
msgstr ""
"¿Cómo afectaría el programa del Partido Pirata sueco al software libre "
"distribuido bajo copyleft? Pasados cinco años, el código fuente pasaría al "
"dominio público, y quienes desarrollan software privativo podrían incluirlo "
"en sus programas. Pero, ¿y el caso inverso?"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Proprietary software is restricted by EULAs, not just by copyright, and the "
"users don't have the source code.  Even if copyright permits noncommercial "
"sharing, the EULA may forbid it.  In addition, the users, not having the "
"source code, do not control what the program does when they run it.  To run "
"such a program is to surrender your freedom and give the developer control "
"over you."
msgstr ""
"Los <cite>EULA</cite><sup><a href=\"#TransNote1\" id=\"IniTransNote1\">1</"
"a></sup>, y no sólo el copyright, restringen el uso del software privativo, "
"y los usuarios no disponen del código fuente. Aún en el caso de que el "
"copyright permita la compartición no comercial, el <cite>EULA</cite> puede "
"prohibirla. Además, los usuarios, al no tener el código fuente, no tienen "
"control sobre lo que hace el programa cuando lo ejecutan. Ejecutar un "
"programa en estas condiciones es renunciar a su libertad y dar el control "
"sobre usted al programador."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"So what would be the effect of terminating this program's copyright after 5 "
"years? This would not require the developer to release source code, and "
"presumably most will never do so.  Users, still denied the source code, "
"would still be unable to use the program in freedom.  The program could even "
"have a &ldquo;time bomb&rdquo; in it to make it stop working after 5 years, "
"in which case the &ldquo;public domain&rdquo; copies would not run at all."
msgstr ""
"Entonces, ¿cuál sería el efecto de la finalización del copyright de este "
"programa pasados cinco años? Este hecho no requeriría que el autor del "
"programa publicara el código fuente y, presumiblemente, la mayoría nunca lo "
"hará. Los usuarios, a los que todavía se les denegaría el acceso al código "
"fuente, seguirían sin poder utilizar el programa en libertad. El programa "
"podría incluso contener una «bomba de relojería» que hiciera que dejara de "
"funcionar transcurridos cinco años, en cuyo caso las copias en el «dominio "
"público» no funcionarían."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Thus, the Pirate Party's proposal would give proprietary software developers "
"the use of GPL-covered source code after 5 years, but it would not give free "
"software developers the use of proprietary source code, not after 5 years or "
"even 50 years.  The Free World would get the bad, but not the good.  The "
"difference between source code and object code and the practice of using "
"EULAs would give proprietary software an effective exception from the "
"general rule of 5-year copyright&mdash;one that free software does not share."
msgstr ""
"En consecuencia, la propuesta del Partido Pirata concedería a quienes "
"desarrollan software privativo la posibilidad de usar el código fuente "
"cubierto por la GPL pasados cinco años, pero no permitiría a quienes "
"desarrollan software libre el uso del código fuente privativo, ni pasados "
"cinco años ni tampoco cincuenta.  El Mundo Libre obtendría la parte mala, "
"pero no la buena. La diferencia entre código fuente y código objeto, y la "
"práctica del uso de los <cite>EULA</cite> concedería  al software privativo "
"una excepción efectiva a la regla general del copyright de cinco años, "
"excepción no compartida por el software libre."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"We also use copyright to partially deflect the danger of software patents.  "
"We cannot make our programs safe from them&mdash;no program is ever safe "
"from software patents in a country which allows them&mdash;but at least we "
"prevent them from being used to make the program effectively nonfree.  The "
"Swedish Pirate Party proposes to abolish software patents, and if that is "
"done, this issue would go away.  But until that is achieved, we must not "
"lose our only defense for protection from patents."
msgstr ""
"También usamos el copyright para evitar parcialmente el peligro de las "
"patentes de software. No podemos hacer que nuestros programas estén "
"completamente a salvo de ellas &mdash;ningún programa está nunca a salvo de "
"las patentes de software en un país que las permite&mdash; pero por lo menos "
"impedimos que sean usadas para convertir el programa en un programa "
"privativo a efectos prácticos. El Partido Pirata sueco propone la abolición "
"de las patentes de software, lo cual, de llevarse a cabo, haría desaparecer "
"este problema. Pero hasta que eso se logre, no debemos perder nuestra única "
"defensa contra las patentes."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Once the Swedish Pirate Party had announced its platform, free software "
"developers noticed this effect and began proposing a special rule for free "
"software: to make copyright last longer for free software, so that it can "
"continue to be copylefted.  This explicit exception for free software would "
"counterbalance the effective exception for proprietary software.  Even ten "
"years ought to be enough, I think.  However, the proposal met with "
"resistance from the Pirate Party's leaders, who objected to the idea of a "
"longer copyright for a special case."
msgstr ""
"Una vez que el Partido Pirata sueco hizo público su programa, los "
"programadores de software libre se dieron cuenta de este efecto y "
"propusieron una regla especial para el software libre: hacer que el "
"copyright durase más en el caso de software libre, de forma que continuara "
"cubierto por el copyleft. Esta excepción explícita para el software libre "
"compensaría la excepción efectiva favorable al software privativo. Incluso "
"diez años serían suficientes, en mi opinión. Sin embargo, la propuesta "
"encontró resistencia por parte de los líderes del Partido Pirata, quienes "
"pusieron objeciones a la idea de un copyright más duradero para un caso en "
"particular."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"I could support a law that would make GPL-covered software's source code "
"available in the public domain after 5 years, provided it has the same "
"effect on proprietary software's source code.  After all, copyleft is a "
"means to an end (users' freedom), not an end in itself.  And I'd rather not "
"be an advocate for a stronger copyright."
msgstr ""
"Yo podría respaldar una ley que hiciera que el código fuente cubierto por la "
"GPL quedara disponible en el dominio público tras cinco años, siempre que "
"afectara de la misma forma al código fuente del software privativo. Después "
"de todo, el copyleft es un medio para un fin (la libertad de los usuarios), "
"no un fin en sí mismo. Y preferiría no ser un defensor de un copyright más "
"fuerte."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"So I proposed that the Pirate Party platform require proprietary software's "
"source code to be put in escrow when the binaries are released.  The "
"escrowed source code would then be released in the public domain after 5 "
"years.  Rather than making free software an official exception to the 5-year "
"copyright rule, this would eliminate proprietary software's unofficial "
"exception.  Either way, the result is fair."
msgstr ""
"Así que propuse que el programa del Partido Pirata incluyera la obligación "
"de poner bajo custodia el código fuente del software privativo cuando se "
"publicaran los binarios. Entonces, el código fuente mantenido bajo custodia "
"sería puesto en el dominio público transcurridos cinco años. En lugar de "
"hacer del software libre una excepción oficial a la regla del copyright de "
"cinco años, esto eliminaría la excepción no oficial favorable al software "
"privativo. En ambos casos, el resultado es justo."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"A Pirate Party supporter proposed a more general variant of the first "
"suggestion: a general scheme to make copyright last longer as the public is "
"granted more freedoms in using the work.  The advantage of this is that free "
"software becomes part of a general pattern of varying copyright term, rather "
"than a lone exception."
msgstr ""
"Un simpatizante del Partido Pirata propuso una variante más general de la "
"primera sugerencia: un esquema general que haga que el copyright dure más en "
"la medida en que conceda al público más libertad en el uso de la obra. La "
"ventaja de esto es que el software libre pasa a ser parte de un patrón "
"general de duración variable del copyright, en lugar de ser una excepción "
"singular."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"I'd prefer the escrow solution, but any of these methods would avoid a "
"prejudicial effect specifically against free software.  There may be other "
"solutions that would also do the job.  One way or another, the Pirate Party "
"of Sweden should avoid placing a handicap on a movement to defend the public "
"from marauding giants."
msgstr ""
"Yo preferiría la solución de la puesta bajo custodia de los archivos fuente, "
"pero cualquiera de estos métodos evitaría un efecto perjudicial específico "
"para el software libre. Puede haber también otras soluciones. De una forma u "
"otra, el Partido Pirata sueco debería evitar poner en desventaja a un "
"movimiento que defiende al público de los gigantes que le acechan."

# type: Content of: <div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""
"<b>Notas del traductor</b>:\n"
"<ol>\n"
"<li id=\"TransNote1\">Acuerdo de licencia con el usuario final, <cite>EULA</"
"cite> por sus iniciales en inglés: <cite>End-User License Agreement</cite>. "
"<a href=\"#IniTransNote1\">&#8593;</a>.</li>\n"
"</ol>"

# type: Content of: <div><p>
#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envíe sus consultas acerca de la FSF y GNU a <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>. Existen también <a href=\"/contact/\">otros "
"medios para contactar</a> con la FSF. <br /> Para avisar de enlaces rotos y "
"proponer otras correcciones o sugerencias, diríjase a <a href=\"mailto:"
"webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"El equipo de traductores al español se esfuerza por ofrecer traducciones "
"fieles al original y de buena calidad, pero no estamos libres de cometer "
"errores.<br /> Envíe sus comentarios y sugerencias sobre las traducciones a "
"<a  href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte la <a href=\"/server/standards/README.translations.html"
"\">Guía para las traducciones</a> para obtener información sobre la "
"coordinación y el envío de traducciones de las páginas de este sitio web."

# type: Content of: <div><p>
#. type: Content of: <div><p>
msgid "Copyright &copy; 2009, 2012, 2021 Richard Stallman"
msgstr "Copyright &copy; 2009, 2012, 2021 Richard Stallman"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Esta página está bajo licencia <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.es_ES\">Creative Commons "
"Reconocimiento-SinObraDerivada 4.0 Internacional</a>."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr "<strong>Traducción: Rafa Pereira</strong>, 2009."

# type: Content of: <div><p>
#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última actualización:"

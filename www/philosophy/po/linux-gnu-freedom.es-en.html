<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="TAGS" value="essays aboutfs free-open" -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>Linux, GNU, and Freedom
- GNU Project - Free Software Foundation</title>
<meta http-equiv="Keywords"
      content="GNU, FSF, Free Software Foundation, Linux, freedom, software, power, rights, Richard Stallman, rms, SIGLINUX, Joe Barr" />
<meta http-equiv="Description" content="In this essay, Linux, GNU, and freedom, Richard M. Stallman responds to Joe Barr's account of the FSF's dealings with the Austin Linux users group." />
<!--#include virtual="/philosophy/po/linux-gnu-freedom.translist" -->
<!--#include virtual="/server/banner.html" -->
<!--#include virtual="/philosophy/ph-breadcrumb.html" -->
<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->
<div class="article reduced-width">
<h2>Linux, GNU, and Freedom</h2>

<address class="byline">by Richard M. Stallman</address>

<p>
  Since <a
  href="https://web.archive.org/web/20190404115541/http://linux.sys-con.com/node/32755">Joe Barr's
  article</a> criticized my dealings with SIGLINUX, I would like to
  set the record straight about what actually occurred, and state my
  reasons.</p>
<p>
  When SIGLINUX invited me to speak, it was a &ldquo;Linux User
  Group&rdquo;; that is, a group for users of the GNU/Linux system
  which calls the whole system &ldquo;Linux.&rdquo;  So I replied
  politely that if they'd like someone from the GNU Project to give a
  speech for them, they ought to treat the GNU Project right, and call
  the system &ldquo;GNU/Linux.&rdquo;  The system is a variant of GNU,
  and the GNU Project is its principal developer, so social convention
  says to call it by the name we chose.  Unless there are powerful
  reasons for an exception, I usually decline to give speeches for
  organizations that won't give GNU proper credit in this way.  I
  respect their freedom of speech, but I also have the freedom not to
  give a speech.</p>
<p>
  Subsequently, Jeff Strunk of SIGLINUX tried to change the group's
  policy, and asked the FSF to list his group in our page of GNU/Linux
  user groups.  Our webmaster told him that we would not list it under
  the name &ldquo;SIGLINUX&rdquo; because that name implies that the
  group is about Linux.  Strunk proposed to change the name to
  &ldquo;SIGFREE,&rdquo; and our webmaster agreed that would be fine.
  (Barr's article said we rejected this proposal.)  However, the group
  ultimately decided to stay with &ldquo;SIGLINUX.&rdquo;</p>
<p>
  At that point, the matter came to my attention again, and I
  suggested they consider other possible names.  There are many names
  they could choose that would not call the system
  &ldquo;Linux,&rdquo; and I hope they will come up with one they
  like.  There the matter rests as far as I know.</p>
<p>
  Is it true, as Barr writes, that some people see these actions as an
  &ldquo;application of force&rdquo; comparable with Microsoft's
  monopoly power?  Probably so.  Declining an invitation is not
  coercion, but people who are determined to believe that the entire
  system is &ldquo;Linux&rdquo; sometimes develop amazingly distorted
  vision.  To make that name appear justified, they must see molehills
  as mountains and mountains as molehills.  If you can ignore the
  facts and believe that Linus Torvalds developed the whole system
  starting in 1991, or if you can ignore your ordinary principles of
  fairness and believe that Torvalds should get the sole credit even
  though he didn't do that, it's a small step to believe that I owe
  you a speech when you ask.</p>
<p>
  Just consider: the GNU Project starts developing an operating
  system, and years later Linus Torvalds adds one important piece.
  The GNU Project says, &ldquo;Please give our project equal
  mention,&rdquo; but Linus says, &ldquo;Don't give them a share of
  the credit; call the whole thing after my name alone!&rdquo; Now
  envision the mindset of a person who can look at these events and
  accuse the GNU Project of egotism.  It takes strong prejudice to
  misjudge so drastically.</p>
<p>
  A person who is that prejudiced can say all sorts of unfair things
  about the GNU Project and think them justified; his fellows will
  support him, because they want each other's support in maintaining
  their prejudice.  Dissenters can be reviled; thus, if I decline to
  participate in an activity under the rubric of &ldquo;Linux,&rdquo;
  they may find that inexcusable, and hold me responsible for the ill
  will they feel afterwards.  When so many people want me to call the
  system &ldquo;Linux,&rdquo; how can I, who merely launched its
  development, not comply?  And forcibly denying them a speech is
  forcibly making them unhappy.  That's coercion, as bad as
  Microsoft!</p>
<p>
  Now, you might wonder why I don't just duck the issue and avoid all
  this grief.  When SIGLINUX invited me to speak, I could simply have
  said &ldquo;No, sorry&rdquo; and the matter would have ended there.
  Why didn't I do that?  I'm willing to take the risk of being abused
  personally in order to have a chance of correcting the error that
  undercuts the GNU Project's efforts.</p>
<p>
  Calling this variant of the GNU system &ldquo;Linux&rdquo; plays
  into the hands of people who choose their software based only on
  technical advantage, not caring whether it respects their freedom.
  There are people like Barr, that want their software &ldquo;free
  from ideology&rdquo; and criticize anyone that says freedom matters.
  There are people like Torvalds that will pressure our community into
  use of a nonfree program, and challenge anyone who complains to
  provide a (technically) better program immediately or shut up.
  There are people who say that technical decisions should not be
  &ldquo;politicized&rdquo; by consideration of their social
  consequences.</p>
<p>
  In the 70s, computer users lost the freedoms to redistribute and
  change software because they didn't value their freedom.  Computer
  users regained these freedoms in the 80s and 90s because a group of
  idealists, the GNU Project, believed that freedom is what makes a
  program better, and were willing to work for what we believed in.</p>
<p>
  We have partial freedom today, but our freedom is not secure.  It is
  threatened by the <abbr title="Consumer Broadband and Digital
  Television Promotion Act">CBDTPA</abbr>
  (formerly <abbr title="Security Systems Standards and Certification Act">SSSCA</abbr>),
  by the Broadcast &ldquo;Protection&rdquo; Discussion Group
  (see <a href="https://www.eff.org/">www.eff.org</a>) which
  proposes to prohibit free software to access digital TV broadcasts,
  by software patents (Europe is now considering whether to have
  software patents), by Microsoft nondisclosure agreements for vital
  protocols, and by everyone who tempts us with a nonfree program
  that is &ldquo;better&rdquo; (technically) than available free
  programs.  We can lose our freedom again just as we lost it the
  first time, if we don't care enough to protect it.</p>
<p>
  Will enough of us care?  That depends on many things; among them,
  how much influence the GNU Project has, and how much influence Linus
  Torvalds has.  The GNU Project says, &ldquo;Value your
  freedom!&rdquo;  Joe Barr says, &ldquo;Choose between nonfree and
  free programs on technical grounds alone!&rdquo;  If people credit
  Torvalds as the main developer of the GNU/Linux system, that's not
  just inaccurate, it also makes his message more
  influential&mdash;and that message says, &ldquo;Nonfree software is
  OK; I use it and develop it myself.&rdquo; If they recognize our
  role, they will listen to us more, and the message we will give them
  is, &ldquo;This system exists because of people who care about
  freedom. Join us, value your freedom, and together we can preserve
  it.&rdquo;
  See <a href="/gnu/thegnuproject.html">The GNU Project</a>
  for the history.</p>
<p>
  When I ask people to call the system GNU/Linux, some of them respond
  with <a href="/gnu/gnu-linux-faq.html"> silly excuses and straw men</a>.
  But we probably haven't lost
  anything, because they were probably unfriendly to begin with.
  Meanwhile, other people recognize the reasons I give, and use that
  name.  By doing so, they help make other people aware of why the
  GNU/Linux system really exists, and that increases our ability to
  spread the idea that freedom is an important value.</p>
<p>
  This is why I keep butting my head against bias, calumny, and grief.
  They hurt my feelings, but when successful, this effort helps the GNU
  Project campaign for freedom.</p>
<p>
  Since this came up in the context of Linux (the kernel) and Bitkeeper,
  the nonfree version control system that Linus Torvalds now uses, I'd
  like to address that issue as well.</p>

<h3 id="bitkeeper">Bitkeeper issue</h3>
<p>
  (See the <a href="#update">update</a> below.)</p>
<p>
  The use of Bitkeeper for the Linux sources has a grave effect on the
  free software community, because anyone who wants to closely track
  patches to Linux can only do it by installing that nonfree program.
  There must be dozens or even hundreds of kernel hackers who have done
  this.  Most of them are gradually convincing themselves that it is ok
  to use nonfree software, in order to avoid a sense of cognitive
  dissonance about the presence of Bitkeeper on their machines.  What
  can be done about this?</p>
<p>
  One solution is to set up another repository for the Linux sources,
  using CVS or another free version control system, and arranging to
  load new versions into it automatically.  This could use Bitkeeper to
  access the latest revisions, then install the new revisions into CVS.
  That update process could run automatically and frequently.</p>
<p>
  The FSF cannot do this, because we cannot install Bitkeeper on our
  machines.  We have no nonfree systems or applications on them now,
  and our principles say we must keep it that way.  Operating this
  repository would have to be done by someone else who is willing to
  have Bitkeeper on his machine, unless someone can find or make a way
  to do it using free software.</p>
<p>
  The Linux sources themselves have an even more serious problem with
  nonfree software: they actually contain some.  Quite a few device
  drivers contain series of numbers that represent firmware programs to
  be installed in the device.  These programs are not free software.  A
  few numbers to be deposited into device registers are one thing; a
  substantial program in binary is another.</p>
<p>
  The presence of these binary-only programs in &ldquo;source&rdquo;
  files of Linux creates a secondary problem: it calls into question
  whether Linux binaries can legally be redistributed at all.  The GPL
  requires &ldquo;complete corresponding source code,&rdquo; and a
  sequence of integers is not the source code. By the same token,
  adding such a binary to the Linux sources violates the GPL.</p>
<p>
  The Linux developers have a plan to move these firmware programs
  into separate files; it will take a few years to mature, but when
  completed it will solve the secondary problem; we could make a
  &ldquo;free Linux&rdquo; version that doesn't have the nonfree
  firmware files.  That by itself won't do much good if most people
  use the nonfree &ldquo;official&rdquo; version of Linux.  That may
  well occur, because on many platforms the free version won't run
  without the nonfree firmware.  The &ldquo;free Linux&rdquo; project
  will have to figure out what the firmware does and write source code
  for it, perhaps in assembler language for whatever embedded
  processor it runs on.  It's a daunting job.  It would be less
  daunting if we had done it little by little over the years, rather
  than letting it mount up.  In recruiting people to do this job, we
  will have to overcome the idea, spread by some Linux developers,
  that the job is not necessary.</p>
<p>
  Linux, the kernel, is often thought of as the flagship of free
  software, yet its current version is partially nonfree.  How did
  this happen?  This problem, like the decision to use Bitkeeper,
  reflects the attitude of the original developer of Linux, a person
  who thinks that &ldquo;technically better&rdquo; is more important
  than freedom.</p>
<p>
  Value your freedom, or you will lose it, teaches history.
  &ldquo;Don't bother us with politics,&rdquo; respond those who don't
  want to learn.</p>
<div class="column-limit"></div>

<p id="update">
  <strong>Update:</strong> Since 2005, BitKeeper
  is no longer used to manage the Linux kernel source tree.  See the
  article, <a href="/philosophy/mcvoy.html">Thank You, Larry
  McVoy</a>.  The Linux sources still contain nonfree firmware blobs,
  but as of January 2008,
  a <a href="//directory.fsf.org/project/linux"> free version of
  Linux</a> is now maintained for use in free GNU/Linux
  distributions.</p>
</div>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to <a
href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.  There are also <a
href="/contact/">other ways to contact</a> the FSF.  Broken links and other
corrections or suggestions can be sent to <a
href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations README</a> for
information on coordinating and contributing translations of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2002, 2021 Richard M. Stallman</p>

<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2021/10/20 09:32:08 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

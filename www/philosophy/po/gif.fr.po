# French translation of http://www.gnu.org/philosophy/gif.html
# Copyright (C) 2000 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Frédéric Couchet, 1998. http://web.archive.org/web/19971009070944/http://april.org/
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2008, 2009.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: gif.html\n"
"POT-Creation-Date: 2021-11-03 13:55+0000\n"
"PO-Revision-Date: 2024-03-03 16:18+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Why There Are No GIF Files on GNU Web Pages - GNU Project - Free Software "
"Foundation"
msgstr ""
"Pourquoi il n'y a pas de fichiers GIF sur le site de GNU - Projet GNU - Free "
"Software Foundation"

#. type: Content of: <div><h2>
msgid "Why There Are No GIF Files on GNU Web Pages"
msgstr "Pourquoi il n'y a pas de fichiers GIF sur le site de GNU"

#. type: Content of: <div><div><p>
msgid ""
"<em>There is no special patent threat to GIF format nowadays as far as we "
"know; <a href=\"#venuenote\">the patents that were used to attack GIF have "
"expired</a>.  Nonetheless, this article will remain pertinent as long as "
"programs can be forbidden by patents, since the same sorts of things could "
"happen in any area of computing.  See <a href=\"/server/standards/gnu-"
"website-guidelines.html#UseofGraphics\">our website policies regarding GIFs</"
"a>.</em>"
msgstr ""
"<em>À notre connaissance, il n'y a pas de brevet menaçant le format GIF de "
"nos jours ; les brevets qui ont servi à attaquer GIF ont expiré <a class="
"\"ftn\" id=\"f1a-rev\" href=\"#venuenote\">[1a]</a>. Néanmoins, cet article "
"restera pertinent aussi longtemps que des programmes seront interdits par "
"des brevets, puisque le même genre de choses pourrait arriver dans n'importe "
"quel domaine de l'informatique. Consultez <a href=\"/server/standards/gnu-"
"website-guidelines.html#UseofGraphics\">les règles suivies par notre site "
"web concernant GIF</a>.</em>"

#. type: Content of: <div><p>
msgid ""
"There are no GIFs on the GNU web site because of the patents (Unisys and "
"IBM) covering the LZW compression algorithm which is used in making GIF "
"files.  These patents make it impossible to have free software to generate "
"proper GIFs.  They also apply to the <strong>compress</strong> program, "
"which is why GNU does not use it or its format."
msgstr ""
"Il n'y a aucun fichier GIF sur le site web de GNU en raison des brevets "
"(Unisys et IBM) couvrant l'algorithme de compression LZW qui est utilisé "
"dans la création des fichiers GIF. Ces brevets rendent impossible "
"l'utilisation de logiciels libres pour générer des GIF véritables. Ils "
"s'appliquent également au programme <strong>compress</strong>, raison pour "
"laquelle GNU ne l'utilise pas."

#. type: Content of: <div><p>
msgid ""
"Unisys and IBM both applied for patents in 1983.  Unisys (and perhaps IBM) "
"applied for these patents in a number of countries.  Of the places whose "
"patent databases we were able to search, the latest expiration date seems to "
"be 1 October 2006&#8239;<a class=\"ftn\" href=\"#venuenote\">[1]</a>. Until "
"then, anyone who releases a free program for making GIF files is likely to "
"be sued.  We don't know any reason to think that the patent owners would "
"lose these lawsuits."
msgstr ""
"Unisys et IBM déposèrent tous deux des brevets en 1983. Unisys (et peut-être "
"IBM) ont déposé ces brevets dans de nombreux pays. Dans les pays pour "
"lesquels nous avons été capables de rechercher dans les bases de données de "
"brevets, la dernière date d'expiration semble être le 1<sup>er</"
"sup> octobre 2006 <a class=\"ftn\" id=\"f1b-rev\" href=\"#venuenote\">[1b]</"
"a>. Jusque là, il est vraisemblable que quiconque voudra distribuer un "
"programme libre pour la création de fichiers GIF sera poursuivi. Nous "
"n'avons aucune raison de penser que les détenteurs des brevets pourraient "
"perdre ces procès."

#. type: Content of: <div><p>
msgid ""
"If we released such a program, Unisys and IBM might think it wiser (for "
"public relations reasons) not to sue a charity like the FSF.  They could "
"instead sue the users of the program, including the companies who "
"redistribute GNU software.  We feel it would not be responsible behavior for "
"us to set up this situation."
msgstr ""
"Si nous distribuions un tel programme, Unisys et IBM pourraient estimer plus "
"prudent (pour des raisons de relations publiques) de ne pas poursuivre une "
"œuvre de bienfaisance comme la FSF. Par contre, ils pourraient poursuivre "
"les utilisateurs du programme, ainsi que les entreprises qui redistribuent "
"des logiciels GNU. Nous jugeons que ce ne serait pas un comportement "
"responsable de notre part de provoquer cette situation."

#. type: Content of: <div><p>
msgid ""
"Many people think that Unisys has given permission for distributing free "
"software to make GIF format.  Unfortunately that is not what Unisys has "
"actually done.  Here is what Unisys actually said about the matter in 1995:"
msgstr ""
"De nombreuses personnes pensent qu'Unisys a donné son autorisation pour la "
"distribution de logiciels libres produisant du format GIF. Malheureusement, "
"ce n'est pas ce qu'Unisys a fait en réalité. Voici ce qu'Unisys disait à ce "
"sujet en 1995 :"

#. type: Content of: <div><blockquote><p>
msgid ""
"Unisys does not require licensing, or fees to be paid, for non-commercial, "
"non-profit GIF-based applications, including those for use on the on-line "
"services.  Concerning developers of software for the Internet network, the "
"same principle applies.  Unisys will not pursue previous inadvertent "
"infringement by developers producing versions of software products for the "
"Internet prior to 1995. The company does not require licensing, or fees to "
"be paid for non-commercial, non-profit offerings on the Internet, including "
"&ldquo;Freeware.&rdquo;"
msgstr ""
"Unisys n'exige ni licence, ni royalties, pour les applications non "
"commerciales et à but non lucratif basées sur du GIF, y compris celles "
"utilisées par les services en ligne. En ce qui concerne les développeurs de "
"logiciel pour le réseau Internet, le même principe s'applique. Unisys "
"n'engagera aucune poursuite pour des infractions involontaires commises "
"avant 1995 par des développeurs de logiciels pour Internet. La société "
"n'exige ni licence, ni royalties, pour les offres non commerciales sur "
"Internet, y compris les <i>freeware</i>."

#. type: Content of: <div><p>
msgid ""
"Unfortunately, this doesn't permit <a href=\"/philosophy/free-sw.html\">free "
"software</a> which can be used in a free operating system such as GNU.  It "
"also does not permit <em>at all</em> the use of LZW for other purposes such "
"as compression of files.  This is why we think it is still best to reject "
"LZW, and switch to alternatives such as <a href=\"/software/gzip/gzip.html"
"\">GNU Gzip</a> and PNG."
msgstr ""
"Malheureusement, cela ne permet pas l'utilisation d'un <a href=\"/philosophy/"
"free-sw.html\">logiciel libre</a> dans un système d'exploitation libre tel "
"que GNU. De plus, cela ne permet pas  <em>du tout</em> l'utilisation de LZW "
"pour d'autres usages que la compression de fichiers. C'est pourquoi nous "
"pensons qu'il vaut mieux rejeter LZW et utiliser des alternatives comme <a "
"href=\"/software/gzip/gzip.html\">GNU Gzip</a> et le format PNG."

#. type: Content of: <div><p>
msgid ""
"<a href=\"/philosophy/selling.html\">Commercial redistribution of free "
"software</a> is very important, and we want the GNU system as a whole to be "
"redistributed commercially.  This means we can't add a GIF-generating "
"program to GNU, not under the Unisys terms."
msgstr ""
"La <a href=\"/philosophy/selling.html\">redistribution commerciale de "
"logiciels libres</a> est très importante, et nous voulons que le système GNU "
"en entier puisse être redistribué commercialement. Cela signifie que nous ne "
"pouvons ajouter un programme de génération de GIF au système GNU, pas selon "
"les termes d'Unisys."

#. type: Content of: <div><p>
msgid ""
"The <a href=\"https://www.fsf.org\">Free Software Foundation</a> is a non-"
"commercial, non-profit organization, so strictly speaking the income from "
"our sales of <a href=\"https://shop.fsf.org/\">CD-ROMs</a> is not &ldquo;"
"profit.&rdquo; Perhaps this means we could include a GIF program on our CD-"
"ROM and claim to be acting within the scope of the Unisys permission&mdash;"
"or perhaps not.  But since we know that other redistributors of GNU would be "
"unable to include it, doing this would not be very useful."
msgstr ""
"La <a href=\"https://www.fsf.org\">Fondation pour le logiciel libre</a> est "
"une organisation non commerciale, à but non lucratif, donc strictement "
"parlant, les revenus provenant de la vente de nos <a href=\"https://shop.fsf."
"org/\">CD-ROM</a> ne sont pas considérés comme des « profits ». Cela "
"signifie peut-être que nous pourrions inclure un programme de génération de "
"GIF sur nos CD-ROM et prétendre agir dans les limites de la permission "
"d'Unisys – ou peut-être pas. Mais puisque nous savons que les autres "
"redistributeurs de GNU ne pourraient pas l'inclure, faire ceci ne serait pas "
"très utile."

#. type: Content of: <div><p>
msgid ""
"Shortly after Unisys made its announcement, when the net in general was "
"reassured thinking that Unisys had given permission for free GIF-generating "
"software, we wrote to the Unisys legal department asking for clarification "
"of these issues.  We did not receive a response."
msgstr ""
"Peu après l'annonce d'Unisys, quand le net en général était assuré qu'Unisys "
"avait donné son autorisation pour des logiciels libres de génération de GIF, "
"nous avons écrit au service juridique d'Unisys pour leur demander des "
"éclaircissements à ce sujet. Nous n'avons pas reçu de réponse."

#. type: Content of: <div><p>
msgid ""
"Even if Unisys really did give permission for free software to generate "
"GIFs, we would still have to deal with the IBM patent.  Both the IBM and the "
"Unisys patents cover the same &ldquo;invention&rdquo;&mdash;the LZW "
"compression algorithm.  (This could reflect an error on the part of the US "
"Patent and Trademark Office, which is famous for incompetence and poor "
"judgment.)"
msgstr ""
"Même si Unisys donnait vraiment l'autorisation à des logiciels libres de "
"générer des GIF, nous aurions toujours à traiter avec le brevet d'IBM. Les "
"deux brevets, celui d'IBM et celui d'Unisys, couvrent la même "
"« invention » : l'algorithme de compression LZW (ceci pourrait refléter une "
"erreur de la part de l'Office américain des brevets et des marques, le <i>US "
"Patent and Trademark Office</i>, qui est célèbre pour son incompétence et "
"ses erreurs d'appréciation)."

#. type: Content of: <div><p>
msgid ""
"Decoding GIFs is a different issue.  The Unisys and IBM patents are both "
"written in such a way that they do not apply to a program which can only "
"uncompress LZW format and cannot compress.  Therefore we can and will "
"include support for displaying GIF files in GNU software."
msgstr ""
"Le décodage de GIF est un problème différent. Les brevets d'Unisys et d'IBM "
"sont tous deux écrits de telle manière qu'ils ne s'appliquent pas à un "
"programme qui peut seulement décompresser le format LZW et ne peut le "
"compresser. Par conséquent, nous pouvons inclure un moyen d'afficher les "
"fichiers GIF dans les logiciels GNU, et nous le ferons."

#. type: Content of: <div><p>
msgid ""
"Given this situation, we could still include GIF files in our web pages if "
"we wanted to.  Many other people would be happy to generate them for us, and "
"we would not be sued for having GIF files on our server."
msgstr ""
"Étant donné la situation, nous pourrions toujours inclure des fichiers GIF "
"sur nos pages web si nous le voulions. Beaucoup d'autres personnes seraient "
"heureuses de les générer pour nous, et nous ne pourrions pas être poursuivis "
"pour avoir des fichiers GIF sur notre serveur."

#. type: Content of: <div><p>
msgid ""
"But we feel that if we can't distribute the software to enable people to "
"generate GIF files properly, then we should not have other people run such "
"software for us.  Besides, if we can't provide software in GNU to generate "
"GIF files, we have to recommend an alternative.  We ourselves should use the "
"alternative that we recommend."
msgstr ""
"Mais nous estimons que si nous ne pouvons distribuer le logiciel permettant "
"de générer correctement des fichiers GIF, alors nous ne devons pas demander "
"à d'autres personnes d'utiliser de tels programmes pour nous. D'ailleurs, si "
"nous ne pouvons fournir dans le système GNU de logiciel capable de générer "
"des fichiers GIF, nous devons recommander une alternative. Nous devons "
"utiliser nous-mêmes l'alternative que nous recommandons."

#. type: Content of: <div><p>
msgid ""
"In 1999, Unisys had the following to say about the issue of their patent:"
msgstr "En 1999, Unisys indiquait au sujet de son brevet :"

#. type: Content of: <div><blockquote><p>
msgid ""
"Unisys has frequently been asked whether a Unisys license is required in "
"order to use LZW software obtained by downloading from the Internet or from "
"other sources. The answer is simple. In all cases, a written license "
"agreement or statement signed by an authorized Unisys representative is "
"required from Unisys for all use, sale or distribution of any software "
"(including so-called &ldquo;freeware&rdquo;) and/or hardware providing LZW "
"conversion capability (for example, downloaded software)."
msgstr ""
"On a fréquemment demandé à Unisys si une licence d'Unisys est exigée afin "
"d'utiliser le logiciel LZW obtenu par téléchargement sur Internet ou depuis "
"d'autres sources. La réponse est simple. Dans tous les cas, une licence "
"écrite ou une attestation signée par un représentant autorisé d'Unisys est "
"exigée par Unisys pour toute utilisation, vente ou distribution de n'importe "
"quel logiciel (y compris les logiciels appelés <i>freeware</i>), et/ou "
"matériel capable de faire la conversion LZW (par exemple un logiciel "
"téléchargé)."

#. type: Content of: <div><p>
msgid ""
"With this statement, Unisys is trying to take back what they said in 1995 "
"when they gave parts of the patent to the public. The legality of such a "
"move is questionable."
msgstr ""
"Avec cette déclaration, Unisys essaie de revenir sur ce qu'ils disaient en "
"1995 quand ils ont donné des parties de leur brevet au public. La légalité "
"d'un tel changement de position est discutable."

#. type: Content of: <div><p>
msgid ""
"A further issue is that the LZW patents&mdash;and computational idea patents "
"in general&mdash;are an offense against the freedom of programmers "
"generally, and all programmers need to work together to <a href=\"/"
"philosophy/limit-patent-effect.html\">protect software from patents.</a>"
msgstr ""
"Cela amène une autre question : les brevets LZW – et d'une manière générale "
"les brevets sur des algorithmes – sont une atteinte à la liberté des "
"programmeurs ; tous les programmeurs ont besoin de s'unir pour <a href=\"/"
"philosophy/limit-patent-effect.html\">protéger le logiciel des brevets</a>."

#. type: Content of: <div><p>
msgid ""
"So even if we could find a solution to enable the free software community to "
"generate GIFs, that isn't really a solution, not for the problem as a "
"whole.  The solution is switching to another format and not using GIF any "
"more."
msgstr ""
"Ainsi, même si nous pouvions trouver une solution pour permettre à la "
"communauté du logiciel libre de générer des fichiers GIF, ce n'est pas "
"vraiment une solution, pas pour le problème dans sa globalité. La solution "
"est de basculer vers un autre format et de ne plus utiliser de fichiers GIF."

#. type: Content of: <div><p>
msgid "Therefore, we don't use GIF, and we hope you won't use it either."
msgstr ""
"Donc, nous n'utilisons pas de fichiers GIF, et nous espérons que vous n'en "
"utiliserez pas non plus."

#. type: Content of: <div><p>
msgid ""
"It is possible to make non-compressed images that act like GIFs, in that "
"they work with programs that decode GIF format.  This can be done without "
"infringing patents.  These pseudo-GIFs are useful for some purposes."
msgstr ""
"Il est possible de faire des images non compressées qui se comportent comme "
"des GIF, dans le sens où elles fonctionnent avec des programmes qui décodent "
"le format GIF. Ceci peut être fait sans violer de brevet. Ces pseudo-GIF "
"sont utiles pour quelques usages."

#. type: Content of: <div><p>
msgid ""
"It is also possible to create GIFs using a patent-free run length encoding "
"but this doesn't achieve the compression that one normally expects in a GIF."
msgstr ""
"Il est également possible de créer des GIF en utilisant le codage <i>run "
"length</i>, non breveté, mais celui-ci ne réalise pas la compression "
"normalement attendue d'un GIF."

#. type: Content of: <div><p>
msgid ""
"We decided not to use these pseudo-GIFs on our web site because they are not "
"a satisfactory solution to the community's problem.  They work, but they are "
"very large.  What the web needs is a patent-free compressed format, not "
"large pseudo-GIFs."
msgstr ""
"Nous avons décidé de ne pas utiliser ces pseudo-GIF sur notre site web parce "
"que ce n'est pas une solution satisfaisante au problème de la communauté. "
"Ils fonctionnent, mais ils sont très gros. Ce dont le web a besoin, c'est "
"d'un format compressé non breveté, non de lourds pseudo-GIF."

#. type: Content of: <div><p>
msgid ""
"The <a href=\"https://en.wikipedia.org/wiki/Portable_Network_Graphics\">PNG "
"format</a> is a patent-free compressed format.  We hope it will become "
"widely supported; then we will use it.  We do provide PNG versions of most "
"of the <a href=\"/graphics/graphics.html\">images on this server</a>."
msgstr ""
"Le <a href=\"https://fr.wikipedia.org/wiki/Portable_Network_Graphics"
"\">format PNG</a> est un format de compression non breveté. Nous espérons "
"qu'il sera largement supporté, alors nous l'utiliserons. Nous avons des <a "
"href=\"/graphics/graphics.html\">versions PNG</a> de la plupart des images "
"sur ce serveur."

#. type: Content of: <div><p>
msgid ""
"For more information about the GIF patent problems, see <a href=\"https://"
"web.archive.org/web/20150329143651/http://progfree.org/Patents/patents.html"
"\">the League for Programming Freedom GIF page</a>.  Through that page you "
"can find more information about the <a href=\"https://endsoftwarepatents.org/"
"\">problem of software patents in general.</a>"
msgstr ""
"Pour plus de renseignements sur les problèmes de brevet affectant GIF, allez "
"voir la <a href=\"https://web.archive.org/web/20150329143651/http://progfree."
"org/Patents/patents.html\">page dédiée de la <i>League for Programming "
"Freedom</i></a>. Elle vous aiguillera vers des sources d'information "
"complémentaire sur le <a href=\"https://endsoftwarepatents.org/\">problème "
"des brevets en général</a>."

#. type: Content of: <div><p>
msgid ""
"There is a library called libungif that reads gif files and writes "
"uncompressed gifs to circumvent the Unisys patent."
msgstr ""
"La bibliothèque appelée <code>libungif</code> lit les fichiers GIF et "
"produit des GIF non compressés pour contourner le brevet d'Unisys."

#. type: Content of: <div><p>
msgid ""
"<a href=\"https://web.archive.org/web/20171203193534/http://burnallgifs.org/"
"\"> burnallgifs.org</a> is a web site devoted to discouraging the use of GIF "
"files on the web."
msgstr ""
"<a href=\"https://web.archive.org/web/20171203193534/http://burnallgifs.org/"
"\">http://burnallgifs.org</a> essaie de décourager l'utilisation de fichiers "
"GIF sur le web."

#. type: Content of: <div><h3>
msgid "Footnote"
msgstr "Note"

#. type: Content of: <div><ol><li><p>
msgid ""
"We were able to search the patent databases of the USA, Canada, Japan, and "
"the European Union. The Unisys patent expired on 20 June 2003 in the USA, in "
"Europe it expired on 18 June 2004, in Japan the patent expired on 20 June "
"2004 and in Canada it expired on 7 July 2004. The U.S. IBM patent expired 11 "
"August 2006. The Software Freedom Law Center says that after 1 October 2006, "
"there will be no significant patent claims interfering with the use of "
"static GIFs."
msgstr ""
"<a href=\"#f1a-rev\" class=\"nounderline\">[a]</a> <a href=\"#f1b-rev\" "
"class=\"nounderline\">[b]</a>  Nous avons pu chercher dans les bases de "
"données de brevets des États-Unis, du Canada, du Japon et de l'Union "
"européenne. Le brevet Unisys a expiré le 20 juin 2003 aux États-Unis ; en "
"Europe, le 18 juin 2004 ; au Japon, le 20 juin 2004 et au Canada, le "
"7 juillet 2004. Le brevet américain d'IBM a expiré le 11 août 2006. Le "
"<i>Software Freedom Law Center</i> dit qu'après le 1<sup>er</"
"sup> octobre 2006, il n'y aura plus de revendication de brevet significative "
"pour interférer avec l'emploi d'images GIF statiques."

#. type: Content of: <div><ol><li><p>
msgid ""
"Animated GIFs are a different story.  We do not know what patents might "
"cover them.  However, we have not heard reports of threats against use of "
"animated GIFs.  Any software can be threatened by patents, but we have no "
"reason to consider animated GIFs to be in particular danger&mdash;no "
"particular reason to shun them."
msgstr ""
"Pour les GIF animés, c'est une autre histoire. Nous ne savons pas quels "
"brevets pourraient bien les couvrir. Cependant nous n'avons pas entendu "
"parler de menaces contre leur emploi. N'importe quel logiciel peut être sous "
"la menace de brevets, mais nous n'avons aucune raison de penser que les GIF "
"animés sont particulièrement en danger, aucune raison particulière de les "
"éviter."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

# Pas de changement significatif en 2021.
#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1997-1999, 2003, 2004, 2006, 2010, 2013, 2021 Free Software "
"Foundation, Inc."
msgstr ""
"Copyright &copy; 1997-1999, 2003, 2004, 2006, 2010, 2013 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Frédéric Couchet<br /> Révision : <a href=\"mailto:trad-"
"gnu@april.org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="TAGS" value="essays upholding action" -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>Avoiding Ruinous Compromises
- GNU Project - Free Software Foundation</title>
<style type="text/css" media="print,screen"><!--
 .imgleft, .imgright { display: block; height: 4.25em; width: auto; }
@media (max-width:25em) {
 .imgleft, .imgright { float: none; margin: 0 auto; }
}
--></style>
<!--#include virtual="/philosophy/po/compromise.translist" -->
<!--#include virtual="/server/banner.html" -->
<!--#include virtual="/philosophy/ph-breadcrumb.html" -->
<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->
<div class="article reduced-width">
<h2>Avoiding Ruinous Compromises</h2>

<address class="byline">by Richard Stallman</address>

<p class="introduction">Twenty-five years
ago <a href="/gnu/initial-announcement.html">on September 27, 1983, I
announced a plan</a> to create a completely free operating system
called GNU&mdash;for &ldquo;GNU's Not Unix.&rdquo;  As part of the
25th anniversary of the GNU system, I have written this article on how
our community can avoid ruinous compromises.  In addition to avoiding
such compromises, there are many ways you can <a href="/help/help.html">
help GNU</a> and free software.  One way is to say no to the use of a 
nonfree program or an online disservice as often as you can or 
<a href="/philosophy/saying-no-even-once.html"> 
even once</a>.</p>
<hr class="no-display" />

<p>The free software movement aims for a social
change: <a href="/philosophy/free-sw.html">to make all software
free</a> so that all software users are free and can be part of a
community of cooperation.  Every nonfree program gives its developer
unjust power over the users.  Our goal is to put an end to that
injustice.</p>

<p>The road to freedom
is <a href="https://www.fsf.org/bulletin/2008/spring/the-last-mile-is-always-the-hardest/">
a long road</a>.  It will take many steps and many years to reach a
world in which it is normal for software users to have freedom.  Some
of these steps are hard, and require sacrifice.  Some of them become 
easier if we make compromises with people that have different goals.</p>

<img src="/graphics/gplv3-with-text-136x68.png" alt="&nbsp;[GPL Logo]&nbsp;"
class="imgright" />

<p>Thus, the <a href="https://www.fsf.org/">Free Software
Foundation</a> makes compromises&mdash;even major ones.  For
instance, we made compromises in the patent provisions of version 3 of
the <a href="/licenses/gpl.html">GNU General Public License</a> 
(GNU GPL) so that major companies would contribute to and distribute 
GPLv3-covered software and thus bring some patents under the effect of 
these provisions.  </p>

<img src="/graphics/lgplv3-with-text-154x68.png" alt="&nbsp;[LGPL Logo]&nbsp;"
class="imgleft" />

<p><a href="/licenses/lgpl.html">The Lesser GPL</a>'s purpose is a
compromise: we use it on certain chosen free libraries to permit their
use in nonfree programs because we think that legally prohibiting
this would only drive developers to proprietary libraries instead.  We
accept and install code in GNU programs to make them work together
with common nonfree programs, and we document and publicize this in
ways that encourage users of the latter to install the former, but not
vice versa.  We support specific campaigns we agree with, even when we
don't fully agree with the groups behind them.</p>

<p>But we reject certain compromises even though many others in our
community are willing to make them.  For instance,
we <a href="/distros/free-system-distribution-guidelines.html">
endorse only the GNU/Linux distributions</a> that have policies not to
include nonfree software or lead users to install it.  To endorse
nonfree distributions would be a <abbr title="ruinous
(r&#363;'&#601;-n&#601;s) adj. 1. Causing or apt to cause ruin;
destructive.  2. Falling to ruin; dilapidated or
decayed.">ruinous</abbr> compromise.</p>

<p>Compromises are ruinous if they would work against our aims in the
long term.  That can occur either at the level of ideas or at the 
level of actions.</p>

<p>At the level of ideas, ruinous compromises are those that reinforce
the premises we seek to change.  Our goal is a world in which software
users are free, but as yet most computer users do not even recognize
freedom as an issue.  They have taken up &ldquo;consumer&rdquo;
values, which means they judge any program only on practical 
characteristics such as price and convenience.</p>

<p>Dale Carnegie's classic self-help book, <cite>How to Win Friends 
and Influence People</cite>, advises that the most effective way to
persuade someone to do something is to present arguments that appeal
to per values.  There are ways we can appeal to the consumer values
typical in our society.  For instance, free software obtained gratis
can save the user money.  Many free programs are convenient and
reliable, too.  Citing those practical benefits has succeeded in
persuading many users to adopt various free programs, some of which
are now quite successful.</p>

<p>If getting more people to use some free programs is as far as you
aim to go, you might decide to keep quiet about the concept of
freedom, and focus only on the practical advantages that make sense
in terms of consumer values.  That's what the term &ldquo;open
source&rdquo; and its associated rhetoric do.</p>

<p>That approach can get us only part way to the goal of freedom.  
People who use free software only because it is convenient will stick 
with it only as long as it is more convenient.  And they will see no 
reason not to use convenient proprietary programs along with it.</p>

<p>The philosophy of open source presupposes and appeals to consumer
values, and this affirms and reinforces them.  That's why we
<a href="/philosophy/open-source-misses-the-point.html">do not
advocate open source.</a></p>

<div class="pict narrow">
<img src="/graphics/gnulaptop.png"
     alt=" [Levitating Gnu with a laptop] " />
</div>

<p>To establish a free community fully and lastingly, we need to do
more than get people to use some free software.  We need to spread the
idea of judging software (and other things) on &ldquo;citizen
values,&rdquo; based on whether it respects users' freedom and
community, not just in terms of convenience.  Then people will not
fall into the trap of a proprietary program baited by an attractive,
convenient feature.</p>

<p>To promote citizen values, we have to talk about them and show how
they are the basis of our actions.  We must reject the Dale Carnegie
compromise that would influence their actions by endorsing their
consumer values.</p>

<p>This is not to say we cannot cite practical advantage at 
all&mdash;we can and we do.  It becomes a problem only when the 
practical advantage steals the scene and pushes freedom into the 
background.  Therefore, when we cite the practical advantages of free 
software, we reiterate frequently that those are just <em>additional, 
secondary</em> reasons to prefer it.</p>

<p>It's not enough to make our words accord with our ideals; our
actions have to accord with them too.  So we must also avoid
compromises that involve doing or legitimizing the things we aim to
stamp out.</p>

<p>For instance, experience shows that you can attract some users to
<a href="/gnu/why-gnu-linux.html">GNU/Linux</a> if you include some
nonfree programs.  This could mean a cute nonfree application that
will catch some user's eye, or a nonfree programming platform such
as <a href="/philosophy/java-trap.html">Java</a> (formerly) or the
Flash runtime (still), or a nonfree device driver that enables
support for certain hardware models.</p>

<p>These compromises are tempting, but they undermine the goal.  If
you distribute nonfree software, or steer people towards it, you will
find it hard to say, &ldquo;Nonfree software is an injustice, a
social problem, and we must put an end to it.&rdquo; And even if you
do continue to say those words, your actions will undermine them.</p>

<p>The issue here is not whether people should be <em>able</em>
or <em>allowed</em> to install nonfree software; a general-purpose
system enables and allows users to do whatever they wish.  The issue
is whether we guide users towards nonfree software.  What they do on
their own is their responsibility; what we do for them, and what we
direct them towards, is ours.  We must not direct the
users towards proprietary software as if it were a solution, because
proprietary software is the problem.</p>

<p>A ruinous compromise is not just a bad influence on others.  It can
distort your own values, too, through cognitive dissonance.  If you
have certain values, but your actions imply other, conflicting values,
you are likely to change your values or your actions so as to resolve 
the contradiction.  Thus, projects that argue only from practical
advantages, or direct people toward some nonfree software, nearly
always shy away from even <em>suggesting</em> that nonfree software
is unethical.  For their participants, as well as for the public, they
reinforce consumer values.  We must reject these compromises if we wish
to keep our values straight.</p>

<p>If you want to move to free software without compromising the goal
of freedom, look at <a href="https://www.fsf.org/resources">the FSF's
resources area</a>.  It lists hardware and machine configurations that
work with free software, <a href="/distros/distros.html"> totally free 
GNU/Linux distros</a> to install, and <a href="https://directory.fsf.org/">
thousands of free software packages</a> that work in a 100 percent 
free software environment.  If you want to help the community stay on 
the road to freedom, one important way is to publicly uphold citizen 
values.  When people are discussing what is good or bad, or what to 
do, cite the values of freedom and community and argue from them.</p>

<p>A road that lets you go faster is not better if it leads to the
wrong place.  Compromise is essential to achieve an ambitious goal,
but beware of compromises that lead away from the goal.</p>

<hr class="column-limit"/>

<p>
For a similar point in a different area of life,
see <a
href="https://www.guardian.co.uk/commentisfree/2011/jul/19/nudge-is-not-enough-behaviour-change">
&ldquo;Nudge&rdquo; is not enough</a>.
</p>
</div>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to <a
href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.  There are also <a
href="/contact/">other ways to contact</a> the FSF.  Broken links and other
corrections or suggestions can be sent to <a
href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations README</a> for
information on coordinating and contributing translations of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2008, 2021 Richard Stallman</p>

<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2021/09/11 09:37:22 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

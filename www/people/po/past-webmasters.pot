# LANGUAGE translation of https://www.gnu.org/people/past-webmasters.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: past-webmasters.html\n"
"POT-Creation-Date: 2024-02-28 02:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "GNU's Webmasters - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <div><h2>
msgid "GNU's Webmasters Past and Present"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"These people are, or have been at some stage, <a "
"href=\"/people/webmeisters.html\">webmasters of www.gnu.org</a>.  If you see "
"something you would like added to, or changed, on this web site, please "
"contact us at <a href=\"mailto:webmasters@gnu.org\"> "
"<em>webmasters@gnu.org</em> </a> send other questions to <a "
"href=\"mailto:gnu@gnu.org\"><em>gnu@gnu.org</em></a>).  We are always "
"looking for comments and are open to suggestions."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "(-:-------------:-)"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"This place is reserved for your name, if you join our team of webmasters. If "
"you'd like to join us as a webmaster, <a "
"href=\"/server/standards/webmaster-quiz.html\">please complete the webmaster "
"quiz</a>."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong><a href=\"http://alexm.org/\">Alex Muntada</a></strong> <a href= "
"\"mailto:alexm@gnu.org\">&lt;alexm@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Is the current GNU web translation manager and translation teams "
"coordinator."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Alex Patel</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Alex was a volunteer GNU webmaster and a 2014 Summer <a "
"href=\"http://www.fsf.org/blogs/community/introducing-alex-patel-our-summer "
"-campaigns-intern\">campaigns intern</a> at the FSF."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong id=\"bandali\">Amin Bandali</strong> &lt;<a href= "
"\"mailto:bandali@gnu.org\">bandali@gnu.org</a>&gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"<a href=\"https://kelar.org/~bandali/\">Amin</a> is a <a "
"href=\"/philosophy/free-sw.html\">free software</a> activist and a volunteer "
"GNU webmaster since early 2016 who wears <a "
"href=\"/people/people.html#bandali\">a few other hats</a> around GNU as "
"well."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Andias T. Wira Alam</strong> <a href= "
"\"mailto:andias.alam@rus.uni-stuttgart.de\"> "
"&lt;andias.alam@rus.uni-stuttgart.de&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Andias is a volunteer GNU webmaster and computer science student. He also "
"works at the University as GNU/Linux System Administrator."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Avdesh Kr Singh</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Avdesh was a GNU webmaster, originally joining the team in September 2014."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Baishampayan Ghose</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Baishampayan is a volunteer GNU Webmaster from India."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Brett Gilio</strong> <a "
"href=\"mailto:brettg@gnu.org\">&lt;brettg@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Brett joined the GNU webmastering team in late 2019. In addition to free "
"software advocacy, he also champions functional programming, formal "
"mathematics in software design, as well as hygienic and ethical computing "
"practices.."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Cesar J. Alaniz</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Cesar is a volunteer GNU webmaster and student."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Christiano Anderson</strong> <a href= "
"\"mailto:anderson@gnu.org\">&lt;anderson@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Christiano is a GNU Project webmaster and <a href=\"/directory\">Free "
"Software Directory</a> maintainer."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Chris M. Simon</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Free Software hacker and activist, volunteer GNU webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Corey Goldberg</strong> <a href= "
"\"mailto:cmg_at_gnu_dot_org\">&lt;cmg_at_gnu_dot_org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Corey is a GNU Project webmaster and lives in Boston, MA."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<a href=\"http://dma.tx0.org\">David Allen</a>&nbsp;<a href= "
"\"mailto:dma@gnu.org\">&lt;dma@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"David is a GNU webmaster from the United States, he joined the team December "
"2015."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "David Thompson"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"David was a Web Developer at the FSF and assisted with Chief Webmaster "
"tasks."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<a href=\"http://oberon07.com/dee\" id=\"sinuhe\"> <strong>D. E. Evans (aka "
"sinuhe)</strong></a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Sinuhe is described as brusque by rms, so beware. He is dedicated to free "
"software and served as Chief Webmaster after John Paul."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Derek Chen-Becker</strong> <a href= "
"\"mailto:derek@chen-becker.org\">&lt;derek@chen-becker.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Derek was Chief Webmaster before John Sullivan, and is involved in various "
"free software projects."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Dmitri Alenitchev</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Dmitri is a GNU webmaster and a free software programmer from Russia."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Edward Alfert</strong> <a href= "
"\"mailto:ealfert_at_rootmode_dot_com%3E\">&lt;ealfert at rootmode dot "
"com&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Edward has been using GNU software for over nine years. He is giving back to "
"the community by volunteering as a GNU Project webmaster and designing GNU "
"graphics."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Exal de Jesus Garcia Carrillo</strong> <a href= "
"\"mailto:exal@gnu.org\">&lt;exal@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Exal is a GNU project webmaster, he tries to convince the people around him "
"to use free software and follow the GNU philosophy, he also works with the "
"spanish translation team."
msgstr ""

#.  broken link 2014-07-19 a href="http://francoiacomella.org/" 
#.  /a 
#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Franco Iacomella</strong> <a href= \"mailto:yaco@gnu.org\">&lt;yaco "
"at gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Franco is the argentinian GNU Webmaster, he is also envolved in others areas "
"of the project related with administration, licences and education."
msgstr ""

#.  2017-04-22 - Commenting out this link out of consideration
#.           for visitors' ears. ~ Thérèse <a href="http://schmoo.me.uk/">
#. </a>
#
#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Gaz Collins</strong> &lt;<a href= "
"\"mailto:schmooster-at-gmail-dot-com\">schmooster-at-gmail-dot-com</a> &gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Gaz is a volunteer GNU webmaster, and GNU/Linux user since around 1997."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Hatem Hossny</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Was a GNU webmaster and Arabic translation team co-leader."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong><a href=\"mailto:hellekin@gnu.org\">hellekin</a></strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Was a GNU Webmaster from 2012 - 2020."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Hicham Yamout</strong> <a href= "
"\"mailto:hicham@gmail.com\">&lt;hicham(@)gmail.com&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Is a Computer &amp; Communications Engineer, and was a GNU webmaster and a "
"member of Arabic translation team."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Hossam Hossny</strong> <a href= \"mailto:hossam@gnu.org\">&lt;hossam "
"at gnu dot org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Was a webmaster, and the maintainer of the mirrors page."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>James Taylor</strong> <a href=\"mailto:virtualboxjames@gnu.org\"> "
"&lt;virtualboxjames@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Is a GNU Webmaster and comp sci student."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>James Turner</strong> <a href= "
"\"mailto:jturner@gnu.org\">&lt;jturner@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "James is a volunteer GNU webmaster and free software advocate."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>John Paul Wallington</strong> <a href= "
"\"mailto:jpw@gnu.org\">&lt;jpw@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Is a webmaster, and was chief webmaster after Brett Smith."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Joakim Olsson</strong> <a href= "
"\"mailto:jocke@gnu.org\">&lt;jocke@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Joakim (a.k.a. Jocke) is a Swedish webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong><a href=\"mailto:johns@gnu.org\">John Sullivan</a></strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Is the Executive Director of the <a href=\"http://www.fsf.org/\">FSF</a>, a "
"GNU webmaster and was chief webmaster after Derek Chen-Becker until Matt "
"Lee."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<a href=\"http://jonaskoelker.homeunix.org/\"><strong>Jonas "
"Kölker</strong></a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Jonas is a volunteer GNU webmaster, hobbyist programmer, GNU/Linux user and "
"computer science student."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Jorge Barrera Grandon</strong> &lt;<a href= "
"\"mailto:jorge%5Bat%5Dgnu%5Bdot%5Dorg\">jorge[at]gnu[dot]org</a>&gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Jorge is a chilean GNU Project webmaster and works as a GNU/Linux and "
"Windows systems administrator in Norway."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Justin Pence</strong> <a href= "
"\"mailto:jlpence_at_gnu_spamsucks_org\"> "
"&lt;jlpence_at_gnu_spamsucks_org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Is a webmaster for the GNU Project."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Leo Jackson</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Volunteers on <a href=\"http://savannah.gnu.org/projects/www/\"> GNU Project "
"WWW</a>, and <a href=\"http://www.fsf.org/campaigns/playogg/sites\"> Ogg "
"Sites listing.</a>"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong><a href=\"mailto:luiji@users.sourceforge.net\">Luiji "
"Maryo</a></strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Luiji was a volunteer GNU Webmaster and is a hacker of many a program "
"designed to distract him from actually working. He's also massively "
"paranoid."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Lorenzo L. Ancora</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Lorenzo is a full-stack web and desktop developer from Southern Italy. He "
"served as <a href=\"https://directory.fsf.org\">Free Software Directory</a> "
"administrator and GNU webmaster from 2018/2019 to 2021."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Luis F. Araujo</strong> <a href= "
"\"mailto:araujo_at_es_gnu_org\">&lt;araujo_at_es_gnu_org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Luis is a GNU Project webmaster; he is also a free software programmer."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Luis M. Arteaga</strong> &lt;<a href= "
"\"mailto:lmiguel@gnu.org\">lmiguel@gnu.org</a>&gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Formerly administered web pages written in other languages apart from "
"English. Also coordinated the <a "
"href=\"/server/standards/README.translations.html\"> translation efforts</a> "
"until he retired in 2003.  He volunteers since 1999 and lives currently in "
"Germany.  Occasionally gives speeches about free software."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Mark Par</strong> <a href= "
"\"mailto:markp@ph.inter.net\">&lt;markp@ph.inter.net&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Mark is a GNU Project webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<a href=\"http://www.mhatta.org/\"><strong>Masayuki Hatta "
"(a.k.a. mhatta)</strong></a> <a href= "
"\"mailto:mhatta@gnu.org\">&lt;mhatta@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Was the chief GNU translation coordinator, who administers <a "
"href=\"/server/standards/README.translations.html\"> web translation "
"efforts</a> into other languages apart from English, after Luis "
"M. Arteaga. He is one of the GNU webmasters, too."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Matt Lee</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Was the Chief Webmaster for the <a href=\"/\">GNU Project</a> from 2005 to "
"2008, when he took a role at the <a href=\"http://www.fsf.org/\">Free "
"Software Foundation</a>."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "Rhea Myers, GNU Chief Webmaster, 2008-2010"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Rhea Myers is an artist and programmer who has been involved in free "
"software and free culture for over a decade."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Michael Presley</strong> <a href= "
"\"mailto:mpresley@member.fsf.org\">&lt;mpresley@member.fsf.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Michael was recruited for webmaster duty by RMS."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong id=\"demrit\">Miguel Vázquez Gocobachi</strong> &lt;<a href= "
"\"mailto:demrit@gnu.org\">demrit@gnu.org</a>&gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"GNU webmasters working with XHTML validation for www.gnu.org. Also he is a "
"member of GNU Spanish Translation Team and he is working in his country "
"(Mexico) promoting the GNU philosophy."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Miquel Puigpelat</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Works on the translation into Catalan of the GNU webpages and coordinates "
"the Catalan translation team. He also maintains a personal site about <a "
"href=\"http://www.puigpe.org/\">free software</a> and related issues."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Moises Martinez</strong> <a href= "
"\"mailto:moimart@moimart.org\">&lt;moimart@moimart.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "A spanish developer and GNU webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong><a href=\"mailto:navaneeth@gnu.org\">Navaneeth</a></strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "GNU Webmaster, developer and GNU Malayalam translator."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Neelakanth Nadgir</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "worked as a webmaster for www.gnu.org."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Ofer Waldman</strong> (a.k.a the duke)  &lt;the_duke at "
"intermail.co.il&gt;"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"was a GNU project <a href=\"/server/standards/README.translations.html\">web "
"translations</a> co-coordinator and backup person with Masayuki Hatta."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Pablo Palazon</strong> <a href= "
"\"mailto:ppalazon_at_gnu_org\">&lt;ppalazon<strong>@</strong>gnu "
"<strong>.</strong>org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "He's student in computer science, and GNU webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Paul Visscher</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Bkuhn recruited paulv to be a GNU and FSF Webmaster.  After six months "
"webmastering, Paul took over the position of Chief Webmaster from Jonas "
"Öberg."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Prashant Srinivasan</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Is a webmaster for the <a href=\"/\">GNU</a> website, he also does other "
"miscellaneous things around the site :-)."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Quentin Pradet</strong> <a "
"href=\"mailto:quentin@pradet.me\">&lt;quentin@pradet.me&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Quentin was a GNU webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong><a href=\"http://ramprasadb.blogspot.fr/\">Ramprasad B</a></strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"He is the GNU Emacs w32 <a href=\"/software/emacs/windows/big.html\">FAQ</a> "
"maintainer recruited by <a href=\"http://www.stallman.org/\">RMS</a>, and <a "
"href=\"/people/\">GNU Project Webmaster</a> from Bangalore, India. You can "
"reach him at &lt;ramprasad at gnu period org&gt;"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong><a href=\"http://www.soucy.org/\">Ray P.  Soucy</a></strong> <a "
"href= \"mailto:rps@soucy.org\">&lt;rps@soucy.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Ray was a GNU Project webmaster, among other things."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Reed Loden</strong> <a href= "
"\"mailto:reed_at_gnu.org\">&lt;reed<b>@</b>gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Reed is a volunteer <a href=\"/\">GNU</a> Webmaster, an <a "
"href=\"http://member.fsf.org/join?referrer=3569\">Associate Member</a> of "
"the <a href=\"http://www.fsf.org/\">FSF</a>, and a programmer of <a "
"href=\"/philosophy/free-sw.html\">free software</a>."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<a href=\"http://www.stallman.org/\"><strong>Richard Stallman</strong></a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Founded the GNU Project and the Free Software Foundation.  He also works on "
"the web pages."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong><a href=\"https://rob.musials.net\">Rob Musial</a> </strong> <a "
"href=\"mailto:robmusial@gnu.org\">&lt;robmusial<b>@</b>gnu.org&gt; </a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Rob is a <a href=\"/\">GNU</a> Webmaster, an <a "
"href=\"http://www.fsf.org/register_form?referrer=9143\">Associate Member</a> "
"of the <a href=\"http://www.fsf.org/\">FSF</a> since 2003, and a programmer "
"of <a href=\"/philosophy/free-sw.html\">free software</a>."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Sam Marx</strong> <a href= "
"\"mailto:sam@obsidian.co.za\">&lt;sam@obsidian.co.za&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Sam is a GNU Project webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Shailesh Ghadge</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "GNU Webmaster"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "<strong>Steve Hoeg</strong>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Steve is a GNU Project webmaster and free software developer."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Steven Richards</strong> <a href= "
"\"mailto:sbrichards@gnu.org\">&lt;sbrichards@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Steven is a GNU Project webmaster and a free software developer from "
"Seattle."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<a href=\"http://www.sauter-online.de/\"><strong>Thorsten "
"Sauter</strong></a> <a href= "
"\"mailto:tsauter@gmx.net\">&lt;tsauter@gmx.net&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Thorsten is a GNU Project webmaster; he is also a Debian GNU/Linux "
"developer."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Vivek Varghese Cherian</strong> <a href= "
"\"mailto:vivekvc@gnu.org\">&lt;vivekvc@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid ""
"Vivek Varghese Cherian, is a mechanical engineer from Cochin, India. He was "
"a GNU project webmaster."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid ""
"<strong>Wacław Jacek</strong> <a "
"href=\"mailto:wwj@gnu.org\">&lt;wwj@gnu.org&gt;</a>"
msgstr ""

#. type: Content of: <div><dl><dd>
msgid "Webmastering the GNU since 2010."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, "
"2024 Free Software Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

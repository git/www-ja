# LANGUAGE translation of https://www.gnu.org/graphics/emacs-ref.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: emacs-ref.html\n"
"POT-Creation-Date: 2023-03-12 09:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "GNU Emacs Reference Card - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "GNU Emacs Reference Card"
msgstr ""

#. type: Content of: <address>
msgid "by Loic Duros and FSF Staff"
msgstr ""

#. type: Content of: <div><a>
msgid "<a href=\"/graphics/gnuemacsref.png\">"
msgstr ""

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Reference card with GNU face]&nbsp;"
msgstr ""

#. type: Content of: <div>
msgid "</a>"
msgstr ""

#. type: Content of: <p>
msgid ""
"A wallpaper showing a well organized list of Emacs commands with a GNU face "
"in the center. The yellowish background looks like an old piece of paper, "
"wrinkled and stained by years of intensive use."
msgstr ""

#. type: Content of: <ul><li>
msgid "JPEG&nbsp; <a href=\"/graphics/emacs-ref.jpg\">63kB</a> (400x225)"
msgstr ""

#. type: Content of: <ul><li>
msgid "PNG&nbsp; <a href=\"/graphics/gnuemacsref.png\">1.8MB</a> (1366x768)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2010 Free Software Foundation, Inc. (<a "
"href=\"https://www.gnu.org/software/emacs/refcards/\">GNU Emacs Reference "
"Card</a>)"
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 2011 Loic Duros (Desktop background and GNU Face)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Permission is granted to make and distribute modified or unmodified copies "
"of this card provided the copyright notice and this permission notice are "
"preserved on all copies."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020, 2023 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

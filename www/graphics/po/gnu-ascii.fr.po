# French translation of http://www.gnu.org/graphics/gnu-ascii.html
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cédric Corazza, 2007.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gnu-ascii.html\n"
"POT-Creation-Date: 2020-10-06 08:26+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"ASCII Gnu by Przemys&#322;aw Borys - GNU Project - Free Software Foundation"
msgstr ""
"GNU en ASCII, de Przemys&#322;aw Borys - Projet GNU - Free Software "
"Foundation"

#. type: Content of: <h2>
msgid "ASCII Gnu"
msgstr "GNU en ASCII"

#. type: Content of: <address>
msgid "by Przemys&#322;aw Borys"
msgstr "de Przemys&#322;aw Borys"

#. type: Content of: <div><pre>
#, no-wrap
msgid ""
"  ,           , \n"
" /             \\ \n"
"((__-^^-,-^^-__)) \n"
" `-_---' `---_-' \n"
"  `--|o` 'o|--' \n"
"     \\  `  / \n"
"      ): :( \n"
"      :o_o: \n"
"       \"-\" \n"
msgstr ""
"  ,           ,\n"
" /             \\\n"
"((__-^^-,-^^-__))\n"
" `-_---' `---_-'\n"
"  `--|o` 'o|--'\n"
"     \\  `  /\n"
"      ): :(\n"
"      :o_o:\n"
"       \"-\"\n"

#. type: Content of: <div><p>
msgid ""
"This is an ASCII drawing of a gnu's head, suitable for non-graphical "
"terminals."
msgstr ""
"Voici un dessin en ASCII d'une tête de gnou, adaptée aux terminaux non "
"graphiques."

#. type: Content of: <div><div><p>
msgid "Copyright &copy; 2001 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2001 Free Software Foundation, Inc."

#. type: Content of: <div><div><p>
msgid ""
"This work is available under the <a rel=\"license\" href=\"/licenses/old-"
"licenses/gpl-2.0.html\">GNU General Public License</a>, version&nbsp;2 or "
"any later version, or the <a rel=\"license\" href=\"/licenses/fdl.html\">GNU "
"Free Documentation License</a>, version&nbsp;1.1 or any later version."
msgstr ""
"Cette image est disponibe sous la <a href=\"/licenses/old-licenses/gpl-2.0."
"html\">licence publique générale GNU</a>, version 2 ou toute version "
"ultérieure, ou bien la <a href=\"/licenses/fdl.html\">licence GNU de "
"documentation libre</a>, version 1.1 ou toute version ultérieure."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""
"<em>Texte de la page :</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" href=\"https://creativecommons.org/"
"licenses/by-nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 "
"International License</a>."
msgstr ""
"Disponible sous la licence <a rel=\"license\" href=\"https://creativecommons."
"org/licenses/by-nd/4.0/deed.fr\">Creative Commons attribution, pas de "
"modification, 4.0 internationale</a> (CC BY-ND 4.0)."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza<br />Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

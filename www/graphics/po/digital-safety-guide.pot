# LANGUAGE translation of https://www.gnu.org/graphics/digital-safety-guide.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: digital-safety-guide.html\n"
"POT-Creation-Date: 2024-12-11 12:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Digital Safety Guide by Jeison Yehuda Amihud - GNU Project - Free Software "
"Foundation"
msgstr ""

#. type: Attribute 'title' of: <div><div><a><img>
msgid "Digital Safety Guide"
msgstr ""

#. type: Content of: <address>
msgid "by Jeison Yehuda Amihud"
msgstr ""

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/digital_safety_guide.jpg\">"
msgstr ""

#. type: Attribute 'alt' of: <div><div><a><img>
msgid ""
"&nbsp;[Poster describing the dangers of proprietary software and how to "
"replace it with free/libre software]&nbsp;"
msgstr ""

#. type: Content of: <div><div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This poster is a good resource to briefly show the dangers of nonfree "
"software and the grave consequences for its users. It includes a brief list "
"of free/libre replacements for the most common proprietary programs, with "
"links to find more."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"JPEG&nbsp; &ensp;<a "
"href=\"/graphics/digital_safety_guide.jpg\">122kB</a>&nbsp;(794&times;1122)"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "ODG&nbsp; &ensp;<a href=\"/graphics/digital_safety_guide.odg\">1.1MB</a>"
msgstr ""

#. type: Content of: <div><p>
msgid "Spanish version:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "PDF&nbsp; &ensp;<a href=\"/graphics/digital_safety_guide.es.pdf\">262kB</a>"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Copyright (English original version) &copy; 2022 Jeison Yehuda Amihud"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Copyright (Spanish version) &copy; 2024 Jordán and Iván Ávalos"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"These posters are available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons "
"Attribution ShareAlike 4.0 International License</a>"
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2024 Free Software Foundation, "
"Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

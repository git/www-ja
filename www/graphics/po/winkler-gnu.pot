# LANGUAGE translation of https://www.gnu.org/graphics/winkler-gnu.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: winkler-gnu.html\n"
"POT-Creation-Date: 2020-10-06 08:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Alternative GNU Logo by Kyle Winkler - GNU Project - Free Software "
"Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "Alternative GNU Logo"
msgstr ""

#. type: Content of: <address>
msgid "by Kyle Winkler"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "GNU logo"
msgstr ""

#. type: Content of: <div><dl><dd><div><a>
msgid "<a href=\"/graphics/winkler-gnu.png\">"
msgstr ""

#. type: Attribute 'alt' of: <div><dl><dd><div><a><img>
msgid "&nbsp;[Kyle's Alternative GNU logo]&nbsp;"
msgstr ""

#. type: Content of: <div><dl><dd><div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><dl><dd><p>
msgid ""
"The word &ldquo;GNU&rdquo;, underlined and topped with curly horns (based on "
"Aur&eacute;lio A. Heckert's <a href=\"/graphics/heckert_gnu.html\">Bold GNU "
"Head</a>)."
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"PNG&nbsp; <a href=\"/graphics/winkler-gnu-small.png\">5kB</a> "
"(203x100),&nbsp; <a "
"href=\"/graphics/winkler-gnu.png\">39kB</a>&nbsp;(1275x629)"
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid "SVG&nbsp; <a href=\"/graphics/winkler-gnu.svg\">17kB</a> (1275x629)"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "Blue wallpaper"
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid "PNG&nbsp; <a href=\"/graphics/winkler-gnu-blue.png\">93kB</a> (1280x800)"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Copyright &copy; 2007 Kyle Winkler"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"These images are available under the <a rel=\"license\" "
"href=\"https://directory.fsf.org/wiki/License:FAL1.3\">Free Art License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

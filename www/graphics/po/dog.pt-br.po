# Brazilian Portuguese translation of https://www.gnu.org/graphics/dog.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Rafael Fontenelle <rafaelff@gnome.org>, 2016-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: dog.html\n"
"POT-Creation-Date: 2020-10-06 08:26+0000\n"
"PO-Revision-Date: 2020-12-13 17:41-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#. type: Content of: <title>
msgid "Dog Cartoon - GNU Project - Free Software Foundation"
msgstr "Imagem de Cachorro - Projeto GNU - Free Software Foundation"

#. type: Content of: <h2>
msgid "Dog Cartoon"
msgstr "Imagem de Cachorro"

#. type: Content of: <address>
msgid "by Richard Stallman and Antonomakia"
msgstr "por Richard Stallman e Antonomakia"

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/dog.small.jpg\">"
msgstr "<a href=\"/graphics/dog.small.jpg\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[Dog, wondering at pop-up ads]&nbsp;"
msgstr "&nbsp;[Cachorro, em dúvida sobre umas propagandas]&nbsp;"

#. type: Content of: <div><div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><p>
msgid ""
"A cute beagle is sitting by his computer, which shows pop-up ads for a yummy "
"bone, a dog toy, and a comfortable-looking doggy bed. These ads partially "
"cover the text of an article by Richard Stallman, <a href=\"/philosophy/"
"surveillance-vs-democracy\">How Much Surveillance Can Democracy Withstand?</"
"a> (only visible in the large picture). The dog is wondering, <em>&ldquo;How "
"did they find out I'm a dog?&rdquo;</em>"
msgstr ""
"Um beagle fofo está sentado próximo ao seu computador, que mostra "
"propagandas para um osso delicioso, um brinquedo de cachorro e uma cama de "
"cachorro que parece confortável. Essas propagandas cobrem o texto de um "
"artigo por Richard Stallman, <a href=\"/philosophy/surveillance-vs-democracy"
"\">Qual o Nível de Vigilância Que a Democracia Pode Suportar?</a> (visível "
"apenas na imagem grande). O cachorro está se perguntando <em>“Como eles "
"descobriram que eu sou um cachorro”</em>"

#. type: Content of: <div><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/dog.small.jpg\">58kB</a> (600x464),&nbsp; <a "
"href=\"https://stallman.org/images/dog.jpg\">6.5MB</a>&nbsp;(3300x2550)"
msgstr ""
"JPEG&nbsp; <a href=\"/graphics/dog.small.jpg\">58kB</a> (600x464),&nbsp; <a "
"href=\"https://stallman.org/images/dog.jpg\">6.5MB</a>&nbsp;(3300x2550)"

#. type: Content of: <div><div><p>
msgid "Copyright &copy; 2014 Richard Stallman and Antonomakia"
msgstr "Copyright &copy; 2014 Richard Stallman and Antonomakia"

#. type: Content of: <div><div><p>
msgid ""
"This image is available under the <a rel=\"license\" href=\"https://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Essa imagem está disponível sob a licença <a rel=\"license\" href=\"https://"
"creativecommons.org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e o "
"envio de traduções das páginas deste site."

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""
"<em>Texto da página:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" href=\"https://creativecommons.org/"
"licenses/by-nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 "
"International License</a>."
msgstr ""
"Disponível sob a licença <a rel=\"license\" href=\"https://creativecommons."
"org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons Atribuição-"
"SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle <a href=\"mailto:rafaelff@gnome.org\">&lt;"
"rafaelff@gnome.org&gt;</a>, 2016-2020"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"

#~ msgid "This picture is available in the following formats:"
#~ msgstr "Essa imagem está disponível nos seguintes formatos:"

#~ msgid "large jpg <a href=\"https://stallman.org/images/dog.jpg\">6.5MB</a>"
#~ msgstr ""
#~ "jpg grande <a href=\"https://stallman.org/images/dog.jpg\">6,5MB</a>"

#~ msgid ""
#~ "It is released under the <a rel=\"license\" href=\"http://creativecommons."
#~ "org/licenses/by-nd/4.0/\">Creative Commons Attribution-NoDerivs 4.0 "
#~ "International License</a>."
#~ msgstr ""
#~ "Ela foi lançada sob uma licença <a rel=\"license\" href=\"http://"
#~ "creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Atribuição-"
#~ "SemDerivações 4.0 Internacional</a>."

#~ msgid ""
#~ "This image is available for use under the terms of the <a rel=\"license\" "
#~ "href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
#~ "Attribution-NoDerivs 4.0 International License</a>."
#~ msgstr ""
#~ "Essa imagem está disponível sob os termos da licença <a rel=\"license\" "
#~ "href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.pt_BR"
#~ "\">Creative Commons Atribuição-SemDerivações 4.0 Internacional</a>."

# LANGUAGE translation of https://www.gnu.org/graphics/supergnu-ascii.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: supergnu-ascii.html\n"
"POT-Creation-Date: 2020-10-06 08:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "ASCII Super Gnu by Vijay Kumar - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "ASCII Super Gnu"
msgstr ""

#. type: Content of: <address>
msgid "by Vijay Kumar"
msgstr ""

#. type: Content of: <div><pre>
#, no-wrap
msgid ""
"   \t                            .   .\n"
"\t                           /'   `\\\n"
"\t                          //     \\\\\n"
"\t                         ( ~\\\\~//~ )\n"
"\t                          ~| @\"@ |~\n"
"\t                           '  :  ' .\n"
"\t                  / \\~\\~\\~  \\ : /' ~/~/~/\\\n"
"\t                 / \\        (._.)        / \\\n"
"\t                 /   \\___-/   ;   \\-___/   \\\n"
"\t               /      |               |      \\\n"
"\t              /      /    ---___---    \\      \\\n"
"\t              (   ./      \\ / __  /      \\.   ?\n"
"\t             /   /  (  --  .|   |.  --  ?  \\   \\\n"
"\t             :   ,.  \\      \\---/      /   / ' :\n"
"\t            | \\_/  '   (     \\./     ?    \\___/,\n"
"\t              \\    `.__ |           | __.'    /|\n"
"\t            |   \\      : ---_____--- :      /  |\n"
"\t                  `-___:`---_|_|_---':___-'\n"
"\t            |           /\\         /\\          |\n"
"\t                       /  \\       /  \\\n"
"\t            |         /     \\   /     \\        |\n"
"\t                      |       v       |\n"
"\t            |         |       |       |        |\n"
"\t            |         |       |       |        |\n"
"\t                      \\      / \\      /        |\n"
"\t            |          |    |   |    |\n"
"\t                        \\  /     \\  /          |\n"
"\t           |            [  ]     [  ]\n"
"\t             /  /      \\-..-/   \\-..-/     \\   \\\n"
"\t          /            |    |   |    |\n"
"\t            /  /    /   |  |     |  |   \\     \\    \\\n"
"\t        /  ~ ~  ~  ~    :  :     :  :   ~   ~ ~ ~   \\\n"
"\t       ~ ~              :__:     :__:              ~ ~\n"
"\t                       (____)   (____)\n"
"\n"
"\t       _____                          _______   ____  __\n"
"\t      / ___/__  ______  ___  _____   / ____/ | / / / / /\n"
"\t      \\__ \\/ / / / __ \\/ _ \\/ ___/  / / __/  |/ / / / /\n"
"\t     ___/ / /_/ / /_/ /  __/ /     / /_/ / /|  / /_/ /\n"
"\t    /____/\\__,_/ .___/\\___/_/      \\____/_/ |_/\\____/\n"
" \t              /_/\n"
"\n"
" ___       __             _                __   _   _           ___ ___ _\n"
"|   \\ ___ / _|___ _ _  __| |___ _ _   ___ / _| | |_| |_  ___   / __| _ \\ "
"|\n"
"| |) / -_)  _/ -_) ' \\/ _` / -_) '_| / _ \\  _| |  _| ' \\/ -_) | (_ |  _/ "
"|__\n"
"|___/\\___|_| \\___|_||_\\__,_\\___|_|   \\___/_|    \\__|_||_\\___|  "
"\\___|_| |____|\n"
"\n"
"\n"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Copyright &copy; 2003 Vijay Kumar"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"This work is available under the <a rel=\"license\" "
"href=\"/licenses/old-licenses/gpl-2.0.html\">GNU General Public License</a>, "
"version&nbsp;2 or any later version, or the <a rel=\"license\" "
"href=\"/licenses/fdl.html\">GNU Free Documentation License</a>, "
"version&nbsp;1.1 or any later version."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

# LANGUAGE translation of https://www.gnu.org/graphics/fromagnulinux.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: fromagnulinux.html\n"
"POT-Creation-Date: 2020-10-06 08:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Fromagnulinux by Denis Trimaille - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "Fromagnulinux"
msgstr ""

#. type: Content of: <address>
msgid "by Denis Trimaille"
msgstr ""

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/fromagnulinux.png\">"
msgstr ""

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[Fromagnulinux]&nbsp;"
msgstr ""

#. type: Content of: <div><div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Denis created &ldquo;The smiling gnu&rdquo; <cite>(Le gnou qui "
"sourit)</cite> on the model of &ldquo;The Laughing Cow&rdquo; <cite>(La "
"vache qui rit)</cite>.  Many people will recognize the origin of this "
"artwork. It's all about a French cheese in small round boxes. The English "
"title (From a GNU/Linux) is phonetically similar to the French title "
"<cite>&ldquo;Fromage GNU/Linux&rdquo;</cite> (Cheese GNU/Linux)."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This image is based on <a href=\"/graphics/agnuhead.html\">A GNU Head</a>, "
"by Etienne Suvasa, and <a "
"href=\"https://commons.wikimedia.org/wiki/File:Tux.png\">Tux</a>, by Larry "
"Ewing."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"PNG&nbsp; <a href=\"/graphics/fromagnulinux.tiny.png\">22kB</a> (130x130), "
"<a href=\"/graphics/fromagnulinux.png\">112kB</a> (354x354), <a "
"href=\"/graphics/fromagnulinux.big.png\">280kB</a> (1024x1024)"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "XCF&nbsp; <a href=\"/graphics/fromagnulinux.xcf\">677kB</a> (1024x1024)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Also take a look at <a "
"href=\"http://web.archive.org/web/20060719060454/http://www.linuxfranch-county.org/artlinux.html\"> "
"Denis' web page</a> [Archived]."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Copyright &copy; 2004, 2005 Cr&eacute;ations LinuxFranch-County"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"This image is available under the <a rel=\"license\" "
"href=\"https://directory.fsf.org/wiki/License:FAL1.3\">Free Art License</a>."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"The GNU head is, however, also a trademark for the GNU Project.  <strong>If "
"you want to use the GNU head to link to a website run by the Free Software "
"Foundation or the GNU project, feel free, or if you're using it in contexts "
"talking about GNU in a supportive and accurate way, you can also do this "
"without permission.  For any other requests, please ask &lt;<a "
"href=\"mailto:licensing@fsf.org\">licensing@fsf.org</a>&gt; for permission "
"first.</strong>"
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

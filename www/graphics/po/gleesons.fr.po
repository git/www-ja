# French translation of http://www.gnu.org/graphics/gleesons.html
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Frédéric Couchet, 2007.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gleesons.html\n"
"POT-Creation-Date: 2020-10-06 08:26+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Gleeson's GNU Art - GNU Project - Free Software Foundation"
msgstr "Art GNU de Gleeson - Projet GNU - Free Software Foundation"

#. type: Content of: <h2>
msgid "GNU Art"
msgstr "Art GNU"

#. type: Content of: <address>
msgid "by Brendon Gleeson"
msgstr "de Brendon Gleeson"

#. type: Content of: <div><div><p>
msgid ""
"You may prefer to use the <a href=\"/graphics/heckert_gnu.html\">Bold GNU "
"Head</a>, which is the version now used by the Free Software Foundation on "
"any new GNU-emblem items and stickers."
msgstr ""
"Vous souhaiterez peut-être utiliser la <a href=\"/graphics/heckert_gnu.html"
"\">tête de GNU contrastée</a>. C'est la version utilisée actuellement par la "
"Free Software Foundation sur tous les nouveaux objets portant l'emblème de "
"GNU, ainsi que sur les autocollants."

#. type: Content of: <div><p>
msgid ""
"This image is inspired from <a href=\"/graphics/agnuhead.html\">A GNU Head</"
"a> but is in color. Two variants are also offered, perhaps for linking to "
"the GNU project."
msgstr ""
"Ce dessin est inspiré de la <a href=\"/graphics/agnuhead.html\">Tête de GNU</"
"a>, avec la couleur en plus. Deux variantes en sont proposées, peut-être "
"utilisables pour mettre en lien le projet GNU."

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/gleeson_head.jpg\">"
msgstr "<a href=\"/graphics/gleeson_head.jpg\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[Colorful rounded image of the head of a GNU]&nbsp;"
msgstr " [Image colorée d'une tête de GNU avec impression de relief] "

#. type: Content of: <div><div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><dl><dt>
msgid "Simple head"
msgstr "Tête isolée"

#. type: Content of: <div><dl><dd><p>
msgid ""
"It was used until 2007 to help identify pages in the <a href=\"/fun/humor."
"html\">GNU Humor</a> section. This is also the logo of GNU TeXmacs."
msgstr ""
"Elle a servi jusqu'en 2007 à identifier les pages de la section <a href=\"/"
"fun/humor.html\">Rions avec le GNU</a>. c'est aussi le logo de GNU TeXmacs."

#. type: Content of: <div><dl><dd><ul><li>
msgid "JPEG&nbsp; <a href=\"/graphics/gleeson_head.jpg\">6kB</a> (153x128)"
msgstr "JPEG&nbsp; <a href=\"/graphics/gleeson_head.jpg\">6 ko</a> (153x128)"

#. type: Content of: <div><dl><dt>
msgid "Gnu saying &ldquo;Gnu's Not Unix&rdquo;"
msgstr "Gnu disant « Gnu's Not Unix »"

#. type: Content of: <div><dl><dd><ul><li>
msgid "JPEG&nbsp; <a href=\"/graphics/gnu_not_unix.jpg\">17kB</a> (342x186)"
msgstr "JPEG&nbsp; <a href=\"/graphics/gnu_not_unix.jpg\">17 ko</a> (342x186)"

#. type: Content of: <div><dl><dt>
msgid "GNU &mdash; Free Software Foundation"
msgstr "GNU – Free Software Foundation"

#. type: Content of: <div><dl><dd><ul><li>
msgid "JPEG&nbsp; <a href=\"/graphics/gleeson_logo.jpg\">12kB</a> (371x120)"
msgstr "JPEG&nbsp; <a href=\"/graphics/gleeson_logo.jpg\">12 ko</a> (371x120)"

#. type: Content of: <div><div><p>
msgid ""
"These images are available under the <a rel=\"license\" href=\"https://"
"creativecommons.org/publicdomain/zero/1.0/\">Creative Commons Zero 1.0 "
"Universal Public Domain Dedication</a>."
msgstr ""
"Ces images ont été versées au domaine public selon les termes de <a rel="
"\"license\" href=\"https://creativecommons.org/publicdomain/zero/1.0/"
"\">Creative Commons Zero 1.0 universel (CC0)</a>."

#. type: Content of: <div><div><p>
msgid ""
"The GNU head is, however, also a trademark for the GNU Project.  <strong>If "
"you want to use the GNU head to link to a website run by the Free Software "
"Foundation or the GNU project, feel free, or if you're using it in contexts "
"talking about GNU in a supportive and accurate way, you can also do this "
"without permission.  For any other requests, please ask &lt;<a href=\"mailto:"
"licensing@fsf.org\">licensing@fsf.org</a>&gt; for permission first.</strong>"
msgstr ""
"Toutefois, la tête de GNU est aussi un logo déposé du projet GNU. <strong>Si "
"vous voulez vous servir de cette tête de GNU pour mettre en lien un site web "
"de la <i>Free Software Foundation</i> ou du projet GNU, n'hésitez pas ; de "
"même, vous n'avez pas besoin de permission supplémentaire pour l'utiliser "
"dans des contextes qui parlent de GNU de manière positive et exacte. Pour "
"tout autre usage, veuillez au préalable demander la permission à <a href="
"\"mailto:licensing@fsf.org\">licensing@fsf.org</a>.</strong>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""
"<em>Texte de la page :</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" href=\"https://creativecommons.org/"
"licenses/by-nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 "
"International License</a>."
msgstr ""
"Disponible sous la licence <a rel=\"license\" href=\"https://creativecommons."
"org/licenses/by-nd/4.0/deed.fr\">Creative Commons attribution, pas de "
"modification, 4.0 internationale</a> (CC BY-ND 4.0)."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza<br />Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

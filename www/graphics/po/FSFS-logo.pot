# LANGUAGE translation of https://www.gnu.org/graphics/FSFS-logo.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FSFS-logo.html\n"
"POT-Creation-Date: 2020-10-06 08:26+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Free Software, Free Society Title Logo - GNU Project - Free Software "
"Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "The book cover logo for <cite>Free Software, Free Society</cite>"
msgstr ""

#. type: Content of: <address>
msgid "by Jonathan Richard"
msgstr ""

#. type: Content of: <div><p>
msgid "This logo was on the hard cover of the first edition (2002)."
msgstr ""

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/FSFS-title2.jpg\">"
msgstr ""

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[The book cover logo for 'Free Software, Free Society']&nbsp;"
msgstr ""

#. type: Content of: <div><div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "JPEG&nbsp; <a href=\"/graphics/FSFS-title2.jpg\">17kB</a> (555x117)"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"This image is available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/publicdomain/zero/1.0/\">Creative Commons "
"Zero 1.0 Universal Public Domain Dedication</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

# LANGUAGE translation of https://www.gnu.org/graphics/gnulove.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnulove.html\n"
"POT-Creation-Date: 2022-10-14 18:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "GNU Love by Alison Upton - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "GNU Love"
msgstr ""

#. type: Content of: <address>
msgid "by Alison Upton"
msgstr ""

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/Gnulove.jpg\">"
msgstr ""

#. type: Attribute 'title' of: <div><div><a><img>
msgid "Tux hugs Gnu"
msgstr ""

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[Tux hugs Gnu]&nbsp;"
msgstr ""

#. type: Content of: <div><div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Freedo is hugging Gnu's back with a contented smile and eyes closed, while "
"Gnu kindly looks at him."
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "Original image: Tux hugs Gnu"
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid "PNG&nbsp; <a href=\"/graphics/gnulove-thumb.png\">22kB</a> (140x90)"
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/Gnulove.medium.jpg\">70kB</a> "
"(500×321),&nbsp; <a href=\"/graphics/Gnulove.jpg\">437kB</a>&nbsp;(1250×802)"
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"TIF 400 dpi&nbsp; <a href=\"/graphics/GNULove400dpi.tif\">7.2MB</a> "
"(2175×1300)"
msgstr ""

#. type: Content of: <div><dl><dt>
msgid "Variant: Freedo hugs Gnu"
msgstr ""

#. type: Content of: <div><dl><dd><p>
msgid ""
"Tux took a bath and became Freedo, the Linux-libre mascot. In 2022, the "
"original image was modified by iShareFreedom."
msgstr ""

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/Gnulove-freedo.medium.jpg\">60kB</a> "
"(500×321),&nbsp; <a "
"href=\"/graphics/Gnulove-freedo.jpg\">977kB</a>&nbsp;(1250×802)"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Copyright &copy; 2011 Alison Upton &lt;<a "
"href=\"mailto:illustration@alisonupton.co.uk\">illustration@alisonupton.co.uk</a>&gt;"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"These images are available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by/3.0/\">Creative Commons "
"Attribution 3.0 Unported License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2022 Free Software "
"Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" "
"href=\"https://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

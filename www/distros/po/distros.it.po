# Italian translation of distros.html
# Copyright (C) 2010 Free Software Foundation, Inc.
# Riccardo Pili, 2010.
# Revised by Luca Padrin, Andrea Pescetti, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: distros.html\n"
"POT-Creation-Date: 2022-01-22 17:57+0000\n"
"PO-Revision-Date: 2022-02-11 23:04+0100\n"
"Last-Translator: Andrea Pescetti <pescetti@gnu.org>\n"
"Language-Team: Italian\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.3\n"

# type: Content of: <h3>
#. type: Content of: <title>
msgid "GNU/Linux Distros - GNU Project - Free Software Foundation"
msgstr "Distribuzioni GNU/Linux - Progetto GNU - Free Software Foundation"

# type: Content of: <h3>
#. type: Attribute 'title' of: <link>
msgid "Free GNU/Linux distributions"
msgstr "Distribuzioni GNU/Linux libere"

# type: Content of: <h2>
#. type: Content of: <div><h2>
msgid "GNU/Linux Distros"
msgstr "Distribuzioni GNU/Linux"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Free distributions (or &ldquo;distros&rdquo;) of the <a href=\"/gnu/linux-"
"and-gnu.html\">GNU/Linux system</a> only include and only propose free "
"software.  They reject nonfree applications, nonfree programming platforms, "
"nonfree drivers, nonfree firmware &ldquo;blobs,&rdquo; and any other nonfree "
"software and documentation.  If they discover that by mistake some had been "
"included, they remove it."
msgstr ""
"Le distribuzioni (o \"distro\") libere del <a href=\"/gnu/linux-and-gnu.html"
"\">sistema GNU/Linux</a> includono o propongono solamente software libero. "
"Rifiutano le applicazioni non libere, le piattaforme di programmazione non "
"libere, i driver non liberi, firmware (&ldquo;blob,&rdquo;) non liberi e "
"qualunque altro programma o documentazione non liberi. Se queste "
"distribuzioni scoprono che per errore qualche parte non libera è stata "
"inclusa, la eliminano."

# type: Content of: <h3>
#. type: Content of: <div><h3>
msgid "Free GNU/Linux Distros"
msgstr "Distribuzioni GNU/Linux libere"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"We recommend that you use a free GNU/Linux system distribution, one that "
"does not include proprietary software at all.  That way you can be sure that "
"you are not installing any nonfree programs. Here is our list of such "
"distros:"
msgstr ""
"Noi raccomandiamo l'uso di distribuzioni GNU/Linux libere, che non includano "
"neanche in piccola parte programmi proprietari. In questo modo si può essere "
"sicuri che non si sta installando alcun programma non libero. Un elenco di "
"queste distribuzioni:"

#. type: Content of: <div><p>
msgid ""
"<a href=\"/distros/free-distros.html\"> Free GNU/Linux distributions</a>."
msgstr ""
"<a href=\"/distros/free-distros.html\"> Distribuzioni libere GNU/Linux</a>."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"All of these existing distros could use more development help.  Thus, if you "
"want to make an effective contribution to free GNU/Linux distributions, we "
"suggest that you join the development of an existing free distro rather that "
"starting a new free distro."
msgstr ""
"Ognuna di queste distribuzioni esistenti ha bisogno di più aiuto per il suo "
"sviluppo. Per questo se volete dare un effettivo contributo alle "
"distribuzioni GNU/Linux libere, vi suggeriamo di aderire allo sviluppo di "
"una di queste distribuzioni già esistenti piuttosto che iniziare una nuova "
"distribuzione."

# type: Content of: <h3>
#. type: Content of: <div><h3>
msgid "Free Non-GNU Distros"
msgstr "Distribuzioni non GNU/Linux libere"

#. type: Content of: <div><p>
msgid ""
"These system distributions are free but quite different from GNU.  Using "
"them is not similar to using GNU/Linux.  However, they satisfy the same "
"ethical criteria that we apply to GNU/Linux distros."
msgstr ""
"Queste distribuzioni di sistema sono libere ma abbastanza diverse da GNU. "
"Usarle non è come usare GNU/Linux. Tuttavia soddisfano gli stessi criteri "
"etici che applichiamo alle distribuzioni GNU/Linux."

#. type: Content of: <div><p>
msgid ""
"<a href=\"/distros/free-non-gnu-distros.html\"> Free Non-GNU distributions</"
"a>."
msgstr ""
"<a href=\"/distros/free-non-gnu-distros.html\"> Distribuzioni libere non GNU/"
"Linux</a>."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"All of these existing distros could use more development help.  Thus, if you "
"want to make an effective contribution in this area, we suggest that you "
"join the development of an existing free distro rather that starting a new "
"free distro."
msgstr ""
"Ognuna di queste distribuzioni esistenti ha bisogno di più aiuto per il suo "
"sviluppo. Per questo, se volete dare un effettivo contributo in quest'area, "
"vi suggeriamo di aderire allo sviluppo di una di queste distribuzioni già "
"esistenti piuttosto che iniziare una nuova distribuzione."

# type: Content of: <h3>
#. type: Content of: <div><h3>
msgid "Free Distro Guidelines"
msgstr "Linee guida per le distribuzioni libere"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Here is the list of problems that can prevent a distro from being considered "
"entirely free:"
msgstr ""
"Un elenco di problemi che possono fare in modo che una distribuzione non "
"venga considerata completamente libera:"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"<a href=\"/distros/free-system-distribution-guidelines.html\"> Guidelines "
"for free system distributions</a>."
msgstr ""
"<a href=\"/distros/free-system-distribution-guidelines.html\"> Linee guida "
"per distribuzioni libere</a>."

# type: Content of: <h3>
#. type: Content of: <div><h3>
msgid "Common Distros"
msgstr "Distribuzioni comuni"

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"Many common and well-known GNU/Linux software distributions don't meet our "
"guidelines. You can read about their problems here:"
msgstr ""
"Molte distribuzioni GNU/Linux comuni e conosciute non rispettano le nostre "
"linee guida. Si può leggere il perché qui:"

#. type: Content of: <div><p>
msgid ""
"<a href=\"/distros/common-distros.html\"> Why we can't endorse many well-"
"known GNU/Linux distros</a>."
msgstr ""
"<a href=\"/distros/common-distros.html\"> Perché non approviamo molte "
"distribuzioni GNU/Linux note</a>."

# type: Content of: <p>
#. type: Content of: <div><p>
msgid ""
"We appeal to the developers of these distros to remove the nonfree parts and "
"thus make them entirely free software."
msgstr ""
"Facciamo appello agli sviluppatori di queste distribuzioni affinché "
"rimuovano le parti non libere in modo da renderle provviste di solo software "
"libero."

#. type: Content of: <div><h3>
msgid "Optionally Free Is Not Enough"
msgstr "\"Libero se si desidera\" non basta"

#. type: Content of: <div><p>
msgid ""
"Some GNU/Linux distributions allow the user the option of installing only "
"free software. You can read:"
msgstr ""
"Alcune distribuzioni GNU/Linux danno all'utente la possibilità di scegliere "
"di installare solo software libero. Leggere:"

#. type: Content of: <div><p>
msgid ""
"<a href=\"/distros/optionally-free-not-enough.html\"> Why optionally free is "
"not enough.</a>"
msgstr ""
"<a href=\"/distros/optionally-free-not-enough.html\"> Perché \"Libero se si "
"desidera\" non basta.</a>"

#. type: Content of: <div><h3>
msgid "Why Is This Important?"
msgstr "Perché è importante?"

#. type: Content of: <div><p>
msgid ""
"When a GNU/Linux distro includes nonfree software, it causes two kinds of "
"problems:"
msgstr ""
"Quando una distribuzione GNU/Linux include software non libero, provoca due "
"tipi di problemi:"

#. type: Content of: <div><ul><li>
msgid "If you install it, you may install and use nonfree software."
msgstr "Chi la installa può trovarsi a utilizzare software non libero."

#. type: Content of: <div><ul><li>
msgid "It gives people the wrong idea of what the goal is."
msgstr "Dà l'idea sbagliata di quale sia il nostro scopo."

#. type: Content of: <div><p>
msgid ""
"The first problem is a direct problem: it affects users of the distro, if "
"they install the nonfree software.  However, the second problem is the more "
"important one, because it affects the community as a whole."
msgstr ""
"Il primo problema è un problema diretto: colpisce gli utenti della "
"distribuzione, se installano il software non libero. Però il secondo "
"problema è più importante, perché colpisce la comunità nel suo complesso."

#. type: Content of: <div><p>
msgid ""
"The developers of nonfree distros don't say, &ldquo;We apologize for the "
"presence of nonfree components in our distribution.  We don't know what "
"possessed us to include them.  We hope that next release we will keep our "
"minds on freedom.&rdquo; If they did, they would have less of a bad "
"influence."
msgstr ""
"Gli sviluppatori di distribuzioni non libere non dicono \"Scusateci per la "
"presenza di componenti non liberi nella nostra distribuzione: non sappiamo "
"cosa ci ha indotto ad includerli e speriamo che nella prossima versione "
"saremo più orientati alla libertà\". Se lo facessero, sarebbero meno dannosi."

#. type: Content of: <div><p>
msgid ""
"Instead, they generally present the nonfree software in their systems as a "
"positive feature; they say that their goal is &ldquo;the best possible user "
"experience,&rdquo; or something like that, rather than freedom.  In other "
"words, they lead people to place convenience above freedom&mdash;working "
"directly against our campaign to make freedom the primary goal."
msgstr ""
"Invece, di solito presentano il software non libero presente nei loro "
"sistemi come una caratteristica positiva; dicono che si pongono come "
"obiettivo di offrire &ldquo;un'esperienza utente ottimale&rdquo; o qualcosa "
"del genere, e non parlano di libertà. In altre parole, portano il pubblico a "
"considerare che la comodità sia più importante della libertà, e con questo "
"vanificano la nostra campagna che considera la libertà come obiettivo "
"principale."

#. type: Content of: <div><p>
msgid ""
"The fact that these distros don't deliver freedom is why we don't endorse "
"them.  That they teach people not to value freedom is why we are strongly "
"concerned about this issue."
msgstr ""
"Il motivo per cui non sosteniamo queste distribuzioni è che non diffondono "
"la libertà, e ci preoccupa in particolare il fatto che queste distribuzioni "
"insegnino a non dare valore alla libertà."

# type: Content of: <div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Per informazioni su FSF e GNU rivolgetevi, possibilmente in inglese, a <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Ci sono anche <a href="
"\"/contact/\">altri modi di contattare</a> la FSF. Inviate segnalazioni di "
"link non funzionanti e altri suggerimenti relativi alle pagine web a <a href="
"\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Le traduzioni italiane sono effettuate ponendo la massima attenzione ai "
"dettagli e alla qualità, ma a volte potrebbero contenere imperfezioni. Se ne "
"riscontrate, inviate i vostri commenti e suggerimenti riguardo le traduzioni "
"a <a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;"
"</a> oppure contattate direttamente il <a href=\"http://savannah.gnu.org/"
"projects/www-it/\">gruppo dei traduttori italiani</a>.<br/>Per informazioni "
"su come gestire e inviare traduzioni delle nostre pagine web consultate la "
"<a href=\"/server/standards/README.translations.html\">Guida alle "
"traduzioni</a>."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2009, 2012, 2015, 2022 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2009, 2012, 2015, 2022 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Questa pagina è distribuita secondo i termini della licenza <a rel=\"license"
"\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.it\">Creative "
"Commons Attribuzione - Non opere derivate 4.0 Internazionale</a> (CC BY-ND "
"4.0)."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Tradotto originariamente da Riccardo Pili.  Revisioni successive di Luca "
"Padrin, Andrea Pescetti."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Ultimo aggiornamento:"

#~ msgid ""
#~ "Copyright &copy; 2014, 2015, 2017, 2018 Free Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2014, 2015, 2017, 2018 Free Software Foundation, Inc."

#~ msgid "Copyright &copy; 2014, 2015, 2017 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014, 2015, 2017 Free Software Foundation, Inc."

#~ msgid "Copyright &copy; 2014, 2015 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014, 2015 Free Software Foundation, Inc."

#~ msgid "Copyright &copy; 2014 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014 Free Software Foundation, Inc."

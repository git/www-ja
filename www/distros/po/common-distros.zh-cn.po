# Simplified Chinese translation of https://www.gnu.org/distros/common-distros.html
# Copyright (C) 2024 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Wensheng Xie  <wxie@member.fsf.org>, 2017-2024.
#
msgid ""
msgstr ""
"Project-Id-Version: common-distros.html\n"
"POT-Creation-Date: 2024-01-06 03:25+0000\n"
"PO-Revision-Date: 2024-01-17 20:14+0800\n"
"Last-Translator: Wensheng Xie <wxie@member.fsf.org>\n"
"Language-Team: Chinese <www-zh-cn-translators@gnu.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Explaining Why We Don't Endorse Other Systems - GNU Project - Free Software "
"Foundation"
msgstr "详解我们为什么不认可其他系统 - GNU工程 - 自由软件基金会"

#. type: Attribute 'title' of: <link>
msgid "Free GNU/Linux distributions"
msgstr "自由的GNU/Linux发行版"

#. type: Content of: <div><h2>
msgid "Explaining Why We Don't Endorse Other Systems"
msgstr "详解我们为什么不认可其他系统"

#. type: Content of: <div><p>
msgid ""
"We're often asked why we don't endorse a particular system&mdash;usually a "
"popular GNU/Linux distribution.  The short answer to that question is that "
"they don't follow the <a href=\"/distros/free-system-distribution-guidelines."
"html\">free system distribution guidelines</a>.  But since it isn't always "
"obvious how a particular distro fails to follow the guidelines, this list "
"gives more information about the problems of certain well-known nonfree "
"system distros."
msgstr ""
"我们经常会被问到为什么我们不认可某个系统&mdash;&mdash;通常是一个流行的 GNU/"
"Linux 发行版。简短的回答是他们没有遵从 <a href=\"/distros/free-system-"
"distribution-guidelines.html\">自由系统发布指南</a>。但是，由于某个系统怎么就"
"没有遵从该指南并不是一个显而易见的事情，所以本列表就某些著名的非自由系统发行"
"版的问题给出更详尽的信息。"

#. type: Content of: <div><p>
msgid ""
"To learn more about the GNU/Linux systems that we do endorse, check out our "
"list of <a href=\"/distros/free-distros.html\">free GNU/Linux distributions</"
"a>."
msgstr ""
"要了解更多关于我们认可的GNU/Linux系统，请查看我们的<a href=\"/distros/free-"
"distros.html\">自由的GNU/Linux发行版</a>列表。"

#. type: Content of: <div><p>
msgid ""
"Except where noted, all of the distributions listed on this page fail to "
"follow the guidelines in at least two important ways:"
msgstr ""
"除了另外指出的问题，本页面列出的发行版至少在两个重要的方面没有遵从我们的指"
"南："

#. type: Content of: <div><ul><li><p>
msgid ""
"They do not have a policy of <em>only</em> including free software, and "
"removing nonfree software if it is discovered.  Most of them have no clear "
"policy on what software they'll accept or reject at all.  The distributions "
"that do have a policy unfortunately aren't strict enough, as explained below."
msgstr ""
"他们没有政策来<em>只</em>包含自由软件，并且一旦发现就移除非自由软件。其中大多"
"数完全没有明确的政策决定他们会接受或拒绝哪些软件。那些有政策的发行版也不够严"
"格，下面会解释。"

#. type: Content of: <div><ul><li><p>
msgid ""
"The kernel that they distribute (in most cases, Linux) includes &ldquo;"
"blobs&rdquo;: pieces of object code distributed without source, usually "
"firmware to run some device."
msgstr ""
"这些发行版发布的内核（大多数是Linux）包含有&ldquo;blobs&rdquo;：没有源代码的"
"目标程序，通常是设备驱动的固件。"

#. type: Content of: <div><p>
msgid ""
"Here is a list of some popular nonfree GNU/Linux distributions in "
"alphabetical order, with brief notes about how they fall short.  We do not "
"aim for completeness; once we know some reasons we can't endorse a certain "
"distro, we do not keep looking for all the reasons."
msgstr ""
"以下按字母顺序列举了一些流行的非自由GNU/Linux发行版，并就其为何不合格给出简要"
"的说明。我们的目标并不是找到所有的不合格；一旦我们有理由不认可某个发行版，我"
"们就不再寻找其他的原因。"

#. type: Content of: <div><p>
msgid ""
"A distro may have changed since we last updated information about it; if you "
"think one of the problems mentioned here has been corrected, please <a href="
"\"mailto:webmasters@gnu.org\">let us know</a>.  However, we will study and "
"endorse a distro only if its developers ask for our endorsement."
msgstr ""
"一个发行版可能在我们最后发布列表之后做出了修改；如果你认为此处列出的问题已经"
"得到更正，请<a href=\"mailto:webmasters@gnu.org\">告诉我们</a>。但是，只有在"
"其开发者要求我们认可时，我们才会研究和认可一个发行版。"

#. type: Content of: <div><h3>
msgid "Arch GNU/Linux"
msgstr "Arch GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Arch has no policy against distributing nonfree software through their "
"normal channels, and nonfree blobs are shipped with their kernel, Linux."
msgstr ""
"Arch没有反对通过其正常渠道发布非自由软件的政策，而且其内核（Linux）也带有非自"
"由的blobs。"

#. type: Content of: <div><h3>
msgid "Canaima GNU/Linux"
msgstr "Canaima GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Canaima GNU/Linux is a distribution made by Venezuela's government to "
"distribute computers with GNU/Linux.  While the overall plan is admirable, "
"Canaima is flawed by the inclusion of nonfree software."
msgstr ""
"Canaima GNU/Linux是由委内瑞拉政府为发布GNU/Linux电脑而制作的发行版。尽管其整"
"体计划令人赞赏，Canaima却错误地包含了非自由软件。"

#. type: Content of: <div><p>
msgid ""
"Its main menu has an option, &ldquo;Install nonfree software,&rdquo; which "
"installs all the nonfree drivers (even the ones that are not necessary). The "
"distro also ships blobs for the kernel, Linux, and invites installing "
"nonfree applications including Flash Player."
msgstr ""
"其主菜单带有一个选项，&ldquo;安装非自由软件&rdquo;，它会安装所有的非自由驱动"
"软件（甚至包括那些不必要的）。该发行版还提供带有blobs的Linux内核，并邀请大家"
"安装包括Flash Player在内的非自由应用。"

#. type: Content of: <div><h3>
msgid "CentOS"
msgstr "CentOS"

#. type: Content of: <div><p>
msgid ""
"We're not aware of problems in CentOS aside from the two usual ones: there's "
"no clear policy about what software can be included, and nonfree blobs are "
"shipped with Linux, the kernel.  Of course, with no firm policy in place, "
"there might be other nonfree software included that we missed."
msgstr ""
"除了两点常见问题外，我们不知道CentOS的其他问题：它没有明确的软件包含政策，其"
"Linux内核带有非自由的blobs。当然，由于没有明确的政策，它可能还包括了其他非自"
"由软件。"

#. type: Content of: <div><h3>
msgid "Debian GNU/Linux"
msgstr "Debian GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Until 2022, Debian GNU/Linux came fairly close to qualifying as a free "
"distro: it was simple to specify that you wanted to install Debian without "
"any nonfree software."
msgstr ""
"在 2022 年之前，Debian GNU/Linux 非常接近一个自由发行版：设置不安装任何非自由"
"软件很简单。"

#. type: Content of: <div><p>
msgid ""
"Debian's Social Contract states the goal of making Debian entirely free "
"software, and Debian conscientiously keeps nonfree software out of the "
"official Debian system.  However, Debian also maintains a repository of "
"nonfree software.  It asserts that this software is &ldquo;not part of the "
"Debian system,&rdquo; but the repository is hosted on many of the project's "
"main servers, so people are likely to learn from Debian itself about those "
"nonfree packages by browsing Debian's package database and wiki, and then "
"might install them."
msgstr ""
"Debian 的社群契约声明其目标是使 Debian 成为完全自由软件，并且 Debian 自觉地把"
"非自由软件排除在其正式发布之外。然而，Debian 还是维护着非自由软件的软件库。"
"Debian 声明这些软件 &ldquo;不是 Debian 系统的一部分&rdquo;，但是该软件库由 "
"Debian 项目的许多主要服务器托管，所以人们很可能通过浏览 Debian 的软件包数据库"
"和其 wiki 了解这些非自由软件，进而安装这些非自由软件。"

#. type: Content of: <div><p>
msgid ""
"Until 2022, Debian GNU/Linux did not offer nonfree packages for installation "
"unless the user explicitly enabled use of that repository.  Thus, it was "
"easy to make a free installation if you wanted to."
msgstr ""
"在 2022 年之前，除非用户明确启用非自由软件库，Debian GNU/Linux 的安装不会提供"
"非自由软件包。因此，只要你想，安装一个自由版本很容易。"

#. type: Content of: <div><p>
msgid ""
"That is no longer true, because Debian has <a href=\"https://lists.debian."
"org/debian-devel-announce/2022/10/msg00001.html\"> changed its policy</a>.  "
"In Debian 12, initially, the installer offered to install nonfree firmware "
"whenever some hardware devices &ldquo;needed&rdquo; that."
msgstr ""
"现在情况变了，因为 Debian <a href=\"https://lists.debian.org/debian-devel-"
"announce/2022/10/msg00001.html\">改变了政策</a>。在 Debian 12 上，无论硬件设"
"备是否 &ldquo;需要&rdquo;，安装程序一开始就提供非自由的固件。"

#. type: Content of: <div><p>
msgid ""
"Since then, there has been another change for the worse.  Debian now "
"recommends preferentially a new installer program which, on most computers, "
"installs all the nonfree firmware without even asking."
msgstr ""
"从此，还有另一个更糟糕的改变。Debian 现在优先推荐一个新的安装程序，在大多数电"
"脑上，该程序会默认安装所有的非自由固件。"

#. type: Content of: <div><p>
msgid ""
"It is no longer easy to install only the free packages of Debian.  There are "
"ways to request this, but they require specific knowledge.  See <a href=\"/"
"distros/optionally-free-not-enough.html\"> Optionally Free Is Not Enough</"
"a>.  In effect, Debian has become more like the other nonfree distros."
msgstr ""
"在 Debian 上只安装自由软件包不再那么简单。虽然你可以要求只安装自由软件，但是"
"需要特别的知识。请参看 <a href=\"/distros/optionally-free-not-enough.html\">"
"自由作为一个选项是不够的</a>。实际上，Debian 越来越像其他非自由发行版。"

#. type: Content of: <div><p>
msgid ""
"Debian also has a &ldquo;contrib&rdquo; repository; its packages are free, "
"but some of them exist to load separately distributed proprietary programs.  "
"This too is not thoroughly separated from the main Debian distribution."
msgstr ""
"它还有一个 &ldquo;contrib&rdquo; 软件库；其软件包是自由的，但是其中有些需要加"
"载另外发布的专有软件。这也没完全和 Debian 主发行版分离。"

#. type: Content of: <div><p>
msgid ""
"In addition, some of the free programs that are officially part of Debian "
"invite the user to install some nonfree programs.  Specifically, the Debian "
"versions of Firefox and Chromium suggest nonfree plug-ins to install into "
"them."
msgstr ""
"此外，Debian 的一些官方自由软件会邀请用户安装一些非自由的软件。具体来说，"
"Debian 版的 Firefox 和 Chromium 会推荐安装非自由的附加组件。"

#. type: Content of: <div><p>
msgid "Debian's wiki also includes pages about installing nonfree firmware."
msgstr "Debian 的 wiki 含有关于安装非自由固件的页面。"

#. type: Content of: <div><h3>
msgid "Fedora"
msgstr "Fedora"

#. type: Content of: <div><p>
msgid ""
"Fedora does have a clear policy about what can be included in the "
"distribution, and it seems to be followed carefully.  The policy requires "
"that most software and all fonts be available under a free license, but "
"makes an exception for certain kinds of nonfree firmware.  Unfortunately, "
"the decision to allow that firmware in the policy keeps Fedora from meeting "
"the free system distribution guidelines."
msgstr ""
"Fedora有关于其发行版可以包含何种软件的明确政策，而且看起来它也认真执行了政"
"策。其政策要求大多数软件和所有字体按自由许可证发布，但是对某些非自由固件做了"
"例外。不幸的是，允许固件例外的决定使Fedora不再符合自由系统发布指南。"

#. type: Content of: <div><h3>
msgid "Gentoo GNU/Linux"
msgstr "Gentoo GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Gentoo includes installation recipes for a number of nonfree programs in its "
"primary package system."
msgstr "Gentoo的主要软件包系统包含了安装一些非自由软件的步骤。"

#. type: Content of: <div><h3>
msgid "Mandriva GNU/Linux"
msgstr "Mandriva GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Mandriva does have a stated policy about what can be included in the main "
"system.  It's based on Fedora's, which means that it also allows certain "
"kinds of nonfree firmware to be included.  On top of that, it permits "
"software released under the original Artistic License to be included, even "
"though that's a nonfree license."
msgstr ""
"Mandriva声明了关于何种软件可以进入主系统的政策。该政策基于Fedora的政策，就是"
"说它也允许某类非自由的固件。在此之上，它允许包含按照原始Artistic许可证发布的"
"软件，尽管该许可证是非自由许可证。"

#. type: Content of: <div><p>
msgid "Mandriva also ships nonfree software through dedicated repositories."
msgstr "Mandriva还提供专用的非自由软件库。"

#. type: Content of: <div><h3>
msgid "Manjaro GNU/Linux"
msgstr "Manjaro GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Manjaro includes nonfree software through its normal channels, and ships "
"nonfree blobs with its kernel, Linux. It includes a proprietary office suite "
"and proprietary games with DRM. The distro also recommends the installation "
"of nonfree drivers."
msgstr ""
"Manjaro 在其常规的发布通道中带有非自由软件，并且在其 Linux 内核中包含非自由"
"的 blobs。它还提供专有的办公套件和专有的带 DRM 的游戏。该发行版还推荐安装非自"
"由的驱动软件。"

#. type: Content of: <div><h3>
msgid "Mint GNU/Linux"
msgstr "Mint GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Mint does not have a policy against including nonfree software, it includes "
"nonfree binary blobs in drivers packaged with the kernel, and it includes "
"nonfree programs in its repositories.  It even includes proprietary codecs."
msgstr ""
"Mint没有拒绝非自由软件的政策，它的内核驱动包含有非自由的二进制blobs，而且它的"
"软件库中也带有非自由软件。它甚至包含了专有的编解码程序。"

#. type: Content of: <div><h3>
msgid "NixOS"
msgstr "NixOS"

#. type: Content of: <div><p>
msgid ""
"NixOS doesn't have any policy that completely forbids nonfree software. "
"Instead, it has an option that needs to be activated to install nonfree "
"packages. But even with that option disabled, it still ships nonfree blobs "
"in its main repository, either with Linux (the kernel), or through separate "
"package(s)  like <a href=\"/philosophy/tivoization.html\">sof-firmware</a>."
msgstr ""
"NixOS 没有任何完全禁止非自由软件的政策。反之，它有一个可以激活来安装非自由软"
"件包的选项。即使禁用该选项，其核心软件库仍然带有非自由的 blobs，包括 Linux "
"(内核) 和诸如 <a href=\"/philosophy/tivoization.html\">sof-firmware</a> 等的"
"独立软件包。"

#. type: Content of: <div><h3>
msgid "openSUSE"
msgstr "openSUSE"

#. type: Content of: <div><p>
msgid ""
"openSUSE offers a repository of nonfree software.  This is an instance of "
"how <a href=\"/philosophy/open-source-misses-the-point.html\"> &ldquo;"
"open&rdquo; is weaker than &ldquo;free&rdquo;</a>."
msgstr ""
"openSUSE提供非自由软件库。这就是<a href=\"/philosophy/open-source-misses-the-"
"point.html\">&ldquo;开放（open）&rdquo;弱于&ldquo;自由（free）&rdquo;</a>的一"
"个明证。"

#. type: Content of: <div><h3>
msgid "Red Hat GNU/Linux"
msgstr "Red Hat GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Red Hat's enterprise distribution primarily follows the same licensing "
"policies as Fedora, with one exception.  Thus, we don't endorse it for <a "
"href=\"#Fedora\">the same reasons</a>.  In addition to those, Red Hat has no "
"policy against making nonfree software available for the system through "
"supplementary distribution channels."
msgstr ""
"Red Hat的企业发行版基本上执行的是Fedora的许可证政策，带有一个例外。所以，我们"
"由于<a href=\"#Fedora\">同样的原因</a>不认可它。除了这些，Red Hat没有拒绝通过"
"附属渠道在其系统上包含非自由软件的政策。"

#. type: Content of: <div><h3>
msgid "Slackware"
msgstr "Slackware"

#. type: Content of: <div><p>
msgid ""
"Slackware has the two usual problems: there's no clear policy about what "
"software can be included, and nonfree blobs are included in Linux, the "
"kernel.  It also ships with the nonfree image-viewing program xv.  Of "
"course, with no firm policy against them, more nonfree programs could get in "
"at any time.  There is an <a href=\"http://freeslack.net/\">unofficial list</"
"a> of nonfree software in Slackware."
msgstr ""
"Slackware有两个常见的问题：没有关于可以包含何种软件的明确政策，其Linux内核带"
"有非自由的blobs。它还发布非自由的图像查看软件xv。当然，由于没有明确的政策，更"
"多的非自由软件随时可以被加进来。这里有Slackware带有的非自由软件的<a href="
"\"http://freeslack.net/\">非官方列表</a>。"

#. type: Content of: <div><h3>
msgid "SteamOS"
msgstr "SteamOS"

#. type: Content of: <div><p>
msgid ""
"SteamOS, a version of GNU/Linux to be distributed by Valve. It contains "
"proprietary software, including the Steam client and proprietary drivers. "
"Steam uses <a href=\"https://www.defectivebydesign.org/"
"what_is_drm_digital_restrictions_management\">Digital Restrictions "
"Management (DRM)</a> to impose restrictions on the software it distributes, "
"as well as on the proprietary software it promotes via the Steam store."
msgstr ""
"SteamOS 是由 Valve 发布的 GNU/Linux 版本。它带有专有软件，包括 Steam 客户端和"
"专有驱动。Steam 使用 <a href=\"https://www.defectivebydesign.org/"
"what_is_drm_digital_restrictions_management\">数字权限管理（DRM）</a>来限制其"
"发布的软件，包括其通过 Steam 商店发布的专有软件。"

#. type: Content of: <div><h3>
msgid "SUSE GNU/Linux Enterprise"
msgstr "SUSE GNU/Linux Enterprise"

#. type: Content of: <div><p>
msgid ""
"In addition to the usual two problems, several nonfree software programs are "
"available for download from SUSE's official FTP site."
msgstr "除了两个通常的问题外，SUSE的官方FTP站点提供多个非自由软件的下载。"

#. type: Content of: <div><h3>
msgid "Tails"
msgstr "Tails"

#. type: Content of: <div><p>
msgid ""
"Tails uses the vanilla version of Linux, which contains nonfree firmware "
"blobs."
msgstr "Tails使用基础版的Linux，其中包含有非自由的固件blobs。"

#. type: Content of: <div><h3>
msgid "Ubuntu GNU/Linux"
msgstr "Ubuntu GNU/Linux"

#. type: Content of: <div><p>
msgid ""
"Ubuntu maintains specific repositories of nonfree software, and Canonical "
"expressly promotes and recommends nonfree software under the Ubuntu name in "
"some of their distribution channels.  Ubuntu offers the option to install "
"only free packages, which means it also offers the option to install nonfree "
"packages too.  In addition, the version of Linux, the kernel, included in "
"Ubuntu contains firmware blobs."
msgstr ""
"Ubuntu为非自由软件维护着特别的软件库，而且Canonical以Ubuntu的名义在其发布渠道"
"明确表示推广和建议非自由软件。Ubuntu提供了仅安装自由软件的选项，这表示它也提"
"供了安装非自由软件的选项。另外，Ubuntu的Linux内核包含了固件blobs。"

#. type: Content of: <div><p>
msgid ""
"Ubuntu <a href=\"https://www.ubuntu.com/legal/intellectual-property-"
"policy#your-use-of-ubuntu\">appears to permit commercial redistribution of "
"exact copies with the trademarks</a>; removal of the trademarks is required "
"only for modified versions.  That is an acceptable policy for trademarks.  "
"However, the same page, further down, makes a vague and ominous statement "
"about &ldquo;Ubuntu patents,&rdquo; without giving enough details to show "
"whether that constitutes aggression or not."
msgstr ""
"Ubuntu <a href=\"https://www.ubuntu.com/legal/intellectual-property-"
"policy#your-use-of-ubuntu\">显然允许以其商标发布完全一样的商业拷贝</a>；只有"
"修改过的版本才需要去除其商标。这个商标政策可以接受。不过，在同一页面往下看，"
"可以看到含糊不明的&ldquo;Ubuntu 专利&rdquo;声明，而它并未就以上是否构成侵权给"
"出详尽的说明。"

#. type: Content of: <div><p>
msgid ""
"That page spreads confusion by using the misleading term &ldquo;<a href=\"/"
"philosophy/not-ipr.html\">intellectual property rights</a>,&rdquo; which "
"falsely presumes that trademark law and patent law and several other laws "
"belong in one single conceptual framework.  Use of that term is harmful, "
"without exception, so after making a reference to someone else's use of the "
"term, we should always reject it.  However, that is not a substantive issue "
"about Ubuntu as a GNU/Linux distribution."
msgstr ""
"该页面通过使用<a href=\"/philosophy/not-ipr.html\">&ldquo;知识产权&rdquo;</a>"
"这一误导性术语散布混乱，错误地假设商标法、专利法和其他一些法律属于同一概念性"
"框架。使用该术语是有害的，没有例外，所以除了引用别人对该术语的使用之外，我们"
"应该拒绝使用它。但是，这并不是Ubuntu作为一个GNU/Linux发行版的主要问题。"

#. type: Content of: <div><p>
msgid ""
"In addition, Ubuntu is moving more and more packages to a new package "
"manager called Snap, which is not good for users' freedom and autonomy. Snap "
"uses a special kind of repository implemented on Canonical's unreleased "
"software. In practice this makes it very inconvenient to package modified "
"versions of the free programs in Ubuntu such that users of Ubuntu can easily "
"install them."
msgstr ""
"另外，Ubuntu 把越来越多的软件包转移到使用一个叫做 Snap 的新软件包管理器，而这"
"个管理器对用户的自由和自主并不好。Snap 使用一种由 Canonical 未发布的软件实现"
"的软件库。实际上，这样做使得在 Ubuntu 上打包自由软件的修改版非常不方便，进而"
"用户安装也不太容易。"

#. type: Content of: <div><h2>
msgid "Some Other Distros"
msgstr "其他发行版"

#. type: Content of: <div><p>
msgid ""
"Here we discuss some well-known or significant non-GNU/Linux system distros "
"that do not qualify as free."
msgstr ""
"下面我们讨论一些知名的或者重要的非GNU/Linux系统发行版，它们都不是合格的自由系"
"统发布。"

#. type: Content of: <div><h3>
msgid "Android"
msgstr "Android"

#. type: Content of: <div><p>
msgid ""
"<a href=\"/philosophy/android-and-users-freedom.html\">Android</a> as "
"released by Google contains many nonfree parts as well as many free parts.  "
"Most of the free parts are covered by a pushover license (not <a href=\"/"
"licenses/copyleft.html\">copyleft</a>), so manufacturers that distribute "
"Android in a product sometimes make those parts nonfree as well."
msgstr ""
"Google发布的<a href=\"/philosophy/android-and-users-freedom.html\">Android</"
"a>包含了许多自由和非自由的部件。其大多数自由部分采用的是简单许可证（不是<a "
"href=\"/licenses/copyleft.html\">copyleft</a>），所以发布Android产品的制造商"
"有时会把这部分也变成非自由的。"

#. type: Content of: <div><h3>
msgid "BSD systems"
msgstr "BSD系统"

#. type: Content of: <div><p>
msgid ""
"FreeBSD, NetBSD, and OpenBSD all include instructions for obtaining nonfree "
"programs in their ports system.  In addition, their kernels include nonfree "
"firmware blobs."
msgstr ""
"FreeBSD、NetBSD和OpenBSD全部在其软件安装系统中包含了获取非自由软件的指导。另"
"外，其内核还包含了非自由的固件blobs。"

#. type: Content of: <div><p>
msgid ""
"Nonfree firmware programs used with Linux, the kernel, are called &ldquo;"
"blobs,&rdquo; and that's how we use the term.  In BSD parlance, the term "
"&ldquo;blob&rdquo; means something else: a nonfree driver.  OpenBSD and "
"perhaps other BSD distributions (called &ldquo;projects&rdquo; by BSD "
"developers) have the policy of not including those.  That is the right "
"policy, as regards drivers; but when the developers say these distributions "
"&ldquo;contain no blobs,&rdquo; it causes a misunderstanding.  They are not "
"talking about firmware blobs."
msgstr ""
"Linux内核使用的非自由固件程序被称做&ldquo;blobs&rdquo;，我们使用该术语描述此"
"类程序。在BSD术语中，&ldquo;blob&rdquo;说的是另外的东西：非自由驱动。"
"OpenBSD，还可能包括其他BSD发行版（BSD开发者称之为&ldquo;项目&rdquo;）有不包含"
"这些非自由驱动的政策。对驱动，这是正确的政策；但是当开发者说其发行版&ldquo;没"
"有包含blobs&rdquo;时，就形成了误解。他们说的不是固件blobs。"

#. type: Content of: <div><p>
msgid ""
"None of those BSD distributions has policies against proprietary binary-only "
"firmware that might be loaded even by free drivers."
msgstr ""
"这些BSD发行版都没有声明它们有针对专有二进制固件的政策，即使这些二进制固件可能"
"会被自由驱动加载。"

#. type: Content of: <div><h3>
msgid "Chrome OS"
msgstr "Chrome OS"

#. type: Content of: <div><p>
msgid ""
"The central part of Chrome OS is the nonfree Chrome browser.  It may contain "
"other nonfree software as well."
msgstr ""
"Chrome OS的核心部分是非自由的Chrome浏览器。它可能还含有其他的非自由软件。"

#. type: Content of: <div><p>
msgid ""
"The rest of it is based on <a href=\"#ChromiumOS\">ChromiumOS</a>, so it "
"also has the problems of Chromium OS, plus the nonfree parts of Android."
msgstr ""
"它的其他部分基于 <a href=\"#ChromiumOS\">ChroniumOS</a>，因此它和 Chronium "
"OS 的问题一样，此外它还有 Android 的非自由部分。"

#. type: Content of: <div><h3>
msgid "Chromium OS"
msgstr "Chromium OS"

#. type: Content of: <div><p>
msgid ""
"Chromium OS contains proprietary software, including firmware blobs and "
"nonfree user-space binaries to support specific hardware on some computers."
msgstr ""
"Chromium OS 包含专有软件，其中有固件 blobs 和非自由的用户空间二进制文件，它们"
"用来支持某些电脑的具体硬件。"

#. type: Content of: <div><p>
msgid ""
"In addition, the login system surveils users, as it requires a Google "
"account (Chromium OS does not support local accounts).  The &ldquo;"
"guest&rdquo; session feature is not a real alternative to logging in with a "
"Google account, because it doesn't allow persistent storage and limits the "
"system's features."
msgstr ""
"另外，其登录系统会监控用户，因为它需要 Google 帐号（Chromium OS 不支持本地帐"
"号）。&ldquo;访客&rdquo; 会话功能并不是避免 Google 帐号的登录方式，因为它不能"
"长期保存数据而且功能受限。"

#. type: Content of: <div><h3>
msgid "/e/"
msgstr "/e/"

#. type: Content of: <div><p>
msgid ""
"/e/ (formerly eelo) is a modified version of Android, which contains nonfree "
"libraries."
msgstr "/e/ （原来叫做eelo）是Android的一种修改版，它带有非自由的软件库。"

#. type: Content of: <div><h3>
msgid "GrapheneOS"
msgstr "GrapheneOS"

#. type: Content of: <div><p>
msgid ""
"GrapheneOS is a version of Android which is described as &ldquo;open source,"
"&rdquo; but it seems to include software that <a href=\"/philosophy/open-"
"source-misses-the-point.html\"> isn't free software or even &ldquo;open "
"source&rdquo;</a>.  For instance, it comes with firmware programs for "
"installation and it appears that at least some of them are binaries without "
"source code.  It is said to be &ldquo;de-Googled,&rdquo; but includes a way "
"to download and install the nonfree Google Play program."
msgstr ""
"GrapheneOS 是 Android 的一个版本，它号称 &ldquo;开源&rdquo;，但是看来也包含"
"了 <a href=\"/philosophy/open-source-misses-the-point.html\">非自由甚至也非 "
"&ldquo;开源&rdquo;</a> 的软件。例如，它会安装固件，其中有些是没有源代码的二进"
"制程序。它说自己是 &ldquo;去谷歌化&rdquo;，但是却包含了安装非自由谷歌应用程序"
"的方法。"

#. type: Content of: <div><h3>
msgid "Haiku"
msgstr "Haiku"

#. type: Content of: <div><p>
msgid ""
"Haiku includes some software that you're not allowed to modify.  It also "
"includes nonfree firmware blobs."
msgstr "Haiku包含有一些不允许你修改的软件。它也包含了非自由的固件blobs。"

#. type: Content of: <div><h3>
msgid "LineageOS"
msgstr "LineageOS"

#. type: Content of: <div><p>
msgid ""
"LineageOS (formerly CyanogenMod) is a modified version of Android, which "
"contains nonfree libraries. It also explains how to install the nonfree "
"applications that Google distributes with Android."
msgstr ""
"LineageOS（原来的CyanogenMod）是Android的一个修改版，它包含非自由的软件库。它"
"还解释如何安装Google发布Android时带有的非自由应用。"

#. type: Content of: <div><h3>
msgid "ReactOS"
msgstr "ReactOS"

#. type: Content of: <div><p>
msgid ""
"ReactOS is meant as a free binary compatible replacement for Windows.  Use "
"with proprietary software and drivers meant for Windows is one of the stated "
"goals of the project."
msgstr ""
"ReactOS目的在于为替代Windows而提供自由二进制兼容软件。为Windows使用专有软件和"
"驱动是其项目阐述的目标之一。"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"请将有关自由软件基金会（FSF）&amp;GNU的一般性问题发送到<a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>。也可以通过<a href=\"/contact/\">其他联"
"系方法</a>联系自由软件基金会（FSF）。有关失效链接或其他错误和建议，请发送邮件"
"到<a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>。"

# TODO: submitting -> contributing.
#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"我们尽最大努力来提供精准和高质量的翻译，但难免会存在错误和不足。如果您在这方"
"面有评论或一般性的建议，请发送至 <a href=\"mailto:web-translators@gnu.org"
"\">&lt;web-translators@gnu.org&gt;</a>。</p><p>关于进行协调与提交翻译的更多信"
"息参见 <a href=\"/server/standards/README.translations.html\">《译者指南》</"
"a>。"

# type: Content of: <div><p>
#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2009-2019, 2021, 2022, 2023, 2024 Free Software Foundation, "
"Inc."
msgstr ""
"Copyright &copy; 2009-2019, 2021, 2022, 2023, 2024 Free Software Foundation, "
"Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"本页面使用<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-"
"nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 International "
"License</a>授权。"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<b>翻译团队</b>：<a rel=\"team\" href=\"https://savannah.gnu.org/projects/"
"www-zh-cn/\">&lt;CTT&gt;</a>，2017-2024。"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "最后更新："

# type: Content of: <div><p>
#~ msgid ""
#~ "Copyright &copy; 2009-2019, 2021, 2022, 2023 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2009-2019, 2021, 2022, 2023 Free Software Foundation, "
#~ "Inc."

#~ msgid "Canaima"
#~ msgstr "Canaima"

#~ msgid ""
#~ "It is no longer easy to install only the free packages of Debian.  In "
#~ "effect, Debian has become more like the other nonfree distros."
#~ msgstr ""
#~ "仅安装 Debian 的自由软件包不再那么容易了。实际上，Debian 变得更像其他非自"
#~ "由发行版了。"

#~ msgid ""
#~ "The nonfree firmware files live in Debian's nonfree repository, which is "
#~ "referenced in the documentation on debian.org, and the installer in some "
#~ "cases recommends them for the peripherals on the machine."
#~ msgstr ""
#~ "非自由的固件存在于 Debian 的非自由软件库中，这点在 debian.org 的文档中有说"
#~ "明，其安装程序在某些情况下会推荐计算机周边设备安装这些非自由固件。"

#~ msgid ""
#~ "Installation of version 12 of Debian <a href=\"https://lists.debian.org/"
#~ "debian-devel-announce/2022/10/msg00001.html\"> automatically offers to "
#~ "install nonfree firmware</a> if the installer determines that some "
#~ "hardware devices need nonfree firmware to operate.  It is no longer so "
#~ "easy to install only the free packages of Debian.  In effect, Debian 12 "
#~ "has become more similar to the other nonfree distros."
#~ msgstr ""
#~ "如果安装程序检测到需要非自由固件才能工作，那么 Debian 12 会 <a href="
#~ "\"https://lists.debian.org/debian-devel-announce/2022/10/msg00001.html\">"
#~ "自动提供安装非自由固件的方法</a>，而安装完全自由的 Debian 软件包不再那么简"
#~ "单。实际上，Debian 12 变得更像其他非自由发行版。"

# type: Content of: <div><p>
#~ msgid ""
#~ "Copyright &copy; 2009-2019, 2021, 2022 Free Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2009-2019, 2021, 2022 Free Software Foundation, Inc."

#
#
#
#~ msgid ""
#~ "Please see the <a href=\"/server/standards/README.translations.html"
#~ "\">Translations README</a> for information on coordinating and submitting "
#~ "translations of this article."
#~ msgstr ""
#~ "我们尽最大努力来提供精准和高质量的翻译，但难免会存在错误和不足。如果您在这"
#~ "方面有评论或一般性的建议，请发送至 <a href=\"mailto:web-translators@gnu."
#~ "org\">&lt;web-translators@gnu.org&gt;</a>。</p><p>关于进行协调与提交翻译的"
#~ "更多信息参见 <a href=\"/server/standards/README.translations.html\">《译者"
#~ "指南》</a>。"

# type: Content of: <div><p>
#~ msgid "Copyright &copy; 2014-2021 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014-2021 Free Software Foundation, Inc."

# type: Content of: <div><p>
#~ msgid "Copyright &copy; 2014-2022 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014-2022 Free Software Foundation, Inc."

#~ msgid ""
#~ "Copyright &copy; 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021 Free "
#~ "Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021 Free "
#~ "Software Foundation, Inc."

#~ msgid ""
#~ "The &ldquo;Ubuntu Software Center&rdquo; lists proprietary programs and "
#~ "free programs jumbled together.  It is <a href=\"http://www."
#~ "freesoftwaremagazine.com/articles/"
#~ "ubuntu_software_center_proprietary_and_free_software_mixed_confusing_ui"
#~ "\">hard to tell which ones are free</a> since proprietary programs for "
#~ "download at no charge are labelled &ldquo;free&rdquo;."
#~ msgstr ""
#~ "&ldquo;Ubuntu软件中心&rdquo;把自由软件和专有软件混在一起。这样<a href="
#~ "\"http://www.freesoftwaremagazine.com/articles/"
#~ "ubuntu_software_center_proprietary_and_free_software_mixed_confusing_ui\">"
#~ "难以分辨哪些是自由软件</a>，因为免费下载的专有软件贴着&ldquo;自由（free）"
#~ "&rdquo;的标签。"

#~ msgid ""
#~ "Copyright &copy; 2014, 2015, 2016, 2017, 2018, 2019 Free Software "
#~ "Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2014, 2015, 2016, 2017, 2018, 2019 Free Software "
#~ "Foundation, Inc."

#~ msgid ""
#~ "Instead of this nonfree distribution, use <a href=\"/distros/free-distros."
#~ "html#for-pc\">Parabola</a> or <a href=\"https://www.hyperbola.info/?gnu-"
#~ "free-stros-page\">Hyperbola</a>, free distros which are made from it."
#~ msgstr ""
#~ "请不要使用这个非自由的发行版，请使用<a href=\"/distros/free-distros."
#~ "html#for-pc\">Parabola</a>或<a href=\"https://www.hyperbola.info/?gnu-"
#~ "free-stros-page\">Hyperbola</a>，这是从Arch改造的自由发行版。"

#~ msgid ""
#~ "Instead of this nonfree distribution, use one of the free distros which "
#~ "are made from it: <a href=\"/distros/free-distros.html#for-pc"
#~ "\">gNewSense</a>, and <a href=\"/distros/free-distros.html#for-pc"
#~ "\">PureOS</a>."
#~ msgstr ""
#~ "请不要使用这个非自由的发行版，请使用由它改造的自由发行版：<a href=\"/"
#~ "distros/free-distros.html#for-pc\">gNewSense</a>和<a href=\"/distros/free-"
#~ "distros.html#for-pc\">PureOS</a>。"

#~ msgid ""
#~ "Instead of this nonfree distribution, use one of the free distros which "
#~ "are made from it: <a href=\"/distros/free-distros.html#for-pc\">Dyne:"
#~ "bolic</a>, <a href=\"/distros/free-distros.html#for-pc\">Trisquel</a>, "
#~ "and <a href=\"/distros/free-distros.html#for-pc\">Ututo S</a>."
#~ msgstr ""
#~ "请不要使用这个非自由的发行版，请使用由它改造的自由发行版：<a href=\"/"
#~ "distros/free-distros.html#for-pc\">Dyne:bolic</a>、<a href=\"/distros/"
#~ "free-distros.html#for-pc\">Trisquel</a>和<a href=\"/distros/free-distros."
#~ "html#for-pc\">Ututo S</a>。"

#~ msgid ""
#~ "Previous releases of Debian included nonfree blobs with Linux, the "
#~ "kernel.  With the release of Debian 6.0 (&ldquo;squeeze&rdquo;) in "
#~ "February 2011, these blobs have been moved out of the main distribution "
#~ "to separate packages in the nonfree repository.  However, the problem "
#~ "partly remains: the installer in some cases recommends these nonfree "
#~ "firmware files for the peripherals on the machine."
#~ msgstr ""
#~ "Debian以前的发布包括带非自由blobs的Linux内核。从2011年2月的Debian 6.0"
#~ "（&ldquo;squeeze&rdquo;）发布起，这些blobs被其主发布移除到分离的非自由软件"
#~ "库。然而，问题还有遗留：其安装程序在某些情况下会为电脑的外设推荐这些非自由"
#~ "的固件。"

#~ msgid ""
#~ "Arch has the two usual problems: there's no clear policy about what "
#~ "software can be included, and nonfree blobs are shipped with their "
#~ "kernel, Linux.  Arch also has no policy about not distributing nonfree "
#~ "software through their normal channels."
#~ msgstr ""
#~ "Arch有两个常见的问题：它没有关于可以包括什么软件的明确政策，它发布的Linux"
#~ "内核带有非自由的blobs。Arch也没有关于不通过其正常渠道发布非自由软件的政"
#~ "策。"

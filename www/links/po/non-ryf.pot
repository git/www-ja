# LANGUAGE translation of https://www.gnu.org/links/non-ryf.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: non-ryf.html\n"
"POT-Creation-Date: 2022-05-02 08:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Non-RYF Computers with GNU/Linux preinstalled - GNU Project - Free Software "
"Foundation"
msgstr ""

#. type: Content of: <div><h2>
msgid ""
"Non-<abbr title=\"Respects Your Freedom\">RYF</abbr> computers with "
"GNU/Linux preinstalled"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"The computers sold by these companies require the use of a proprietary "
"BIOS.  A BIOS is a program used to load the operating system and start the "
"computer."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Also, if the computer uses an Intel processor, it most probably has the "
"Intel Management Engine (ME) which <a "
"href=\"https://www.fsf.org/blogs/licensing/intel-me-and-why-we-should-get-rid-of-me\"> "
"is a backdoor</a> and is known to have had security <a "
"href=\"/proprietary/proprietary-insecurity.html#intel-me-10-year-vulnerability\">flaws "
"in the past</a>."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Due to the above issues, computers sold by these companies do not have FSF's "
"<a href=\"https://fsf.org/ryf\">Respects Your Freedom certification</a> "
"though they're able to run a <a href=\"/distros/free-distros.html\">fully "
"free GNU/Linux operating system</a>."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If you represent a company and would like to have your computers mentioned "
"here please make sure that the default preinstalled GNU/Linux distro on your "
"computers is one that's <a href=\"/distros/free-distros.html\">endorsed by "
"the FSF</a> or is at least selectable as an option during purchasing."
msgstr ""

#. type: Content of: <div><ul><li><p>
msgid ""
"<a href=\"https://www.thinkpenguin.com/\">ThinkPenguin.com</a> offers "
"computers preinstalled with Trisquel."
msgstr ""

#. type: Content of: <div><ul><li><p>
msgid ""
"<a href=\"https://puri.sm/\">Purism</a> offers laptops and smartphones "
"preinstalled with PureOS."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and submitting translations of this article."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 2017, 2020, 2022 Free Software Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

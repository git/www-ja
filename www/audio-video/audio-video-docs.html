<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.97 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<title>Audio/Video Documentation - GNU Project - Free-Software Foundation</title>
<link rel="stylesheet" type="text/css" href="/side-menu.css" media="screen" />
 <!--#include virtual="/audio-video/po/audio-video-docs.translist" -->
<!--#include virtual="/server/banner.html" -->
<div class="nav">
<a id="side-menu-button" class="switch" href="#navlinks">
 <img id="side-menu-icon" height="32"
      src="/graphics/icons/side-menu.png"
      title="Audio/video menu"
      alt="&nbsp;[Audio-video menu]&nbsp;" />
</a>

<p class="breadcrumb">
 <a href="/"><img src="/graphics/icons/home.png" height="24"
    alt="GNU Home" title="GNU Home" /></a>&nbsp;/
 <a href="/audio-video/audio-video.html">Audio&nbsp;&amp;&nbsp;video</a>&nbsp;/
</p>
</div>

<div id="last-div" class="reduced-width">
<h2 class="c">Audio/Video Documentation</h2>

<div class="toc-inline">
<ul>
  <li><a href="#distribution-formats">Distribution Formats</a></li>
  <li><a href="#accessing-recordings">Accessing Recordings</a></li>
  <li><a href="#request-for-recordings">Request for Recordings</a></li>
</ul>
</div>


<h3 id="distribution-formats">Distribution Formats</h3>

<h4>Audio Formats</h4>

<p>We distribute most of our audio files in the <a
href="https://wiki.xiph.org/Vorbis">Ogg Vorbis</a> format. Unlike MP3,
this format has always been completely free and unencumbered by patents.
The <a href="https://wiki.xiph.org/Vorbis#More_information">Vorbis
wiki</a> provides lists of software and hardware that support it.</p>

<p>A few recent audio files are encoded in <a
href="https://wiki.xiph.org/OpusFAQ">Opus</a>, which is a low-delay, free
format that supports high audio quality.</p>

<h4>Video Formats</h4>

<p>We distribute most of our video files encoded with the Theora video
codec, encapsulated in an Ogg transport layer. They share the .ogg extension
with the audio files (or occasionally have the .ogv or .ogm extension).</p>

<p>The Theora video codec was designed as a free video codec unencumbered
by patent licensing restrictions. Its technical brilliance and superb 
compression rates should not be allowed to overshadow its most important
asset: <a href="/philosophy/free-sw.html">it respects users' freedom</a>!
Visit the <a href="https://wiki.xiph.org/Theora">Theora wiki at xiph.org</a>
for more information or to download players and codecs.</p>

<p>We use <a href="https://www.webmproject.org/">WebM</a> (with the VP8 or
VP9 video codec) instead of Ogg Theora for some videos; WebM is a more
recent format, also unencumbered by known software patents.</p>


<h3 id="accessing-recordings">Accessing Recordings</h3>

<p>Most of the ogg files have been encoded in a stream of 20 kb/s. Most
Internet users, including those with slow connections, should be able to
listen to the files streaming from our server.</p>

<p>Our media files can be streamed by the major browsers (e.g., Firefox),
and streaming will start automatically by default. You can also stream them
with <a href="https://www.videolan.org/vlc/">VLC</a>. For example:</p>

<p class="emph-box">
<kbd>vlc https://audio-video.gnu.org/audio/<var>file</var>.ogg</kbd>
</p>

<p>Another possibility is to download the media file, and play it later:</p>

<p class="emph-box">
<kbd>wget -q https://audio-video.gnu.org/audio/<var>file</var>.ogg</kbd>
</p>

<p>Wget with the <code>-c</code> switch will continue a transfer if a
smaller file of the same name exists in the download location.
For example,</p>

<p class="emph-box">
<kbd>wget -c https://audio-video.gnu.org/video/<var>file</var>.webm</kbd>
</p>


<h3 id="request-for-recordings">Request for Recordings</h3>

<p>If you are planning to attend a GNU event, please make a recording. It
is polite to ask permission from the event organizer. If you wish to make
the recording publicly available in a digital format, please choose one
that is accessible to free software. Theora or Vorbis are the best choice
as using one of these will add support to free, open, unencumbered formats.
Please choose the license we use for the speech recordings, and remember to
embed the license into the digital file so that people who receive a copy
of the file are aware of their freedoms.</p>

<p>If you have a good recording from a GNU event, and you would like to
share it with us, please send an email to &lt;<a
href="mailto:audio-recordings@gnu.org">audio-recordings@gnu.org</a>&gt;.
We are interested in both audio and video recordings, and can accept the
source in a wide range of formats (e.g., those supported by ffmpeg). We will
transcode to a standardised codec for the site, and optimize the encoder
settings.</p>

<p>When submitting recordings, please provide the following information:</p>
<ul>
<li>who recorded the speech and who owns the copyright;</li>
<li>the name of the event where the speech was recorded, including city
and country;</li>
<li>the date the recording was made;</li>
<li>confirmation our usual license is OK to use: <a
href="https://creativecommons.org/licenses/by-nd/4.0">Creative Commons
Attribution-NoDerivatives 4.0 International</a> (CC BY-ND 4.0);</li>
<li>the names of introductory speakers;</li>
<li>the title of the speech.</li>
</ul>

<p>We prefer to handle speech recordings as a single file rather than split
into sections, but if the original is in many parts, we can join it as
required. If the original is compressed with a lossy compression format
such as mpeg, divx or a proprietary format, <b>please don't transcode the
recording before sending it to us.</b> Given that we may transcode it to
a different bit rate and frame size, and insert copying information,
inserting yet another transcoding stage will simply degrade quality.</p>

<p>We don't have a public uploading facility for large media files. So, the
best way to send us the recording is to place it on a website and send us
the link. You can also use a <i>free</i> (as in freedom) file-sharing
service. If you experience any difficulty in this respect, we can probably
find a solution.</p>

<h4>Audio</h4>

<p>We prefer original recordings in the original recorded sample rate up
to 44100Hz. Monophonic is generally adequate for speech recordings and
saves a lot of space over stereo. We will accept the recording in the
original file type. If the original file is large, you may wish to
transcode to 64 kb/s mono Ogg Vorbis.</p>

<h4>Video</h4>

<p>The same is true for video as for audio. Send the recording in the
original frame size. If already compressed with a lossy codec, please
send the original. If your original is uncompressed or has a very low
compression/ large file size, please compress using Theora with video
quality set to 5 or more.</p>

<h4>Transcription</h4>

<p>If you have a transcription of a speech by a GNU speaker, or would like
to make one, please contact &lt;<a
href="mailto:audio-recordings@gnu.org">audio-recordings@gnu.org</a>&gt;.</p>

</div>
<!--#include virtual="/audio-video/audio-video-menu.html" -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2022 Free Software Foundation, Inc.</p>

<p>This page is licensed under a <a rel="license"
href="https://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2022/11/02 16:25:43 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

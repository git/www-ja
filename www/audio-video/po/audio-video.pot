# LANGUAGE translation of https://www.gnu.org/audio-video/audio-video.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: audio-video.html\n"
"POT-Creation-Date: 2022-11-02 15:37+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Audio/Video - GNU Project - Free-Software Foundation"
msgstr ""

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr ""

#. type: Attribute 'title' of: <div><a><img>
msgid "Audio/video menu"
msgstr ""

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Audio-video menu]&nbsp;"
msgstr ""

#. type: Content of: <div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><h2>
msgid "GNU Audio and Video"
msgstr ""

#. type: Attribute 'alt' of: <div><div><img>
msgid "[A gnu listening to music]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Here you can watch video or listen to audio recordings of speeches and other "
"events related to the GNU Project. Many of these recordings are made by "
"volunteers who attend our events, and we thank everyone for sending them in."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Speeches and interviews that are intended for the general public are listed "
"in the category <a href=\"/audio-video/philosophy-recordings.html#content\"> "
"GNU Philosophy and History</a>. Some of them are in Spanish or French, and a "
"few of the recordings have been transcribed and/or subtitled. The <a "
"href=\"/philosophy/speeches-and-interviews.html\">transcriptions</a> are "
"published in the Philosophy section."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Beside speeches and interviews, GNU Philosophy and History lists other "
"interesting audio and video material that a selection by type (at the top of "
"the list) will help you discover."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Resources dealing with more technical subjects are listed separately.  For "
"example,"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"presentations at the <a href=\"/ghm/previous.html\">GNU Hackers' "
"Meetings</a>,"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"speeches and group discussions on the <a "
"href=\"//gplv3.fsf.org/av/\">drafting and launching of GPLv3</a>,"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"videos about some <a "
"href=\"/audio-video/individual-projects.html#content\">individual "
"projects</a>."
msgstr ""

#. type: Content of: <div><h3>
msgid "Audio and video techniques"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Modern web browsers, such as GNU Icecat 3.5 or later can play these files "
"natively, but more information for everyone is available at <a "
"href=\"//www.fsf.org/campaigns/playogg/en/\">PlayOgg</a>, a campaign of the "
"Free Software Foundation."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Recordings are typically made using the <a "
"href=\"https://www.theora.org/\">Theora</a> (video) and <a "
"href=\"https://xiph.org/vorbis/\">Vorbis</a> (audio) codecs in the Ogg "
"container, but some videos are in the <a "
"href=\"https://www.webmproject.org/\">WebM</a> format instead. Their audio "
"tracks are usually encoded in Vorbis, but occasionally in <a "
"href=\"https://opus-codec.org/\">Opus</a>, a low-delay format, well suited "
"for real-time communication."
msgstr ""

#. type: Content of: <div><p>
msgid "The documentation page provides some basic information on"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/audio-video/audio-video-docs.html#distribution-formats\"> free "
"audio and video formats</a>,"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/audio-video/audio-video-docs.html#accessing-recordings\"> how to "
"access and play recordings</a>,"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/audio-video/audio-video-docs.html#request-for-recordings\"> how "
"to make a recording</a> if you wish to contribute one."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"All recordings are made available for verbatim copying and distribution, and "
"many are also available under the terms of the Creative Commons <a "
"href=\"https://creativecommons.org/licenses/by-nd/3.0/\">Attribution-No "
"Derivative Works</a> 3.0 license or later versions."
msgstr ""

#. type: Content of: <div><h3>
msgid "Feedback"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If you find a problem with the audio-video server or have a speech recording "
"to contribute, please report to <a "
"href=\"//savannah.gnu.org/task/?func=additem&amp;group=audio-video\">Make a "
"task</a> for maintainers of the project or fill a <a "
"href=\"//savannah.gnu.org/support/?func=additem&amp;group=audio-video\"> "
"support request</a>."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Come visit the <a href=\"//savannah.gnu.org/projects/audio-video/\"> "
"Audio-Video project page on Savannah</a>."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Add yourself to the low traffic <a "
"href=\"//lists.gnu.org/mailman/listinfo/audio-video\">audio-video mailing "
"list</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 2022 Free Software Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

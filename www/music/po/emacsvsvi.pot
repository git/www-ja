# LANGUAGE translation of https://www.gnu.org/music/emacsvsvi.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: emacsvsvi.html\n"
"POT-Creation-Date: 2021-07-12 16:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Emacs vs Vi - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <style>
#, no-wrap
msgid ""
".chorus { color: #6b3699; }\n"
"#download a { margin: 0 .5em; }\n"
"}\n"
msgstr ""

#. type: Content of: <div><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <div><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <div>
msgid ""
"</a>&nbsp;/ <a href=\"/fun/humor.html#content\">GNU&nbsp;humor</a>&nbsp;/ <a "
"href=\"/music/music.html#content\">Music</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "Emacs vs Vi"
msgstr ""

#. type: Content of: <div><p>
msgid "Sung to the tune of <cite>Ghost Riders in the Sky</cite> by the Outlaws"
msgstr ""

#. type: Content of: <div><div><p>
msgid "A hacker went a-coding armed with coffee and his brain"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Debating in himself whether to put it all in &ldquo;main&rdquo;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "When all at once his IRC was filled with hateful spite"
msgstr ""

#. type: Content of: <div><div><p>
msgid "A plethora of angry words about syntax highlight"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Their hands were writing fire and they called each other n00b&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "He'd never seen such sparking since the days of vacuum tubes"
msgstr ""

#. type: Content of: <div><div><p>
msgid "A bolt of fear went through him as he closed parentheses"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Torn between the &ldquo;write and quit&rdquo; and ol' C-x C-c"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Yippy kai yay&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "File I/O&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Emacs versus vi&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "He'd long endured the arguments, he knew the story well&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "So why condemn an editor and all its bytes to Hell?"
msgstr ""

#. type: Content of: <div><div><p>
msgid "From concepts of simplicity to a buffer-based OS&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "You'd think they'd find a better way to work out daily stress!"
msgstr ""

#. type: Content of: <div><div><p>
msgid "So learn your lesson, hacker, and you'll not regret one day&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "The time you spent recovering, and throwing chars away&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "What's popular is never what you'll get into your head&hellip;"
msgstr ""

#. type: Content of: <div><div><p>
msgid "So take a pointer from the pros&hellip; and choose the mighty ED!"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Other formats: <a href=\"/music/emacsvsvi.tex\">Latex</a> <a "
"href=\"/music/emacsvsvi.pdf\">PDF</a>"
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 2010 James Taylor (the lyrics)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Permission is granted to copy, distribute and/or modify this document under "
"the terms of the <a rel=\"license\" href=\"/licenses/fdl.html\">GNU Free "
"Documentation License, version 1.3</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

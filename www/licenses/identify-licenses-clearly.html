<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.97 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<title>Don't Say &ldquo;Licensed under GNU GPL 2&rdquo;!
- GNU Project - Free Software Foundation</title>
 <!--#include virtual="/licenses/po/identify-licenses-clearly.translist" -->
<!--#include virtual="/server/banner.html" -->
<div class="article reduced-width">
<h2>For Clarity's Sake, Please Don't Say &ldquo;Licensed under GNU GPL 2&rdquo;!</h2>

<address class="byline">by <a href="https://www.stallman.org/">Richard
Stallman</a></address>

<p>When I wrote the GNU General Public License (GNU GPL), in 1989, I
recognized that changes might be necessary: the FSF might someday have
reasons to publish a new version. So I called the license &ldquo;version 1,&rdquo;
and set up a framework to enable users to upgrade programs to later
license versions.</p>

<p>There was no customary way of doing this with a free license; as far
as I know, it had never been done before. Developers had released a
new version of a program under a different license, and maybe they had
made new releases of old versions offering use under a different
license, but they had never set up a systematic way to offer users the
choice of using a future license version for already-released versions
of a program.</p>

<p>I did not know how developers would respond to this innovation, so I
decided to give each developer a choice about allowing future
versions. This meant developers could release a program under GNU GPL
version 1 <b>only</b>, or release it under GPL version 1 <b>or any later
version</b>. The way developers state their choice is in the <b>license
notice</b> that goes at the start of each source file. That's where the
GPL says the decision is stated.</p>

<p>The Free Software Foundation urged developers to choose <b>or any
later version</b>, since that meant users would be free to use that
program under GNU GPL version 1, or under version 2 (once there was a
version 2), or under version 3 (once there was a version 3). And they
will be free to use it under version 4, if we ever have to make a
version 4.</p>

<p>Since then, the FSF has released GNU GPL version 2 in 1991 and version
3 in 2007. Each version offers developers the choice to insist on
that one license version only, or permit use under future license
versions. (GPL 3 also permits the &ldquo;proxy&rdquo; option, where a specified
Web page can subsequently give permission to use a particular future
version.)</p>

<p>Publishing programs with license upgrade permission is vital to avoid
incompatibility between programs released under different GPL versions.</p>

<p>Two different copyleft licenses are almost inevitably incompatible in
the absence of some explicit compatibility mechanism. That is because
each one necessarily requires modified versions of a program to be
released under that very same license. As a consequence, code
released under GPL version 2 only can't be merged with code released
under GPL version 3 only.</p>

<p>The mechanism for compatibility between GPL versions is to release a
program under &ldquo;version N or any later version.&rdquo; A program released
under GPL-2.0-or-later can be merged with code under GPL-3.0-or-later,
because &ldquo;3 or later&rdquo; is a subset of &ldquo;2 or later.&rdquo;</p>

<p>Some developers say, &ldquo;I will release now under GNU GPL version 3 only.
When I see GPL version 4, if I like it, I will relicense my program to
allow use under version 4.&rdquo; That will work fine if you are the only
author, provided you're still alive, healthy, contactable, and paying
attention at that time. But copyright lasts an insanely long time
nowadays; in the absence of major reforms, the copyright on your code
will last 70 years after your death in the US (and 100 years in
Mexico). Have you made arrangements for your heirs to consider the
question of relicensing your code to GPL version 4 if you are no
longer around to consider it?</p>

<p>But trouble will happen even during your lifetime. What if we release
GNU GPL version 4 ten years from now, and by that time 50 others have
added to your program, releasing their added code under GPL-3.0-only
simply because you did? You could approve GPL 4 for the program you
initially released, but it would be a big job to contact the 50
subsequent developers at that time to get their permission for GPL 4
usage of their additions.</p>

<p>The way to avoid these problems is by approving future GPL versions in
the license notice at the outset. Please put on each nontrivial file
of the source release a license notice of the form shown at the end of
the GPL version you are using.</p>

<p>Because we handle license compatibility this way, when people tell you
a program is released &ldquo;under GNU GPL version 2,&rdquo; they are leaving the
licensing of the program unclear. Is it released under GPL-2.0-only,
or GPL-2.0-or-later? Can you merge the code with packages released
under GPL-3.0-or-later?</p>

<p>When sites such as GitHub invite developers to choose &ldquo;GPL 3&rdquo; or &ldquo;GPL
2&rdquo; among other license options, and don't raise the issue of future
versions, this leads thousands of developers to leave their code's
licensing unclear. Asking those users to choose between &ldquo;only&rdquo; and
&ldquo;or later&rdquo; would lead them to make their code's licensing clear. It
also provides an opportunity to explain how the latter choice avoids
future incompatibility.</p>

<p>Abbreviated license indicators such as &ldquo;GPL-2.0&rdquo; or &ldquo;GPL-3.0&rdquo; will
lead to confusion too. People and organizations that don't recognize
the difference between &ldquo;2 only&rdquo; and &ldquo;2 or later&rdquo; will be prone to
write &ldquo;GPL-2.0&rdquo; in both cases, not realizing there is a distinction to
be made.</p>

<p>Therefore, when you use SPDX license indicators, please use these:</p>

<ul><li>GPL-2.0-only or GPL-2.0-or-later</li>
<li>GPL-3.0-only or GPL-3.0-or-later</li>
<li>LGPL-2.0-only or LGPL-2.0-or-later</li>
<li>LGPL-2.1-only or LGPL-2.1-or-later</li>
<li>LGPL-3.0-only or LGPL-3.0-or-later</li>
<li>AGPL-3.0-only or AGPL-3.0-or-later</li>
<li>GFDL-1.3-only or GFDL-1.3-or-later</li></ul>

<p>Please do not use the old, ambiguous license indicators, which will be
deprecated:</p>

<ul><li>GPL-2.0</li>
<li>GPL-3.0</li>
<li>LGPL-2.0</li>
<li>LGPL-2.1</li>
<li>LGPL-3.0</li>
<li>AGPL-3.0</li>
<li>GFDL-1.3.</li></ul>

<p>Giving developers the choice of GPL version &ldquo;1 only&rdquo; or GPL version &ldquo;1
or later&rdquo; seemed obligatory in 1989, but it has created a
complexity that we would be better off without. Meanwhile, several
licenses that unconditionally give users the choice to upgrade to
later license versions have become widely accepted. These include the
Mozilla Public License and the Eclipse Public License.  Each version
says that a user is free to use the work under later versions, if any,
of the same license.  The Creative Commons copyleft license, CC BY-SA,
permits users to upgrade the license version when they modify the work.
</p>

<p>Perhaps we should switch to that approach for future versions of the
GNU GPL. But that is something to think about in the future.</p>

<p>We thank SPDX for deciding to change the short identifiers for the
GNU family of licenses to make the &ldquo;or later&rdquo; versus &ldquo;only&rdquo; choice
explicit. The upcoming version of the SPDX License List for license
identifiers will use the identifiers recommended above. The confusing
identifiers, such as &ldquo;GPL-2.0,&rdquo; will be deprecated. We ask people to
replace them with the new unambiguous identifiers as soon as possible.</p>

<p>By using identifiers that are explicit as to <b>only</b> or <b>any later
version</b>, we can make the community aware of the difference and
encourage developers to state their decisions clearly.</p>
</div>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and contributing translations
of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2017, 2020, 2022 Free Software Foundation, Inc.</p>

<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2022/01/22 18:22:54 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

# Lithuanian translation of http://www.gnu.org/licenses/copyleft.html
# Copyright (C) 2015 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Donatas Klimašauskas <klimasauskas.d@gmail.com>, 2015, 2016.
# April 2017: fix links (TG).
# May 2021: standardize the copyright line.
#
msgid ""
msgstr ""
"Project-Id-Version: copyleft.html\n"
"POT-Creation-Date: 2022-01-02 16:26+0000\n"
"PO-Revision-Date: 2016-01-25 23:37+0200\n"
"Last-Translator: Donatas Klimašauskas <klimasauskas.d@gmail.com>\n"
"Language-Team: Lithuanian <web-translators@gnu.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2016-11-01 14:25+0000\n"

#. type: Content of: <title>
msgid "What is Copyleft? - GNU Project - Free Software Foundation"
msgstr "Kas yra copyleft? - GNU projektas - Laisvos programinės įrangos fondas"

#. type: Attribute 'content' of: <meta>
msgid "GNU, FSF, Free Software Foundation, Linux, Copyleft"
msgstr "GNU, FSF, Laisvos programinės įrangos fondas, Linux, Copyleft"

#. type: Content of: <div><h2>
msgid "What is Copyleft?"
msgstr "Kas yra copyleft?"

#. type: Content of: <div><p>
# | Copyleft is a general method for making a program (or other work) [-<a
# | href=\"/philosophy/free-sw.html\">free</a>,-] {+free (<a
# | href=\"/philosophy/free-sw.html\">in the sense of freedom, not &ldquo;zero
# | price&rdquo;</a>),+} and requiring all modified and extended versions of
# | the program to be free as well.
#, fuzzy
#| msgid ""
#| "Copyleft is a general method for making a program (or other work) <a href="
#| "\"/philosophy/free-sw.html\">free</a>, and requiring all modified and "
#| "extended versions of the program to be free as well."
msgid ""
"Copyleft is a general method for making a program (or other work) free (<a "
"href=\"/philosophy/free-sw.html\">in the sense of freedom, not &ldquo;zero "
"price&rdquo;</a>), and requiring all modified and extended versions of the "
"program to be free as well."
msgstr ""
"Copyleft yra bendras metodas padaryti programą (ar kitą darbą) <a href=\"/"
"philosophy/free-sw.html\">laisva</a> ir reikalaujant visų modifikuotų ir "
"išplėstų programos versijų būti laisvomis taip pat."

#. type: Content of: <div><p>
msgid ""
"The simplest way to make a program free software is to put it in the <a href="
"\"/philosophy/categories.html#PublicDomainSoftware\">public domain</a>, "
"uncopyrighted.  This allows people to share the program and their "
"improvements, if they are so minded.  But it also allows uncooperative "
"people to convert the program into <a href=\"/philosophy/categories."
"html#ProprietarySoftware\">proprietary software</a>.  They can make changes, "
"many or few, and distribute the result as a proprietary product.  People who "
"receive the program in that modified form do not have the freedom that the "
"original author gave them; the middleman has stripped it away."
msgstr ""
"Paprasčiausias būdas padaryti programą laisva programine įranga yra ją "
"patalpinant į <a href=\"/philosophy/categories.html#PublicDomainSoftware"
"\">viešąją sritį</a>, be autorių teisių.  Tai žmonėms leidžia dalintis ta "
"programa ir jos pagerinimais, jei jie taip pageidauja.  Bet jis taip pat "
"leidžia nebendradarbiaujantiems žmonėms tokią programą konvertuoti į <a href="
"\"/philosophy/categories.html#ProprietarySoftware\">nuosavybinę programinę "
"įrangą</a>.  Jie gali padaryti pakeitimus, daug ar kelis, ir platinti "
"rezultatą kaip nuosavybinį produktą.  Žmonės, kurie gauna tą programą ta "
"modifikuota forma neturi laisvės, kurią pirmasis autorius jiems suteikė; "
"tarpininkas ją pašalino."

#. type: Content of: <div><p>
# | In the <a href=\"/gnu/thegnuproject.html\">GNU project</a>, our aim is to
# | give <em>all</em> users the freedom to redistribute and change GNU
# | software.  If middlemen could strip off the freedom, [-we-] {+our code+}
# | might [-have-] {+&ldquo;have+} many [-users,-] {+users,&rdquo;+} but
# | [-those users-] {+it+} would not [-have-] {+give them+} freedom.  So
# | instead of putting GNU software in the public domain, we
# | &ldquo;copyleft&rdquo; it.  Copyleft says that anyone who redistributes
# | the software, with or without changes, must pass along the freedom to
# | further copy and change it.  Copyleft guarantees that every user has
# | freedom.
#, fuzzy
#| msgid ""
#| "In the <a href=\"/gnu/thegnuproject.html\">GNU project</a>, our aim is to "
#| "give <em>all</em> users the freedom to redistribute and change GNU "
#| "software.  If middlemen could strip off the freedom, we might have many "
#| "users, but those users would not have freedom.  So instead of putting GNU "
#| "software in the public domain, we &ldquo;copyleft&rdquo; it.  Copyleft "
#| "says that anyone who redistributes the software, with or without changes, "
#| "must pass along the freedom to further copy and change it.  Copyleft "
#| "guarantees that every user has freedom."
msgid ""
"In the <a href=\"/gnu/thegnuproject.html\">GNU project</a>, our aim is to "
"give <em>all</em> users the freedom to redistribute and change GNU "
"software.  If middlemen could strip off the freedom, our code might &ldquo;"
"have many users,&rdquo; but it would not give them freedom.  So instead of "
"putting GNU software in the public domain, we &ldquo;copyleft&rdquo; it.  "
"Copyleft says that anyone who redistributes the software, with or without "
"changes, must pass along the freedom to further copy and change it.  "
"Copyleft guarantees that every user has freedom."
msgstr ""
"<a href=\"/gnu/thegnuproject.html\">GNU projekte</a>, mūsų tikslas yra "
"suteikti <em>visiems</em> naudotojams laisvę išplatinti ir pakeisti GNU "
"programinę įrangą.  Jei tarpininkai galėtų pašalinti laisvę, mes galime "
"turėti daug naudotojų, bet tie naudotojai neturėtų laisvės.  Taigi, vietoje "
"GNU programinės įrangos patalpinimo viešojoje srityje, mes jai suteikiame "
"&bdquo;copyleft&ldquo;.  Copyleft sako, kad bet kas, kuris išplatina "
"programinę įrangą, su arba be pakeitimų, privalo kartu perleisti laisvę "
"toliau ją kopijuoti ir pakeisti.  Copyleft garantuoja, kad kiekvienas "
"naudotojas turi laisvę."

#. type: Content of: <div><p>
msgid ""
"Copyleft also provides an <a href=\"/philosophy/pragmatic.html\">incentive</"
"a> for other programmers to add to free software.  Important free programs "
"such as the GNU C++ compiler exist only because of this."
msgstr ""
"Copyleft taip pat kitiems programuotojams pateikia <a href=\"/philosophy/"
"pragmatic.html\">paskatą</a> papildyti laisvą programinę įrangą.  Svarbios "
"laisvos programos, tokios kaip GNU C++ kompiliatorius, egzistuoja tik dėl "
"šito."

#. type: Content of: <div><p>
# | Copyleft also helps programmers who want to contribute <a
# | [-href=\"/prep/tasks.html\">improvements</a>-]
# | {+href=\"/help/help.html\">improvements</a>+} to <a
# | href=\"/philosophy/free-sw.html\">free software</a> get permission to do
# | so.  These programmers often work for companies or universities that would
# | do almost anything to get more money.  A programmer may want to contribute
# | her changes to the community, but her employer may want to turn the
# | changes into a proprietary software product.
#, fuzzy
#| msgid ""
#| "Copyleft also helps programmers who want to contribute <a href=\"/prep/"
#| "tasks.html\">improvements</a> to <a href=\"/philosophy/free-sw.html"
#| "\">free software</a> get permission to do so.  These programmers often "
#| "work for companies or universities that would do almost anything to get "
#| "more money.  A programmer may want to contribute her changes to the "
#| "community, but her employer may want to turn the changes into a "
#| "proprietary software product."
msgid ""
"Copyleft also helps programmers who want to contribute <a href=\"/help/help."
"html\">improvements</a> to <a href=\"/philosophy/free-sw.html\">free "
"software</a> get permission to do so.  These programmers often work for "
"companies or universities that would do almost anything to get more money.  "
"A programmer may want to contribute her changes to the community, but her "
"employer may want to turn the changes into a proprietary software product."
msgstr ""
"Copyleft taip pat programuotojams, kurie nori prisidėti <a href=\"/prep/"
"tasks.html\">pagerinimais</a> <a href=\"/philosophy/free-sw.html\">laisvai "
"programinei įrangai</a>, padeda gauti leidimą taip padaryti.  Šie "
"programuotojai dažnai dirba kompanijoms ar universitetams, kurie padarytų "
"beveik viską, kad gautų daugiau pinigų.  Programuotoja gali norėti prie "
"bendruomenės prisidėti jos pakeitimais, bet jos darbdavys gali norėti tuos "
"pakeitimus paversti į nuosavybinės programinės įrangos produktą."

#. type: Content of: <div><p>
msgid ""
"When we explain to the employer that it is illegal to distribute the "
"improved version except as free software, the employer usually decides to "
"release it as free software rather than throw it away."
msgstr ""
"Kai mes darbdaviui paaiškiname, kad yra nelegalu platinti pagerintą versiją "
"kitaip, nei laisva programine įranga, tas darbdavys įprastai nusprendžia ją "
"išleisti laisva programine įranga, o ne ją išmesti."

#. type: Content of: <div><p>
msgid ""
"To copyleft a program, we first state that it is copyrighted; then we add "
"distribution terms, which are a legal instrument that gives everyone the "
"rights to use, modify, and redistribute the program's code, <em>or any "
"program derived from it</em>, but only if the distribution terms are "
"unchanged.  Thus, the code and the freedoms become legally inseparable."
msgstr ""
"Programai suteikdami copyleft, mes pirma pareiškiame, kad jai suteiktos "
"autorių teisės; tada mes pridedame platinimo sąlygas, kurios yra teisinis "
"instrumentas, kuris kiekvienam suteikia teises naudoti, modifikuoti ir "
"išplatinti tos programos kodą, <em>arba bet kurią iš jo išvestą programą</"
"em>, bet tiktais jei tos platinimo sąlygos yra nepakeistos.  Taip, tas kodas "
"ir tos laisvės tampa teisiškai neatskiriamos."

#. type: Content of: <div><p>
msgid ""
"Proprietary software developers use copyright to take away the users' "
"freedom; we use copyright to guarantee their freedom.  That's why we reverse "
"the name, changing &ldquo;copyright&rdquo; into &ldquo;copyleft.&rdquo;"
msgstr ""
"Nuosavybinės programinės įrangos autoriai autorių teises naudoja atimti "
"naudotojų laisvę; mes autorių teises naudojame jų laisvę garantuoti.  Štai "
"kodėl mes apverčiame tą pavadinimą, pakeisdami &bdquo;autorių teisės&ldquo; "
"į &bdquo;copyleft&ldquo;."

#. type: Content of: <div><p>
# | Copyleft is a way of using the copyright on the program.  It doesn't mean
# | abandoning the copyright; in fact, doing so would make copyleft
# | impossible.  The &ldquo;left&rdquo; in &ldquo;copyleft&rdquo; is not a
# | reference to the verb &ldquo;to leave&rdquo;&mdash;only to the direction
# | which is the [-inverse-] {+mirror image+} of [-&ldquo;right&rdquo;.-]
# | {+&ldquo;right.&rdquo;+}
#, fuzzy
#| msgid ""
#| "Copyleft is a way of using the copyright on the program.  It doesn't mean "
#| "abandoning the copyright; in fact, doing so would make copyleft "
#| "impossible.  The &ldquo;left&rdquo; in &ldquo;copyleft&rdquo; is not a "
#| "reference to the verb &ldquo;to leave&rdquo;&mdash;only to the direction "
#| "which is the inverse of &ldquo;right&rdquo;."
msgid ""
"Copyleft is a way of using the copyright on the program.  It doesn't mean "
"abandoning the copyright; in fact, doing so would make copyleft impossible.  "
"The &ldquo;left&rdquo; in &ldquo;copyleft&rdquo; is not a reference to the "
"verb &ldquo;to leave&rdquo;&mdash;only to the direction which is the mirror "
"image of &ldquo;right.&rdquo;"
msgstr ""
"Copyleft yra autorių teisių programai naudojimo būdas.  Tai nereiškia "
"autorių teisių atsisakymo; iš tiesų, taip elgiantis copyleft padarytų "
"neįmanomu.  Su &bdquo;left&ldquo;<sup><a href=\"#TransNote1\">1</a></sup> iš "
"&bdquo;copyleft&ldquo; nėra kalbama apie veiksmažodį &bdquo;palikti&ldquo; "
"&ndash; tik apie kryptį, kuri yra priešinga &bdquo;dešinei&ldquo;."

#. type: Content of: <div><p>
msgid ""
"Copyleft is a general concept, and you can't use a general concept directly; "
"you can only use a specific implementation of the concept.  In the GNU "
"Project, the specific distribution terms that we use for most software are "
"contained in the <a href=\"/licenses/gpl.html\"> GNU General Public License</"
"a>.  The GNU General Public License is often called the GNU GPL for short. "
"There is also a <a href=\"/licenses/gpl-faq.html\">Frequently Asked "
"Questions</a> page about the GNU GPL.  You can also read about <a href=\"/"
"licenses/why-assign.html\">why the FSF gets copyright assignments from "
"contributors</a>."
msgstr ""
"Copyleft yra bendrinė koncepcija ir jūs negalite bendrinės koncepcijos "
"naudoti tiesiogiai; jūs galite naudoti tik konkretų tos koncepcijos "
"įgyvendinimą.  GNU projekte, konkrečios platinimo sąlygos, kurias mes "
"naudojame daugumai programinės įrangos, yra <a href=\"/licenses/gpl.html"
"\">GNU bendrojoje viešojoje licencijoje</a>.  GNU bendroji viešoji licencija "
"dažnai yra sutrumpintai vadinama GNU GPL.  Taip pat yra <a href=\"/licenses/"
"gpl-faq.html\">Dažnai užduodamų klausimų</a> puslapis apie GNU GPL.  Jūs "
"taip pat galite paskaityti apie <a href=\"/licenses/why-assign.html\">kodėl "
"FSF iš prisidedančiųjų gauna autorių teisių priskyrimus</a>."

#. type: Content of: <div><p>
msgid ""
"An alternate form of copyleft, the <a href=\"/licenses/agpl.html\">GNU "
"Affero General Public License (AGPL)</a> is designed for programs that are "
"likely to be used on servers.  It ensures that modified versions used to "
"implement services available to the public are released as source code to "
"the public."
msgstr ""
"Alternatyvi copyleft forma &ndash; <a href=\"/licenses/agpl.html\">GNU "
"Affero bendroji viešoji licencija (AGPL)</a> yra suprojektuota programoms, "
"kurios tikėtina naudojamos serveriuose.  Ji užtikrina, kad modifikuotos "
"versijos panaudotos įgyvendinti visuomenei prieinamas paslaugas yra "
"visuomenei išleistos šaltinio kodu."

#. type: Content of: <div><p>
# | A compromise form of copyleft, the <a href=\"/licenses/lgpl.html\">GNU
# | Lesser General Public License (LGPL)</a> applies to a few (but not all)
# | GNU libraries. To learn more about properly using the LGPL, please read
# | the article [-<a href=\"/philosophy/why-not-lgpl.html\"><cite>Why-]
# | {+&ldquo;<a href=\"/philosophy/why-not-lgpl.html\">Why+} you shouldn't use
# | the Lesser GPL for your next [-library</cite></a>.-]
# | {+library</a>.&rdquo;+}
#, fuzzy
#| msgid ""
#| "A compromise form of copyleft, the <a href=\"/licenses/lgpl.html\">GNU "
#| "Lesser General Public License (LGPL)</a> applies to a few (but not all) "
#| "GNU libraries. To learn more about properly using the LGPL, please read "
#| "the article <a href=\"/philosophy/why-not-lgpl.html\"><cite>Why you "
#| "shouldn't use the Lesser GPL for your next library</cite></a>."
msgid ""
"A compromise form of copyleft, the <a href=\"/licenses/lgpl.html\">GNU "
"Lesser General Public License (LGPL)</a> applies to a few (but not all) GNU "
"libraries. To learn more about properly using the LGPL, please read the "
"article &ldquo;<a href=\"/philosophy/why-not-lgpl.html\">Why you shouldn't "
"use the Lesser GPL for your next library</a>.&rdquo;"
msgstr ""
"Kompromisinė copyleft forma &ndash; <a href=\"/licenses/lgpl.html\">GNU "
"mažiau bendroji viešoji licencija (LGPL)</a> taikoma kelioms (bet ne visoms) "
"GNU bibliotekoms. Siekiant sužinoti daugiau apie tinkamą LGPL naudojimą, "
"prašome perskaitykite straipsnį <a href=\"/philosophy/why-not-lgpl.html"
"\"><cite>Kodėl jūs neturėtumėte naudoti Mažiau GPL jūsų kitai bibliotekai</"
"cite></a>."

#. type: Content of: <div><p>
msgid ""
"The <a href=\"/licenses/fdl.html\">GNU Free Documentation License (FDL)</a> "
"is a form of copyleft intended for use on a manual, textbook or other "
"document to assure everyone the effective freedom to copy and redistribute "
"it, with or without modifications, either commercially or noncommercially."
msgstr ""
"<a href=\"/licenses/fdl.html\">GNU laisvos dokumentacijos licencija (FDL)</"
"a> yra copyleft forma numatyta naudoti vadovėliui, mokomajai knygai ar kitam "
"dokumentui kiekvienam užtikrinant efektyvią laisvę jį kopijuoti ir "
"išplatinti, su arba be modifikacijų, arba komercijai, arba ne komercijai."

#. type: Content of: <div><p>
msgid ""
"The appropriate license is included in many manuals and in each GNU source "
"code distribution."
msgstr ""
"Atitinkamą licenciją turi daug vadovėlių ir kiekvienas GNU šaltinio kodo "
"platinimas."

#. type: Content of: <div><p>
msgid ""
"All these licenses are designed so that you can easily apply them to your "
"own works, assuming you are the copyright holder.  You don't have to modify "
"the license to do this, just include a copy of the license in the work, and "
"add notices in the source files that refer properly to the license."
msgstr ""
"Visos šios licencijos yra suprojektuotos taip, kad jūs lengvai galite jas "
"taikyti jūsų paties darbams, priimant jūs esate autorių teisių turėtojas.  "
"Šito padarymui jūs neturite licencijos modifikuoti, tiesiog į darbą įdėkite "
"licencijos kopiją ir į šaltinio failus įdėkite pastabas, kuriomis tinkamai "
"nukreipiama į tą licenciją."

#. type: Content of: <div><p>
msgid ""
"Using the same distribution terms for many different programs makes it easy "
"to copy code between various different programs.  When they all have the "
"same distribution terms, there is no problem.  The Lesser GPL, version 2, "
"includes a provision that lets you alter the distribution terms to the "
"ordinary GPL, so that you can copy code into another program covered by the "
"GPL.  Version 3 of the Lesser GPL is built as an exception added to GPL "
"version 3, making the compatibility automatic."
msgstr ""
"Tų pačių platinimo sąlygų naudojimas daug skirtingų programų padaro lengvu "
"kodo kopijavimą tarp įvairių skirtingų programų.  Kai jos visos turi tas "
"pačias platinimo sąlygas, nėra jokios problemos.  Mažiau GPL, 2 versija, "
"įtraukia nuostatą, kuri jums leidžia pakoreguoti platinimo sąlygas iki "
"įprastos GPL taip, kad jūs galėtumėte kodą nukopijuoti į dar vieną programą "
"padengtą pagal GPL.  3 Mažiau GPL versija yra sukonstruota kaip prie 3 "
"versijos GPL pridėta išimtis, tą suderinamumą padaranti automatišku."

#. type: Content of: <div><p>
msgid ""
"If you would like to copyleft your program with the GNU GPL or the GNU LGPL, "
"please see the <a href=\"/licenses/gpl-howto.html\">license instructions "
"page</a> for advice.  Please note that you must use the entire text of the "
"license you choose.  Each is an integral whole, and partial copies are not "
"permitted."
msgstr ""
"Jei jūs pageidautumėte savo programai suteikti copyleft su GNU GPL arba GNU "
"LGPL, prašome pamatykite <a href=\"/licenses/gpl-howto.html\">licencijos "
"instrukcijų puslapį</a> patarimui.  Prašome pasižymėkite, kad jūs privalote "
"panaudoti visą jūsų pasirinktos licencijos tekstą.  Kiekviena yra integrali "
"visuma ir dalinės kopijos nėra leidžiamos."

#. type: Content of: <div><p>
msgid ""
"If you would like to copyleft your manual with the GNU FDL, please see the "
"instructions at the <a href=\"/licenses/fdl.html#addendum\">end</a> of the "
"FDL text, and the <a href=\"/licenses/fdl-howto.html\">GFDL instructions "
"page</a>.  Again, partial copies are not permitted."
msgstr ""
"Jei jūs pageidautumėte savo vadovėliui suteikti copyleft su GNU FDL, prašome "
"pamatykite instrukcijas FDL teksto <a href=\"/licenses/fdl.html#addendum"
"\">pabaigoje</a> ir <a href=\"/licenses/fdl-howto.html\">GFDL instrukcijų "
"puslapį</a>.  Dar kartą, dalinės kopijos nėra leidžiamos."

#. type: Content of: <div><p>
# | It is a legal mistake to use a backwards C in a circle instead of a
# | copyright symbol.  Copyleft is based legally on copyright, so the work
# | should have a copyright notice.  A copyright notice requires either the
# | copyright symbol (a C in a circle) or the word
# | [-&ldquo;Copyright&rdquo;.-] {+&ldquo;Copyright.&rdquo;+}
#, fuzzy
#| msgid ""
#| "It is a legal mistake to use a backwards C in a circle instead of a "
#| "copyright symbol.  Copyleft is based legally on copyright, so the work "
#| "should have a copyright notice.  A copyright notice requires either the "
#| "copyright symbol (a C in a circle) or the word &ldquo;Copyright&rdquo;."
msgid ""
"It is a legal mistake to use a backwards C in a circle instead of a "
"copyright symbol.  Copyleft is based legally on copyright, so the work "
"should have a copyright notice.  A copyright notice requires either the "
"copyright symbol (a C in a circle) or the word &ldquo;Copyright.&rdquo;"
msgstr ""
"Atvirkštinės C apskritime vietoje autorių teisių simbolio naudojimas yra "
"teisinė klaida.  Copyleft yra teisiškai paremtas autorių teisėmis, taigi, "
"darbas turėtų turėti autorių teisių pastabą.  Autorių teisių pastabai "
"reikalingas arba autorių teisių simbolis (C apskritime), arba žodis &bdquo;"
"Copyright&ldquo;<sup><a href=\"#TransNote2\">2</a></sup>."

#. type: Content of: <div><p>
msgid ""
"A backwards C in a circle has no special legal significance, so it doesn't "
"make a copyright notice.  It may be amusing in book covers, posters, and "
"such, but <a href=\"https://en.wikipedia.org/wiki/Copyleft#Symbol\"> be "
"careful how you represent it in a web page!</a>"
msgstr ""
"Atvirkštinė C apskritime neturi jokios specialios teisinės svarbos, taigi, "
"ji nesukuria autorių teisių pastabos.  Ji gali atrodyti mielai ant knygų "
"viršelių, skrajučių ir panašiai, bet <a href=\"https://en.wikipedia.org/wiki/"
"Copyleft#Symbol\">būkite atsargūs kaip jūs ją perteikiate tinklapyje!</a>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""
"<h3>Vertėjo pastabos</h3>\n"
"<ol>\n"
"<li id=\"TransNote1\">Angliškas žodis, kuris gali reikšti &bdquo;"
"kairė&ldquo; arba &bdquo;paliktas&ldquo;.</li>\n"
"<li id=\"TransNote2\">Turi būti būtent angliškas žodis &bdquo;"
"Copyright&ldquo;, nes tik toks pripažįstamas tarptautinėmis sutartimis; "
"vertimai į kitas kalbas neturi teisinės svarbos.</li>\n"
"</ol>"

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Bendrus FSF ir GNU užklausimus prašome atsiųsti į <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>.  Taip pat, yra ir <a href=\"/contact/\">kiti "
"būdai susisiekti</a> su FSF.  Neveikiančių nuorodų ir kiti pataisymai arba "
"pasiūlymai gali būti atsiųsti į <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

# TODO: submitting -> contributing.
#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
# || No change detected.  The change might only be in amounts of spaces.
#, fuzzy
#| msgid ""
#| "Please see the <a href=\"/server/standards/README.translations.html"
#| "\">Translations README</a> for information on coordinating and "
#| "contributing translations of this article."
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Mes dirbame sunkiai ir labai stengiamės, kad pateiktume tikslius, geros "
"kokybės vertimus.  Tačiau mes nesame išimtys netobulumui.  Prašome siųskite "
"savo komentarus ir bendrus pasiūlymus šia prasme į <a href=\"mailto:web-"
"translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>.</p>\n"
"<p>Mūsų tinklapių vertimų koordinavimo ir pateikimo informaciją pamatykite "
"<a href=\"/server/standards/README.translations.html\">Vertimų "
"PERSKAITYKITEMANE</a>."

#. type: Content of: <div><p>
# | Copyright &copy; [-2011, 2012, 2016-] {+1996-2001, 2004, 2005, 2007-2009,
# | 2013, 2016, 2022+} Free Software Foundation, Inc.
#, fuzzy
#| msgid "Copyright &copy; 2011, 2012, 2016 Free Software Foundation, Inc."
msgid ""
"Copyright &copy; 1996-2001, 2004, 2005, 2007-2009, 2013, 2016, 2022 Free "
"Software Foundation, Inc."
msgstr ""
"Copyright &copy; 2011, 2012, 2016 Laisvos programinės įrangos fondas, "
"korporacija"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Šiam puslapiui taikoma <a rel=\"license\" href=\"http://creativecommons.org/"
"licenses/by-nd/4.0/deed.lt\">Creative Commons Priskyrimas - Jokių išvestinių "
"kūrinių 4.0 Tarptautinė licencija</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr " "

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Atnaujinta:"

#~ msgid ""
#~ "Please see the <a href=\"/server/standards/README.translations.html"
#~ "\">Translations README</a> for information on coordinating and submitting "
#~ "translations of this article."
#~ msgstr ""
#~ "Mes dirbame sunkiai ir labai stengiamės, kad pateiktume tikslius, geros "
#~ "kokybės vertimus.  Tačiau mes nesame išimtys netobulumui.  Prašome "
#~ "siųskite savo komentarus ir bendrus pasiūlymus šia prasme į <a href="
#~ "\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>.</"
#~ "p>\n"
#~ "<p>Mūsų tinklapių vertimų koordinavimo ir pateikimo informaciją "
#~ "pamatykite <a href=\"/server/standards/README.translations.html\">Vertimų "
#~ "PERSKAITYKITEMANE</a>."

#, fuzzy
#~| msgid ""
#~| "Copyright &copy; 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, "
#~| "2005, 2006, 2007, 2008, 2009, 2014, 2015 Free Software Foundation, Inc."
#~ msgid ""
#~ "Copyright &copy; 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, "
#~ "2005, 2006, 2007, 2008, 2009, 2014, 2015, 2016, 2017, 2018 Free Software "
#~ "Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, "
#~ "2005, 2006, 2007, 2008, 2009, 2014, 2015 Free Software Foundation, Inc. "
#~ "(Laisvos programinės įrangos fondas, korporacija)"

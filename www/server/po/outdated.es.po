# Spanish translation for http://www.gnu.org/server/outdated.html
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the original file.
# Dora Scilipoti <dora AT gnu DOT org>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: outdated.html\n"
"POT-Creation-Date: 2021-10-21 11:26+0000\n"
"PO-Revision-Date: 2014-08-16 10:05+0100\n"
"Last-Translator: Dora Scilipoti <dora AT gnu DOT org>\n"
"Language-Team: Spanish <www-es-general@gnu.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Spanish\n"

#. #set var="ORIGINAL_LINK"
#.        value="<a class=\"original-link\" href=\"${ORIGINAL_FILE}.en\">" 
#. #if expr='${OUTDATED_SINCE}' 
#.  TRANSLATORS: The date will follow this string. 
#. type: Content of: <div><div><p>
msgid "This translation may not reflect the changes made since"
msgstr ""
"Esta traducción podría no reflejar los cambios introducidos después del "

#
#. #echo encoding="none" var="OUTDATED_SINCE" 
#. TRANSLATORS: The link to the English page will follow this string. 
#. type: Content of: <div><div><p>
msgid "in the"
msgstr "en la"

#
#. #echo encoding="none" var="ORIGINAL_LINK" 
#. TRANSLATORS: The text of the link; the next string is the last part
#.              of the sentence (just a period in English). 
#. type: Content of: <div><div><p>
msgid "English original"
msgstr "versión original en inglés"

#. type: Content of: <div><div>
msgid "."
msgstr "."

#
#. #else 
#. TRANSLATORS: The link to the English page will follow this string. 
#. type: Content of: <div><div>
msgid "This translation may not reflect the latest changes to"
msgstr ""
"Esta traducción podría no reflejar los últimos cambios introducidos en la"

#
#. #echo encoding="none" var="ORIGINAL_LINK" 
#. TRANSLATORS: The text of the link; the next string is the last part
#.              of the sentence (just a period in English). 
#. type: Content of: <div><div>
msgid "the English original"
msgstr "versión original en inglés"

#. type: Content of: <div>
msgid "<span>.</span>"
msgstr "<span>.</span>"

#
#. #if expr='${DIFF_FILE}' 
#. #set var="DIFF_LINK" value="<a href=\"${DIFF_FILE}\">" 
#. TRANSLATORS: The link to the page with differences
#.              will follow this string. 
#. type: Content of: <p>
msgid "You should take a look at"
msgstr "Puede consultar"

#
#. #echo encoding="none" var="DIFF_LINK" 
#. TRANSLATORS: The text of the link; the next string is the last part
#.              of the sentence (just a period in English). 
#. type: Content of: <p>
msgid "those changes"
msgstr "los cambios"

#. type: Content of: outside any tag (error?)
msgid "<span class=\"outdated-html-dummy-span\">.</span>"
msgstr "<span class=\"outdated-html-dummy-span\">.</span>"

# type: Content of: <div><p>
#. #endif 
#. #if expr='${LANGUAGE_SUFFIX} = /^[.](de|es|fr)$/' 
#. TRANSLATORS: Please replace web-translators@gnu.org with the email address
#.              of your translation team. 
#. type: Content of: outside any tag (error?)
msgid ""
"If you wish to help us maintain this page, please contact <a href=\"mailto:"
"web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>."
msgstr ""
"Si desea colaborar en el mantenimiento de esta página, escriba a <a href="
"\"mailto:www-es-general@gnu.org\">&lt;www-es-general@gnu.org&gt;</a>."

#
#. #else 
#. type: Content of: outside any tag (error?)
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on maintaining translations of "
"this article."
msgstr ""
"En la <a href=\"/server/standards/translations/es/index.html\">Guía para la "
"traducción al español</a> encontrará información sobre cómo colaborar para "
"actualizar este artículo."

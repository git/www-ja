# Korean translation of http://www.gnu.org/server/footer-text.html
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
#
# Chang-hun Song <chsong@gnu.org>, 2012.
# Jan 2019: unfuzzify (T. Godefroy).
# April 2020: reorganize includes to fit new layout.
#
msgid ""
msgstr ""
"Project-Id-Version: footer-text.html\n"
"POT-Creation-Date: 2021-01-20 22:25+0000\n"
"PO-Revision-Date: 2012-03-07 04:04+0900\n"
"Last-Translator: Chang-hun Song <chsong@gnu.org>\n"
"Language-Team: Korean <www-ko-translators@gnu.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Outdated-Since: 2014-03-29 05:38+0000\n"

#. type: Content of: <div><div>
msgid "<a href=\"#top\"><b>&#9650;</b></a>"
msgstr ""

#. type: Content of: <div><div><div>
# | <a [-href=\"#header\"><span>BACK-] {+href=\"#top\"
# | class=\"close\"><span>BACK+} TO [-TOP </span>&#11165;</a>-]
# | {+TOP</span></a>+}
#, fuzzy
#| msgid "<a href=\"#header\"><span>BACK TO TOP </span>&#11165;</a>"
msgid "<a href=\"#top\" class=\"close\"><span>BACK TO TOP</span></a>"
msgstr "<a href=\"#header\"><span>페이지 상단으로 가기 </span>&#11165;</a>"

#. #echo encoding="none" var="language_selector" 
#. type: Content of: <div><div><div>
msgid "Set language"
msgstr ""

# TODO
#. type: Content of: <div><div><p>
msgid "Available for this page:"
msgstr "이 페이지의 번역문들"

#. type: Content of: <div><div>
# | <a href=\"#header\"><span>BACK TO TOP [-</span>&#11165;</a>-]
# | {+</span>&#9650;</a>+}
#, fuzzy
#| msgid "<a href=\"#header\"><span>BACK TO TOP </span>&#11165;</a>"
msgid "<a href=\"#header\"><span>BACK TO TOP </span>&#9650;</a>"
msgstr "<a href=\"#header\"><span>페이지 상단으로 가기 </span>&#11165;</a>"

#. type: Content of: <div><blockquote><p><a>
msgid "<a href=\"//www.fsf.org\">"
msgstr "<a href=\"//www.fsf.org/\">"

#. type: Attribute 'alt' of: <div><blockquote><p><a><img>
msgid "&nbsp;[FSF logo]&nbsp;"
msgstr ""

#. type: Content of: <div><blockquote><p>
msgid ""
"</a><strong> &ldquo;The Free Software Foundation (FSF) is a nonprofit with a "
"worldwide mission to promote computer user freedom. We defend the rights of "
"all software users.&rdquo;</strong>"
msgstr ""

#. type: Content of: <div><div>
msgid ""
"<a class=\"join\" href=\"//www.fsf.org/associate/support_freedom?"
"referrer=4052\">JOIN</a> <a class=\"donate\" href=\"//donate.fsf.org/"
"\">DONATE</a> <a class=\"shop\" href=\"//shop.fsf.org/\">SHOP</a>"
msgstr ""

#~ msgid "<a href=\"#\" class=\"close\"></a>"
#~ msgstr "<a href=\"#\" class=\"close\"></a>"

#~ msgid ""
#~ "The <a href=\"//www.fsf.org\">Free Software Foundation</a> is the "
#~ "principal organizational sponsor of the GNU Operating System.  "
#~ "<strong>Support GNU and the FSF</strong> by <a href=\"//shop.fsf.org/"
#~ "\">buying manuals and gear</a>, <a href=\"https://my.fsf.org/associate/"
#~ "support_freedom?referrer=4052\"> <strong>joining the FSF</strong></a> as "
#~ "an associate member, or <a href=\"//donate.fsf.org/\"><strong>making a "
#~ "donation</strong></a>."
#~ msgstr ""
#~ "<small><a href=\"http://www.fsf.org\">자유 소프트웨어 재단</a>은 <a href="
#~ "\"http://www.gnu.org/\">GNU 운영 체제</a>의 주요한 기관 스폰서입니다. "
#~ "<strong>우리의 임무는 컴퓨터 소프트웨어를 사용하고 학습하며, 복제, 개작, "
#~ "재배포할 자유를 유지, 보호, 증진하는 것과 자유 소프트웨어 사용자들의 권리"
#~ "를 지키는 것입니다. </strong> <a href=\"http://shop.fsf.org/\">매뉴얼과 물"
#~ "품</a>을 구입하거나 <a href=\"http://www.fsf.org/join\">FSF 회원에 가입</"
#~ "a>하는 방법으로, 그리고 <a href=\"http://donate.fsf.org/\">FSF로 직접</a> "
#~ "또는 <a href=\"http://flattr.com/thing/313733/gnuproject-on-Flattr"
#~ "\">Flattr</a>를 통한 기부로 GNU와 FSF를 지원해 주세요.</small>"

#~ msgid "<a href=\"//www.fsf.org/\">FSF</a>"
#~ msgstr "<a href=\"//www.fsf.org/\">FSF</a>"

#~ msgid "<a href=\"/fun/fun.html\">GNU&nbsp;Fun</a>"
#~ msgstr "<a href=\"/fun/fun.html\">GNU&nbsp;펀</a>"

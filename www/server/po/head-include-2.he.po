# Hebrew translation of http://www.gnu.org/server/head-include-2.html
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnu.org article.
# Amir E. Aharoni <amir.aharoni@member.fsf.org>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: head-include-2.html\n"
"POT-Creation-Date: 2024-11-13 15:26+0000\n"
"PO-Revision-Date: 2009-11-11 09:59+0200\n"
"Last-Translator: Amir E. Aharoni <amir.aharoni@member.fsf.org>\n"
"Language-Team: Hebrew <www-he-translating@nongnu.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2015-05-13 13:59+0000\n"
"X-Generator: KBabel 1.11.4\n"

#. type: Attribute 'content' of: <meta>
msgid "width=device-width, initial-scale=1"
msgstr ""

#.  TRANSLATORS: Change direction to rtl if you translate
#.      the fundraiser and your script is right-to-left.  
#. type: Content of: <style>
#, no-wrap
msgid ""
"\n"
"#fundraiser { direction: ltr; }\n"
msgstr ""

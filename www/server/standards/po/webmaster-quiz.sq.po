# Translation into Albanian
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Besnik Bleta <besnik@programeshqip.org>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2022-10-26 17:55+0000\n"
"PO-Revision-Date: 2022-11-09 21:59+0200\n"
"Last-Translator: Besnik Bleta <besnik@programeshqip.org>\n"
"Language-Team: \n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

# type: Content of: <title>
#. type: Content of: <title>
msgid "Volunteer Webmaster Quiz - GNU Project - Free Software Foundation"
msgstr ""
"Provë për Vullnetar Webmaster - Projekti GNU - Free Software Foundation (FSF)"

# type: Content of: <h2>
#. type: Content of: <h2>
msgid "Volunteer Webmaster Quiz"
msgstr "Provë për Vullnetar Webmaster"

# type: Content of: <p>
#. type: Content of: <p>
msgid ""
"<strong>Want to get involved with www.gnu.org?</strong> If you're "
"enthusiastic, and know HTML and a little bit about our work, we'd love to "
"hear from you! As a volunteer GNU webmaster, you'll be doing great work to "
"help support our mission.  You can usefully help with webmastering in as "
"little as three hours a week or as much time as you have, on a regular basis."
msgstr ""
"<strong>Dëshironi të përfshiheni me www.gnu.org?</strong> Në qofshi i "
"ngazëllyer dhe dini ca HTML dhe pakëz mbi veprën tonë, do të kishim dëshirë "
"të lidheshit me ne! Si webmaster GNU vullnetar, do të bënit punë të mira në "
"ndihmë të përkrahjes së misionit tonë.   Mund të ndihmoni në mënyrë të "
"dobishme edhe me tre orë në javë, ose sa kohë të keni, rregullisht."

#. type: Content of: <p>
msgid ""
"To see what our typical tasks look like, please check our <a href=\"/server/"
"standards/README.webmastering.html\">Webmastering Guidelines</a>. You may be "
"disappointed to find out that these tasks don't call for the elaborate "
"webmastering techniques that are standard nowadays, but on the other hand "
"they are very diverse, and often will require quite a bit of inventiveness "
"on your part.  Moreover, they will give you experience in dealing with "
"people and complex organizations, a skill that may be applied to many other "
"areas of life."
msgstr ""
"Që të shihni se me çfarë ngjajnë punët tona të zakonshme, ju lutemi, hidhni "
"një sy <a href=\"/server/standards/README.webmastering.html\">Udhëzimeve "
"tona mbi Webmastering</a>. Mund të zhgënjeheni kur të shihni se këto punë "
"nuk lypin teknika të stërholluara webmastering-u që tanimë janë standard, "
"por më anë tjetër ato janë shumë të larmishme dhe shpesh kërkojnë jo pak "
"frymë sajuese nga ana juaj.  Për më tepër, ato do t’ju japin përvojë në "
"marrëdhënie me njerëz dhe ente komplekse, aftësu që mund të hyjë në punë në "
"mjaft fusha të tjera të jetës."

#. type: Content of: <p>
msgid "Currently, these are the areas that need more help:"
msgstr "Aktualisht, këto janë fushat që duan më tepër ndihmë:"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#writeentry\">Writing and reviewing items for "
"the Malware section</a>"
msgstr ""
"<a href=\"/server/standards/#writeentry\">Shkrim dhe shqyrtim artikujsh për "
"ndarjen Malware</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#mirrors\">Checking the status of mirrors</a>"
msgstr ""
"<a href=\"/server/standards/#mirrors\">Kontroll i gjendjes së pasqyrave</a>"

#. type: Content of: <ul><li>
msgid "<a href=\"/server/standards/#deadlink\">Fixing dead links</a>"
msgstr ""
"<a href=\"/server/standards/#deadlink\">Ndreqje lidhjesh të vdekura</a>"

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#addimage\">Adding images to the GNU Gallery</a>"
msgstr ""
"<a href=\"/server/standards/#addimage\">Shtim figurash te Galeria GNU</a>"

#. type: Content of: <p>
msgid ""
"If you feel comfortable with our routine tasks, or there is any particular "
"area you would like to work on, please answer the following questions in "
"English as best as you can, in your own words, and email them to <a href="
"\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>, preferably not "
"as an attachment. (If you don't feel like becoming a GNU webmaster after "
"all, there are <a href=\"/help/help.html\">other ways you can help the GNU "
"Project</a>.)"
msgstr ""
"Nëse ndiheni familjar me punët tona rutinë, ose nëse ka një fushë të veçantë "
"në të cilën do të donit të punonit, ju lutemi, përgjigjuni pyetjeve vijuese "
"në anglisht sa më mirë të mundeni, me fjalët tuaja dhe dërgojini me email te "
"<a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>, "
"mundësisht jo si një bashkëngjitje.  (Nëse s’keni ndërmend të bëheni "
"webmaster GNU, ka <a href=\"/help/help.html\">rrugë të tjera sipas të cilave "
"mund të ndihmoni Projektin GNU</a>.)"

#. type: Content of: <ol><li>
msgid ""
"How much time will you be able to devote each week to webmastering tasks?"
msgstr ""
"Sa kohë do të jeni në gjendje t’i kushtoni punëve <em>webmastering</em> çdo "
"javë?"

# type: Content of: <ol><li>
#. type: Content of: <ol><li>
msgid "What is the GNU operating system, and what is its purpose?"
msgstr "Sipas jush, çfarë është sistemi operativ GNU dhe çfarë qëllimi ka?"

#. type: Content of: <ol><li>
msgid "What is the GNU Project?"
msgstr "Ç’është Projekti GNU?"

#. type: Content of: <ol><li>
msgid "What does a GNU package maintainer do?"
msgstr "Ç’bën një mirëmbajtës paketash GNU?"

# type: Content of: <ol><li>
#. type: Content of: <ol><li>
msgid "What is free software?"
msgstr "Ç’është software-i i lirë?"

#. type: Content of: <ol><li>
msgid "What is open source, and how does it relate to free software?"
msgstr "Ç’është burimi i hapët dhe si lidhet ai me software-in e lirë?"

#. type: Content of: <ol><li>
msgid "What is the Free Software Foundation?"
msgstr "Ç’është Free Software Foundation?"

#. type: Content of: <ol><li>
msgid "How comfortable do you feel reading and writing in English?"
msgstr "Sa rehat ndiheni me lexim dhe shkrim në anglisht?"

# type: Content of: <ol><li>
#. type: Content of: <ol><li>
msgid "Do you have any experience with graphic design?"
msgstr "A keni përvojë me grafikën?"

# type: Content of: <ol><li>
#. type: Content of: <ol><li>
msgid ""
"Do you have any experience with CVS, Subversion or other version control "
"systems?"
msgstr ""
"A keni ndopak përvojë me CVS-në, Subversion-in ose të tjerë sisteme "
"kontrolli versionesh?"

# type: Content of: <p>
#. type: Content of: <p>
msgid ""
"Ready? Email your completed quiz to <a href=\"mailto:webmasters@gnu.org?"
"subject=I want to be a GNU Webmaster\">&lt;webmasters@gnu.org&gt;</a> "
"<strong>with a <em>brief</em> introduction about yourself.</strong>"
msgstr ""
"Gati? Provimin tuaj të plotësuar dërgojeni me email te <a href=\"mailto:"
"webmasters@gnu.org?subject=I want to be a GNU Webmaster\">&lt;webmasters@gnu."
"org&gt;</a> <strong>me një prezantim <em>të shkurtër</em> rreth vetes.</"
"strong>"

# type: Content of: <div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr "  "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Ju lutemi, pyetjet dhe kërkesat e përgjithshme rreth FSF-së &amp; GNU-së "
"dërgojini te <a href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  Ka "
"gjithashtu <a href=\"/contact/\">mënyra të tjera për t’u lidhur me</a> FSF-"
"në.  Njoftimet për lidhje të dëmtuara dhe ndreqje apo këshilla të tjera mund "
"të dërgohen te <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Përpiqemi fort dhe bëjmë sa mundemi për të ofruar përkthime me cilësi të "
"mirë dhe të përpikta.  Megjithatë, nuk jemi të përjashtuar nga "
"papërsosuritë. Ju lutemi, komentet dhe këshillat e përgjithshme lidhur me "
"këtë dërgojini te <a href=\"mailto:web-translators@gnu.org\">&lt;web-"
"translators@gnu.org&gt;</a>.</p><p>Për të dhëna mbi bashkërendimin dhe "
"parashtrimin e përkthimeve të faqeve tona web, shihni <a href=\"/server/"
"standards/README.translations.html\">README për përkthimet</a>."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2008, 2009, 2014, 2018, 2020, 2022 Free Software "
"Foundation, Inc."
msgstr ""
"Të drejta kopjimi &copy; 2008, 2009, 2014, 2018, 2020, 2022 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Kjo faqe mund të përdoret sipas një licence <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr "  "

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "U përditësua më:"

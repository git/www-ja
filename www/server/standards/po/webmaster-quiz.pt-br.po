# Brazilian Portuguese translation of https://www.gnu.org/server/standards/webmaster-quiz.html
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Rafael Fontenelle <rafaelff@gnome.org>, 2017-2020.
msgid ""
msgstr ""
"Project-Id-Version: webmaster-quiz.html\n"
"POT-Creation-Date: 2022-10-26 17:55+0000\n"
"PO-Revision-Date: 2020-01-12 15:39-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2022-05-10 12:57+0000\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: Content of: <title>
msgid "Volunteer Webmaster Quiz - GNU Project - Free Software Foundation"
msgstr ""
"Questionário de Voluntário a Webmaster - Projeto GNU - Free Software "
"Foundation"

#. type: Content of: <h2>
msgid "Volunteer Webmaster Quiz"
msgstr "Questionário de Voluntário a Webmaster"

#. type: Content of: <p>
#, fuzzy
#| msgid ""
#| "<strong>Want to get involved with www.gnu.org?</strong> If you're "
#| "enthusiastic, and know HTML and a little bit about our work, we'd love to "
#| "hear from you! As a volunteer GNU webmaster, you'll be doing great work "
#| "to help support our mission.  You can usefully help with webmastering in "
#| "as little as an hour a week, or as much time as you have."
msgid ""
"<strong>Want to get involved with www.gnu.org?</strong> If you're "
"enthusiastic, and know HTML and a little bit about our work, we'd love to "
"hear from you! As a volunteer GNU webmaster, you'll be doing great work to "
"help support our mission.  You can usefully help with webmastering in as "
"little as three hours a week or as much time as you have, on a regular basis."
msgstr ""
"<strong>Deseja colaborar com www.gnu.org?</strong> Se você é entusiasta, e "
"sabe HTML e um pouco do nosso trabalho, nós adoraríamos ouvir de você! Como "
"um voluntário a webmaster GNU, você estará fazendo um ótimo trabalho em "
"ajudar a apoiar nossa missão. Você pode ajudar com o webmastering em menos "
"de uma hora por semana, ou com o tempo que você tiver."

#. type: Content of: <p>
msgid ""
"To see what our typical tasks look like, please check our <a href=\"/server/"
"standards/README.webmastering.html\">Webmastering Guidelines</a>. You may be "
"disappointed to find out that these tasks don't call for the elaborate "
"webmastering techniques that are standard nowadays, but on the other hand "
"they are very diverse, and often will require quite a bit of inventiveness "
"on your part.  Moreover, they will give you experience in dealing with "
"people and complex organizations, a skill that may be applied to many other "
"areas of life."
msgstr ""
"Para ver como são as nossas tarefas típicas, consulte as nossas <a href=\"/"
"server/standards/README.webmastering.html\">Diretrizes para Webmasters</a>. "
"Você pode se decepcionar ao descobrir que essas tarefas não exigem as "
"técnicas elaboradas de webmastering que são padrão hoje em dia, mas, por "
"outro lado, elas são muito diversas e muitas vezes exigirão um pouco de "
"criatividade de sua parte. Além disso, elas lhe darão experiência em lidar "
"com pessoas e organizações complexas, uma habilidade que pode ser aplicada a "
"muitas outras áreas da vida."

#. type: Content of: <p>
msgid "Currently, these are the areas that need more help:"
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#writeentry\">Writing and reviewing items for "
"the Malware section</a>"
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#mirrors\">Checking the status of mirrors</a>"
msgstr ""

#. type: Content of: <ul><li>
msgid "<a href=\"/server/standards/#deadlink\">Fixing dead links</a>"
msgstr ""

#. type: Content of: <ul><li>
msgid ""
"<a href=\"/server/standards/#addimage\">Adding images to the GNU Gallery</a>"
msgstr ""

#. type: Content of: <p>
#, fuzzy
#| msgid ""
#| "If you feel comfortable with our routine tasks, please answer the "
#| "following questions in English as best as you can, and email them to <a "
#| "href=\"mailto:chief-webmaster@gnu.org\">&lt;chief-webmaster@gnu.org&gt;</"
#| "a>.  (If you don't feel like becoming a GNU webmaster after all, there "
#| "are <a href=\"/help/help.html\">other ways you can help the GNU Project</"
#| "a>.)"
msgid ""
"If you feel comfortable with our routine tasks, or there is any particular "
"area you would like to work on, please answer the following questions in "
"English as best as you can, in your own words, and email them to <a href="
"\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>, preferably not "
"as an attachment. (If you don't feel like becoming a GNU webmaster after "
"all, there are <a href=\"/help/help.html\">other ways you can help the GNU "
"Project</a>.)"
msgstr ""
"Se você se sentir confortável com nossas tarefas rotineiras, responda às "
"perguntas a seguir em inglês o melhor que puder e envie-as por e-mail para "
"<a href=\"mailto:chief-webmaster@gnu.org\">&lt;chief-webmaster@gnu.org&gt;</"
"a>. (Se você não se sente como um webmaster do GNU, existem <a href=\"/help/"
"help.html\">outras maneiras de ajudar o Projeto GNU</a>.)"

#. type: Content of: <ol><li>
msgid ""
"How much time will you be able to devote each week to webmastering tasks?"
msgstr ""

#. type: Content of: <ol><li>
#, fuzzy
#| msgid ""
#| "In your own words, what is the GNU operating system, and what is its "
#| "purpose?"
msgid "What is the GNU operating system, and what is its purpose?"
msgstr ""
"Em suas próprias palavras, o que é o sistema operacional GNU e qual o seu "
"propósito?"

#. type: Content of: <ol><li>
msgid "What is the GNU Project?"
msgstr "O que é o Projeto GNU?"

#. type: Content of: <ol><li>
msgid "What does a GNU package maintainer do?"
msgstr "O que um mantenedor de pacote GNU faz?"

#. type: Content of: <ol><li>
msgid "What is free software?"
msgstr "O que é o software livre?"

#. type: Content of: <ol><li>
msgid "What is open source, and how does it relate to free software?"
msgstr "O que é código aberto e qual a relação deste com software livre?"

#. type: Content of: <ol><li>
msgid "What is the Free Software Foundation?"
msgstr "O que é a Free Software Foundation?"

#. type: Content of: <ol><li>
msgid "How comfortable do you feel reading and writing in English?"
msgstr ""

#. type: Content of: <ol><li>
msgid "Do you have any experience with graphic design?"
msgstr "Você tem alguma experiência com design gráfico?"

#. type: Content of: <ol><li>
msgid ""
"Do you have any experience with CVS, Subversion or other version control "
"systems?"
msgstr ""
"Você tem alguma experiência com CVS, Subversion ou outro sistema de controle "
"de versão?"

#. type: Content of: <p>
#, fuzzy
#| msgid ""
#| "<strong>Ready? Email your completed quiz to <a href=\"mailto:chief-"
#| "webmaster@gnu.org?subject=I want to be a GNU Webmaster\">&lt;chief-"
#| "webmaster@gnu.org&gt;</a>.</strong>"
msgid ""
"Ready? Email your completed quiz to <a href=\"mailto:webmasters@gnu.org?"
"subject=I want to be a GNU Webmaster\">&lt;webmasters@gnu.org&gt;</a> "
"<strong>with a <em>brief</em> introduction about yourself.</strong>"
msgstr ""
"<strong>Pronto? Envie por e-mail seu questionário completo para <a href="
"\"mailto:chief-webmaster@gnu.org?subject=I want to be a GNU Webmaster\">&lt;"
"chief-webmaster@gnu.org&gt;</a>.</strong>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e o "
"envio de traduções das páginas deste site."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2008, 2009, 2014, 2018, 2020, 2022 Free Software "
"Foundation, Inc."
msgstr ""
"Copyright &copy; 2008, 2009, 2014, 2018, 2020, 2022 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Esta página está licenciada sob uma licença <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle\n"
"<a href=\"mailto:rafaelff@gnome.org\">&lt;rafaelff@gnome.org&gt;</a>, 2017, "
"2018, 2020"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"

#~ msgid ""
#~ "Copyright &copy; 2008, 2009, 2014, 2018, 2020 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2008, 2009, 2014, 2018, 2020 Free Software Foundation, "
#~ "Inc."

#~ msgid "What is open source?"
#~ msgstr "O que é código aberto?"

#~ msgid ""
#~ "You can usefully help with webmastering in as little as an hour a week, "
#~ "or as much time as you have."
#~ msgstr ""
#~ "Você pode ajudar, com efeito, em <cite>webmastering</cite> por pelo menos "
#~ "uma hora por semana, ou tanto tempo quanto você têm disponível."

#~ msgid ""
#~ "To get started, answer the following questions as best as you can, and "
#~ "email them to <a href=\"mailto:webmasters@gnu.org\"> &lt;webmasters@gnu."
#~ "org&gt;</a>."
#~ msgstr ""
#~ "Para iniciar, responda as seguintes perguntas da melhor que conseguir e "
#~ "envie-as por e-mail para <a href=\"mailto:webmasters@gnu.org\"> &lt;"
#~ "webmasters@gnu.org&gt;</a>."

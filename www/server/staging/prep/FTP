How to get GNU Software.                           $Date: 2024/02/12 17:29:25 $

* GNU mirror list

North America:

Canada - https://mirror.csclub.uwaterloo.ca/gnu/, http://mirror.csclub.uwaterloo.ca/gnu/, ftp://mirror.csclub.uwaterloo.ca/gnu/, rsync://mirror.csclub.uwaterloo.ca/gnu/
Canada - https://mirror.its.dal.ca/gnu, http://mirror.its.dal.ca/gnu, rsync://mirror.its.dal.ca/gnu
Canada - https://mirror2.evolution-host.com/gnu, http://mirror2.evolution-host.com/gnu, rsync://mirror2.evolution-host.com/gnu
US-Arizona [js] - https://mirrors.sarata.com/gnu/, rsync://mirrors.sarata.com/gnu/ (also, mirror alpha: https://mirrors.sarata.com/gnu-alpha/, rsync://mirrors.sarata.com/gnu-alpha/)
US-California - https://mirror.fcix.net/gnu/, http://mirror.fcix.net/gnu/
US-California - https://gnu.mirrors.hoobly.com/, http://gnu.mirrors.hoobly.com
US-California - https://mirrors.kernel.org/gnu/, http://mirrors.kernel.org/gnu/
US-California - http://mirror.keystealth.org/gnu/, ftp://mirror.keystealth.org/gnu/, rsync://mirror.keystealth.org/gnu/
US-California - https://mirrors.ocf.berkeley.edu/gnu/, http://mirrors.ocf.berkeley.edu/gnu/, rsync://mirrors.ocf.berkeley.edu/gnu/
US-Idaho - http://mirrors.syringanetworks.net/gnu/, ftp://mirrors.syringanetworks.net/gnu/, rsync://mirrors.syringanetworks.net/gnu/
US-Illinois - https://mirror.team-cymru.com/gnu/, http://mirror.team-cymru.com/gnu/, rsync://mirror.team-cymru.com/gnu/
US-Indiana [js] - https://gnu.askapache.com/, http://gnu.askapache.com/
US-Massachusetts - https://mirrors.tripadvisor.com/gnu/, http://mirrors.tripadvisor.com/gnu/, rsync://mirrors.tripadvisor.com/gnu/
US-Michigan - https://ftp.wayne.edu/gnu/, http://ftp.wayne.edu/gnu/
US-Michigan - https://mirror.us-midwest-1.nexcess.net/gnu/, http://mirror.us-midwest-1.nexcess.net/gnu/
US-New Jersey - https://gnu.mirror.constant.com/, http://gnu.mirror.constant.com/
US-New York - http://mirror.rit.edu/gnu/
US-North Carolina - https://mirrors.ibiblio.org/gnu/, http://mirrors.ibiblio.org/gnu/

South America:

Brazil - https://gnu.c3sl.ufpr.br/ftp/, http://gnu.c3sl.ufpr.br/ftp/, rsync://gnu.c3sl.ufpr.br/gnu/ftp/ (also, mirrors alpha: https://gnu.c3sl.ufpr.br/alpha/, http://gnu.c3sl.ufpr.br/alpha/, rsync://gnu.c3sl.ufpr.br/gnu/alpha/)
Ecuador - https://mirror.cedia.org.ec/gnu/, http://mirror.cedia.org.ec/gnu/, rsync://mirror.cedia.org.ec/gnu (also, mirrors alpha: https://mirror.cedia.org.ec/gnualpha/, http://mirror.cedia.org.ec/gnualpha/, rsync://mirror.cedia.org.ec/gnualpha)

Africa:

Botswana - https://mirror.retentionrange.co.bw/gnu
Morocco - https://mirror.marwan.ma/gnu/, http://mirror.marwan.ma/gnu/, rsync://mirror.marwan.ma/gnu/ (also, mirrors alpha: https://mirror.marwan.ma/gnualpha/, http://mirror.marwan.ma/gnualpha/, rsync://mirror.marwan.ma/gnualpha/)
South Africa - https://mirror.ufs.ac.za/gnu/, http://mirror.ufs.ac.za/gnu/, rsync://mirror.ufs.ac.za/gnu/

Asia:

Azerbaijan - https://mirror.hostart.az/gnu/, http://mirror.hostart.az/gnu/
China - https://mirrors.nju.edu.cn/gnu/, http://mirrors.nju.edu.cn/gnu/ (also, mirror alpha: https://mirrors.nju.edu.cn/gnu-alpha/, http://mirrors.nju.edu.cn/gnu-alpha/)
China - https://mirrors.ustc.edu.cn/gnu/, http://mirrors.ustc.edu.cn/gnu/, rsync://rsync.mirrors.ustc.edu.cn/gnu/
China - https://mirrors.tuna.tsinghua.edu.cn/gnu/, rsync://mirrors.tuna.tsinghua.edu.cn/gnu/
China - https://mirrors.sjtug.sjtu.edu.cn/gnu/
Hong Kong - https://mirror-hk.koddos.net/gnu/, http://mirror-hk.koddos.net/gnu/, rsync://mirror-hk.koddos.net/gnu
India - https://mirrors.hopbox.net/gnu/, http://mirrors.hopbox.net/gnu/
India - https://gnu.mirror.net.in/gnu/, http://gnu.mirror.net.in/gnu/, rsync://gnu.mirror.net.in/gnu/ (also, mirror alpha: https://gnu.mirror.net.in/gnu-alpha/, http://gnu.mirror.net.in/gnu-alpha/, rsync://gnu.mirror.net.in/gnu-alpha/)
Japan - https://ftp.jaist.ac.jp/pub/GNU/, http://ftp.jaist.ac.jp/pub/GNU/, rsync://ftp.jaist.ac.jp/pub/GNU/
Japan - https://repo.jing.rocks/gnu/, http://repo.jing.rocks/gnu/, rsync://repo.jing.rocks/gnu/ (also, mirror alpha: https://repo.jing.rocks/gnu-alpha/, http://repo.jing.rocks/gnu-alpha/, rsync://repo.jing.rocks/gnu-alpha/)
Russia - https://mirror.truenetwork.ru/gnu, http://mirror.truenetwork.ru/gnu, rsync://mirror.truenetwork.ru/gnu
Singapore - https://mirror.freedif.org/GNU/, http://mirror.freedif.org/GNU/, rsync://mirror.freedif.org/GNU/ (also, mirror alpha: https://mirror.freedif.org/GNU-alpha/, http://mirror.freedif.org/GNU-alpha/, rsync://mirror.freedif.org/GNU-alpha/)
South Korea - https://ftp.kaist.ac.kr/gnu/ (also, mirror alpha: https://ftp.kaist.ac.kr/gnu-alpha/)
Taiwan - https://mirror.ossplanet.net/gnu/, http://mirror.ossplanet.net/gnu/, rsync://mirror.ossplanet.net/gnu
Taiwan - http://ftp.twaren.net/Unix/GNU/gnu/

Europe:

Austria - https://mirror.easyname.at/gnu/, http://mirror.easyname.at/gnu/, ftp://mirror.easyname.at/gnu/, rsync://mirror.easyname.at/gnu/
Austria - https://mirror.kumi.systems/gnu/, http://mirror.kumi.systems/gnu/, rsync://mirror.kumi.systems/gnu/ (also, mirrors alpha: https://mirror.kumi.systems/gnualpha/, http://mirror.kumi.systems/gnualpha/, rsync://mirror.kumi.systems/gnualpha/)
Bulgaria - https://ftp.sotirov-bg.net/pub/mirrors/gnu/, ftp://sotirov-bg.net/pub/mirrors/gnu/
Denmark - http://ftp.klid.dk/ftp/gnu/
Denmark - https://mirrors.dotsrc.org/gnu/, http://mirrors.dotsrc.org/gnu/, ftp://mirrors.dotsrc.org/gnu/
Finland - https://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/, http://www.nic.funet.fi/pub/gnu/ftp.gnu.org/pub/gnu/, ftp://ftp.funet.fi/pub/gnu/prep/, rsync://ftp.funet.fi/ftp/pub/gnu/prep/ (also, mirrors alpha: https://www.nic.funet.fi/pub/gnu/alpha/gnu/, http://www.nic.funet.fi/pub/gnu/alpha/gnu/)
France - https://mirror.ibcp.fr/pub/gnu/, http://mirror.ibcp.fr/pub/gnu/
France - https://mirror.cyberbits.eu/gnu/, http://mirror.cyberbits.eu/gnu/, rsync://rsync.cyberbits.eu/gnu/ (also, mirror alpha: https://mirror.cyberbits.eu/gnu/alpha/, http://mirror.cyberbits.eu/gnu/alpha/, rsync://rsync.cyberbits.eu/gnu/alpha/)
Germany - https://mirror.dogado.de/gnu/, http://mirror.dogado.de/gnu/
Germany - https://mirror.clientvps.com/gnu/, http://mirror.clientvps.com/gnu/
Germany - https://de.freedif.org/gnu/, rsync://de.freedif.org/gnu/ (also, mirrors alpha: https://de.freedif.org/alpha/, rsync://de.freedif.org/alpha/)
Germany - https://ftp.fau.de/gnu, http://ftp.fau.de/gnu, rsync://ftp.fau.de/gnu
Germany - https://www.artfiles.org/gnu.org/, http://www.artfiles.org/gnu.org/
Germany - https://ftp-stud.hs-esslingen.de/pub/Mirrors/ftp.gnu.org/, http://ftp-stud.hs-esslingen.de/pub/Mirrors/ftp.gnu.org/
Germany - https://mirror.checkdomain.de/gnu, http://mirror.checkdomain.de/gnu, ftp://mirror.checkdomain.de/gnu
Germany - https://mirror.netcologne.de/gnu, http://mirror.netcologne.de/gnu, ftp://mirror.netcologne.de/gnu, rsync://mirror.netcologne.de/gnu
Germany - https://www.gutscheinrausch.de/mirror/gnu/, http://www.gutscheinrausch.de/mirror/gnu/
Germany - https://ftp.wrz.de/pub/gnu/, http://ftp.wrz.de/pub/gnu/, rsync://ftp.wrz.de/pub/gnu/
Germany - https://ftp.halifax.rwth-aachen.de/gnu/, http://ftp.halifax.rwth-aachen.de/gnu/, rsync://ftp.halifax.rwth-aachen.de/gnu/
Germany - http://mirror.junda.nl/gnu/
Greece - https://ftp.cc.uoc.gr/mirrors/gnu/, http://ftp.cc.uoc.gr/mirrors/gnu/, ftp://ftp.cc.uoc.gr/mirrors/gnu/
Greece - https://fosszone.csd.auth.gr/gnu/, http://fosszone.csd.auth.gr/gnu/
Hungary - https://quantum-mirror.hu/mirrors/pub/gnu/, http://quantum-mirror.hu/mirrors/pub/gnu/, rsync://quantum-mirror.hu/gnu/ (also, mirror alpha: https://quantum-mirror.hu/mirrors/pub/gnualpha/, http://quantum-mirror.hu/mirrors/pub/gnualpha/, rsync://quantum-mirror.hu/gnualpha/)
Republic of Moldova - https://mirror.ihost.md/gnu/, http://mirror.ihost.md/gnu/, rsync://mirror.ihost.md/gnu (also, mirror alpha: https://mirror.ihost.md/gnu-alpha/, http://mirror.ihost.md/gnu-alpha/, rsync://mirror.ihost.md/gnu-alpha)
Netherlands - https://mirror.lyrahosting.com/gnu, http://mirror.lyrahosting.com/gnu, rsync://mirror.lyrahosting.com/gnu
Netherlands - https://ftp.nluug.nl/pub/gnu/, http://ftp.nluug.nl/pub/gnu/, ftp://ftp.nluug.nl/pub/gnu/
Netherlands - https://ftp.snt.utwente.nl/pub/software/gnu/, http://ftp.snt.utwente.nl/pub/software/gnu/, rsync://ftp.snt.utwente.nl/gnu/
Netherlands - https://mirror.koddos.net/gnu/, http://mirror.koddos.net/gnu/, rsync://mirror.koddos.net/gnu
Norway - https://gnuftp.uib.no/, http://gnuftp.uib.no/ (also, mirrors alpha: https://gnualpha.uib.no/, http://gnualpha.uib.no/)
Poland - https://sunsite.icm.edu.pl/pub/gnu/, http://sunsite.icm.edu.pl/pub/gnu/, rsync://sunsite.icm.edu.pl/pub/gnu/
Poland - https://ftp.man.poznan.pl/gnu/, http://ftp.man.poznan.pl/gnu/
Poland - https://ftp.task.gda.pl/pub/gnu/, http://ftp.task.gda.pl/pub/gnu/, ftp://ftp.task.gda.pl/pub/gnu/
Portugal - https://mirrors.up.pt/pub/gnu/, http://mirrors.up.pt/pub/gnu/, ftp://mirrors.up.pt/pub/gnu/, rsync://mirrors.up.pt/pub/gnu/ (also, mirrors alpha: https://mirrors.up.pt/pub/gnu-alpha/, http://mirrors.up.pt/pub/gnu-alpha/, ftp://mirrors.up.pt/pub/gnu-alpha/, rsync://mirrors.up.pt/pub/gnu-alpha/)
Portugal - https://ftp.eq.uc.pt/software/unix/gnu/, http://ftp.eq.uc.pt/software/unix/gnu/ (also, mirrors alpha: https://ftp.eq.uc.pt/software/unix/gnu-alpha/, http://ftp.eq.uc.pt/software/unix/gnu-alpha/, ftp://ftp.eq.uc.pt/pub/software/unix/gnu-alpha/)
Romania - https://mirrors.nav.ro/gnu, http://mirrors.nav.ro/gnu, rsync://mirrors.nav.ro/gnu
Russia - https://mirror.tochlab.net/pub/gnu/, http://mirror.tochlab.net/pub/gnu/
Spain - https://ftp.rediris.es/mirror/GNU/, http://ftp.rediris.es/mirror/GNU/
Sweden - https://ftp.acc.umu.se/mirror/gnu.org/gnu/, http://ftp.acc.umu.se/mirror/gnu.org/gnu/, rsync://ftp.acc.umu.se/mirror/gnu.org/gnu/ (also, mirrors alpha: https://ftp.acc.umu.se/mirror/gnu.org/alpha/, http://ftp.acc.umu.se/mirror/gnu.org/alpha/, rsync://ftp.acc.umu.se/mirror/gnu.org/alpha/)
Turkey - https://mirror.rabisu.com/gnu/, http://mirror.rabisu.com/gnu/ (also, mirrors alpha: https://mirror.rabisu.com/gnualpha/, http://mirror.rabisu.com/gnualpha/)
UK [js] - https://www.mirrorservice.org/sites/ftp.gnu.org/gnu/, http://www.mirrorservice.org/sites/ftp.gnu.org/gnu/,  ftp://www.mirrorservice.org/sites/ftp.gnu.org/gnu/, rsync://rsync.mirrorservice.org/ftp.gnu.org/gnu/ (also, mirrors alpha: https://www.mirrorservice.org/sites/alpha.gnu.org/gnu/, http://www.mirrorservice.org/sites/alpha.gnu.org/gnu/, ftp://ftp.mirrorservice.org/sites/alpha.gnu.org/gnu/, rsync://rsync.mirrorservice.org/alpha.gnu.org/gnu/)

Oceania:

Australia - https://mirror.endianness.com/gnu/, rsync://mirror.endianness.com/gnu/ (also, mirror alpha: https://mirror.endianness.com/gnu-alpha/, rsync://mirror.endianness.com/gnu-alpha/)
New Caledonia - https://mirror.lagoon.nc/gnu/, http://mirror.lagoon.nc/gnu/, rsync://mirror.lagoon.nc/gnu/


* Special mirrors

<em>We don't monitor these mirrors. Please make sure
the one you are downloading from is up to date by checking
<code>mirror-updated-timestamp.txt</code>.</em>

Onion URL:

Brazil -- http://caseiraocqq4xljst7gj5uxakklcbigsrxc6bixgbyf62nat4m2vfxad.onion/gnu

Centralized networks:

<b>Warning</b>: These mirrors are likely to track or profile users. Use
them as a last resort, <em>with JavaScript and cookies disabled</em>.

Global [cn, js] -- https://mirrors.cicku.me/gnu/, http://mirrors.cicku.me/gnu/ (also, mirror alpha: https://mirrors.cicku.me/gnu-alpha/, http://mirrors.cicku.me/gnu-alpha/)
Global [cn, js] -- https://mirror.downloadvn.com/gnu/, http://mirror.downloadvn.com/gnu/ (also, mirror alpha: https://mirror.downloadvn.com/gnu-alpha/, http://mirror.downloadvn.com/gnu-alpha/)
Global [cn, js] -- https://mirrors.aliyun.com/gnu/, http://mirrors.aliyun.com/gnu/

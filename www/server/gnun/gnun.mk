# Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
#   2016, 2017, 2018, 2019, 2020, 2021, 2022,
#   2023, 2024 Free Software Foundation, Inc.

# This file is input for GNUnited Nations.

# This file is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with GNUnited Nations. If not, see <http://www.gnu.org/licenses/>.

# TRANSLATORS: Add here your language code.  Please keep the
# alphabetical order.
TEMPLATE_LINGUAS := af ar bg ca cs da de el eo es fa fi fr he hr id it ja ko lt \
                    ml ms nb nl pl pt-br ro ru sk sq sr sv ta tr uk zh-cn zh-tw

# TRANSLATORS: Add here your language code if you want PO files with wdiffs
# to previous msgids.
FUZZY_DIFF_LINGUAS := af ar bg ca cs da de el eo es fi fr he hr id it ko lt ml \
                      ms nb nl pl ro ru sk sv ta zh-tw

# List of articles for which GRACE do not apply; i.e. they are
# regenerated even if there are fuzzy strings.
no-grace-articles := home prep/ftp server/sitemap server/takeaction

# The English sitemap HTML file is generated automatically outside of GNUN.
# GNUN uses compendia automatically generated for this file when available.
sitemap := server/sitemap

# List of templates whose URLs are localized, but their "translations"
# are maintained without PO files.
localized-ssis :=	server/header server/head-include-1 \
			server/banner \
			server/html5-header \
			server/footer server/generic

# List of mandatory templates (all %.$lang.po files are generated).
extra-templates :=	server/body-include-1 \
			server/body-include-2 \
			server/bottom-notes \
			server/footer-text \
			server/head-include-2 \
			server/outdated \
			server/top-addendum

# List of templates that are translated or not on the discretion
# of the respective team (the PO files are merged, but are not created).
optional-templates :=	audio-video/audio-video-menu \
			education/education-menu \
			gnu/gnu-breadcrumb \
			licenses/fsf-licensing \
			manual/allgnupkgs \
			philosophy/ph-breadcrumb \
			philosophy/philosophy-menu \
			proprietary/proprietary-menu \
			planetfeeds \
			server/fs-gang \
			server/home-pkgblurbs \
			software/recent-releases-include

ALL_DIRS :=	accessibility \
		audio-video \
		award \
		award/1998 \
		award/1999 \
		award/2000 \
		award/2001 \
		award/2002 \
		award/2003 \
		bulletins \
		contact \
		distros \
		doc \
		education \
		education/misc \
		encyclopedia \
		events \
		fry \
		fun \
		fun/jokes \
		gnu \
		gnu/old-gnu-structure \
		gnu40 \
		graphics \
		graphics/adrienne \
		graphics/bahlon \
		graphics/behroze \
		graphics/fsfsociety \
		graphics/gnu-and-freedo \
		graphics/gnu-post \
		graphics/umsa \
		help \
		licenses \
		licenses/old-licenses \
		links \
		manual \
		music \
		people \
		philosophy \
		philosophy/economics_frank \
		philosophy/sco \
		prep \
		press \
		proprietary \
		proprietary/articles \
		server \
		server/source \
		server/standards \
		software \
		testimonials \
		thankgnus

ROOT :=	gnu-404 \
	home \
	keepingup

accessibility :=	accessibility

audio-video := 	audio-video \
		audio-video-docs \
		individual-projects

award :=	award \
		award-1998 \
		award-1999

award/1998 :=	finalists \
		nominees

award/1999 :=	1999

award/2000 :=	2000

award/2001 :=	2001

award/2002 :=	2002

award/2003 :=	2003 \
		2003-call

bulletins :=	bulletins \
		thankgnus-index

contact :=	contact \
		gnu-advisory

distros :=	common-distros \
		distros \
		free-distros \
		free-non-gnu-distros \
		free-system-distribution-guidelines \
		optionally-free-not-enough \
		screenshot \
		screenshot-gnewsense

doc :=	doc \
	other-free-books

education :=	bigtech-threats-to-education-and-society \
		dangers-of-proprietary-systems-in-online-teaching \
		drm-in-school-ebooks-when-life-imitates-dystopian-stories \
		edu-cases-argentina-ecen \
		edu-cases-argentina \
		edu-cases-india-ambedkar \
		edu-cases-india-irimpanam \
		edu-cases-india \
		edu-cases-italy-south-tyrol \
		edu-cases-italy \
		edu-cases \
		edu-faq \
		edu-free-learning-resources \
		edu-projects \
		edu-resources \
		edu-schools \
		edu-software-gcompris \
		edu-software-gimp \
		edu-software-tuxpaint \
		edu-software \
		edu-system-india \
		edu-team \
		edu-why \
		education \
		educational-malware-app-along \
		governments-let-companies-snoop-on-students \
		how-i-fought-to-graduate-without-using-non-free-software \
		on-privacy-at-school \
		remote-education-children-freedom-privacy-at-stake \
		resisting-proprietary-software \
		successful-resistance-against-nonfree-software \
		teachers-help-students-resist-zoom \
		teaching-my-mit-classes-with-only-free-libre-software

education/misc :=    edu-misc

encyclopedia :=	encyclopedia \
		free-encyclopedia \
		free-encyclopedia-1998-draft

events :=	dinner-20030807 \
		events \
		first-assoc-members-meeting \
		nyc-2004-01 \
		porto-tech-city-2001 \
		sco_without_fear \
		usenix-2001-lifetime-achievement

fry :=	happy-birthday-to-gnu \
	happy-birthday-to-gnu-credits \
	happy-birthday-to-gnu-download \
	happy-birthday-to-gnu-in-your-language \
	happy-birthday-to-gnu-sfd-kaffeine \
	happy-birthday-to-gnu-sfd-mplayer \
	happy-birthday-to-gnu-sfd-totem \
	happy-birthday-to-gnu-sfd-vlc \
	happy-birthday-to-gnu-sfd-xine \
	happy-birthday-to-gnu-translation

fun :=	humor

fun/jokes :=	10-kinds-of-people \
		anagrams \
		any-key \
		brainfuck \
		bug.war \
		c+- \
		courtroom.quips \
		deadbeef \
		dna \
		doctor.manifesto \
		echo-msg \
		ed \
		ed-msg \
		errno.2 \
		error-haiku \
		eternal-flame \
		evilmalware \
		filks \
		freesoftware \
		fsf-in-german \
		gcc_audio \
		gcc \
		gnuemacs.acro.exp \
		gnuemacs \
		gnu-overflow \
		gnu-song \
		gospel \
		gullibility.virus \
		hackersong \
		hackforfreedom \
		hakawatha \
		hap-bash \
		happy-new-year \
		helloworld \
		hello_world_patent \
		know.your.sysadmin \
		last.bug \
		long-options \
		merry-xmas \
		nobody-owns \
		purchase.agreement \
		users-lightbulb

gnu :=		2020-announcement-1 \
		about-gnu \
		byte-interview \
		first-hackers-conference-1984 \
		gnu \
		gnu-history \
		gnu-linux-faq \
		gnu-structure \
		gnu-users-never-heard-of-gnu \
		incorrect-quotation \
		initial-announcement \
		linux-and-gnu \
		manifesto \
		pronunciation \
		rms-lisp \
		road-to-gnu \
		thegnuproject \
		why-gnu-linux \
		why-programs-should-be-shared \
		yes-give-it-away

gnu/old-gnu-structure := old-gnu-structure

gnu40 :=	gnu40

graphics :=	3dbabygnutux \
		3dgnuhead \
		agnubody \
		agnuhead \
		agnuheadterm \
		ahurdlogo \
		alternative-ascii \
		amihud-4-freedoms \
		anfsflogo \
		anlpflogo \
		anothertypinggnu \
		arantxa \
		atypinggnu \
		avatars \
		babygnu \
		barr-gnu \
		bokma-gnu \
		bold-initiative-GNU-head \
		BVBN \
		bwcartoon \
		copyleft-sticker \
		digital-restrictions-management \
		digital-safety-guide \
		dog \
		emacs-ref \
		freedo  \
		freedo_on_the_beach \
		freedom \
		freedom-reflection \
		free-software-dealers \
		french-motto \
		fromagnulinux \
		fsf-logo \
		FSFS-logo \
		gleesons \
		gnu-30 \
		gnu-alternative \
		gnu-and-tux-icon \
		gnu-ascii \
		gnu-ascii-liberty \
		gnu-ascii2 \
		gnu-born-free-run-free \
		gnu-head-luk \
		gnu-head-shadow \
		gnu-inside \
		gnu-jacket \
		gnu-linux-logos \
		gnu-slash-linux \
		gnu-smiling \
		gnubanner \
		gnuhornedlogo \
		gnulove \
		gnuolantern \
		gnupascal \
		gnupumpkin \
		graphics \
		groff-head \
		happy_gnu_year_2019 \
		heckert_gnu \
		hitflip-gnu \
		holiday_deliveries \
		httptunnel-logo \
		hurd_mf \
		jesus-cartoon \
		kafa \
		license-logos \
		listen \
		meditate \
		modern_art_gnu \
		nandakumar-gnu \
		navaneeth-gnu \
		package-logos \
		philosophicalgnu \
		philosoputer \
		plant-onion \
		r4sh-gnu-vaporwave \
		reiss-gnuhead \
		runfreegnu \
		santa-gnu \
		scowcroft \
		skwetu-gnu-logo \
		skwid-wallpapers \
		slickgnu \
		spiritoffreedom \
		stallman-as-saint-ignucius \
		supergnu-ascii \
		this-is-freedom-wallpaper \
		usegnu \
		wallpapers \
		whatsgnu \
		winkler-gnu

graphics/adrienne :=	index

graphics/bahlon :=	index

graphics/behroze :=	index

graphics/fsfsociety :=	fsfsociety

graphics/gnu-and-freedo :=	gnu-and-freedo

graphics/gnu-post :=	index

graphics/umsa :=	umsa

help :=		evaluation \
		gnu-bucks \
		gnu-bucks-recipients \
		help \
		help-hardware \
		help-javascript \
		linking-gnu \
		music-subtraction \
		priority-projects

licenses :=	200104_seminar \
		210104_seminar \
		agpl-3.0 \
		autoconf-exception-3.0 \
		bsd \
		copyleft \
		exceptions \
		fdl-1.3 \
		fdl-1.3-faq \
		fdl-howto \
		fdl-howto-opt \
		gcc-exception-3.0 \
		gcc-exception-3.1 \
		gcc-exception-3.1-faq \
		gpl-3.0 \
		gpl-faq \
		gpl-howto \
		gpl-violation \
		gplv3-the-program \
		hessla \
		identify-licenses-clearly \
		javascript-labels \
		javascript-labels-rationale \
		lgpl-3.0 \
		lgpl-java \
		license-compatibility \
		license-list \
		license-recommendations \
		licenses \
		NYC_Seminars_Jan2004 \
		quick-guide-gplv3 \
		recommended-copylefts \
		rms-why-gplv3 \
		translations \
		why-affero-gpl \
		why-assign \
		why-gfdl \
		why-not-lgpl

licenses/old-licenses :=	fdl-1.1 \
				fdl-1.1-translations \
				fdl-1.2-translations \
				fdl-1.2 \
				gcc-exception-translations \
				gpl-1.0 \
				gpl-2.0 \
				gpl-2.0-faq \
				gpl-2.0-translations \
				lgpl-2.0 \
				lgpl-2.0-translations \
				lgpl-2.1 \
				lgpl-2.1-translations \
				old-licenses

links :=	companies \
		links \
		non-ryf

manual :=	blurbs \
		manual

music :=	blues-song \
		emacsvsvi \
		free-birthday-song \
		free-firmware-song \
		free-software-song \
		gnu-is-not-unix-song \
		gdb-song \
		music \
		till_there_was_gnu \
		whole-gnu-world \
		writing-fs-song

people :=	past-webmasters \
		people \
		speakers \
		webmeisters

philosophy := 	15-years-of-free-software \
		amazon \
		amazon-nat \
		amazon-rms-tim \
		android-and-users-freedom \
		anonymous-response \
		applying-free-sw-criteria \
		apsl \
		assigning-copyright \
		basic-freedoms \
		bdk \
		bill-gates-and-other-communists \
		boldrin-levine \
		bug-nobody-allowed-to-understand \
		can-you-trust \
		categories \
		censoring-emacs \
		compromise \
		computing-progress \
		contradictory-support \
		copyright-and-globalization \
		copyright-versus-community \
		copyright-versus-community-2000 \
		correcting-france-mistake \
		danger-of-software-patents \
		dat \
		devils-advocate \
		digital-inclusion-in-freedom \
		dmarti-patent \
		drdobbs-letter \
		ebooks \
		ebooks-must-increase-freedom \
		eldred-amicus \
		enforcing-gpl \
		essays-and-articles \
		europes-unitary-patent \
		fighting-software-patents \
		fire \
		floss-and-foss \
		free-digital-society \
		free-doc \
		freedom-or-copyright \
		freedom-or-copyright-old \
		freedom-or-power \
		free-hardware-designs \
		free-open-overlap \
		free-software-even-more-important \
		free-software-for-freedom \
		free-software-intro \
		free-software-rocket \
		free-sw \
		free-world \
		free-world-notes \
		fs-and-sustainable-development \
		fs-motives \
		fs-translations \
		funding-art-vs-funding-software \
		gates \
		gif \
		gnutella \
		google-engineering-talk \
		government-free-software \
		gpl-american-dream \
		gpl-american-way \
		greve-clown \
		guardian-article \
		hackathons \
		hague \
		historical-apsl \
		ICT-for-prosperity \
		imperfection-isnt-oppression \
		install-fest-devil \
		ipjustice \
		is-ever-good-use-nonfree-program \
		java-trap \
		javascript-trap \
		judge-internet-usage \
		keep-control-of-your-computing \
		kevin-cole-response \
		kind-communication \
		komongistan \
		kragen-software \
		latest-articles \
		lessig-fsfs-intro \
		lest-codeplex-perplex \
		limit-patent-effect \
		linux-gnu-freedom \
		loyal-computers \
		luispo-rms-interview \
		mcvoy \
		microsoft \
		microsoft-antitrust \
		microsoft-new-monopoly \
		microsoft-old \
		microsoft-verdict \
		misinterpreting-copyright \
		moglen-harvard-speech-2004 \
		motif \
		ms-doj-tunney \
		my_doom \
		netscape \
		netscape-npl \
		netscape-npl-old \
		network-services-arent-free-or-nonfree \
		new-monopoly \
		nit-india \
		no-ip-ethos \
		no-word-attachments \
		nonfree-games \
		nonsoftware-copyleft \
		not-ipr \
		open-source-misses-the-point \
		opposing-drm \
		ough-interview \
		patent-practice-panel \
		patent-reform-is-not-enough \
		philosophy \
		phone-anonymous-payment \
		pirate-party \
		plan-nine \
		posting-videos \
		practical \
		pragmatic \
		privacyaction \
		programs-must-not-limit-freedom-to-run \
		protecting \
		public-domain-manifesto \
		push-copyright-aside \
		reevaluating-copyright \
		rieti \
		right-to-read \
		rms-aj \
		rms-comment-longs-article \
		rms-hack \
		rms-interview-edinburgh \
		rms-kernel-trap-interview \
		rms-kol \
		rms-nyu-2001-transcript \
		rms-on-radio-nz \
		rms-patents \
		rms-pavia-doctoral-address \
		rtlinux-patent \
		savingeurope \
		saying-no-even-once \
		second-sight \
		self-interest \
		selling \
		selling-exceptions \
		shouldbefree \
		social-inertia \
		software-libre-commercial-viability \
		software-literary-patents \
		software-patents \
		speeches-and-interviews \
		stallman-kth \
		stallman-mec-india \
		stallmans-law \
		stophr3028 \
		sun-in-night-time \
		surveillance-testimony \
		surveillance-vs-democracy \
		technological-neutrality \
		the-danger-of-ebooks \
		the-law-of-success-2 \
		the-moral-and-the-legal \
		the-root-of-this-problem \
		third-party-ideas \
		tivoization \
		trivial-patent \
		ubuntu-spyware \
		ucita \
		udi \
		university \
		upgrade-windows \
		uruguay \
		use-free-software \
		using-gfdl \
		vaccination \
		w3c-patent \
		wassenaar \
		whats-wrong-with-youtube \
		when-free-depends-on-nonfree \
		when-free-software-isnt-practically-superior \
		who-does-that-server-really-serve \
		why-audio-format-matters \
		why-call-it-the-swindle \
		why-copyleft \
		why-free \
		wipo-PublicAwarenessOfCopyright-2002 \
		words-to-avoid \
		wsis \
		wsis-2003 \
		wwworst-app-store \
		x \
		you-the-problem-tpm2-solves \
		your-freedom-needs-free-software

philosophy/economics_frank :=	frank

philosophy/sco :=	questioning-sco \
			sco \
			sco-gnu-linux \
			sco-preemption \
			sco-v-ibm \
			sco-without-fear \
			subpoena

prep :=			ftp \
			index

press :=		2001-07-09-DotGNU-Mono \
			2001-07-20-FSF-India \
			2001-09-18-RTLinux \
			2001-09-24-CPI \
			2001-10-12-bayonne \
			2001-10-22-Emacs \
			2001-12-03-Takeda \
			2002-02-16-FSF-Award \
			2002-02-26-MySQL \
			2002-03-01-pi-MySQL \
			2002-03-18-digitalspeech \
			2002-03-19-Affero \
			press

proprietary :=	all \
		malware-adobe \
		malware-amazon \
		malware-apple \
		malware-appliances \
		malware-cars \
		malware-edtech \
		malware-games \
		malware-google \
		malware-in-online-conferencing \
		malware-microsoft \
		malware-mobiles \
		malware-webpages \
		potential-malware \
		proprietary \
		proprietary-addictions \
		proprietary-back-doors \
		proprietary-censorship \
		proprietary-coercion \
		proprietary-coverups \
		proprietary-deception \
		proprietary-drm \
		proprietary-fraud \
		proprietary-incompatibility \
		proprietary-insecurity \
		proprietary-interference \
		proprietary-jails \
		proprietary-manipulation \
		proprietary-obsolescence \
		proprietary-sabotage \
		proprietary-subscriptions \
		proprietary-surveillance \
		proprietary-tethers \
		proprietary-tyrants

proprietary/articles :=	uhd-bluray-denies-your-freedom

server :=	08whatsnew \
		irc-rules \
		mirror \
		select-language \
		server \
		sitemap \
		takeaction \
		tasks

server/source :=	source

server/standards :=	gnu-website-guidelines \
			README.editors \
			README.translations \
			webmaster-quiz \

software :=	devel \
		free-software-for-education \
		gethelp \
		maintainer-tips \
		recent-releases \
		reliability \
		repo-criteria \
		repo-criteria-evaluation \
		software \
		year2000 \
		year2000-list

testimonials :=	reliable \
		supported \
		testimonial_cadcam \
		testimonial_HIRLAM \
		testimonial_media \
		testimonial_mondrup \
		testimonial_research_ships \
		testimonials \
		useful

thankgnus :=	1997supporters \
		1998supporters \
		1999 \
		1999supporters \
		2000supporters \
		2001supporters \
		2002supporters \
		2003supporters \
		2004supporters \
		2005supporters \
		2006supporters \
		2007supporters \
		2008supporters \
		2009supporters \
		2010supporters \
		2011supporters \
		2012supporters \
		2013supporters \
		2014supporters \
		2015supporters \
		2016supporters \
		2017supporters \
		2018supporters \
		2019supporters \
		2020supporters \
		2021supporters \
		2022supporters \
		2023supporters \
		2024supporters \
		thankgnus

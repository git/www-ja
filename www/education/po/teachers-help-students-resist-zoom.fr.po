# French translation of https://www.gnu.org/education/teachers-help-students-resist-zoom.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Nicolas Joubert, 2020.
# François Ochsenbein, 2020.
# Christian Renaudineau, 2020.
# Thérèse Godefroy <godef.th AT free.fr>, 2020.
# Patrick Creusot, 2020 (relecture).
#
msgid ""
msgstr ""
"Project-Id-Version: teachers-help-students-resist-zoom.html\n"
"POT-Creation-Date: 2021-01-27 11:55+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Teachers: Help Your Students Resist Zoom - GNU Project - Free Software "
"Foundation"
msgstr ""
"Enseignants, aidez vos étudiants à résister à Zoom - Projet GNU - Free "
"Software Foundation"

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"

#. type: Attribute 'title' of: <div><a><img>
msgid "Education Contents"
msgstr "Menu Éducation"

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Education Contents]&nbsp;"
msgstr "&nbsp;[Menu Éducation]&nbsp;"

#. type: Content of: <div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><p><a>
msgid "<a href=\"/\">"
msgstr "<a href=\"/\">"

#. type: Attribute 'title' of: <div><p><a><img>
msgid "GNU Home"
msgstr "Accueil GNU"

#. type: Content of: <div><p>
msgid ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Education</a>&nbsp;/ <a "
"href=\"/education/resisting-proprietary-software.html\">Resistance</"
"a>&nbsp;/ <a href=\"/education/dangers-of-proprietary-systems-in-online-"
"teaching.html\"> Online teaching</a>&nbsp;/"
msgstr ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Éducation</a>&nbsp;/ <a "
"href=\"/education/resisting-proprietary-software.html\">Résistance</"
"a>&nbsp;/ <a href=\"/education/dangers-of-proprietary-systems-in-online-"
"teaching.html\">Enseignement en ligne</a>&nbsp;/"

#. type: Content of: <div><h2>
msgid "Teachers: Help Your Students Resist Zoom"
msgstr "Enseignants, aidez vos étudiants à résister à Zoom"

#. type: Content of: <div><div><p>
msgid ""
"When the global COVID-19 emergency began, schools switched to remote "
"classes. The urgency of ensuring the continuity of classes left little space "
"for debate on how to choose <a href=\"/philosophy/free-sw.html\">software "
"that empowers students to learn</a>. As a consequence, <a href=\"/"
"proprietary/proprietary.html\">freedom-denying and privacy-violating "
"software</a> has seen widespread adoption in education."
msgstr ""
"Quand la crise généralisée du COVID-19 a commencé, les écoles sont passées "
"en mode enseignement à distance. L'urgence d'assurer la continuité des cours "
"n'a laissé que peu de place au débat sur le choix de <a href=\"/philosophy/"
"free-sw.html\">logiciels qui donnent aux étudiants les moyens d'apprendre</"
"a>. <a href=\"#TransNote1\" id=\"TransNote1-rev\"><sup>1</sup></a> Par "
"conséquent, <a href=\"/proprietary/proprietary.html\">des logiciels qui "
"bafouent les libertés et la vie privée</a> ont été largement adoptés dans "
"l'enseignement."

#. type: Content of: <div><div><p>
msgid ""
"Zoom, a proprietary online conferencing program that is becoming more and "
"more dangerously popular, is <a href=\"https://www.studentprivacymatters.org/"
"what-you-need-to-know-about-zoom-for-education/\"> an example</a> of such "
"harmful technology. <strong>Please don't ask students to install Zoom on "
"their computers, or to use its web version which also runs Zoom code on "
"their computers.</strong>"
msgstr ""
"Zoom, un programme privateur de conférence en ligne dont la popularité "
"s'accroît dangereusement, est <a href=\"https://www.studentprivacymatters."
"org/what-you-need-to-know-about-zoom-for-education/\">un exemple</a> de "
"cette technologie préjudiciable. <strong>Par pitié, ne demandez pas aux "
"étudiants d'installer Zoom sur leurs ordinateurs, ni même d'utiliser sa "
"version web, qui exécute également le code de Zoom sur leurs ordinateurs.</"
"strong>"

#. type: Content of: <div><div><h3>
msgid "To all those teaching remote classes with Zoom"
msgstr "A tous ceux qui enseignent à distance avec Zoom"

#. type: Content of: <div><div><p>
msgid ""
"It is unfortunate that you are using Zoom, a nonfree program that spies on "
"users and takes away your students' computer freedom, along with your own. "
"Using Zoom, students are dependent on a program that a company can "
"unilaterally pervert. Their freedom to learn about technology and how it "
"works is destroyed."
msgstr ""
"Il est regrettable que vous utilisiez Zoom, un programme non libre qui "
"espionne les utilisateurs tout en privant vos étudiants de leurs libertés "
"numériques – et vous des vôtres. Lorsqu'ils utilisent Zoom, les étudiants "
"dépendent d'un logiciel qu'une entreprise peut unilatéralement pervertir. "
"Leur liberté d'en apprendre la technologie et le fonctionnement est détruite."

#. type: Content of: <div><div><p>
msgid ""
"If you direct your students to use Zoom, they may form a bad habit and "
"continue using it beyond your classes, effectively surrendering their "
"privacy over communication. They will not learn how to keep control of their "
"data and computing."
msgstr ""
"Si vous demandez à vos étudiants d'utiliser Zoom, ils risquent de prendre la "
"mauvaise habitude de continuer à l'utiliser en dehors de vos cours, "
"abandonnant ainsi leur vie privée pour pouvoir communiquer. Ils "
"n'apprendront pas à garder le contrôle de leurs données personnelles ni à "
"maîtriser leur informatique."

#. type: Content of: <div><div><p>
msgid ""
"You can use free software like <a href=\"https://directory.fsf.org/wiki/"
"BigBlueButton\">BigBlueButton</a> and <a href=\"https://directory.fsf.org/"
"wiki/Jitsi\">Jitsi</a> for teaching remote classes without losing technical "
"performance and, most importantly, without surrendering your students' "
"freedom. By choosing these and <a href=\"/software/free-software-for-"
"education.html\">other free programs for education</a>, you are enabling "
"motivated students to learn about the software they use everyday, and some "
"of them one day will be able to adapt it to their needs, serving a larger "
"community. Those who will not pursue such curiosity, will still benefit from "
"using a program that respects their freedom and doesn't spy on them. They "
"will develop the good habit of not throwing away their freedom, too."
msgstr ""
"Vous pouvez utiliser des logiciels libres comme <a href=\"https://directory."
"fsf.org/wiki/BigBlueButton\">BigBlueButton</a> et <a href=\"https://"
"directory.fsf.org/wiki/Jitsi\">Jitsi</a> pour donner des cours à distance "
"sans sacrifier la performance technique, ni surtout la liberté de vos "
"étudiants. En choisissant ces programmes, ainsi que <a href=\"/software/free-"
"software-for-education.html\">d'autres logiciels éducatifs libres</a>, vous "
"donnez la possibilité à des étudiants motivés de se familiariser avec les "
"logiciels qu'ils utilisent au quotidien, et certains d'entre eux pourront un "
"jour les adapter à leurs besoins, ce qui peut aussi profiter à une "
"communauté plus large. Ceux qui ne sont pas animés par cette curiosité "
"bénéficieront quand même d'un logiciel respectueux de leur liberté, qui ne "
"les espionne pas. Ils prendront aussi la bonne habitude de ne pas abandonner "
"leur liberté."

#. type: Content of: <div><div><p>
msgid ""
"Choosing free software for your classes will positively impact life-long "
"learning, a crucial skill for any student. In fact, students will learn that "
"it is in their power to run, study, modify and share any free software they "
"wish, according to their own curiosity and needs. In contrast, by adopting "
"proprietary software, students are taught to become mere &ldquo;"
"consumers&rdquo;<a href=\"#consumer\" id=\"consumer-rev\"><sup>[*]</sup></a> "
"of a user interface, with no power over the technology they are using."
msgstr ""
"Choisir des logiciels libres pour votre enseignement aura un effet positif "
"sur leur aptitude à la formation continue, une compétence essentielle pour "
"tout étudiant. Les étudiants apprendront ainsi qu'ils ont la possibilité "
"d'exécuter, d'étudier, de modifier et de partager tout logiciel libre autant "
"qu'ils le souhaitent, selon leur curiosité ou leurs besoins. En revanche, en "
"adoptant un logiciel privateur, les étudiants n'apprennent qu'à devenir de "
"simples « consommateurs » <a href=\"#consumer\" id=\"consumer-rev\"><sup>[*]"
"</sup></a> d'une interface utilisateur, sans aucun pouvoir sur la "
"technologie qu'ils utilisent."

#. type: Content of: <div><div><p>
msgid ""
"If you independently opted for using Zoom in your classes, we urge you to "
"switch to something more ethical."
msgstr ""
"Si vous avez choisi par vous-même d'utiliser Zoom pour vos cours, nous vous "
"suggérons de vous tourner vers un logiciel plus éthique."

#. type: Content of: <div><div><p>
msgid ""
"If Zoom was imposed on you by your school's administration, we encourage you "
"to contact them and ask them to let you change to a free program. Ask other "
"teachers for help, and explain <a href=\"/education/edu-schools.html\">why</"
"a> software freedom is paramount for education."
msgstr ""
"Si Zoom vous a été imposé par l'administration de votre établissement, nous "
"vous invitons à la contacter pour lui demander la permission d'utiliser un "
"logiciel libre à la place. Demandez à d'autres enseignants de vous soutenir "
"et expliquez-leur <a href=\"/education/edu-schools.html\">pourquoi</a> la "
"liberté du logiciel est primordiale dans l'éducation."

#. type: Content of: <div><div><h3>
msgid "Last resort"
msgstr "En dernier ressort"

#. type: Content of: <div><div><p>
msgid ""
"If you can't remove Zoom, we propose this temporary workaround only as a "
"last resort so that students can take your class without feeling oppressed "
"by Zoom. We do not recommend it as a stable, long-term solution."
msgstr ""
"Si vous ne pouvez pas vous débarrasser de Zoom, nous vous proposons ce pis-"
"aller temporaire pour que vos étudiants puissent suivre les cours sans avoir "
"le sentiment d'être opprimés par Zoom. Nous ne recommandons pas ceci comme "
"une solution stable à long terme."

#. type: Content of: <div><div><ol><li>
msgid ""
"A day or two before the class, post the visual materials in some freedom-"
"respecting way, so students can download them. Any simple old-fashioned "
"website is suitable for this."
msgstr ""
"La veille ou l'avant-veille du cours, mettez les documents visuels en ligne "
"d'une manière respectueuse des libertés, pour que les étudiants puissent les "
"télécharger. N'importe quel site web à l'ancienne convient."

#. type: Content of: <div><div><ol><li>
msgid ""
"Invite students to phone the Zoom server; tell them the phone number and the "
"code for the conversation. That way they can listen to the audio of the "
"session, while they follow the visual materials alongside."
msgstr ""
"Invitez les étudiants à téléphoner au serveur Zoom ; donnez-leur le numéro "
"de téléphone et le code de la session. De cette manière ils peuvent écouter "
"ce qui se dit tout en suivant sur les documents visuels."

#. type: Content of: <div><div><ol><li>
msgid ""
"Tell them that you regret the use of Zoom and show them which are the free "
"programs to replace it. Direct them to <a href=\"/education/edu-why.html\"> "
"https://www.gnu.org/education/edu-why.html</a> to see the reasons why "
"proprietary software such as Zoom has no place in schools (or in any other "
"aspect of life.)"
msgstr ""
"Dites-leur que vous regrettez d'avoir à utiliser Zoom et montrez-leur quels "
"sont les programmes libres qui le remplacent. Dirigez-les vers <a href=\"/"
"education/edu-why.html\">https://www.gnu.org/education/edu-why.html</a> ; "
"ils y verront les raisons pour lesquelles des logiciels privateurs comme "
"Zoom n'ont pas leur place en classe (ni dans aucun aspect de la vie)."

#. type: Content of: <div><div><ol><li>
msgid ""
"If permitted, make a recording of the Zoom conversation of each session, and "
"post it for the students in a freedom-respecting way soon after."
msgstr ""
"Si vous en avez la permission, faites un enregistrement de chaque session "
"Zoom et mettez-la ensuite en ligne pour que les étudiants puissent y accéder "
"d'une manière qui respecte leurs libertés."

#. type: Content of: <div><div><p>
msgid ""
"If your school uses Zoom for examinations and students refuse to use it, "
"then you, as a teacher, could arrange a different meeting in parallel or an "
"alternative exam session using one of the free programs mentioned above. "
"Another possibility is to set up a kiosk with a computer that has Zoom "
"installed and let students take the exam using that computer. That way, they "
"won't be forced to install nonfree software on their computers, and the use "
"of Zoom will be limited to the exam. If you do this, we recommend that the "
"school computer run a <a href=\"/distros/free-distros.html\">free software "
"distribution</a>."
msgstr ""
"Si votre école fait passer les examens sur Zoom et que les étudiants "
"refusent de l'utiliser, alors vous, en tant que professeur, pourriez "
"organiser une réunion différente en parallèle, ou bien une session d'examen "
"alternative utilisant l'un des programmes libres mentionnés ci-dessus. Une "
"autre possibilité est de proposer un ordinateur en libre service sur lequel "
"Zoom est installé et laisser les étudiants passer l'examen dessus. De cette "
"manière, ils ne seront pas forcés d'installer un logiciel privateur sur "
"leurs ordinateurs et l'utilisation de Zoom sera limitée à l'examen. Si vous "
"faites cela, nous vous recommandons de faire fonctionner l'ordinateur de "
"l'école avec une <a href=\"/distros/free-distros.html\">distribution libre</"
"a>."

#. type: Content of: <div><div><p>
msgid ""
"Simply by telling students about this possibility, which you can do more "
"than once, you will help inspire <a href=\"/education/resisting-proprietary-"
"software.html\">resistance</a> to Zoom and other freedom-trampling programs, "
"and avoid inculcating surrender."
msgstr ""
"En informant simplement les étudiants de cette possibilité, ce que vous "
"pouvez faire de manière répétée, vous les encouragerez à <a href=\"/"
"education/resisting-proprietary-software.html\">résister</a> à Zoom et "
"autres logiciels violateurs de liberté, et éviterez de leur apprendre à "
"capituler."

#. type: Content of: <div><div><p>
msgid ""
"You will still be using Zoom, which is deleterious for your freedom. Some of "
"your students will probably still be using Zoom, which is deleterious for "
"their freedom. But, thanks to your efforts, some students will be able to "
"avoid using Zoom, and that is a victory."
msgstr ""
"Vous continuerez à utiliser Zoom, ce qui est néfaste pour votre liberté. "
"Certains de vos étudiants continueront à utiliser Zoom, ce qui est néfaste "
"pour la leur. Mais grâce à vos efforts, d'autres seront capables d'éviter "
"Zoom, et c'est une victoire."

#. type: Content of: <div><div><p>
msgid ""
"This workaround is meant as a short-term solution to enable students to "
"avoid Zoom now rather than wait for the next academic year. In the long-"
"term, it is crucial to migrate to free/libre software and online "
"conferencing programs like Jitsi and BigBlueButton."
msgstr ""
"Il s'agit d'une solution à court terme qui permet aux étudiants d'éviter "
"Zoom maintenant plutôt que d'attendre la prochaine année scolaire. Pour le "
"long terme, il est essentiel de migrer vers du logiciel libre et des "
"programmes de visioconférence comme Jitsi et BigBlueButton."

#. type: Content of: <div><div><p>
msgid ""
"All software must be free, and this is a small step towards that goal. <a "
"href=\"/philosophy/saying-no-even-once.html\">Saying <em>NO</em> to unjust "
"computing even once is progress</a>."
msgstr ""
"Tous les logiciels doivent être libres ; faisons un petit pas dans cette "
"direction. <a href=\"/philosophy/saying-no-even-once.html\">Dire <em>NON</"
"em> à une informatique injuste, même une seule fois, est déjà un progrès</a>."

#. type: Content of: <div><div><p>
msgid ""
"See examples of how people are <a href=\"/education/successful-resistance-"
"against-nonfree-software.html\"> successfully resisting nonfree software</a>."
msgstr ""
"Voyez comment certains <a href=\"/education/successful-resistance-against-"
"nonfree-software.html\">résistent avec succès aux logiciels non libres</a>."

#. type: Content of: <div><div><div><p>
msgid ""
"<a href=\"#consumer-rev\" id=\"consumer\">[*]</a> The word <a href=\"/"
"philosophy/words-to-avoid.html#Consumer\">&ldquo;consumer&rdquo;</a> should "
"be avoided or used with care."
msgstr ""
"<a href=\"#consumer-rev\" id=\"consumer\">[*]</a> Le mot « <a href=\"/"
"philosophy/words-to-avoid.html#Consumer\">consommateur</a> » doit être évité "
"ou utilisé avec précaution."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""
"<hr /><b>Note de traduction</b>\n"
"<ol>\n"
"<li><a id=\"TransNote1\" href=\"#TransNote1-rev\" class=\"nounderline"
"\">&#8593;</a> \n"
"Pour alléger le texte, nous traduisons <i>student</i> par « étudiant », "
"étant entendu que ce mot désigne également les élèves.</li>\n"
"</ol>"

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2020, 2021 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2020, 2021 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Nicolas Joubert, François Ochsenbein, Christian Renaudineau et "
"Thérèse Godefroy<br /> Révision : <a href=\"mailto:trad-gnu@april.org\">trad-"
"gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"

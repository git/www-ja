# Brazilian Portuguese translation of https://www.gnu.org/education/edu-cases-india.html
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Rafael Fontenelle <rafaelff@gnome.org>, 2017-2020.
# May 2020: update to new layout (T. Godefroy).
msgid ""
msgstr ""
"Project-Id-Version: edu-cases-india.html\n"
"POT-Creation-Date: 2024-05-13 12:59+0000\n"
"PO-Revision-Date: 2020-06-22 00:32-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2022-05-02 13:55+0000\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Virtaal 1.0.0-beta1\n"

#. type: Content of: <title>
msgid ""
"Education - Case Studies in India - GNU Project - Free Software Foundation"
msgstr ""
"Educação - Estudos de Caso na Índia - Projeto GNU - Free Software Foundation"

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"

#. type: Attribute 'title' of: <div><a><img>
msgid "Education Contents"
msgstr "Índice da Seção"

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Education Contents]&nbsp;"
msgstr "&nbsp;[Índice da Seção]&nbsp;"

#. type: Content of: <div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><p><a>
msgid "<a href=\"/\">"
msgstr "<a href=\"/\">"

#. type: Attribute 'title' of: <div><p><a><img>
msgid "GNU Home"
msgstr "Página inicial do GNU"

#. type: Content of: <div><p>
msgid ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Education</a>&nbsp;/ <a "
"href=\"/education/edu-cases.html\">Case&nbsp;Studies</a>&nbsp;/"
msgstr ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Educação</a>&nbsp;/ <a "
"href=\"/education/edu-cases.html\">Estudos&nbsp;de&nbsp;Caso</a>&nbsp;/"

#. type: Content of: <div><h2>
msgid "Case Studies in India"
msgstr "Estudos de Caso na Índia"

#. type: Content of: <div><div><p>
#, fuzzy
#| msgid ""
#| "An <a href=\"/education/edu-system-india.html\">article</a> by Indian "
#| "scientist Dr. V. Sasi Kumar which explains in detail how the education "
#| "system is structured in India, with significant notes on its development "
#| "throughout history. The article also covers the case of Kerala, the state "
#| "in India which successfully implemented Free Software in thousands of "
#| "schools thanks to the government's project <a href=\"https://www.itschool."
#| "gov.in/\">IT@School</a>."
msgid ""
"An <a href=\"/education/edu-system-india.html\">article</a> by Indian "
"scientist Dr. V. Sasi Kumar which explains in detail how the education "
"system is structured in India, with significant notes on its development "
"throughout history. The article also covers the case of Kerala, the state in "
"India which successfully implemented free software in thousands of schools "
"thanks to the government's project <a href=\"https://web.archive.org/"
"web/20181214064758/https://www.itschool.gov.in/otherprograms.php\"> "
"IT@School</a>."
msgstr ""
"Um <a href=\"/education/edu-system-india.html\">artigo</a>, do cientista "
"indiano Dr. V. Sasi Kumar, que explica em detalhes como o sistema "
"educacional está estruturado na Índia, com notas significativas sobre o seu "
"desenvolvimento ao longo da história. O artigo também cobre o caso de "
"Kerala, o estado na Índia que implementou com sucesso o Software Livre em "
"milhares de escolas graças ao projeto do governo <a href=\"https://www."
"itschool.gov.in/\">IT@School</a>."

#. type: Content of: <div><div><p>
#, fuzzy
#| msgid ""
#| "Some notions on the <a href= \"http://en.wikipedia.org/wiki/"
#| "Education_in_India\">education system of India</a> can also be found in "
#| "Wikipedia."
msgid ""
"Some notions on the <a href= \"https://en.wikipedia.org/wiki/"
"Education_in_India\">education system of India</a> can also be found in "
"Wikipedia."
msgstr ""
"Algumas noções sobre o <a href= \"http://en.wikipedia.org/wiki/"
"Education_in_India\">sistema educacional na Índia</a> também podem ser "
"encontradas na Wikipédia."

#. type: Content of: <div><div><p>
#, fuzzy
#| msgid ""
#| "Below are listed some of the educational institutions in India who are "
#| "using Free Software."
msgid ""
"Below are listed some of the educational institutions in India who are using "
"free software."
msgstr ""
"Abaixo estão listadas algumas das instituições educacionais da Índia que "
"estão usando Software Livre."

#. type: Content of: <div><div><ul><li>
msgid ""
"<a href= \"/education/edu-cases-india-ambedkar.html\">Ambedkar Community "
"Computing Center (AC3)</a>"
msgstr ""
"<a href= \"/education/edu-cases-india-ambedkar.html\">Centro de Computação "
"Comunitária de Ambedkar (AC3)</a>"

#. type: Content of: <div><div><ul><li><p>
#, fuzzy
#| msgid ""
#| "A group of Free Software advocates set up a computing center in a slum in "
#| "Bangalore to teach computer skills to underprivileged children."
msgid ""
"A group of free software advocates set up a computing center in a slum in "
"Bangalore to teach computer skills to underprivileged children."
msgstr ""
"Um grupo de defensores do Software Livre instalou um centro computacional em "
"uma favela em Bangalore para ensinar habilidades de informática para "
"crianças desprivilegiadas."

#. type: Content of: <div><div><ul><li>
msgid ""
"<a href= \"/education/edu-cases-india-irimpanam.html\">Vocational Higher "
"Secondary School Irimpanam (VHSS Irimpanam)</a>"
msgstr ""
"<a href= \"/education/edu-cases-india-irimpanam.html\">Escola de Ensino "
"Profissionalizante de Irimpanam (VHSS Irimpanam)</a>"

#. type: Content of: <div><div><ul><li><p>
#, fuzzy
#| msgid ""
#| "One of the several thousand schools that are using exclusively Free "
#| "Software as the result of a <a href= \"https://web.archive.org/"
#| "web/20180211103420/http://www.kite.kerala.gov.in/KITE/index.php/welcome/"
#| "wedo/1\"> project</a> carried out by the government in the state of "
#| "Kerala."
msgid ""
"One of the several thousand schools that are using exclusively free software "
"as the result of a <a href= \"https://web.archive.org/web/20180211103420/"
"http://www.kite.kerala.gov.in/KITE/index.php/welcome/wedo/1\"> project</a> "
"carried out by the government in the state of Kerala."
msgstr ""
"Um dos vários milhares de escolas que estão usando exclusivamente Software "
"Livre como o resultado de um <a href= \"https://web.archive.org/"
"web/20180211103420/http://www.kite.kerala.gov.in/KITE/index.php/welcome/"
"wedo/1\"> projeto</a> realizado pelo governo no estado de Kerala."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e a "
"contribuição com traduções das páginas deste site."

#. type: Content of: <div><p>
#, fuzzy
#| msgid "Copyright &copy; 2014, 2015 Free Software Foundation, Inc."
msgid "Copyright &copy; 2011, 2022, 2024 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2014, 2015 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Esta página está licenciada sob uma licença <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle <a href=\"mailto:rafaelff@gnome.org\">&lt;"
"rafaelff@gnome.org&gt;</a>, 2017-2020."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"

#~ msgid ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020, 2021 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020, 2021 Free Software Foundation, "
#~ "Inc."

#~ msgid ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020, 2021, 2022 Free Software "
#~ "Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020, 2021, 2022 Free Software "
#~ "Foundation, Inc."

#~ msgid ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020 Free Software Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2011, 2015, 2016, 2020 Free Software Foundation, Inc."

#~ msgid "<a href=\"/education/edu-cases.html\">Back to Case Studies</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-cases.html\">Voltar para Estudos de Caso</a>"

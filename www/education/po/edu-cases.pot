# LANGUAGE translation of https://www.gnu.org/education/edu-cases.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: edu-cases.html\n"
"POT-Creation-Date: 2024-05-18 13:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Education - Case Studies - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr ""

#. type: Attribute 'title' of: <div><a><img>
msgid "Education Contents"
msgstr ""

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Education Contents]&nbsp;"
msgstr ""

#. type: Content of: <div>
msgid "</a>"
msgstr ""

#. type: Content of: <div><p><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <div><p><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <div><p>
msgid "</a>&nbsp;/ <a href=\"/education/education.html\">Education</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "Case Studies"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"We perform worldwide research to examine cases of private and public "
"educational institutions as well as informal educational environments that "
"have successfully implemented the use of Free Software. We do not attempt to "
"build a comprehensive database here. Instead, we present specific cases of "
"success, in the belief that testimony will be inspiring to anyone "
"considering first time installation or migration to Free Software."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If you know of a case that could be included here, please contact us <a "
"href=\"mailto:education@gnu.org\">&lt;education@gnu.org&gt;</a> to let us "
"know."
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"&nbsp;<a href=\"/education/edu-cases-argentina.html\">Argentina</a>&nbsp; "
"&nbsp;<a href=\"/education/edu-cases-india.html\">India</a>&nbsp; &nbsp;<a "
"href=\"/education/edu-cases-italy.html\">Italy</a>&nbsp;"
msgstr ""

#. type: Content of: <div><h3>
msgid "Argentina"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/education/edu-cases-argentina-ecen.html\">Escuela Cristiana "
"Evang&eacute;lica de Neuqu&eacute;n (ECEN)</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"An elementary teacher with little technical skills manages to get her school "
"to migrate all work stations, including the administration offices and the "
"library."
msgstr ""

#. type: Content of: <div><h3>
msgid "India"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/education/edu-cases-india-ambedkar.html\">Ambedkar Community "
"Computing Center (AC3)</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"A group of Free Software advocates in Bangalore teaches computer skills to "
"underprivileged children using exclusively Free systems and programs."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/education/edu-cases-india-irimpanam.html\">Vocational Higher "
"Secondary School Irimpanam (VHSS Irimpanam)</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"One of the several thousand schools that migrated completely to free/libre "
"software under the government's project <a "
"href=\"https://web.archive.org/web/20181214064758/https://www.itschool.gov.in/otherprograms.php\"> "
"IT@School</a>."
msgstr ""

#. type: Content of: <div><h3>
msgid "Italy"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/education/edu-cases-italy-south-tyrol.html\"> Migration in South "
"Tyrol</a>"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"All Italian-language schools in the Autonomous Province of Bolzano (South "
"Tyrol) were migrated to free/libre software."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 2011, 2016, 2020, 2021, 2024 Free Software Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

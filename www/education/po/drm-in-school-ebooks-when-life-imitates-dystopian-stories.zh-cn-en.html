<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>DRM In School eBooks: When Life Imitates Dystopian Stories
- GNU Project - Free Software Foundation</title>
<link rel="stylesheet" type="text/css" href="/side-menu.css" media="screen" />
 <!--#include virtual="/education/po/drm-in-school-ebooks-when-life-imitates-dystopian-stories.translist" -->
<!--#include virtual="/server/banner.html" -->

<div class="nav">
<a id="side-menu-button" class="switch" href="#navlinks">
 <img id="side-menu-icon" height="25" width="31"
      src="/graphics/icons/side-menu.png"
      title="Education Contents"
      alt="&nbsp;[Education Contents]&nbsp;" />
</a>

<p class="breadcrumb">
 <a href="/"><img src="/graphics/icons/home.png" height="26" width="26"
    alt="GNU Home" title="GNU Home" /></a>&nbsp;/
 <a href="/education/education.html">Education</a>&nbsp;/
 <a href="/education/education.html#indepth">In&nbsp;Depth</a>&nbsp;/</p>
</div>

<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->

<div style="clear: both"></div>
<div id="last-div" class="article reduced-width">

<h2>DRM In School eBooks: When Life Imitates Dystopian Stories</h2>

<address class="byline">by Barra O'Cathain <a href="#barra" id="barra-rev"><sup>[*]</sup></a></address>

<div class="article">
<p>It always feels surreal to come across situations that are just a little 
too close to something you've read. It's even worse when you realize that 
something you've read is a dystopian story warning about the dangers of 
corporate greed and abuse of students.</p>

<p>In February 1997, the magazine <cite>Communications of the
<abbr title="Association for Computing Machinery">ACM</abbr></cite> 
published Richard M. Stallman's <a href="/philosophy/right-to-read.html">
<cite>The Right to Read</cite></a>, a cautionary tale of a future where 
publishers and the government crack down on so-called piracy&nbsp;<a 
href="#piracy" id="piracy-rev">[1]</a> to a massive extent.</p>

<p>In <cite>The Right to Read</cite>, a college student named Lissa Lenz has 
an issue. Her computer, which contains all her textbooks and is the only 
tool for writing her midterm project, breaks down. She asks her friend Dan 
Halbert to borrow his computer. This is a big problem for Dan. If Lissa were 
to read his books, the SPA&mdash;a government agency created to combat 
sharing&mdash;would arrest him for copyright infringement and brand him 
a criminal. In the end, out of concern for his friend, he does the 
unthinkable: he gives Lissa his password in an attempt to hide the copyright 
infringement from the SPA, breaking the law with that simple act.</p>

<p>Stallman predicted a lot of bad things in that piece of fiction. Sadly, 
they have already come true. <cite>The Right to Read</cite> is no longer 
a hypothetical scenario, no longer just a story warning about a possible 
future.</p>

<p>It is our <em>present</em>.</p>

<h3>What is DRM?</h3>

<p>DRM is an initialism which is supposed to stand for 
&ldquo;Digital Rights Management,&rdquo; but in practice it's more accurate 
to say it stands for &ldquo;Digital <em>Restrictions</em> Management.&rdquo; 
It refers to any means used to control copyrighted and proprietary digital 
works and hardware. Its purpose is to restrict what users can do. DRM is an 
umbrella term for various tools aimed at achieving that goal, such as legal 
agreements (which is the technique the dis-service in question is using), 
or malware that seeks to prevent specific actions. For example, to prevent 
users from connecting to a website through the TOR network or from outside 
of a certain geographical area (Ireland, in my case). For some examples of 
Digital Restrictions Management, take a look at the page on <a 
href="/proprietary/proprietary-drm.html">proprietary DRM</a></p>

<h3>A Real-Life Encounter With Becoming Illegal</h3>

<p>During the course of my secondary school education, I was contacted by a
friend who was finding it difficult to study because he had managed to mess
things up by leaving his textbooks in his locker over a mid-term break. 
Silly mistake aside, I thought nothing of lending him a modified version of 
my password so that he could access my copies of the ebooks, hosted at the 
publisher's platform (the &ldquo;service&rdquo;). He'd be able to 
study and pass the upcoming exams, no harm done. Little did I know that, 
according to the terms and conditions of the dis-service, I had just 
committed the most vile, despicable act a human being could commit: 
help my friend&mdash;or, in the eyes of the publisher, 
&ldquo;piracy.&rdquo;</p>
 
<p>The terms and conditions&nbsp;<a href="#terms" id="terms-rev">[2]</a> 
of the dis-service are somewhat hard to find, which makes one feel the 
publisher is untrustworthy. They are not readily available on the login page 
or on the main library page; instead, they are hidden in the help section. 
I won't quote them exactly, but they do expressly forbid the sharing of 
passwords. They also contain several other things worth noting, which I will 
discuss later.</p>

<p>The terms and conditions are very, very clear about one thing: you're not
allowed to share the ebook in any way, with any means, under any  
circumstances.</p>

<p>Let me clear up one thing. I don't actually own the ebook. The physical 
version of the book proudly displays a notice on the cover saying you'll get 
a free ebook version along with your purchase. That's misleading, at best. 
What I get is a time-limited license to access its contents, exclusively on 
the publisher's proprietary platform. I can't download it to get a local 
copy to read offline because the publisher claims it's &ldquo;too big&rdquo; 
to fit in removable media, ignoring the fact that I may just want it on my 
hard drive. I decided to see if the claim was true and found that the grand 
total size of the ebook came in orders of magnitude lower than even the 
capacity of a CD-R disc. Are we really to believe the reason we can't 
download a copy of a digital book is that it can't fit in removable media? 
In my opinion, the real reason they don't want people to download copies is 
to prevent sharing.</p>

<h3>Common Restrictions</h3>

<p>Some new schools where I live in Ireland are using iPads (which have a
whole host of <a href="/proprietary/malware-apple.html">privacy and ethical 
concerns</a> in and of themselves) with the goal of moving all their students' 
books to these online dis-services. Benefits cited often include 
reduced weight in students' bags, ease of organization, and multimedia 
capabilities. All of which are true, but what is often neglected is that the 
move to digital devices requires students to agree to terms of service 
imposed by companies. These terms restrict students' ability to explore, 
research, and <em>learn</em>.</p>

<p>There are also a lot of practical downsides to ebooks on these platforms. 
They have to be used with a constant connection to the Internet, which will 
be difficult for many schools to maintain. They can't be downloaded, so 
students who don't have easy access to the Internet will be essentially 
stuck with no books. They may not be supported on all devices, or may be 
restricted to a single operating system or browser. Probably the biggest 
downside is that they can be obtained only from one centralized location, 
with authorized access granted only to the person who paid for it, and taken 
away after a limited time. Could you imagine a company coming to your 
graduation and wordlessly snatching your physical books back? A silly, 
ridiculous image, but it's what happens with ebooks.</p>

<p>When schools use physical books, students at least have the option of 
buying them second-hand, or getting them handed down from a friend or a 
sibling. If the practice of getting an ebook access code from a single 
centralized publisher continues, we may see a publisher's monopoly  
where textbooks needed for our free education are held away from us with a 
massive price tag. We may end up with a situation like Texas Instruments, 
where a company with a stranglehold on education can charge astronomical 
prices without the need to innovate or upgrade. Such a position was gained  
by pushing themselves as the educational standard in the National Council of 
Teachers of Mathematics. Once established as such, the company began to 
abuse its position by refusing to reduce the price of their calculators as 
they become cheaper to manufacture year after year. This leaves the company 
with gross profit margins of up to 90 percent, all the while making it very 
difficult for lower income families to educate their children.</p>

<p>Students don't have much of a say about which platforms they'll be 
required to use. The school may give them an email address, provided by 
Microsoft Office&nbsp;365, and require them to agree to the terms imposed 
by the publisher. Students may need books from different publishers, and may 
have to agree to multiple contracts. And even if they do agree to a given 
version of a contract, most publishers reserve the right to change it. 
Perhaps the publisher might&mdash;as I discovered in the terms of the 
dis-service I mentioned earlier&mdash;reserve the right to later charge fees 
to access the books. Do the students really have a choice? Not at the moment. 
Unless something changes, they don't have a choice. They're forced to accept 
the terms, no matter what they think of them, otherwise they lose their 
chances for education by losing their books.</p>

<h3>Challenging the Assumptions</h3>

<p>Some may say that these terms are reasonable, that students aren't 
entitled to learn how the tools they use during their education work, or 
to share information with their peers.</p>

<p>Would you object to a student reading her schoolbook while on holidays in 
France? If she reads it while traveling to Northern Ireland? On a bus? In a 
public library?</p>

<p>Of course not.</p>

<p>Would you also object to, say, a student lending a copy of his book to a 
friend? Allowing someone sitting next to him to look at his book? If a 
student copies a sentence from a book into his notes, is he a thief or a 
pirate? Should the teacher report him for illegal activity?</p>

<p>Of course not.</p>

<p>And what if the student were to ask how the book was bound? How the paper 
was made? What the ink was made up of? How the process of writing works? How 
they are delivered to bookstores to be sold? Should this student be punished 
for attempting to learn about the publisher's techniques?</p>

<p>Of course not.</p>

<p>And finally, would you object to students selling their textbooks when 
they no longer have a need for them? To giving away their notes, made using 
information from the book, to other students? Would you say students 
shouldn't be allowed to give away their book if it has a line crossed out 
and rewritten?</p>

<p>Of course not.</p>

<p>My friend made quite an apt summary: <q>It's like [school systems] put 
the rights of companies over the rights of the students.</q></p>

<p>With the current landscape of educational institutions planning to 
introduce new technologies, we need to be careful. Without proper 
consideration and action, we may find ourselves in a reality even closer to 
the one described in <cite>The Right to Read</cite>. School boards have 
already made mistakes in the past, like with Texas Instruments. I would urge 
everyone to start pushing against this sort of terms. Here are some 
suggestions:</p>

<ul>
<li>During the decision process about which textbooks to use, you could 
petition your school to consider the terms and conditions of ebook 
services and make it a requirement that ebooks be DRM-free and 
downloadable.</li> 

<li>You could start the preparation of a textbook for your local 
curriculum and publish it under a free license such as the <a 
href="/licenses/licenses.html#FDL">GNU Free Documentation License</a>, 
<a 
href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a>, or
similar.</li>

<li>Support the FSF's <a 
href="https://www.defectivebydesign.org/ebooks.html">campaign</a> to 
abolish eBook DRM.</li>
</ul>

<p>Let's make sure schools don't punish learning. </p>

<p>Let's make sure <a href="/philosophy/ebooks-must-increase-freedom.html">
ebooks increase our freedom, not decrease it</a>.</p>
<div class="column-limit"></div>
</div>

<h3 class="footnote">Author's Notes</h3>
<ol >
<li><a href="#piracy-rev" id="piracy">&#8593;</a> &ldquo;Piracy&rdquo; is a 
<a href="/philosophy/words-to-avoid.html#Piracy">smear word</a>.</li>

<li><a href="#terms-rev" id="terms">&#8593;</a> Some notes from the Terms and 
Conditions of dis-services:
<ul>
 <li>Passwords must not be shared. </li>
 <li>The publisher reserves the right to later charge for access to the 
     dis-service.</li>
 <li>The reader can't distribute any information from the dis-service unless 
     in ways explicitly allowed.</li>
 <li>It is forbidden to attempt to learn how the dis-service works by
     reverse-engineering, attempting to derive source code, or by any other 
     means.</li>
 <li>The books are region-locked (only accessible in a certain area) to the
     Republic of Ireland. </li>
 <li>No warranties are provided. The dis-service shall not be liable for any
     damages, yet expects you to be liable for damages to them.</li>
</ul></li>
</ol>

<h3 class="footnote">Thanks</h3>

<p style="font-size:1rem">
Thanks to Richard Stallman, Andy Oram, and the GNU Education Team for 
the idea and the help.</p>

<div class="infobox extra" role="complementary">
<hr />

<p><a href="#barra-rev" id="barra">[*]</a> Barra O'Cathain is a young hacker 
from Ireland. He is currently persuing a bachelor's degree in Computer 
Science. His fascination with free software and programming began when he 
came across the Quake III Arena source code, which was made available under 
the GNU GPL in 2005.</p>

</div>

</div>

<!--#include virtual="/education/education-menu.html" -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and contributing translations
of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2021 Barra O'Cathain</p>

<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by/4.0/">Creative
Commons Attribution 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2021/12/22 06:41:34 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

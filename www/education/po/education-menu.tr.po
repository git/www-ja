# Turkish translation of https://gnu.org/education/education-menu.html.
# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# T. E. Kalaycı, 2020.
# May 2020: update to new layout (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: education-menu.html\n"
"Report-Msgid-Bugs-To: Webmasters <webmasters@gnu.org>\n"
"POT-Creation-Date: 2022-04-20 17:56+0000\n"
"PO-Revision-Date: 2022-04-22 18:29+0200\n"
"Last-Translator: T. E. Kalayci <tekrei@member.fsf.org>\n"
"Language-Team: Turkish <www-tr-comm@gnu.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0.1\n"

#. type: Content of: <div><div>
msgid "<a href=\"#content\"><span>BACK TO TOP </span>&#9650;</a>"
msgstr "<a href=\"#content\"><span>BAŞA DÖN </span>&#9650;</a>"

#. type: Content of: <div><div><h3>
msgid "Education"
msgstr "Eğitim"

#. type: Content of: <div><div><dl><dt>
msgid "<a href=\"/education/education.html#content\">Main page</a>"
msgstr "<a href=\"/education/education.html#content\">Ana sayfa</a>"

#. type: Content of: <div><div><dl><dt>
msgid "<a href=\"/education/edu-cases.html#content\">Case studies</a>"
msgstr "<a href=\"/education/edu-cases.html#content\">Durum çalışmaları</a>"

#. type: Content of: <div><div><dl><dt>
msgid "<a href=\"/education/edu-resources.html#content\">Resources</a>"
msgstr "<a href=\"/education/edu-resources.html#content\">Kaynaklar</a>"

#. type: Content of: <div><div><dl><dt>
msgid ""
"<a href=\"/education/bigtech-threats-to-education-and-society.html#content"
"\">Big Tech Threats</a>"
msgstr ""
"<a href=\"/education/bigtech-threats-to-education-and-society.html#content"
"\">Büyük Teknolojilerin Tehditleri</a>"

#. type: Content of: <div><div><dl><dt>
msgid ""
"<a href=\"/education/resisting-proprietary-software.html#content"
"\">Resistance</a>"
msgstr ""
"<a href=\"/education/resisting-proprietary-software.html#content\">Direniş</"
"a>"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/successful-resistance-against-nonfree-software."
"html#content\">Successful resistance</a>"
msgstr ""
"<a href=\"/education/successful-resistance-against-nonfree-software."
"html#content\">Başarılı direniş</a>"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/dangers-of-proprietary-systems-in-online-teaching."
"html#content\">Online teaching</a>"
msgstr ""
"<a href=\"/education/dangers-of-proprietary-systems-in-online-teaching."
"html#content\">Çevrim içi öğretim</a>"

#. type: Content of: <div><div><dl><dt>
msgid "<a href=\"/education/edu-faq.html#content\">FAQ</a>"
msgstr "<a href=\"/education/edu-faq.html#content\">SSS</a>"

#. type: Content of: <div><div><dl><dt>
msgid "<a href=\"/education/edu-team.html#content\">Our team</a>"
msgstr "<a href=\"/education/edu-team.html#content\">Eğitim ekibi</a>"

#. type: Content of: <div><div><dl><dt>
msgid "In depth"
msgstr "Ayrıntılı"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/edu-why.html#content\">Why educational institutions "
"should use and teach free software</a>"
msgstr ""
"<a href=\"/education/edu-why.html#content\">Eğitim kurumları neden özgür "
"yazılım kullanmalı ve öğretmeli</a>"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/edu-schools.html#content\">Why schools should "
"exclusively use free software</a>"
msgstr ""
"<a href=\"/education/edu-schools.html#content\">Okullar neden sadece özgür "
"yazılım kullanmalı</a>"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/edu-system-india.html#content\">The education system in "
"India</a>"
msgstr ""
"<a href=\"/education/edu-system-india.html#content\">Hindistan'daki eğitim "
"sistemi</a>"

#. type: Content of: <div><div><dl><dd><ul><li>
msgid ""
"<a href=\"/education/drm-in-school-ebooks-when-life-imitates-dystopian-"
"stories.html#content\">DRM in school eBooks: When life imitates dystopian "
"stories</a>"
msgstr ""
"<a href=\"/education/drm-in-school-ebooks-when-life-imitates-dystopian-"
"stories.html#content\">Okul e-kitaplarındaki DRM: Yaşam distopyan öyküleri "
"örnek alırsa</a>"

#. type: Content of: <div><div><dl><dt>
msgid ""
"<a href=\"/education/misc/edu-misc.html#content\">Miscellaneous materials</a>"
msgstr ""
"<a href=\"/education/misc/edu-misc.html#content\">Çeşitli materyaller</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-cases-argentina.html#content\">Argentina</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-cases-argentina.html#content\">Arjantin</a>"

#~ msgid "<a href=\"/education/edu-cases-india.html#content\">India</a>"
#~ msgstr "<a href=\"/education/edu-cases-india.html#content\">Hindistan</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-software.html#content\">Educational Software</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-software.html#content\">Eğitsel Yazılım</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-projects.html#content\">Groups &amp; projects</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-projects.html#content\">Gruplar ve projeler</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-free-learning-resources.html#content\">Free "
#~ "learning resources</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-free-learning-resources.html#content\">Özgür "
#~ "öğrenme kaynakları</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-cases-argentina-ecen.html#content\">ECE Neuquén</"
#~ "a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-cases-argentina-ecen.html#content\">ECE Neuquén</"
#~ "a>"

#~ msgid ""
#~ "<a href=\"/education/edu-cases-india-ambedkar.html#content\">Ambedkar "
#~ "Center</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-cases-india-ambedkar.html#content\">Ambedkar "
#~ "Merkezi</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-cases-india-irimpanam.html#content\">VHSS "
#~ "Irimpanam</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-cases-india-irimpanam.html#content\">VHSS "
#~ "Irimpanam</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-software-gcompris.html#content\">Gcompris</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-software-gcompris.html#content\">Gcompris</a>"

#~ msgid "<a href=\"/education/edu-software-gimp.html#content\">Gimp</a>"
#~ msgstr "<a href=\"/education/edu-software-gimp.html#content\">Gimp</a>"

#~ msgid ""
#~ "<a href=\"/education/edu-software-tuxpaint.html#content\">Tux Paint</a>"
#~ msgstr ""
#~ "<a href=\"/education/edu-software-tuxpaint.html#content\">Tux Paint</a>"

#~ msgid "<a href=\"/education/edu-team.html\">OUR&nbsp;TEAM</a>"
#~ msgstr "<a href=\"/education/edu-team\">EKİBİMİZ</a>"

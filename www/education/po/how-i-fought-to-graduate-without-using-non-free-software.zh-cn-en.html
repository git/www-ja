<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>How I Fought To Graduate Without Using Nonfree Software
- GNU Project - Free Software Foundation</title>
<link rel="stylesheet" type="text/css" href="/side-menu.css" media="screen" />
 <!--#include virtual="/education/po/how-i-fought-to-graduate-without-using-non-free-software.translist" -->
<!--#include virtual="/server/banner.html" -->

<div class="nav">
<a id="side-menu-button" class="switch" href="#navlinks">
 <img id="side-menu-icon" height="25" width="31"
      src="/graphics/icons/side-menu.png"
      title="Education Contents"
      alt="&nbsp;[Education Contents]&nbsp;" />
</a>

<p class="breadcrumb">
 <a href="/"><img src="/graphics/icons/home.png" height="26" width="26"
    alt="GNU Home" title="GNU Home" /></a>&nbsp;/
 <a href="/education/education.html">Education</a>&nbsp;/
 <a href="/education/resisting-proprietary-software.html">Resistance</a>&nbsp;/
 <a href="/education/successful-resistance-against-nonfree-software.html">Successful resistance</a>&nbsp;/</p>
</div>

<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->

<div style="clear: both"></div>
<div id="last-div" class="article reduced-width">

<h2>How I Fought To Graduate Without Using Nonfree Software</h2>

<address class="byline">by Wojciech Kosior 
<a href="#thanks" id="thanks-rev"><sup>[1]</sup></a></address>

<p>As a university student, I have struggled during the pandemic like
everyone else. Many have experienced deaths in their families, or have
lost their jobs. While studying informatics at the AGH University of
Science and Technology in Kraków, Poland, I have been fighting
another, seemingly less important battle, but one I passionately feel
is vital to our future freedoms. I describe my fight below, so as to
encourage and inspire others.</p>

<h3>Unethical platforms</h3>

<p>Software freedom is a huge but hidden issue in our time. Digital
communications technologies such as videoconferencing have taken center
stage in our lives, and for many the use of these has been a savior.
They do not notice the danger concealed in the way it works:
whoever controls this technology controls our lives. Recently we have
seen the power of Big Tech to subvert democracy, control speech,
exclude groups, and invade our privacy.</p>

<p>Software freedom is a fight to return control to people. It is a 
fight against &ldquo;nonfree&rdquo; software, also called 
<em>proprietary software</em>, which imposes unjust and invasive harms 
on its users. In pursuit of our liberating mission, advocates of 
software freedom like myself insist on using <em>libre</em> software.</p>

<p>It is especially important to spread these ideals to new generations.
Unfortunately, we often see the opposite trend. The default operating 
system found in most computer classrooms of my country is proprietary 
Microsoft Windows, with some universities even providing students 
licenses for it. At some point I came to realize this practice really 
only benefits the proprietary operating system vendor. Similarly 
terrifying is the level of dependence of course organization on 
nonfree Google Sheets and Google Forms.</p>

<p>During the pandemic we saw educational facilities hastily embrace 
proprietary tools such as Microsoft Teams, Zoom, and WhatsApp, 
pressured by the network they generate. Schools and universities then 
tried to impose them on students, who subsequently suffered the loss 
of freedom from using programs that users don't control, as well as 
bad security and violations of privacy.</p>

<p>Because I refuse to use unethical software, the complete reliance on
proprietary platforms has created an ethical conflict. My aim has been 
to complete my university degree without surrendering to the imposed 
nonfree services, by convincing my 
professors<a href="#teachers" id="teachers-rev"><sup>[2]</sup></a> to 
allow me to use only free-software replacements to proprietary 
applications. I didn't expect to win a fight against such power, but 
now, through polite but firm action, I think I may have prevailed. 
Hopefully this story will help you resist too.</p>

<h3>Ethical studying</h3>

<p>Over time I've become more and more determined to avoid nonfree
software. Among other challenges, that meant getting a Libreboot'ed
ThinkPad and switching to GNU/Linux distros that include only libre
packages. One might ask:</p>

<blockquote>
<p>What about studies? Weren't you required to use
Windows?  Or MS Office? Or some other proprietary tools?</p>
</blockquote>

<p>Actually, a majority of classroom assignments could be completed 
with free software. Today we have the luxury of excellent libre 
operating systems and libre tools for most tasks.  Most popular 
programming languages have free software implementations. On those few 
occasions when some nonfree tool was strictly required, I was able 
either to convince the professor to let me make a substitution&mdash;for
example, to complete the exercises with a PostgreSQL database instead
of Oracle&mdash;or to do the assignment on a university computer in 
the lab. I admit, running nonfree software on a computer other than 
one's own doesn't fully solve the ethical problem. It just seemed fair, 
but it is not something I'm proud of.</p>

<p>I also take the issue of in-browser JavaScript (js) more seriously
now.  Web js runs in an isolated sandbox, which leads many to
believe it's acceptable, even though it's nonfree.  Sandboxing might
indeed solve security issues, but the true problem with proprietary
programs lies elsewhere, in its denial of letting users have control. 
Currently, maintaining one's software freedom in the field of web 
browsing is not easy. Sites frequently malfunction when js is 
disabled.  I have had to ask colleagues to help me enter study-related 
data into a Google Sheet because I couldn't do that without js
enabled.  In addition, js code is used to implement browser 
fingerprinting which is used to track users.</p>

<h3>Gentle persuasion</h3>

<p>Without serious problems, I completed the fifth semester of my 
studies. At the beginning of the sixth semester, the pandemic began. 
Universities closed their physical facilities, so most students 
returned home and professors started organizing remote classes. 
Unsurprisingly, they all chose proprietary platforms. Cisco WebEx, 
Microsoft Teams, ClickMeeting, and Skype were popular choices.  I could 
not find a free software client for any of those. Also, not realizing 
the problem of nonfree js, professors expected everyone to be able to 
easily join the video sessions using any web interface.</p>

<p>How did I handle these requirements? I would <em>very</em> politely 
email every single professor who announced something would be done 
using a problematic platform, explaining the lack of a suitable free 
software client. I often included a link to a popular online 
explanation of the issues of software freedom and universities, the 
&ldquo;Costumed Heroes&rdquo; video created by the Free Software 
Foundation (FSF), along with some other links to free videoconferencing 
programs like Jami and Jitsi Meet.</p>

<p>Although there are many documented surveillance and security issues
on these centralized platforms, I explained that, for me, software
freedom was the troubling factor. Replies urging me to &ldquo;run the
program in a virtual machine&rdquo; or saying that I &ldquo;don't need 
the source code to use the service,&rdquo; made it clear that some of 
my professors didn't understand, or understood only part of the issues.  
Had I been studying anything other than informatics, I suspect the 
fraction of those who understood the problem would be far smaller.</p>

<h3>Missing out</h3>

<p>There were two distinct areas of concern. The first was with
accessing and participating in the teaching materials; for example,
in a Machine Learning class I found someone to forward on to me what
the professor had said.  The second was around registration and
assessment. For some remote classes, presence was not checked. I
skipped those. Uploading my homework to Moodle also didn't pose any
issues.</p>

<p>The first real problem arose with the Artificial Intelligence (AI)
course. It was taught by rotation. The first professor gave
homework requiring the proprietary Framsticks application, but
allowed me to do a neural networks exercise instead. Another professor
agreed I could use Webots instead of Choreographe for a simulation
exercise. Yet another one asked us to complete an online NVIDIA course 
that required nonfree js. That professor did not respond to my email.</p>

<p>One Distributed Systems homework was supposed to be submitted via
WebEx, but that professor agreed to let me use Jami instead.</p>

<h3>Uncertainty and doubt</h3>

<p>Another issue that arose was gnawing uncertainty in the absence of a
clear policy.  Not knowing whether the university would recognize my
principles was a cause of ongoing stress. Despite my small early
victory, other rotational courses meant that three more professors
would each need to agree if I were to pass, so until June I could not
be sure I would succeed. In March, System Programming classes started.  
The professor, who didn't want to lose time connecting to a libre 
platform to rate my homework, gave me little hope, so again I was to 
live in uncertainty through the Easter and beyond.</p>

<p>I believe every class should <em>at minimum</em> be offered ways to
interoperate with libre tools so that students can at least read class
assignments on free platforms, and upload their answers from them.
Unless universities offer interoperability, the reliance on proprietary 
software costs both students and professors time and headaches. At one 
point I emailed two professors about the use of nonfree platforms for 
lectures. One didn't respond and the other replied rudely. They seemed 
not to understand, but I suspect they were avoiding any extra work. 
This had a corrosive impact on my engagement and I stopped caring about 
lectures. Avoiding a language-specific package manager that I felt put 
me at risk of security and freedom issues cost me considerable time 
and delayed my studies. Time is precious for us all.</p>

<h3>Friction over freedom</h3>

<p>Although stressful, thus far things had gone fairly smoothly.  But
after Easter, a Software Engineering course presented the first big
problem. This professor first ignored my emails, but eventually wrote
a long reply and threatened to fail me if I missed one more meeting.
That email's tone showed great annoyance, perhaps anger. It was
suggested that I use a colleague's help to participate in the meeting.
Another classmate and I connected through Mumble, through which the 
professor was also intermediated&mdash;not perfect, but it worked!</p>

<p>The issues of software freedom, which are ethical, must be separated
from other concerns to which open source supporters often give priority.
For example, advocates of open source refrain from bringing those 
important freedom issues to the table and only say that software with 
source code publicly available is going to achieve higher quality
with the help of the community. Meanwhile, our opponents claim 
proprietary software can bring higher revenue, allowing the hiring of 
more developers to work on improving it.</p>

<p>The Compilers course exam was to be conducted through Microsoft 
Teams. Again, sticking to my principles, I thought I would fail. 
Funnily enough, it was Teams that failed. It could not handle dozens 
of students connecting, so instead the exam was conducted via email. 
On the other hand, during contact with my thesis supervisor in July, 
Jami broke during the meeting. No software is perfect. But with libre 
software you at least get to keep both pieces when it breaks.</p>

<p>It is not necessarily the functional aspects of the software that
creates friction around lack of software freedom. During the summer I 
had to do an internship. I backed out of a paid offer after learning 
that the employer would make my code nonfree.  I eventually did 
another, unpaid internship.</p>

<p>So after all my struggles, I finally passed the summer semester and 
even had decent grades. What at some point seemed almost impossible, 
was now a reality.</p>

<h3>Proprietary imposed at all levels</h3>

<p>Before the winter semester, a list of allowed videoconferencing 
platforms that comply with the data protection law was given to 
professors.  It contained Microsoft Teams, Cisco WebEx, ClickMeeting, 
and Google Meet.  You will surely see the irony here!</p>

<p>One professor agreed to use Jitsi Meet for all his classes and
suggested that I ask the student council to recommend it to the Dean,
but the council never responded to my emails.  High quality software
offering better data protection capabilities was deliberately
sidelined in favor of commercial nonfree solutions in what seems like 
a case of corrupt corporate capture of an educational institution. The 
libre software didn't get approved and the professor kept 
communicating with other students via WebEx.</p>

<h3>Misconceptions</h3>

<p>As I mentioned earlier, despite being highly knowledgeable
computer scientists and experienced in informatics, many
academics demonstrated a generally poor understanding of
the politics and ethics around software.</p>

<p>The professor giving the seminar claimed that because a libre 
platform also runs on someone else's server, &ldquo;it
cannot be safer.&rdquo; I responded that Jitsi Meet allows for 
independent instances to be created, which eliminates the need to rely 
on a single company. I also noted that the lack of libre clients is 
the main problem with other services. It is a shame that professors at 
this level, who fully grasp the difference between intermediated 
encryption and end-to-end encryption, will teach it in their classes 
but not practice it in their daily profession.</p>

<p>On another occasion, I objected to using a Windows VM for a 
penetration testing exercise. The professor remarked that one would 
not be a good penetration tester if restricted to testing only libre
servers. I gave up on responding to him, but I think proprietary 
platforms should be considered insecure by default due to, for instance,
possible backdoors they may have.</p>

<h3>Having resolve</h3>

<p>At some point I had an argument with my supervisor, who gave me an
ultimatum that I must use Microsoft Teams. I didn't agree and my 
supervisor was supposed to inform the Dean about withdrawing from 
supervising me. Perhaps the Dean didn't read that email? I'm just 
guessing. Anyway, a few weeks later I even borrowed some electronics 
from my supervisor&mdash;almost as if the argument had never happened.</p>

<p>Later, one professor who didn't agree to let me pass a course
without using Teams wanted to fail me for my &ldquo;absences,&rdquo; 
despite my uploading homework throughout the semester. After 
a protracted argument, I was offered an option to meet online on 
January 8th&hellip; on Teams! I politely refused again, and reiterated my 
points. The professor eventually CC'd the Associate Dean in an email. 
In the meantime, the deadline to upload my thesis for a January defense 
expired. After many reminder emails, a response finally came, and 
through the Dean's intercession I got a grade, passed my seventh 
semester and successfully defended my thesis in March.</p>

<h3>Conclusions</h3>

<p>Looking back, I'm proud of my actions. I took the risk of failing 
my studies, and I would end up with lower final grade than if I had
submitted to the use of unethical and insecure software products.
But I am content with this.  I don't think surrendering to nonfree
platforms would bring any long-term benefits&mdash;only more 
compromises.</p>

<p>We can see some people are intolerant to software freedom principles, 
but in the end those were few and most university staff at the AGH
were actually kind to me. Thanks to them I now have a proof that it
<em>is possible</em> to study, graduate&hellip; indeed to <em>live</em> 
without relying on proprietary software. After all this hard 
experience, I feel more independent than ever, and I even received 
appreciation from the well-known 
RMS <a href="#rms" id="rms-rev"><sup>[3]</sup></a>. Hopefully, my 
story will help more students get to where I am.</p>

<p>Struggling to run only libre programs forced, and continues to force
me, to gain new skills. I now know enough about web technologies to
make several sites function without JavaScript. But what is best
about my experience is that I will be able to share my fixes with 
others and eventually make a subset of the World Wide Web usable in 
freedom.</p>

<div class="infobox">
<hr />
<p><a href="#thanks-rev" id="thanks">[1]</a> Thanks to Andy Farnell, 
Andy Oram and Richard Stallman for their help.</p>

<p><a href="#teachers-rev" id="teachers">[2]</a> Throughout this essay, 
I refer to all university teachers in the vernacular as my professors, 
although only some wear that official academic title.</p>

<p><a href="#rms-rev" id="rms">[3]</a> Dr. Richard M. Stallman, 
Founder of the Free Software Foundation and Chief GNUisance of the 
GNU Project.</p>
</div>

</div>
<!--#include virtual="/education/education-menu.html" -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and contributing translations
of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2021 Wojciech Kosior</p>

<p>This page is released under the terms of  <a rel="license"
href="https://creativecommons.org/publicdomain/zero/1.0/legalcode">
Creative Commons Zero</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2024/07/05 06:30:17 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

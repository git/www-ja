# Simplified Chinese translation of https://www.gnu.org/education/edu-software-tuxpaint.html
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Wensheng Xie <wxie@member.fsf.org>, 2017.
# May 2020: update to new layout. Unfuzzify a footer string: TODO (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: edu-software-tuxpaint.html\n"
"POT-Creation-Date: 2021-05-26 21:26+0000\n"
"PO-Revision-Date: 2017-10-06 21:08+0800\n"
"Last-Translator: Wensheng Xie <wxie@member.fsf.org>\n"
"Language-Team: Chinese <www-zh-cn-translators@gnu.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Tux Paint - GNU Project - Free Software Foundation"
msgstr "Tux Paint - GNU工程 - 自由软件基金会"

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"

#. type: Attribute 'title' of: <div><a><img>
msgid "Education Contents"
msgstr "教育内容"

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Education Contents]&nbsp;"
msgstr "&nbsp;[教育内容]&nbsp;"

#. type: Content of: <div><div><div><div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><p><a>
msgid "<a href=\"/\">"
msgstr "<a href=\"/\">"

#. type: Attribute 'title' of: <div><p><a><img>
msgid "GNU Home"
msgstr "GNU主页"

#. type: Content of: <div><p>
msgid ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Education</a>&nbsp;/ <a "
"href=\"/education/edu-resources.html\">Resources</a>&nbsp;/ <a href=\"/"
"education/edu-software.html\">Educational&nbsp;software</a>&nbsp;/"
msgstr ""
"</a>&nbsp;/ <a href=\"/education/education.html\">教育</a>&nbsp;/ <a href=\"/"
"education/edu-resources.html\">教育资源</a>&nbsp;/ <a href=\"/education/edu-"
"software.html\">教育相关的自由软件</a>&nbsp;/"

#. type: Content of: <div><h2>
msgid "Tux Paint"
msgstr "Tux Paint"

#. type: Content of: <div><div><h3>
msgid "Description"
msgstr "描述"

#. type: Content of: <div><div><p>
msgid ""
"Tux Paint is a cross-platform drawing program created specifically for "
"children. Little kids as young as 3 years old have no difficulty in finding "
"their way around its clear and intuitive interface which features large "
"buttons identified by a label as well as an icon so that the child can "
"easily recognize them. At the center of the screen there is a white canvas "
"for the child to draw making use of a wide variety of tools and paint "
"brushes. As a start, the child can load outlined pictures to be colored, as "
"in a coloring-book."
msgstr ""
"Tux Paint是一个跨平台的画图软件，它专为儿童设计。它有清晰和直觉化的界面，大大"
"的按钮带有图标和标签，三岁小孩用起来也没啥困难。在其屏幕中间是白色的画布，孩"
"子们可以在此用画笔和各种工具尽情发挥。作为入门，孩子们可以为预先设计的图画上"
"色，就像描红一样。"

#. type: Content of: <div><div><p>
msgid ""
"The program includes all of the most common drawing tools such as lines and "
"brushes for free-hand drawing and coloring, geometric shapes, sizing, an "
"eraser, the &ldquo;Redo&rdquo; and &ldquo;Undo&rdquo; options, plus sound "
"that plays while painting and a special tool called &ldquo;Magic&rdquo; for "
"impressive effects: rainbow, glitter, chalk, blur, flip, and more."
msgstr ""
"该程序囊括了所有最常用的画图工具，比如线条、画笔、色彩、几何图形、缩放、橡皮"
"擦、&ldquo;重做（Redo）&rdquo;和 &ldquo;撤销（Undo）&rdquo;操作，以及画画时的"
"声音和一个特殊的&ldquo;魔术（Magic）&rdquo;工具，它可以制造引人注目的效果：彩"
"虹、闪光、粉笔效果、模糊效果、翻转等等。"

#.  GNUN: localize URL /education/misc/tuxpaint-start-sm.png, /education/misc/tuxpaint-start.png 
#. type: Content of: <div><div><div><a>
msgid "<a href=\"/education/misc/tuxpaint-start.png\">"
msgstr "<a href=\"/education/misc/tuxpaint-start.png\">"

#. type: Attribute 'alt' of: <div><div><div><a><img>
msgid "Screenshot of the Tux Paint interface."
msgstr "Tux Paint界面的截屏"

#. type: Content of: <div><div><p>
msgid ""
"Along with the Magic tool, the other popular feature among children is "
"&ldquo;Stamp,&rdquo; which contains loads of pictures and clip art that can "
"be &ldquo;stamped&rdquo; on the canvas, such as plants and flowers, animals, "
"holiday art, planets, and much more. Many of these stamps are provided with "
"the program out of the box, and others are available as separate collections "
"to be installed. Many users contribute their own art work to be included as "
"stamps in the program; here we are going to see an example of how this was "
"done by a group of children from a school in India, thus putting into "
"practice the software freedom that the program guarantees."
msgstr ""
"除了魔术工具，还有一个受孩子们喜爱的功能叫做&ldquo;印章（Stamp）&rdquo;，它包"
"含许多图片和剪贴画，能够盖在画布上，比如花草、动物、节日艺术、星球等等，举不"
"胜举。许多印章和程序一起发布，还有一些可以作为单独的软件包安装。很多用户为该"
"程序贡献了剪贴画作为印章；我们下面会看一个由印度学校的小朋友制作的例子，它展"
"示了自由软件提供的自由实践。"

#. type: Content of: <div><div><p>
msgid ""
"Tux Paint is available in more than 80 languages, including minority and "
"right-to-left languages. Such a large number of languages is the result of "
"contributions made by users from all over the world."
msgstr ""
"Tux Paint支持80多种语言，包括小语种和从右向左的语言。这么丰富的语言支持得益于"
"来自世界各地的用户贡献。"

#. type: Content of: <div><div><p>
msgid ""
"To learn more: <a href=\"http://directory.fsf.org/project/TuxPaint/\"> FSF "
"Directory</a>, <a href=\"http://tuxpaint.org\"> Tux Paint Official Website</"
"a>"
msgstr ""
"了解更多：<a href=\"http://directory.fsf.org/project/TuxPaint/\">FSF目录</"
"a>、<a href=\"http://tuxpaint.org\">Tux Paint官方网站</a>"

#. type: Content of: <div><div><h3>
msgid "Who's Using It and How"
msgstr "谁在使用它以及用来做什么"

#. type: Content of: <div><div><p>
msgid ""
"Comments by <a href=\"http://tuxpaint.org/comments/\">home users</a> and "
"stories from <a href=\"http://tuxpaint.org/schools/\">schools</a> using Tux "
"Paint are presented in the official website. There are reports that the "
"program is one of the most helpful tools for children to get acquainted with "
"basic computer graphic skills while providing a highly attractive "
"environment for them. However, the distinctive feature that makes Tux Paint "
"preferable to similar drawing software for kids is the fact that it is Free "
"Software, meaning it comes with no restrictions of any sort and the user is "
"granted a series of freedoms. For example, one of the freedoms is that the "
"user is allowed to install the program in as many work stations as needed, "
"which is specially important for schools."
msgstr ""
"来自Tux Paint<a href=\"http://tuxpaint.org/comments/\">家庭用户</a>的评论和"
"<a href=\"http://tuxpaint.org/schools/\">学校用户</a>的故事都呈现在Tux Paint"
"的官方网站。还有报道说该程序是让孩子们了解计算机图形技术基础的最有帮助的工具"
"之一，因为它有非常吸引孩子的使用环境。然而，和其他儿童画图软件比起来，使Tux "
"Paint更出色的因素是它是一款自由软件，这意味着它没有任何限制，它的用户拥有一系"
"列的自由。例如，其中一个自由是用户可以按需要安装，多少台都行，这对学校尤为重"
"要。"

#. type: Content of: <div><div><div><a>
msgid "<a href=\"/education/misc/tuxpaint-stamps.jpg\">"
msgstr "<a href=\"/education/misc/tuxpaint-stamps.jpg\">"

#. type: Attribute 'alt' of: <div><div><div><a><img>
msgid "Screenshot of the Tux Paint interface in Malayalam with native flowers."
msgstr "Tux Paint的界面截图，这是带有地方花卉的马拉雅拉姆语（Malayalam）。"

#. type: Content of: <div><div><p>
msgid ""
"Another significant freedom that Free Software guarantees is the freedom to "
"modify the program so as to adapt it to the user's needs and to redistribute "
"copies of the modified version. It is thanks to this freedom that Tux Paint "
"is available in so many languages, including those spoken by minority "
"groups. In fact, translations into less widely spoken languages have been "
"provided by the users themselves. This is so because most of the time, "
"companies whose business consists of development of nonfree software adopt "
"policies on the basis of market size: if the market is not large enough to "
"ensure profit, they are generally reluctant to invest on it."
msgstr ""
"另一个显著的自由是自由软件保证了用户按需要修改程序的自由、重新发布修改后版本"
"的自由。正是这项自由使得Tux Paint可以支持如此众多的语言，包括超小语种。事实"
"上，那些小语种的翻译都是由用户自己提供的。大多数情况下，开发非自由软件公司的"
"政策是根据市场大小来制定的：如果市场的大小不能确保盈利，那么他们通常是不愿意"
"投资开发的。"

#. type: Content of: <div><div><div><div><a>
msgid "<a href=\"/education/misc/anthoorium.jpg\">"
msgstr "<a href=\"/education/misc/anthoorium.jpg\">"

#. type: Attribute 'alt' of: <div><div><div><div><a><img>
msgid "Image of the Anthoorium flower."
msgstr "火鹤花（Anthoorium）的图片。"

#. type: Content of: <div><div><div><div><p>
msgid ""
"<a href=\"/education/misc/anthoorium.ogg\">Listen</a> to a student pronounce "
"the name of the Anthoorium flower in Malayalam."
msgstr ""
"<a href=\"/education/misc/anthoorium.ogg\">请听</a>一个学生用马拉雅拉姆语朗读"
"火鹤花（Anthoorium）的名字。"

#. type: Content of: <div><div><div><div><a>
msgid "<a href=\"/education/misc/appooppanthaady.jpg\">"
msgstr "<a href=\"/education/misc/appooppanthaady.jpg\">"

#. type: Attribute 'alt' of: <div><div><div><div><a><img>
msgid "Image of the Appooppan Thaady flower."
msgstr "Appooppan Thaady花的图片。"

#. type: Content of: <div><div><div><div><p>
msgid ""
"<a href=\"/education/misc/appooppanthady.ogg\">Listen</a> to a student "
"pronounce the name of the Appooppan Thaady flower in Malayalam."
msgstr ""
"<a href=\"/education/misc/appooppanthady.ogg\">请听</a>一个学生用马拉雅拉姆语"
"朗读Appooppan Thaady花的名字。"

#. type: Content of: <div><div><p>
msgid ""
"A good example of how software freedom can be applied in Tux Paint is the "
"work done by 11 and 12 years old students from the <a href=\"/education/edu-"
"cases-india-irimpanam.html\">VHSS Irimpanam</a> school in the State of "
"Kerala, in India. The work consisted in adding a series of stamps to the "
"program, from photographs taken by the students themselves. They took "
"pictures of autochthonous flowers and processed the digital images with the "
"free libre GNU Image Manipulation Program <a href= \"http://directory.fsf."
"org/project/gimp/\">GIMP</a>, adding also the name of each flower in English "
"and in Malayalam, the local language.  As Tux Paint has a sound function, "
"students also recorded with their own voices the name of the flowers in "
"Malayalam, so when one of these flowers is chosen to be stamped onto the "
"canvas, the user will see and hear the name of the flower in Malayalam.  <a "
"href=\"http://audio-video.gnu.org/video/irimpanam-high-sub.en.ogv\"> Watch "
"and download the video</a> and <a href=\"/education/misc/irimpanam.en.srt"
"\">the SubRip subtitles</a>."
msgstr ""
"作为自由软件的理念应用到Tux Paint上的好例子，我们看看印度Kerala州的<a href="
"\"/education/edu-cases-india-irimpanam.html\">VHSS Irimpanam</a>学校的一群11"
"到12岁的学生做的工作。他们的工作包括为Tux Paint程序添加一系列的印章，而印章的"
"图案是学生们自己拍的照片。他们为土生土长的花卉拍照并使用自由软件<a href= "
"\"http://directory.fsf.org/project/gimp/\">GIMP</a>（GNU图片处理程序）处理这"
"些数字照片，还为每朵花配上英语和马拉雅拉姆语（当地语言）的名字。因为Tux Paint"
"有配音的功能，学生们还用自己的声音录制了马拉雅拉姆语的花名，这样当用户在画布"
"上盖上这些花的印章时，她们就可以看到和听到马拉雅拉姆语的花名。<a href="
"\"http://audio-video.gnu.org/video/irimpanam-high-sub.en.ogv\">请观看和下载视"
"频</a>和<a href=\"/education/misc/irimpanam.en.srt\">SubRip字幕</a>。"

#. type: Content of: <div><div><p>
msgid ""
"An additional useful activity done by this school by applying the freedom to "
"modify the program, was the translation of the Tux Paint interface into "
"Malayalam, the language spoken in the state of Kerala."
msgstr ""
"另外，该学校还把Tux Paint的界面翻译为马拉雅拉姆语，Kerala州的当地语言。这是一"
"项有益的活动，它是学校在实践修改软件的自由。"

#. type: Content of: <div><div><h3>
msgid "Why"
msgstr "为什么"

#. type: Content of: <div><div><p>
msgid ""
"The freedom to modify the program is an important resource that was used by "
"the school to reach beyond the scope of teaching basic computer graphical "
"skills or entertaining children. It was used to show them that information "
"technology is not something to be subjected to, not something that should be "
"imposed upon the user, but an instrument to serve users according to their "
"requirements."
msgstr ""
"修改软件的自由是学校可以利用的一项重要资源，学校能够用它达到超越教授计算机图"
"形基础技术或使学生快乐的目标。它可以用来向学生们展示我们不能受制于信息技术，"
"信息技术也不应强加于用户，它只是服务于用户需求的工具。"

#. type: Content of: <div><div><p>
msgid ""
"The freedom to install the program in all computers in the Lab was also "
"important, since the school has limited economical resources to invest in "
"licenses. By distributing copies of the original and modified version of the "
"program to the students, the school provided assistance to families "
"undergoing economic hardship."
msgstr ""
"为学校实验室的全部计算机安装该程序也是一项重要的自由，因为学校投资许可证的经"
"济资源有限。通过向学生们发放拷贝和修改后的拷贝，学校也帮助了经济上有困难的家"
"庭。"

#. type: Content of: <div><div><h3>
msgid "Results"
msgstr "结果"

#. type: Content of: <div><div><p>
msgid ""
"Adding stamps to Tux Paint was an exciting and enriching experience for the "
"students. First, they analyzed how the program works and discovered the "
"mechanism that the program uses for the implementation of stamps. Then they "
"learned how this particular characteristic of the program permitted the "
"addition of stamps and how to do it. They also acquired a deeper knowledge "
"of GIMP during the manipulation of the images. The whole process gave them "
"an opportunity not only to develop new technical abilities, but also to "
"identify and appreciate local flora. Most importantly, they learned that "
"anyone, even non programmers or children, can actually influence and improve "
"information technology when software freedom is granted."
msgstr ""
"为Tux Paint添加印章是一个令学生们兴奋和使学生们得到发展的经历。首先，他们分析"
"了程序如何工作，发现了程序实现印章的机制。然后，他们学习了该机制如何允许添加"
"印章和怎么添加。他们也通过修改图片更深入地学习了GIMP的知识。整个过程不但是发"
"展新技术能力的机会，也是辨认和欣赏本地花卉的机会。最重要的是，他们学到了一旦"
"有了软件自由，任何人，即使不是程序员，甚至是孩子，实际上也能够影响和改善信息"
"技术。"

#. type: Content of: <div><div><div><h3>
msgid "Credits"
msgstr "鸣谢"

#. type: Content of: <div><div><div><p>
msgid ""
"Images of the Tux Paint interface in Malayalam with native flowers, of the "
"Appooppan Thaady and Anthoorium flowers, as well as both sound files are "
"courtesy of the Vocational Higher Secondary School Irimpanam and are "
"licensed under CC-BY-SA, <a rel=\"license\" href=\"http://creativecommons."
"org/licenses/by-sa/3.0/\"> Creative Commons Attribution-ShareAlike 3.0 "
"Unported</a>."
msgstr ""
"向提供带有本地花卉（Appooppan Thaady和Anthoorium）的Tux Paint马拉雅拉姆语界面"
"图片及其声音文件的Irimpanam高级职业中学致谢，图片和声音文件都使用CC-BY-SA、"
"<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/3.0/\"> "
"Creative Commons Attribution-ShareAlike 3.0 Unported</a>授权。"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"请将有关自由软件基金会(FSF) &amp; GNU 的一般性问题发送到<a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>。也可以通过 <a href=\"/contact/\">其他"
"联系方法</a> 联系自由软件基金会(FSF)。有关失效链接或其他错误和建议，请发送邮"
"件到<a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>。"

# TODO: submitting -> contributing.
#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"若您想翻译本文，请参看<a href=\"/server/standards/README.translations.html\">"
"翻译须知</a>获取有关协调和提交翻译的相关事项。"

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2011, 2012, 2014, 2016, 2020, 2021 Free Software "
"Foundation, Inc."
msgstr ""
"Copyright &copy; 2011, 2012, 2014, 2016, 2020, 2021 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"本页面使用<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-"
"nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 International "
"License</a>授权。"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<b>翻译团队</b>：<a rel=\"team\" href=\"https://savannah.gnu.org/projects/"
"www-zh-cn/\">&lt;CTT&gt;</a>，2017。"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "最后更新："

#~ msgid ""
#~ "Copyright &copy; 2011, 2012, 2014, 2016, 2020 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2011, 2012, 2014, 2016, 2020 Free Software Foundation, "
#~ "Inc."

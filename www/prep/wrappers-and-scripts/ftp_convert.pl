#!/usr/local/bin/perl -w
# $Id: ftp_convert.pl,v 1.23 2024/02/13 19:42:41 th_g Exp $
#
# Converts the FTP file to a html file - just prints on the standard out
# and it is the responsibility of who ever running this to redirect to a
# flat_file. Also standard GNU HTML header and trailer needs to be added to
# this page. See the Makefile.
#
# Public domain.  Original script written by joelh@gnu.org.
#
# 01/02/1999 - Murali - Added support for http sites on the ftp list
#   Also fixed the bug with <tt></tt>
# 26/07/2010 - bjg - add rel="nofollow" to mirror links
# 01/02/2024 - th_g - simplify (disable support for <p>); add support for
#   several lists.
# 06/02/2024 - ineiev - don't convert http(s) URLs marked with '[m0, m1...]'.
# 10/02/2024 - th_g - restore conversion of text to <p>.

use strict;

my $last_place = "";
my $place = "";
my $marked_place = "";
my $ftpsite = "";
my $warn = "";
my $ret = "";
my @ftp_sites = ();

sub display_link {
  my ($url, $display) = @_;
  $display = $url if !defined($display);
  # external links are nofollow by default to deter abuse
  $ret = qq(<a rel="nofollow" href="$url">$display</a>);
  return $ret;
}
sub header {
  my ($headlvl, $title) = @_;
  (my $anchor_id = $title) =~ tr/ A-Z/_a-z/;
  $ret = qq(<h$headlvl id=\"$anchor_id\">$title</h$headlvl>);
  return $ret;
}

$_ = <>;
die "Incorrect format, start with How to get." unless /^How to get/;

sub get_place_markers
{
  my $markers = '';
  my @ret;
  $markers = $1 if $_[0] =~ m/.*\[(.*)\]$/;
  $markers =~ s/\s*//g;
  foreach my $mark (split /,/, $markers) {
    push @ret,
      "<a href='#$mark'><span class='warn-$mark'>[" . uc ($mark)
        . "]</span></a>";
  }
  return '' if $#ret < 0;
  return join (' ', @ret) . ' ';
}

sub print_site
{
  my ($site, $place) = @_;
  $site = get_place_markers ($place) . $site if $site =~ /^https?:/;
  $site = display_link ($site) if $site =~ /^(https?|ftp):/;
  print $site;
}

while (<>) {
  chomp;

## Headers
  if (/^\*\s*(\S.*\S)\s*$/) {
    print "\n\n". header(3, $1) ."\n\n";

  } elsif (/^\s*(\S.*\S):\s*$/) {
    print "\n". header(4, $1) ."\n\n";
    $place = "";

## Lists
  # Country and URLs are separated by ' - ' or ' -- '.
  # Lines with ' -- ' will be skipped by Mirmon.
  } elsif (/^(.*\S) --? (\S+.*)$/) {
    # State (US) or country (everything else)
    $marked_place = $1;
    @ftp_sites = split /,/,$2;
    if ((!$place) and (!$last_place)) {
      print "<ul>\n";
    }
    ($place = $marked_place) =~ s/ \[.*\]$//;
    if ((!$last_place) or ($place ne $last_place)) {
      if ($last_place) {
        if ($place ne $last_place) {
          print " </ul></li><!-- end of $last_place -->\n";
        }
      }
      $last_place = $place;
      print " <li>$place\n <ul>\n";
    }
    $place = "";

    foreach $ftpsite (@ftp_sites) {
      print "  <li>" ;
      $ftpsite =~ s,^.*\s((https?|ftp|rsync):\S+),$1,;
      $ftpsite =~ s,((https?|ftp|rsync):\S+)\s.*$,$1,;
      $ftpsite =~ s,\)$,,;
      # Label alpha mirror sites.
      if ($ftpsite =~ /(alpha|ALPHA)/) {
        print "(alpha)\n      ";
      }
      # Mark problematic sites.
      print_site ($ftpsite, $marked_place);
      print "</li>\n";
    }

## Paragraphs (= rest of the text)
  } elsif (/\S+/) {
    if (!($warn) and !($last_place)) {
      if (/<b>Warning<\/b>:/) {
        print "<p class='highlight'>\n";
      } else {
        print "<p>\n";
      }
      print;
      $warn = 1;
    } else {
      die "Validation error: text outside <li>.\n"
      unless !($last_place);
      print "\n$_";
    }
  }

## Closing tags
  if ((/^\s*$/) or (eof)) {
    if (!($place) and ($last_place)) {
      print " </ul></li><!-- end of $last_place -->\n";
      print "</ul>\n";
      $last_place = "";
    }
    if ((!$last_place) and ($warn)) {
      print "\n</p>\n";
      $warn = "";
    }
  }
}

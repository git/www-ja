# $Id: gm-check.pl,v 1.2 2013/02/25 17:41:42 karl Exp $
# The check actions for the gm script (see --help message).
# 
# Copyright 2007, 2008, 2009, 2010, 2012, 2013
# Free Software Foundation Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Originally written by Karl Berry.



# Return list of packages in the activity report that are not in the
# maintainers file.  (Implementation not finished.)
# 
sub check_activityfile_ {
  my @ret = ();

  my %pkgs = &read_maintainers ("by-package");
  my %maints = &read_maintainers ("by-maintainer");
  
  my %activity = &read_activity ("by-package");
  
  for my $ap (sort by_lineno keys %activity) {
    next if $ap eq "*";  # our last_sendemail placeholder
    my ($email,$name,$time,$line) = split (/\|/, $activity{$ap});
    
    push (@ret, "$ACTIVITY_FILE:$line: active package $ap does not exist"
                . " ($email|$name|$time)")
      unless exists $pkgs{$ap}; #|| exists $missing_pkg_ok{$ap};
  }
  return @ret;

  
  sub by_lineno {
    my (undef,undef,undef,$aline) = split (/\|/, $activity{$a});
    my (undef,undef,undef,$bline) = split (/\|/, $activity{$b});
    $aline <=> $bline;
  }
}



# Return inconsistencies between copyright.list and the
# copyright-holder: field in gnupackages.  There should not be any.
# 
sub check_fsfcopyright_ {
  my @ret = ();

  my %pkgs = &read_gnupackages ();
  my @cl = &list_copyrightfsf_ (0, 1);
  my %cl;
  @cl{@cl} = (); # make hash from list
  
  for my $cl (keys %cl) {
    if (! exists $pkgs{$cl}) {
      # should be caught by daily checks.
      push (@ret, "$0: FSF-copyrighted $cl missing from $GNUPACKAGES_FILE");
      next;
    }
    my $p = $pkgs{$cl};
    #&warn_hash ($cl, $p);
    if ($p->{"copyright-holder"} !~ /^(fsf|see-)/) { # allow redirects too
      push (@ret, &gnupkgs_msg
                   ("copyright-holder: not fsf, but in copyright.list", %$p));
    }
    delete $pkgs{$cl};
  }
  
  for my $pkgname (keys %pkgs) {
    my $p = $pkgs{$pkgname};
    if ($p->{"copyright-holder"} =~ /^fsf/) {
      push (@ret, &gnupkgs_msg
                    ("copyright-holder: fsf, but not in copyright.list", %$p));
    }
  }

  return @ret;
}



# Return list of entries in the ftp listing that are not in the official
# list of packages.  (Implementation not finished.)
# 
sub check_ftplisting_ {
  my @ret = ();
  
  my %pkgs = &read_gnupackages ();
  my @ftp = &read_ftplisting ();
  
  # known-unusual entries or aliases under ftp.gnu.org:gnu/.
  my @special = qw(GNUinfo GNUsBulletins Licenses MailingListArchives
                   MicrosPorts aspell- commonc\+\+ dotgnu flex git glibc
                   non-gnu phantom queue savannah speedx windows);
  
  for my $f (sort @ftp) {
    next if exists $pkgs{$f};
    next if grep { $f =~ /^$_/ } @special;
    # read oldpackages?  next if grep { $f =~ /^$_/ } @old;
    push (@ret, $f);
  }
  
  return @ret;
}



# Return list of email addresses or packages in the ftp-upload setup
# that are not in maintainers.
# 
sub check_ftpupload_ {
  my @ret = ();
  
  my %maint_file = &read_maintainers ("by-package");
  my %ftpupload_file = &read_ftpupload_email ();
  
  for my $p (sort keys %ftpupload_file) {
    # The ftp-upload package itself better be in the maintainers file.
    # (Many packages do not release through ftp.gnu.org, unfortunately,
    #  so there is no use in worrying about the reverse.)
    if (! exists $maint_file{$p}) {
      push (@ret, "$FTPUPLOAD_EMAIL_FILE: not in maintainers: $p");
      next;
    }

    # see if we can find each ftp-upload email in maintainers.
   FTP_EMAIL:
    for my $ftp_email (split (" ", $ftpupload_file{$p})) {
      
      # for each maintainer of this package ...
      my @maint_refs = @{$maint_file{$p}};
      for my $m (@maint_refs) {

        # for each of the keys that have email values ...
        for my $mkey ("email", "privateemail", "altemail", "bademail") {
          my @mval = split (/\|/, $m->{$mkey}); # split multiple vals

          # for each value of this key ...
          for my $mval (@mval) {
            if ($mval eq $ftp_email) {
              if ($mkey eq "bademail") {
                warn "$p: ftp email $ftp_email known bad!\n\n";
              } else {
                warn "$p: good, found $mval in $mkey\n";
              }
              next FTP_EMAIL;
            }
          }  # each of multiple email values
        }    # each possible email key
      }      # each maintainer
      warn "$p: did not find $ftp_email\n";
      warn "\n";
    }        # each ftp-upload email for this package
  }          # each ftp-upload package
  
  return @ret;
}


# Return list of packages in the gnupackages file that are not in the
# maintainers file, and vice versa.  Run hourly from karl cron on fp.
# 
sub check_maintainers_ {
  my @ret = ();
  
  my %maint_file = &read_maintainers ("by-package");
  my %pkg_file = &read_gnupackages ();
  
  for my $m (keys %maint_file) {
    if (exists $pkg_file{$m}) {
      delete $maint_file{$m};
      delete $pkg_file{$m};
    }
  }
  
  for my $p (sort keys %pkg_file) {
    push (@ret, "$GNUPACKAGES_FILE:$pkg_file{$p}->{lineno}: "
                . "$p not in maintainers");
  }
  
  for my $p (sort keys %maint_file) {
    next if $p =~ /\.nongnu/;
    push (@ret, "$MAINTAINERS_FILE:$maint_file{$p}[0]->{lineno}: "
                . "$p not in gnupackages");
  }
  
  return @ret;
}



# Return list of packages in the savannah GNU groups.tsv list that are
# not in the gnupackages or oldpackages files.  We check against these
# rather than maintainers because some are legitimately only on sv and
# we want to have info for them.
# 
# On the other hand, there's no expectation that everything in
# gnupackages (or maintainers) is on savannah, so don't check that
# direction.
# 
# The sv file is at http://savannah.gnu.org/cooperation/groups.tsv
# and is updated via cron on savannah.
# 
# (Implementation not finished.)
#
sub check_savannah_ {
  my @ret = ();
  
  my %pkg_file = &read_gnupackages ();
  my %sv_file = &read_savannah ();
  
  for my $m (keys %sv_file) {
    if (exists $pkg_file{$m}) {
      delete $sv_file{$m};
    }
  }
  
  for my $p (sort keys %sv_file) {
    push (@ret, "$SAVANNAH_FILE:$sv_file{$p}->{lineno}: "
                . "$p ($sv_file{$p}->{name}) not in gnupackages");
  }
  
  return @ret;
}


1;

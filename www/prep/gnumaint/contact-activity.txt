# $Id: contact-activity.txt,v 1.52 2015/05/15 18:02:02 brandon Exp $
# Public domain.
# 
# This file records information about contact activity with package
# maintainers.  There is nothing private in this file. 
#
# Canonical order:
# package
# reply-from
# query-to
# next-release
# next-contact
# note
#
# The package name should always be all-lowercase (and match
# gnupackages.txt and maintainers).
#
# Finally, this file as a whole is more or less sorted by package
# identifier.  However, software reading this file should accept any
# order of packages or fields.

package: 3dldf
reply-from: 04jun13/laurence, 23apr13, 3apr13, 28mar13
query-to: 29mar13/brandon, 28mar13
next-release: 04jun13/later this summer?, 23apr13/in preparation

package: a2ps
reply-from: 30sep12, 19feb12 replied to list, 20aug11
query-to: 28jun14/karl, 5may14/karl, 18sep12/karl, 11jun12, 5may12

package: acct
query-to: 4dec12/karl
next-release: 4dec12/no eta

package: acm
reply-from: 23mar15/sergio, 16nov10
query-to: 22mar15/brandon
next-release: 23mar15/stepping down, 16nov10/soon

package: adns
reply-from: 13may14/ian, 04jun13, 19feb13, 18feb13, 10mar11
query-to: 12may13/brandon, 28may13, 7feb13/karl, 19jan12/rms
next-release: 13may14/next 6 months, 04jun13/not certain, 19feb13/probably not soon maybe just gplv3, 10mar11/in a month

package: anubis
reply-from: 15may14/sergey
query-to: 12may14/brandon
next-release: 15may14/two weeks

package: avl
query-to:  12may14/brandon

package: ballandpaddle
reply-from: 04jun13, 11feb13
query-to: 12may14/brandon
next-release: 04jun13/fall 2013 or spring 2014, 11feb13/soon

package: barcode
reply-from: 12jun12, 22nov10 newmaint, 26jul09
query-to: 11jun12/karl, 15nov10

package: bc
reply-from: 13may14/phil, 7may12/ken, 6may12/phil, 22may08/pizzini
query-to: 12may14/brandon, 28may13
next-release: 13may14/fall 2014

package: bool
reply-from: 28mar15/marc
query-to: 23mar15/brandon, 12may14, 28may13, 5sep08/karl, 17dec09
next-release: 28mar15/this year

package: bpel2owfn
reply-from: 15may15/Niels, 9apr10
query-to: 14may15/brandon, 22mar15
next-release: 15may15/never (stable)

package: cfengine
reply-from: feb10/gscrivano

package: cim
query-to: 12may14/brandon, 28may13, 22dec09/karl

package: clisp
reply-from: 23mar15/sam, 3jan11
query-to: 22mar15/brandon, 4oct12/karl/rms
next-release: 23mar15/never

package: cobol
reply-from: 26oct08
note: dedicated maintainer, but will not be able to resume full-time work
note: for 4-5 years (after 6 months full-time work).  But it's a Cobol
note: compiler, hardly likely to find other implementors, and lots and lots
note: of work has been done.  Mail from tej, 26 Oct 2008 12:42:10 +1100.

package: combine
reply-from: 14oct08
query-to: 28may13/brandon, 18jun10/karl, 7nov10
next-release: 14oct08/end of month

package: commoncpp
query-to: 8jun10/karl

package: dap
query-to: 19feb12/jmd, 22jan12

package: ddd
query-to: 17dec12/karl, 4may11
reply-from: 29oct14
next-release: 29oct14+soonh

package: dejagnu
reply-from: 10mar11

package: denemo
reply-from: 12may10

package: dia
reply-from: 9jun08

package: dionysus
reply-from: 23mar15/jean michel
query-to: 22mar15/brandon
next-release: 23mar15/never

package: dismal
reply-from: 29may13, 25jan11
query-to: 28may13/brandon, 17nov10/karl, 29oct08
next-release: 29may13/never, 25jan11/soon

package: dmd
reply-from: 30oct08
query-to: 28may13/brandon, 20jun10/karl

package: dominion
query-to: 12may14/brandon, 28may13, 8jan10/karl

package: emacs-muse
query-to: 12may14/brandon

package: enscript
reply-from: 19sep12

package: eprints
reply-from: 23nov09

package: epsilon
reply-from: 14apr15/luca, 12dec12
query-to: 23mar15/brandon, 11may14
next-release: 23mar15/1.0 possible soon, 12dec12/alpha soon

package: findutils
reply-from: 24mar15/james, 1may12
query-to: 23mar15/brandon, 12may14
next-release: 24mar15/two months, 1may12/four blocking bugs

package: flex manual
query-to: 9nov08/karl
note: 9nov08 source?

package: fontutils
reply-from: 12feb13
query-to: 12may14/brandon
next-release: 12feb13/not soon

package: freeipmi
reply-from: 14jan10
next-release: 14jan10/next major release

package: freetalk
query-to: 12may14/brandon

package: fribidi
query-to: 12may14/brandon

package: garpd
reply-from: 21nov10
next-release: 21nov10/soon

package: gcl
reply-from: 07jun13, 8nov10
query-to: 28may13/brandon
next-release: 07jun13/next week

package: gdbm
query-to: 10mar11/karl

package: gengen
reply-from: 23mar15/raman
query-to: 22mar15/brandon
next-release: 23mar15/2-3 weeks

package: ggradebook
reply-from: 22feb13/anto
query-to: 21apr13/brandon, 18feb13

package: gift
reply-from: 18feb13
query-to: 18feb13/brandon

package: glue
reply-from: 14may14/markus, 18jan10
query-to: 12may14/brandon, 4apr13/karl
next-release: 14may14/years from now, 18jan10/2011 release

package: gmediaserver
reply-from: 14apr15/dmitry, 14may14, 20nov10
query-to:  23mar15/brandon, 12may14, 16may13
next-release: 14apr15/suggest retirement, 14may14/this summer, 20nov10/this year

package: gmorph
reply-from: 24jan12
query-to: 25apr13/rms/karl, 16feb13/brandon, 4jan11/karl
next-release: 14jan12/replied to jmd but doing nothing

package: gnats
reply-from: 6apr13, 1jan13
next-release: ?
next-contact: 6nov13

package: gnatsweb
reply-from: 25jul11
query-to: 22jan12/jmd
next-release: 25jul11/little work done

package: gnowsys
reply-from: 24mar15/nagarjuna, 13may14, 28may13, 18mar10
query-to: 22mar15/brandon, 12may14, 28may13
next-release: 24mar15/april 2015, 13may14/august 2014, 28may13/august 2013

package: gnu-arch
reply-from: 28may13/andy, 10oct11, 2dec08
query-to: 28may13/brandon, 1jul12/karl
next-release: 28may13/probably none should retire

package: gnubg
query-to: 8may13/brandon, 25apr13/rms/karl, 20mar13/brandon

package: gnucap
query-to: 12may14/brandon, 28may13, 15aug08/karl
note: 15aug08/asked about gplv3

package: gnucash
query-to: 24jan10/karl

package: gnufm
reply-from: 26mar13/matt
query-to: 12may14/brandon, 26mar13
next-release: 26mar13/almost ready

package: gnugo
query-to: 12may14/brandon

package: gnuit
query-to: 12may14/brandon

package: gnujdoc
reply-from: 15dec08
query-to: 26jan10/karl

package: gnukart
reply-from: 04jun13, 21apr10
query-to: 28may13/brandon
next-release: 04jun13/never

package: gnumed
reply-from: 19jan10
query-to: 18nov10/karl

package: gnump3d
query-to: 11feb10/karl

package: gnupod
reply-from: 13may14/adrian
query-to: 12may14/brandon
next-release: 13may14/none in sight
note: the software is moribund due to being tied to moribund hardware

package: gnurobots
query-to: 12may14/brandon, 13feb11/karl

package: gnuschool
reply-from: 15jun13, 29may13, 7apr13, 1apr13
query-to: 28may13/brandon, 26mar13/karl, 28mar12/karl
next-release: 15jun13/emailed-to-karl-told-him-to-try-uploading
next-contact: 1jul13

package: gnushogi
reply-from: 13mar12

package: gnuskies
reply-from: 8apr13/rahul
query-to: 20mar13/brandon, 24jun09/karl
next-release: never

package: gnusound
query-to: 12may14/brandon, 17sep11/karl, 30jan10
note: 17sep11/#709362

package: gnuspeech
reply-from: 1apr15/david, 26mar13
query-to: 23mar15/brandon, 12may14, 26mar13
next-release: 1apr15/mid may?, 26mar13/unknown but possible

package: goptical
reply-from: 29nov10
next-release: 29nov10/before christmas

package: gpaint
reply-from: 30may14/andy, 28may13, 10oct11, 12dec08
query-to: 12may14/brandon, 28may13
next-release: 31may14/two months, 28may13/one month

package: gperf
query-to: 28jun14/karl, 5may14/karl (and many in the past)
also: libiconv, libsigsegv, libunistring

package: gprolog
query-to: 1feb10/karl

package: grabcomics
reply-from: 18nov10

package: greg
query-to: 12may14/brandon, 28may13, 14dec08/karl

package: gretl
query-to: 21nov10/karl

package: gsegrafix
query-to: 13feb11/karl

package: gsl
query-to: 25apr13/rms/karl, 19feb13/brandon

package: gtick
query-to: 12may14/brandon

package: gtypist
reply-from: 5dec10
note: 5dec10/volunteer

package: guile-rpc
reply-from: 13may14/ludovic, 25mar13, 17dec08
query-to: 12may14/brandon, 24mar13
next-release: 13may14/next month, 25mar13/ready now, 17dec08/official eta unknown

package: gurgle
reply-from: 23mar15/tim
query-to: 22mar15/brandon
next-release: 23mar15/1 year but it's effectively stable

package: halifax
reply-from: 6jul12

package: hp2xx
reply-from: 13may14/martin, 11feb10
query-to: 12may14/brandon, 11may13, 13nov12
next-release: 13may14/maybe later this year, unlikely because it's stable

package: httptunnel
reply-from: 28apr09
query-to: 12may14/brandon, 28may13, 9feb10/karl

package: intlfonts
query-to: 12may14/brandon, 28may13, 27jan09/karl

package: jdresolve
query-to: 25apr13/rms/karl, 24mar13/brandon, 24nov10/karl, 9feb10

package: jel
reply-from: 24jun09

package: jwhois
reply-from: 12dec12, 20jun09
next-release: 20jun09/maybe soon

package: kawa
reply-from: 8jun09

package: kopi
query-to: 12may14/brandon, 28may13, 10mar12/benheni

package: leg
reply-from: 16feb13, 8nov10
query-to: 16feb13/brandon
next-release: 16feb13/end of summer

package: liberty-eiffel
query-to: 11may14/brandon

package: libffcall
reply-from: 28may13, 27nov10
query-to: 28may13/brandon
next-release: 28may13/orphaned, 27nov10/unknown

package: libmatheval
reply-from: 18jun11
query-to: 18jun11/karl, 10nov10

package: libredwg
reply-from: 16feb10
query-to: 27mar13/karl

package: libxmi
query-to: 12may14/brandon, 28may13, 16feb10/karl

package: lightning
query-to: 28may13/brandon, 27sep12/karl, 31dec08

package: lrzsz
reply-from: 11apr09
query-to: 12may14/brandon, 28may13, 28nov10/karl
next-release: 11apr09/next 3 months

package: lsh
reply-from: 29may13/niels, 9mar13
query-to: 28may13/brandon
next-release: 29may13/within a few months, 9mar13/sometime

package: macchanger
reply-from: 18jan09

package: make
reply-from: 21jan09

package: maverik
reply-from: 21feb13
query-to: 18feb13/brandon

package: mc
query-to: 28may13/brandon, 19jun12/karl, 20jan09, 12nov08

package: mcsim
reply-from: 29jan11, 29nov10

package: melting
query-to: 22feb10/karl

package: metaexchange
reply-from: 27mar13/laurence
query-to: 24mar13/brandon
next-release: 27mar13/indefinitely no

package: metahtml
reply-from: 19jan11, 23feb10
query-to: 27nov11/gerald

package: miscfiles
reply-from: 17nov10

package: nana
query-to: 3jul12/jmd, 10mar11/karl

package: nano
query-to: 11jun12/karl

package: oleo
reply-from: 21jan13, 5apr12
query-to: 21jan13/brandon, 1apr12/jmd, 19feb12, 22jan12

package: orgadoc
reply-from: 1mar10

package: panorama
query-to: 12may14/brandon, 28may13, 11may09/karl

package: pascal
reply-from: 23feb09

package: paxutils
query-to: 12may14/brandon, 3apr13/karl
reply-from: 9dec12, 4feb09, autumn09

package: pcb
query-to: 2jul12/karl, 2dec10, 4mar10, 24mar09

package: pdf
reply-from: 31mar13/jose
query-to: 12may14/brandon, 24mar13

package: pexec
query-to: 13may14/brandon

package: phantom_home
query-to: 13may14/brandon, 28may13, 1jun09/karl

package: phpgroupware
reply-from: 28apr12/hamet
query-to: 25apr13/rms/karl, 16feb13/brandon, 15apr12/karl, 8mar10

package: pies
reply-from: 24may14/sergey
query-to: 13may14/brandon
next-release: 24may14/within two months

package: pipo
query-to: 13may14/brandon, 28may13, 9mar10/karl

package: plotutils
reply-from: 14may14, 13jun11
query-to: 30apr14/karl
next-release: summer14

package: powerguru
next-release: 3dec10/never
reply-from: 3dec10
query-to: 26mar13/karl

package: proxyknife
query-to: 13may14/brandon

package: pspp
reply-from: 9dec12
next-release: 9dec12/early next year

package: pth
reply-from: 3dec11, 4jan11, 1jun09
query-to: 13may14/brandon, 28may13, 10mar11/karl, 12mar10
note: 10mar11 rms gave up, 4jan11 to rms

package: pythonwebkit
reply-from: 17feb13
query-to: 16feb13/brandon
note: 17feb13 no release yet (urged doing so)

package: quickthreads
reply-from: 23jul09
query-to: 12mar10/karl
note: 23jul09 awaiting code

package: radius
query-to: 13may14/brandon

package: rpge
query-to: 25apr13/rms/karl, 16feb13/brandon, 19feb12/jmd, 22jan12, 14feb11/karl

package: sather
reply-from: 14apr13
query-to: 19feb13/brandon, 14apr12/karl
next-release: unknown
next-contact: 14oct13

package: screen
reply-from: 27jan12
query-to: 28may13/brandon
next-release: 27jan12/early next week

package: serveez
reply-from: 29jan11, 25jun09
query-to: 26jan11/karl, 8jun10

package: shmm
reply-from: 25mar15/jeannie
query-to: 23mar15/brandon, 13may14
next-release: 25mar15/probably never (stable) maybe update for MacOS
support later this year

package: shtool
query-to: 13may14/brandon

package: smarteiffel
query-to: 28may13/brandon

package: social
reply-from: 13may14/brandon, 26mar13/matt
query-to: 13may14/brandon, 26mar13
next-release: 13may14/waiting on copyright assignment, 26mar13/almost ready

package: spacechart
query-to: 13may14/brandon, 28may13, 21mar10/karl, 14feb09

package: speex
reply-from: 13may14/jean-marc
query-to: 13may14/brandon
next-release: 13may14/new maintainer so hopefully sometime

package: sqltutor
reply-from: 13may14/ales, 9dec12
query-to: 13may14/brandon
next-release: 14may14/this summer, 9dec12/next year

package: stalkerfs
query-to: 13may14/brandon, 20feb09/karl

package: stump
reply-from: 13may14/igor, 16mar09
query-to: 13may14/brandon, 28may13, 9dec10/karl, 24mar10
next-release: 13may14/never

package: superopt
next-contact: jun14->ttn
reply-from: 16jan14/ttn, 18mar09
query-to: 28may13/brandon, 18jan11/karl, 25mar10

package: sysutils
query-to: 13may14/brandon, 28may13, 19jan11/karl, 26mar10

package: talkfilters
reply-from: 30may14/mark
query-to: 13may14/brandon
next-release: 30may14/never, stable

package: termcap
reply-from: 14may14/gary
query-to: 13may14/brandon, 28may13
next-release: 14may14/never

package: termutils
reply-from: 14may14/gary, 23feb09
query-to: 13may14/brandon, 28may13, 8nov10/karl, 23feb09
next-release: 14may14/never

package: thales
reply-from: 27feb13
query-to: 26feb13/brandon

package: time
reply-from: 23mar15/david, 16feb13/bug-gnu-utils, 14oct11, 13jan11
query-to: 22mar15/brandon, 13may14, 28may13
next-release: 23mar15/recent release via git tag, 16feb13/no eta, 13jan11/within a month

package: trueprint
reply-from: 15jan13
note: 15jan13 "try to work more"

package: userv
reply-from: 04jun13, 19feb13
query-to: 13may14/brandon, 28may13
next-release: 04jun13/some time this year, 19feb13/maybe soon

package: uucp
reply-from: 29may13, 19may09
query-to: 28may13/brandon, 9jan11/karl, 3apr10
next-release: 29may13/never

package: vcdimager
reply-from: 26feb09

package: vmslib
reply-from: 18feb13, 12jun12
query-to: 21mar13/brandon, 18feb13
next-contact: 18mar13

package: w3
reply-from: 6apr10
query-to: 16may13/brandon, 13may13, 25apr13/rms/karl, 27mar13/brandon, 10jan11/karl, 8nov10

package: webstump
reply-from: 22mar15/igor
query-to: 22mar15/brandon, 13may14, 28may13

package: which
reply-from: 15may14/carlo
query-to: 13may14/brandon
next-release: 15may14/none, stable

package: xhippo
reply-from: 13may14/adam
query-to: 13may14/brandon
next-release: 13may14/never

package: xlogmaster
reply-from: 23mar15/john
query-to: 22mar15/brandon, 13may14, 19feb12/jmd
note: 19feb12 asked about penguin
next-release: 23mar15/couple months

package: xmlat
reply-from: 27feb13
query-to: 26feb13/brandon

package: xaos
reply-from: 14may14/zoltan
query-to: 13may14/brandon

# End. (Do not remove this line or the blank line before it.  Thanks.)

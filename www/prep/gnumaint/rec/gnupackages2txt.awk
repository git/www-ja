# $Id: gnupackages2txt.awk,v 1.5 2019/04/13 10:11:05 ineiev Exp $
# gnupackages.rec to old gnupackages.txt
#
#  Copyright (C) 2018 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script converts the gnupackages.rec recfile into the old txt format
# that used to be committed directly to the repository.  The txt format
# will eventually be entirely removed; this is intended to allow that
# transition to proceed incrementally.

BEGIN {
    FS = OFS = ": "
}

{
    if ( ( substr( $1, 1, 1 ) == "+" ) && ( key != "" ) ) {
        # next line of a multiline record
        sub( /^[+] */, "", $0 )
        add_key( key, $0 )
        next
    }
    key   = is_key() ? $1 : ""
    value = $2
}

# recfile headers (replace the first one with a warning)
/^%rec:/ {
    print "# THIS FILE IS NOW GENERATED FROM rec/gnupackages.rec;"
    print "# DO NOT MODIFY DIRECTLY!"
}
/^%/ { next }

is_key_assign() {
    add_key( key, value )
}

# A number of keys are handled differently in the recfile
key == "package"         { package = value }
key == "blurb_id"        { next }
key == "activity_status" { next }
key == "last_activity"   { next }
key == "container"       { next }
key == "last_contact"    { next }  # output at end (reorder)
key == "next_contact"    { next }  # output at end (reorder)
key == "note"            { next }  # output at end (reorder)
key ~ /^last_release/    { next }

# End of package block
eob() {
    print "activity-status", mkactivity()
    printkey( "last_contact" )
    printkey( "next_contact" )
    printnotes()

    package = ""
    delete pkginfo
    delete pkginfon
}

{
    # recfiles use snake case and gnupackages.txt uses dashes
    gsub( /_/, "-", $1 )
    print
}


# Key abstractions and predicates
function is_key()        { return $1 ~ /^[a-z_]+$/ }
function is_key_assign() { return is_key() && value }
function eob()           { return $0 ~ /^$/ && package }
function is_symnote()    { return get_key( "note" ) ~ /^[a-z-]+$/ }


# Print a "key: value" line for each value of key K
#
# K may have multiple values if `add_key' was called multiple times for
# the same K.
function printkey( k,   n, i )
{
    n = keylen( k )

    for ( i = 0; i < n; i++ ) {
        print gensub( /_/, "-", "g", k ), get_key( k, i )
    }
}

# Print note unless it is a symbolic note
#
# Symbolic notes have special meaning and are concatenated with the
# activity status in the output.  Note that `is_symnote` only checks
# the first key value, and so this may not work correctly given multiple
# notes; it just hasn't been a problem yet, as of this time of writing.
function printnotes()
{
    if ( is_symnote() ) {
        return
    }

    printkey( "note" )
}


# Generate activity status line
function mkactivity()
{
    return mkstatus() mkrelease()
}


# Generate status portion of activity status
#
# The status may be suffixed by a date and a symbolic note.
function mkstatus(    status, last_act )
{
    status   = get_key( "activity_status" )
    last_act = get_key( "last_activity" )

    switch ( status )
    {
        case "newmaint":
        case "newcomaint":
        case "newpkg":
        case "nomaint":
            status = status "/" last_act
            break;

        case "container":
            status = status " " get_key( "container" )
            break;
    }

    # notes that look like symbols were taken from the
    # old activity-status, so re-add them
    if ( is_symnote() ) {
        status = status " " get_key( "note" )
    }

    return status;
}


# Generate release portion of activity status
#
# If there are multiple release versions, then all but the first will
# have a date suffixed with the form "%s/%d".
function mkrelease(    last_date, last_ver, release, i )
{
    i = keylen( "last_release" ) - 1

    if ( i == -1 ) {
        return ""
    }

    # begin string with latest version number (last index)
    release = sprintf( " %d (%s",
                       get_key( "last_release_date", i ),
                       get_key( "last_release", i ) )

    # all other releases, if any
    while ( i-- > 0 ) {
        release = sprintf( "%s, %s/%d",
                           release,
                           get_key( "last_release", i ),
                           get_key( "last_release_date", i ) )
    }

    return release ")"
}


# Append a value V for key K
#
# If K already has a value, another will be added; values are
# never replaced by this function.
function add_key( k, v,   i )
{
    i = pkginfon[k]++;
    pkginfo[k][i] = v
}

# Get value of key K at index I (default 0)
function get_key( k, i )
{
    return pkginfo[k][ or( i, 0 ) ]
}

# Get number of values for key K
function keylen( k )
{
    return pkginfon[k]
}

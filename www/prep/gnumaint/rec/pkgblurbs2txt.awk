# $Id: pkgblurbs2txt.awk,v 1.3 2019/05/11 11:29:36 ineiev Exp $
# pkgblurbs.rec to old pkgblurbs.txt
#
#  Copyright (C) 2018 Mike Gerwitz
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# This script converts the pkgblurbs.rec recfile into the old txt format
# that used to be committed directly to the repository.

BEGIN {
  print "# Public domain."
  print "#"
  print "# This file is maintained in web page CVS repository of GNU www,"
  print "# http://web.cvs.savannah.gnu.org/viewvc/www/www/prep/gnumaint/"
  print "#"
  print "# This file specifies a short blurb for each GNU package.  These are"
  print "# used by www.gnu.org, GSRC, and Guix."
  print "#"
  print "# THIS FILE IS NOW GENERATED FROM rec/pkgblurbs.rec;"
  print "# DO NOT MODIFY DIRECTLY!"
  print ""
}

# essentially just omits ^id: and %rec headers
/^package:/,/^$/ { print }

END {
  print "# Local Variables:"
  print "# compile-command: \"make pkgblurbs.txt\""
  print "# End:"
}

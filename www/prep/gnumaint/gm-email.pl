# $Id: gm-email.pl,v 1.1 2012/12/03 19:06:17 karl Exp $
# Generate a batch of email messages.  See the help string below.
# In a separate file just because it gets kind of long.
#
# Copyright 2012 Free Software Foundation Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Originally written by Karl Berry.


# Maybe someday we will need "bymaintainer", or something else.
#
sub generate_email_bypackage {
  splice (@ARGV, 0, 3);  # lose the "generate email ..."
  my ($output_dir, $pkg_list, $template_file);
  while (my $arg = shift @ARGV) {
    if ($arg eq "-h") {
      print <<END_GENEMAIL_HELP;
Usage: $0 generate email batch  -o OUTDIR  -p PKGLIST-FILE  -t TEMPLATE-FILE

Generate a batch of email messages in OUTDIR,
one for each package in PKGLIST-FILE (plain text, one package name per line),
doing substitutions in TEMPLATE-FILE to make each actual message.
All three of these options are required.

Any previous contents of OUTDIR is removed, so the program can be run
many times while tweaking the template and package list.

No email is actually sent (do that by running, e.g., sendmail -t on each
file in OUTDIR after you are satisfied with the contents).
END_GENEMAIL_HELP
      exit 0;

    } elsif ($arg eq "-o") {
      $output_dir = shift @ARGV;
    } elsif ($arg eq "-p") {
      $pkglist_file = shift @ARGV;
    } elsif ($arg eq "-t") {
      $template_file = shift @ARGV;
    }
  }
  
  # Check values.
  die "$0: generate email group needs all of -o, -p, -t; try -h for help.\n"
    if (! $output_dir || ! $pkglist_file || ! $template_file);
  #
  die "$0: package list file nonexistent or empty: $pkglist_file"
    if ! -s $pkglist_file;
  #
  die "$0: template file nonexistent or empty: $template_file"
    if ! -s $template_file;
  # 
  if (! -e $output_dir || -d $output_dir) {
    system ("rm -rf $output_dir");
    mkdir ($output_dir, 0777) || die "$0: mkdir($output_dir) failed: $!\n";
  } else {
    die "$0: output directory exists and is not a directory: $output_dir\n";
  }
  
  my %maint_file = &read_maintainers ("by-package");
  
  my $count = 0;
  open (PKGLIST, $pkglist_file) || die "open($pkglist_file) failed: $!";
  while (<PKGLIST>) {
    (my $pkg = $_) =~ s/\s.*$//;  # take first word of each line, ignore rest.
    die "$0: Not a package in the maintainers file: $pkg\n"
      if ! exists $maint_file{$pkg};

    # for msg.1201, rms said to explicitly exclude Hurd from the query.
    # And there's no point in checking on gnustandards or trans-coord.
    next if $pkg =~ /^(mig|hurd|gnumach|gnustandards|trans-coord)$/;

    my $msg = &make_msg ($template_file, $pkg, $maint_file{$pkg});
    next if ! $msg;  # skipping this one since unmaintained, etc.
    
    local *MSG;
    $MSG = ">$output_dir/$pkg";
    open (MSG) || die "open($MSG) failed: $!";
    print MSG $msg;
    close (MSG) || die "close($MSG) failed: $!";
    
    $count++;
  }
  close (PKGLIST) || warn "close($pkglist_file) failed: $!";
  
  return "$count messages generated in $output_dir/.";
}


# PKGREF is a hash with the information for PKGNAME from the maintainers
# file.  Expand TEMPLATE_FILE and return it as a string.  Except if the
# package is unmaintained, then return undef.
# 
sub make_msg {
  my ($template_file,$pkgname,$pkgref) = @_;
  
  # ignore generic entry if present, we want to write the individuals.
  my ($maintainer_email_addrs,undef) = &maintainer_email_addrs ($pkgref);
  return undef if $maintainer_email_addrs =~ /^unmaintained/;

  # Ok, we're unnecessarily rereading the template file every time.
  open (TEMPLATE, $template_file) || die "open($template_file) failed: $!";
  my $msg = join ("", <TEMPLATE>);
  close (TEMPLATE) || warn "close($template_file) failed: $!";
  
  my $pretty_email_addrs = &prettify_for_msg ($maintainer_email_addrs);
  
  # Template substitutions.
  $msg =~ s/%PACKAGE_NAME/$pkgname/g;
  $msg =~ s/%PACKAGE_MAINTAINERS/$pretty_email_addrs/g;
  
  return $msg;
}

# Given single string of email ADDRS, break into multiple lines at a
# somewhat reasonable length, prepending tabs to second and subsequent
# lines.  The idea being that the result can be used in the To: header.
# 
sub prettify_for_msg {
  my ($addrs) = @_;
  my $ret = "";
  
  my @addrs = split (/, /, $addrs);
  return $addrs if @addrs == 1;  # only one, just return it.
  
  my $line_len = 0;
  for (my $i = 0; $i < @addrs; $i++) {
    $ret .= $addrs[$i];              # add addr
    $ret .= "," if $i + 1 < @addrs;  # add comma if not the last one

    $line_len += length ($addrs[$i]) + 1;  # what we just added
    if ($line_len > 71) {
      # long enough, end this line and start the next:
      $ret .= "\n\t" if $i +1 < @addrs;  # unless it's the last one.
      $line_len = 0;
    } else {
      $ret .= " ";  # add space, will continue on this line
    }
  }
  
  return $ret;
}

1;

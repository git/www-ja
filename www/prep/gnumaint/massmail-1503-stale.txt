From: maintainers@gnu.org (GNU Project)
To: %PACKAGE_MAINTAINERS
Bcc: maintainers@gnu.org
Subject: release of GNU %PACKAGE_NAME

Greetings,

We're sending you this message on behalf of the GNU Project because as
far as we know, you are the maintainer(s) of %PACKAGE_NAME.  (If you're
not maintaining it, please just reply to let us know.)

As far as we've been able to discern, %PACKAGE_NAME 
has gone several years without a new (official) release.  If you have in
fact made a recent release, please let us know, and sorry for the noise.
We could not determine the status with absolute certainty.  Please
note that the best ways for us to be aware of your releases are making
the release available on ftp.gnu.org and making release announcements
via info-gnu@gnu.org and your Savannah project's "News" section (which
shows up on the Planet GNU RSS feed).

Otherwise ... it has always been a basic goal of GNU that its
constituent packages be actively developed and have releases from time
to time.  Without a release, we periodically ping the maintainer(s) to see
how things are going.  So, could you please reply to this message with a
response to the following questions?  Please include all and any
relevant information, better too much than too little.

1) Can you estimate, even roughly, when a new release will be made?

2) Have you been able to make progress (fixing bugs, updating
infrastructure, adding features, ...) since the last release, or since
our last contact with you?

3) Are there technical, legal, or other barriers to making a release?
We'd like to help if we can.

3b) Do you have any difficulties in making your package work with
other GNU packages or are there any bugs in other GNU packages that
affect your software?

4) Other comments?

In general, if you have any news or questions regarding the package,
please let us know at maintainers@gnu.org.  Thanks for contributing to
GNU, and happy hacking.

Brandon & Karl (GNUisances)

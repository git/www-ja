#!/usr/bin/env perl
# ftp releases by time,via  rsync listing.
# -rw-r--r--         918 2002/01/14 14:28:56 .message

$ARGV[0] = "/u/karl/sys/ftp/fp.gnu" unless @ARGV;

while (<>) {
  next if / -> /;
  next if /\.(diff|xdelta)\./;
  next if /snapshot\.pkg/;
  next if /\.darwin/;
  next if /-latest/;
  next if /\.sig/;
  next unless /\.[tj]ar\./;
  my (undef,undef,$date,undef,$pkg) = split (" "); # rsync
  # my ($date,undef,$pkg) = split (" "); # ftp
  $date =~ tr,-/,,d;
  $pkg{$pkg} = $date;
}

for my $p (sort { $pkg{$b} <=> $pkg{$a} } keys %pkg) {
  ($pfile = $p) =~ s/\.tar\..*$//;
  print "$pkg{$p}\t$pfile\n";
}

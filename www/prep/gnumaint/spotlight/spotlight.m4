NN new GNU releases in the last month (as of DATE):

RELEASES

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available most reliably from
<https://ftp.gnu.org/gnu/>.  Optionally, you may find faster download
speeds at a mirror located geographically closer to you by choosing
from the list of mirrors published at
<https://www.gnu.org/prep/ftp.html>, or you may use
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

This month, we welcome NEWMAINT.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance.  Please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help.  The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like
to offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to me, <bandali@gnu.org>,
with any GNUish questions or suggestions for future installments.

FP = fencepost.gnu.org
FP_GNU_EXCLUDES = --exclude=*.sig --exclude=video/TIME --exclude=find.txt.gz \
	--exclude=ls-l*.txt.gz --exclude=gnu-keyring.gpg

SL_DIR = spotlight
GNUBYTIME = perl $(SL_DIR)/gnubytime.pl
SL_M4 = $(SL_DIR)/spotlight.m4

SL_TMP = spotlight-tmp
REL = $(SL_TMP)/releases.gnu
REL_BY_TIME = $(SL_TMP)/releases-by-time.gnu
REL_THIS_MONTH = $(SL_TMP)/releases-this-month.gnu
REL_READY = $(SL_TMP)/releases-ready.gnu
REL_MD = $(SL_TMP)/releases.md
SL_MD = $(SL_DIR)/gnu-spotlight-$(shell date +%Y-%m-%d).md
ACTIVITY_REPORT = $(SL_TMP)/activity-report.txt

define HELP
Procedure:
	1. make release-list
	2. [manually edit $(REL_THIS_MONTH), rename to $(REL_THIS_MONTH)-ready]
	3. make markdown
endef

release-list: $(REL_THIS_MONTH)

markdown: $(SL_MD)

help:
	$(info $(HELP))

.PHONY: release-list markdown help

$(REL):
	mkdir -p $(SL_TMP)
	rsync --no-h -r $(REL_EXCLUDES) $(FP):/srv/data/ftp-mirror/ftp/ \
		| egrep -v '^[dl]' >"$@"

$(ACTIVITY_REPORT):
	scp $(FP):~brandon/src/womb/gnumaint/activity-report.txt $@

$(REL_BY_TIME): $(REL)
	$(GNUBYTIME) "$<" >"$@"

ifdef END_DATE
$(REL_THIS_MONTH): $(REL_BY_TIME)
	if grep -q "$(END_DATE)" $<; then \
		sed '/$(END_DATE)/,$$d;s|^.*/||' "$<" | sort -u >"$@"; \
		printf "\nNow manually edit $@, rename to $@-ready\n"; \
		printf "Run '$(MAKE) markdown' to continue.\n"; \
	else \
		printf "Invalid end date (no packages released on that date)\n"; \
		exit; \
	fi
else
$(REL_THIS_MONTH):
	$(error No end date (end of previous Spotlight) specified (e.g. END_DATE=20150222))
endif

$(REL_THIS_MONTH)-ready:
	$(error You must create $@ by manually editing $(REL_THIS_MONTH))

$(REL_READY): $(REL_THIS_MONTH)-ready
	sort "$<" >"$@"

$(REL_MD): $(REL_READY)
	sed \
		-e 's,\(.*\)-\([^-]*\),* [\1-\2](https://www.gnu.org/software/\1/),' \
	  "$<" >"$@"

$(SL_MD): $(REL_MD)
	m4 \
		-DNN="`wc -l <$(REL_MD)`" \
		-DDATE="`date '+%B %d, %Y'`" \
		-DRELEASES="`cat $(REL_MD)`" \
		-DNEWMAINT="TODO new maintainers" \
		$(SL_M4) >"$@"

# $Id: gm-generate.pl,v 1.29 2022/06/23 16:22:13 th_g Exp $
# The generate actions for the gm script (see --help message).
# 
# Copyright 2007, 2008, 2009, 2010, 2012, 2013, 2014, 2020
# Free Software Foundation Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Originally written by Karl Berry.



# Return links to home page and logo, for packages that have a logo.
# The result is included in
# https://www.gnu.org/graphics/package-logos.html via SSI.
# 
sub generate_logos_html {
  my $autostamp = &generated_by_us ();
  my @ret = ("<!-- File $autostamp -->\n<ul>\n");
  
  my %pkgs = &read_gnupackages ();
  for my $pkgname (sort keys %pkgs) {
    next if &skip_pkg_p ($pkgname);
    my $logo = $pkgs{$pkgname}->{"logo"};
    next unless $logo;
    
    push (@ret, qq!<li><a href="/software/$pkgname/">$pkgname</a><br />!);
    push (@ret, qq!<a href="$logo"><img alt="$pkgname" !
                . qq!src="/graphics/pkg-logos-250x100/$pkgname.250x100.png" /></a>!
                . qq!\n</li>\n!);
  }

  push (@ret, "</ul>\n<!-- End file $autostamp -->");

  return @ret;
}  



# Return all packages with all their maintainers, one package per
# line, like the original format of the maintainers file.  We run this
# from cron.
# 
sub generate_maintainers_bypackage {
  my @ret = ();
  
  my %pkgs = &read_maintainers ("by-package");
  
  for my $p (sort { lc($a) cmp lc($b) } keys %pkgs) {
    my ($entries,$generic_entry,$uploaders) = &maintainer_email_addrs ($pkgs{$p});
    
    # might not be anything in @entries if the only maintainer was generic.
    push (@ret, "$p - $entries") if $entries;
    
    # if we had a generic maintainer for this package, add that as a
    # separate entry, since that's the way rms wants it.
    push (@ret, "$p (generic) - $generic_entry") if $generic_entry;

    # if there are named uploaders (distinct from maintainers), add
    # them under a separate entry
    push (@ret, "$p (uploaders) - $uploaders") if $uploaders;
  }
  
  return @ret;
}



# Return doc links for all packages.  The result is included in
# www.gnu.org/manual/manual.html via SSI.
# 
sub generate_manual_html {
  my $autostamp = &generated_by_us ();
  my @ret = ("<!-- File $autostamp -->");
  
  # we want to output by category, so keep a hash with category names
  # for keys, and lists of package references for values.
  my %cat;

  my %pkgs = &read_gnupackages ();
  
  # Sort packages into their categories.
  for my $pkgname (sort keys %pkgs) {
    next if &skip_pkg_p ($pkgname);
    my %p = %{$pkgs{$pkgname}};

    my $short_cat = $p{"doc_category"};
    if (! $short_cat) {
      warn (&gnupkgs_msg ("lacks doc_category\n", %p));
      next;
    }
    
    # Append to the list of packages for this category.
    my @x = exists $cat{$short_cat} ? @{$cat{$short_cat}} : ();
    push (@x, \%p);
    $cat{$short_cat} = \@x;
  }

  # show list of all categories at top
  push (@ret, &output_category_list (%cat));
  
  # Sort first by full category name, since the abbreviations sometimes
  # start with a completely different string (Libraries -> Software
  # libraries).  Then, for each category, sort list of packages and output.
  # 
  for my $short_cat (sort by_full_cat keys %cat) {
    my ($full_cat,$cat_url) = @{ &read_doc_categories ($short_cat) };

    push (@ret, &output_cat ($short_cat, $full_cat, $cat_url));
    
    # For each package ...
    for my $pkg_ref (sort by_pkgname @{$cat{$short_cat}}) {
      my %p = %$pkg_ref;
      my ($doc_url,$doc_summary) = ($p{"doc_url"}, $p{"doc_summary"});
      if (! $doc_url || ! $doc_summary) {
        warn (&gnupkgs_msg ("lacks doc_(url|summary)\n", %p));
        next;
      }
      
      # convert the doc-url value to a hash of manuals and urls.
      my $pkgname = $p{"package"};
      my $home_url = "/software/$pkgname/";
      
      my %doc_urls = &find_doc_urls ($pkgname, $p{"doc_url"});      
      
      # if there was no explicit url for the package, use the home page.
      if (! exists ($doc_urls{$pkgname})) {
        $doc_urls{$pkgname} = $home_url;
      }
      
      # have to replace & with &amp; for XHTML.
      for my $manual (keys %doc_urls) {
        (my $doc_url_xhtml = $doc_urls{$manual}) =~ s,\&,\&amp;,g;
        $doc_urls{$manual} = $doc_url_xhtml;
      }
      
      # start building output string for this package.
      # first column is the package name and additional manuals.
      # Add an id for each package for easy linking;
      # but XHTML, as usual, introduces gratuitious problems.
      my $xhtml_id = &xhtml_id ($pkgname);
      my $str = qq!<dt id="$xhtml_id">!;
      
      # the main package identifier and its doc url.  If we have a
      # mundane name, use it.  Otherwise, prettify the pkg identifier.
      my $human_label = $p{"human_label"};
      my $url = $doc_urls{$pkgname};
      $url =~ s,^http(s?)://www.gnu.org/,/,;
      $str .= qq!<a href="$url">$human_label</a>!;
      
      # followed by other manual identifiers if present.
      my @more_manuals = ();
      for my $manual (keys %doc_urls) {
        next if $manual eq $pkgname; # already took care of that
        $url = $doc_urls{$manual};
        $url =~ s,^http(s?)://www.gnu.org/,/,;
        push (@more_manuals,
              sprintf (qq!<a href="%s">$manual</a>!, $url));
      }
      if (@more_manuals) {
        $str .= "\n&nbsp;<span>(";
        $str .= join ("\n&nbsp;", sort @more_manuals);
        $str .= ")</span>\n";
      }

      # Second column is the package synopsis, any shop url's, and
      # the package home page.
      my $summary = "$doc_summary."; # yep, add period
      my $shop = &find_shop_urls (%p);
      my $home = qq!\n    [<a href="$home_url">$pkgname&nbsp;home</a>]!;

      $str .= qq!</dt>\n<dd>$summary$shop$home!;
      $str .= qq!</dd>!;
      
      push (@ret, $str);
    }
    push (@ret, "</dl>\n</div>");
  }
  # show list of categories again at the end:
#  push (@ret, "\n" . &output_category_list (%cat));
  push (@ret, "<!-- End file $autostamp -->");

  return @ret;


  # HTML output for the beginning of each doc category --
  # the <h3> section with the text, a header, a link.
  # 
  sub output_cat {
    my ($short_cat,$full_cat,$cat_url) = @_;
    my $ret = qq!\n<div class='category'>\n<h3 id="$short_cat">!;
    $cat_url =~ s,^http(s?)://,//,;
    $ret .= qq!<a href="$cat_url">! if $cat_url;
    $ret .= "$full_cat";
    $ret .= "</a>" if $cat_url;
    $ret .= "\n<span class=\"gnun-split\"></span>\n&nbsp;<span><a class='up' \
    title='Back to categories' href='#categories'>&#11165;</a></span></h3>\n<dl>";
    return $ret;    
  }

  # given two package references, compare their names (for sorting).
  sub by_pkgname { $a->{"package"} cmp $b->{"package"}; }

  # given two short categories, compare their full names (for sorting).
  sub by_full_cat { &full_category ($a) cmp &full_category ($b); }

  # return just the full category name for SHORT_CAT.
  sub full_category {
    my ($short_cat) = @_;
    my ($full,undef) = @{ &read_doc_categories ($short_cat) };
    return $full;
  }

  # return string with all categories as links, as a sort of toc.
  sub output_category_list {
    my (%cat) = @_;
    my $ret = "<div id='categories'>\n";

    for my $short_cat (sort by_full_cat keys %cat) {
      my ($full_cat,$cat_url) = &full_category ($short_cat);
      $ret .= qq!<a href="#$short_cat">$full_cat</a><span class="no-display"> -</span>\n!;
    }
    $ret =~ s,<span class="no-display"> -</span>\n$,\n,;
    
    $ret .= "</div>";
    return $ret;
  }
  
  # interpret the doc-url value, return hash where keys are manual
  # identifiers and values are their urls.
  #
  sub find_doc_urls {
    my ($pkgname,$doc_url_val) = @_;
    my %ret;
    
    my @vals = split (/\|/, $doc_url_val); # result of parsing is | separators
    for my $val (@vals) {
      if ($val eq "none") {
        ; # nothing to return, let caller handle it.
        
      } elsif ($val eq "htmlxref") {
        my %htmlxref = &read_htmlxref ($pkgname);
        for my $manual (keys %htmlxref) {
          # do not overwrite a url from gnupackages for the main package
          # name with one from the htmlxref.  Instead, add junk to make
          # the htmlxref manual have a different key.  We don't want to
          # lose it, since if we have a general entry for "Texinfo"
          # (pointing to all its manuals), say, it's still useful to
          # have the direct link to the "texinfo" manual specifically.
          # Since we uppercase the main label, they're visually
          # distinct, too.
          # 
          if ($manual eq $pkgname && exists $ret{$pkgname}) {
            $ret{"$manual<!-- again -->"} = $htmlxref{$manual}
          } else {
            # otherwise, take what we are given.
            $ret{$manual} = $htmlxref{$manual};
          }
        }

      } else {
        warn "$pkgname: overwriting first doc_url value ($ret{$pkgname}) "
             . "with $val"  # source file should not do this.
          if (exists $ret{$pkgname}); 
        $ret{$pkgname} = $val;  # always prefer url given in gnupackages.
      }
    }
    return %ret;
  }
  
  # Handle FSF shop references.  We assume they come in pairs:
  # description in one entry and url in the next.  We return the HTML to
  # insert in the output, or the empty string.
  #
  sub find_shop_urls {
    my (%pkg) = @_;
    my $ret;
    my @shop = split (/\|/, $pkg{"doc_shop"});
    if (@shop) {
      $ret =  "\n       <br/>Available in print:";
      # keep same ordering as input.
      my @books = ();
      for (my $i = 0; $i < @shop; $i += 2) {
        my $title = $shop[$i];
        my $url = $shop[$i+1];
        if ($url !~ /https?:/) {
          warn (&gnupkgs_msg ("doc_shop url lacks http (misordered?)\n",%pkg));
        }
        push (@books, qq!\n       <cite><a href="$url">$title</a></cite>!);
      }
      $ret .= join (",", @books);
      $ret .= ".";
    } else {
      $ret = "";
    }
    return $ret;
  }
}



# Return all packages as relative HTML links to directories by the
# package name.  We carefully maintain https://www.gnu.org/software/
# so this works.  Use the simple pkgname/ directory, since nothing else
# (neither /index.html nor /pkgname.html) always works.
# 
sub generate_packages_html {
  my $autostamp = &generated_by_us ();
  my @ret = ("<!-- File $autostamp -->");

  if (!%pkgs) {
    %pkgs = &read_gnupackages ();
  }
  for my $pkgname (sort keys %pkgs) {
    next if &skip_pkg_p ($pkgname);
    push (@ret, qq!<a href="$pkgname/">$pkgname</a><span class="no-display">&nbsp;</span>!);
  }

  push (@ret, "<!-- End file $autostamp -->");
  return @ret;
}


# Return old packages  as relative HTML links to directories by the
# package name.
sub generate_oldpackages_html {
  local %pkgs = &read_oldpackages ();
  my @ret = &generate_packages_html ();
  return @ret;
}


# Return HTML-formatted blurbs about the packages for the featured
# package on https://www.gnu.org.
#
sub generate_blurbs_html {
  my $autostamp = &generated_by_us ();
  my @ret = ("<!-- File $autostamp -->");
  
  # so we can cross-check blurbs against packages.
  my %gnupkgs = &read_gnupackages ();

  my %pkgs = &read_pkgblurbs ();
  for my $pkgname (sort keys %pkgs) {
    next if &skip_pkg_p ($pkgname);
    
    my %pkg = %{ $pkgs{$pkgname} };
    my $msgprefix = "$PKGBLURBS_FILE:$pkg{lineno}";
    warn "$msgprefix: $pkgname not in gnupackages\n"
      if ! exists $gnupkgs{$pkgname};
    #
    # Retrieve info from packages file.
    my $pkglabel = $gnupkgs{$pkgname}->{"human_label"};
    my $pkgactivity = $gnupkgs{$pkgname}->{"activity_status"};
    my $pkglogo = $gnupkgs{$pkgname}->{"logo"};
    delete $gnupkgs{$pkgname}; # so we can see what's left over
    
    # we intentionally omit blurbs for some packages.
    next if $pkg{"blurb"} =~ /^null/;
    
    # Need the anchor version of the package name for the links.
    my $xhtml_id = &xhtml_id ($pkgname);
    
    # Need the regexp-safe version of the package name for the SSI (for gtk+).
    my $rxsafe_id = quotemeta ($pkgname);
    $rxsafe_id =~ s,\\-,-,g;  # \- troubles ineiev's request-blurbs

    # This ssi stuff lets the homepage extract a single blurb to display.
    push (@ret, qq,\n<!--,
                  . qq,#if expr="\$pkg_selection = /:($rxsafe_id|ALL):/" -->,);

    # Include the package logo (scaled) in the head, if there is one.
    $logo_xhtml = $pkglogo
      ?    qq!\n  <img src="/graphics/pkg-logos-250x100/$pkgname.250x100.png" style="height:1.3em"\n!
         . qq!       alt="logo for $pkgname" />!
      : "";
    
    # Note for translators if the package is stale.
    my $stale_comment = "";
    $stale_comment = "<!-- TRANSLATORS: stale -->" if $pkgactivity =~ /^stale/;

    # Include it in both HTML elements so that, e.g., msggrep can be used.
    push (@ret, qq,<h4 id="$xhtml_id">,
                  . qq,$stale_comment,
                  . qq,$logo_xhtml,
                  . qq,\n  <a href="/software/$pkgname/">$pkglabel</a></h4>,
                  . ($stale_comment ? "\n" : "")
                  . qq,<p>$stale_comment,);

    my $blurb = $pkg{"blurb"};
    # a very short blurb should be a redirect, since we already skipped null.
    if (length ($blurb) < 70) {
      my @words = split (" ", $blurb);
      if ($words[0] eq "redirect") {
        my $target = &xhtml_id ($words[1]);
        if (length ($target) <= 1) {
          warn "$msgprefix: redirection target too short: $target\n";
          next;
        }
        push (@ret, qq!See <a href="#$target">$target</a>.!);
      } else {
        warn "$msgprefix: unexpectedly short blurb: $words[0]\n";
        next;      
      }

    } else {  # normal blurb
      $blurb =~ s/\&/\&amp;/g;
      $blurb =~ s/</\&lt;/g;
      $blurb =~ s/>/\&gt;/g;

      # `...' is for commands, options, functions, etc.
      $blurb =~ s,`(.*?)',<code>$1</code>,g;
      $blurb =~ s,"(.*?)",\&ldquo;$1\&rdquo;,g;
      $blurb =~ s/"/\&quot;/g;  # quote any stray "

      $blurb =~ s/'/\&#39;/g;  # be sure we can quote text for shell
      chomp ($blurb = `echo '$blurb' | fmt`);
      $blurb =~ s/\&#39;/'/g;  # convert apostrophe back for translators
      push (@ret, $blurb);
    }

    # let's advertise if the package is looking for a maintainer.  maybe
    # we'll find someone.
    push (@ret, "This package is looking for a maintainer.")
      if $pkgactivity =~ /^nomaint/;

    my $doc_links = "/manual/manual.html#$xhtml_id";
    push (@ret, qq!<small>(<a href="$doc_links">doc</a>)!
              . qq!</small></p>!);

    push (@ret, "<!--#endif -->");
  }

  # make sure we've covered every package.
  for my $leftover (sort keys %gnupkgs) {
    next if &skip_pkg_p ($leftover);
    warn "$PKGBLURBS_FILE:1: $leftover missing\n";
  }
  
  push (@ret, "\n<!-- End file $autostamp -->");
  return @ret;
}

1;

$Id: howto-commission.txt,v 1.6 2021/12/29 16:34:20 jmd Exp $
    Copyright 2015 Free Software Foundation, Inc.

    Copying and distribution of this file, with or without modification,
    are permitted in any medium without royalty provided the copyright
    notice and this notice are preserved.

Notes on what an "assistant GNUisance" needs to do when a new package is
officially dubbed.  Only rms can dub a new package; he sends mail to
new-gnu@gnu.org (which all and only such assistants should be on), and
other recipients.

When a replacement or co-maintainer for an existing package is dubbed,
most of the below is unnecessary: just add the person to
gnu-prog[-discuss] (item 1), and to the maintainers file (item 4), and
send them the post-dubbing message (item 10).

1. Add the new maintainer to gnu-prog, gnu-prog-discuss, and
gnu-community-private right away, as stated at the end of the dubbing
message.
https://lists.gnu.org/mailman/admin/gnu-prog/members/add
https://lists.gnu.org/mailman/admin/gnu-prog-discuss/members/add
https://lists.gnu.org/mailman/admin/gnu-community-private/members/add

2. Sometimes the package identifier is not completely clear from rms's
message, especially if it has not gone through the normal evaluation
process.  The identifier should be all lowercase (no exceptions!), no
spaces, not start with "gnu" unless there is good reason for it, and be
reasonably descriptive and unique in the universe of free software (at
least).  The identifier is the common key linking the package in many
disparate places, so it is worth a little time and discussion with the
new maintainer if need be to make it good.  rms does not generally need
to be involved in that detail.

3. A package can have a "mundane name" that includes spaces, capitals,
etc.  Sometimes that dispels fears the new maintainer might have.

4. Once the package identifier is settled, things can proceed.  Add the
new maintainer to maintainers.private, with the new package name.
See /gd/gnuorg/README for editing specifics.  Update activity-report.txt
at the same time, as described in that README.

5. In prep/gnumaint, add the entry to rec/gnupackages.rec.  The only entries
likely to require some thought are the doc-category and doc-summary.
It can always be fixed later, but usually isn't, so it's worth thinking
about.  Whatever else, do not commit a placeholder or other fake
entries, since it will get propagated to www.gnu.org below.  The doc-url
will typically be "none" for a new package.

6. Also in prep/gnumaint, add an entry to rec/pkgblurbs.rec.  Here it can
be a three-line placeholder:
 id: newpkgid_blurb
 package: newpkgid
 blurb: null (newpkg/YYYYMMDD)
We'll ask the new maintainer for a blurb in the post email, below.

7. Items 5 and 6 should be done right away, as otherwise consistency
checks will start to fail (as soon as /gd/gnuorg/maintainers is
updated).  Commit these changes.

8. Regenerate the allgnupkgs.html et al. web page includes here:
  make gnupackages.txt
  make html-update # writes into www checkout at ${gw}
  make html-diff   # cvs diff of the generated files
  cd $gw && cvs commit ...
It's important to do the diff and actually look at the output, to ensure
both that the files have not been hand-edited on the www side, and that
the generation actually did what is expected.

9. The savannah people should ensure that the new package is marked gnu,
if it already exists on savannah, or accept it.  And for
replacement/co-maintainers, they should ensure that the new person is
made an admin of the package, if it exists.

10. Send the post-appointment email from the template temail-4post.txt,
suitably edited.

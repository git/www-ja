# $Id: gm-read.pl,v 1.26 2019/04/10 01:00:44 mikegerwitz Exp $
# Subroutines for gm script that read various external data file.
# 
# Copyright 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014
# Free Software Foundation Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Originally written by Karl Berry.



# Read $COPYRIGHT_LIST_FILE.  If HOW is "by-line", return hash with the
# keys being package names and the values their line numbers in the
# file.  If "by-year", return hash with keys as the assignment years and
# the values another hash mapping packages to names.
# 
sub read_copyright_list {
  my ($how) = @_;
  my %ret;
  
  if ($how !~ /^by-(year|line)$/) {
    die "unknown argument $how to read_copyright_list";
  }

  open (COPYRIGHT_LIST_FILE) || die "open($COPYRIGHT_LIST_FILE) failed: $!";
  while (<COPYRIGHT_LIST_FILE>) {
    # Look at lines following a blank line.
    next unless /^\s*$/;
    $_ = <COPYRIGHT_LIST_FILE>;
    chomp;
    
#new    # Split at tabs.
#new    my (@parts) = split (/\t+/);
#new    
#new    # If there is only one part, or more than five, something is totally awry.
#new    if (@parts == 1 || @parts > 5) {
#new      warn "$COPYRIGHT_LIST_FILE:$.: strange line tabs: $_\n";
#new      next;
#new
#new    # If there are only two or three parts, we hope it is a company
#new    # assignment, like
#new    # Telmark       2010-4-29
#new    # Just skip those for now.
#new    } elsif (@parts == 2) { # xx complain if a pkgname is first?
#new      warn "2 parts: @parts\n";
#new      next;
#new    
#new    # If there are three parts, we hope it is also a company assignment
#new    # with package name, like
#new    # ANY   CNOC vof        2010-5-26
#new    # Just skip these, too.
#new    } elsif (@parts == 3) { # xx complain if a pkgname is first?
#new      warn "3 parts: @parts\n";
#new      next;
#new    
#new    # If there are four parts, it is a mystery.
#new    } elsif (@parts == 4) { # xx complain if a pkgname is first?
#new      warn "4 parts?: @parts\n";
#new      next;
#new    }
#new    
#new    # Five parts is the normal case of a personal assignment, like
#new    # EMACS Richard Stallman        USA     1953    1986-09-29
#new    my ($pkgs,$name,$birthplace,$birthyear,$date) = @parts;
#new    warn "$pkgs by $name on $date\n";
#new
    
    # Drop everything after the first tab, we don't want to see all-caps
    # company names.
    s/\t.*//;
    
    # Sometimes commas are used to separate package names.  Just replace
    # them with spaces as a small simplification.
    s/,/ /g;
    
    # Split remainder into words at whitespace.
    my @words = split (" ");
    
    my $year = -1;
    my @line_pkgs = (); # if by-year
    for my $w (@words) {
      # the word has to start with an uppercase letter or number,
      # and be followed only by possible constituent characters,
      # or we're done with this line.
      last unless $w =~ /^[A-Z0-9][A-Z0-9._-]+$/;
      
      # and if it is only digits, never mind (1559643 Ontario Limited).
      next if $w =~ /^\d+$/;
      
      # and if it is only digits and -, that's a date.
      # remember the year if we need it, and we're done with this line.
      if ($w =~ /^\d+-[-\d]+$/) {
        if ($how eq "by-year") {
          $year = substr ($w, 0, 4);
          if ($year < 1980 || $year > 2222) {
            warn "$COPYRIGHT_LIST_FILE:$.: strange year: $year\n";
            next;
          }
        }
        last;
      }
      
      # keyword ANY in copyright.list is not a package name for us, etc.
      next if $w =~ /^(ANY|MISC|TRANSLATIONS|UNUSED)$/;
      
      # Special case assignment, not a GNU package.
      next if $w =~ /^(READINGRECORD)$/;
      
      $w = lc ($w);
      $canonical_pkg_name = &canonicalize_pkg_name ($w);
      
      # Some packages have legitimate entries in copyright.list, but are
      # nevertheless known not to be copyright FSF.  Probably would be
      # better to maintain this information in gnupackages.txt and get it
      # from there.  Maybe someday.
      next if $canonical_pkg_name =~ /^(# not really ours:
                            3dldf       #finstol
                           |a2ps	#demaille, hatta, et al.
                           |aspell	#atkinson
                           |autogen	#korb
                           |bayonne	#dyfet
                           |ccrtp	#dyfet
                           |djgpp	#delorie
                           |ed      	#moore "licenses"
                           |epsilon	#french university
                           |gcal        #esken
                           |ghostscript #aladdin
                           |gnome       #gnome
                           |gnu-arch    #atai
                           |gnuucp      #abandoned = unrelated to current uucp
                           |gnutrition  #original author
                           |gnuzilla    #mozilla
                           |gsasl	#josefsson
                           |gsl		#gough
                           |gsrc	#sampson
                           |icecat      #mozilla
                           |indent      #bsd et al.
                           |inetutils   #bsd
                           |less        #nudelman
                           |libgcrypt   #koch
                           |libidn	#josefsson
                           |octave      #many
                           |osip	#josefsson
                           |patch       #lwall
                           |rcs		#tichy et al.
                           |screen	#weigert et al.
                           |shishi	#josefsson
                           |social      #new social is not the old package
                           |uucp        #airs
                           |vera        #not an assignment
                          )$/x;

      # if by line, we can just save this package in the return hash.
      # if by year, have to accumulate all the info on this line.
      if ($how eq "by-year") {
        push (@line_pkgs, $canonical_pkg_name);
      } elsif ($how eq "by-line") {
        $ret{$canonical_pkg_name} = $.;
      }
    }
  }
  close (COPYRIGHT_LIST_FILE) || warn "close($COPYRIGHT_LIST_FILE) failed: $!";
  
  $ret{"gnustandards"} = 1; # no papers, but is copyright FSF
  $ret{"libtasn1"} = 1;     # split off from gnutls, so no separate papers
  $ret{"lispintro"} = 1;    # no papers, but is copyright FSF
  $ret{"mig"} = 1;          # part of hurd
  $ret{"trans-coord"} = 1;  # container
  
  return %ret;

  
  # lots of names in copyright.list don't match current package
  # identifiers, for whatever reason.
  #
  sub canonicalize_pkg_name {
    my ($w) = @_;
    
    $w =~ s/_manual//;  # manuals are not separate packages for us.

    %map = (
#      "dotgnu"          => "dotgnu-pnet",#decommissioned
      "enterprise"      => "gnue",	# remain?
      "glibc"           => "libc",	# will remain
#      "gnu-c"           => "gcc",
#      "gnuchess"        => "chess",
#      "gnugsl"          => "gsl",
#      "gnupascal"       => "pascal",
#      "gnus"            => "emacs",
#      "gpc"             => "pascal",
#      "graphics"        => "plotutils",
#      "hashcash.el"     => "emacs",
#      "info"            => "texinfo",
#      "libavl"          => "avl",
#      "libgcj"          => "gcc",
#      "libgen.a"        => "libc",
#      "libgsasl"        => "gsasl",
#      "libutf8"         => "libunistring",
#      "mach"            => "gnumach",
#      "makeinfo"        => "texinfo",
#      "malloc"          => "libc",
#      "memcmp"          => "libc",
#      "memcpy"          => "libc",
#      "memset"          => "libc",
#      "midnight"        => "mc",
#      "midnight_commander" => "mc",
#      "mp"              => "gmp",
#      "muse"            => "emacs-muse",
#      "objc"            => "gcc",
#      "obstack"         => "libc",
#      "pfe"             => "gforth",
#      "pnet"            => "dotgnu-pnet",
#      "portable.net"    => "dotgnu-pnet",
#      "ps"              => "sysutils",
#      "ptx"             => "coreutils",
#      "queue"           => "gnu-queue",
#      "radio"           => "gnuradio",
#      "readlink"        => "coreutils",
#      "regcmp"          => "libc",
#      "regex"           => "libc",
#      "regexp"          => "regex",
#      "sasl"            => "gsasl",
#      "shogi"           => "gnushogi",
#      "shred"           => "coreutils",
#      "snprintfv"       => "libc",
#      "sql.el"          => "emacs",
#      "strchr"          => "libc",
#      "strftime"        => "libc",
#      "strtod"          => "libc",
#      "superoptimizer"  => "superopt",
#      "texi2html"       => "texinfo",
#      "verilog-mode.el" => "emacs",
# fixed into /subentries:
#      "gas"            => "binutils",
#      "getopt"          => "libc",
#      "gprof"          => "binutils",
#      "glob"            => "libc",
#      "gnu.regexp"      => "libc",
#      "ld"             => "binutils",
#      "robotussin"     => "binutils",
#      "winboard"       => "xboard",
    );
    return $map{$w} || $w;
  }
}



# Read $COPYRIGHT_PAPERS_FILE, generated with
#   cd /srv/data/copyright-mirror && find -type f | sort
#
# Return hash with the keys being years and the values another hash
# (reference): this one with keys being the package names and the values
# yet a third hash (reference): this one just last names for keys, to
# easily avoid muliple entries.  This is the information given in the
# filenames.
# 
# Implementation not finished.  This is trying to check consistency
# between the papers the FSF actually has and the records of them.
# 
sub read_copyright_papers {
  my %ret;
  
  open (COPYRIGHT_PAPERS_FILE) ||die "open($COPYRIGHT_PAPERS_FILE) failed: $!";
  while (<COPYRIGHT_PAPERS_FILE>) {
    # Examples: ./1985/Curry.grep.pdf  ./1985/Robinson.emacs.1.pdf
    chomp;
    my (undef,$year,$file) = split ("/");
    next if $file eq "";   # ignore top-level files
    next if $file =~ /#/;  # ignore autosave files.
    next if $file =~ /~$/; # ignore backup files.
    
    if ($year < 1980 || $year > 2222) {
      warn "$COPYRIGHT_PAPERS_FILE:$.: strange year: $year\n";
      next;
    }
    
    $file =~ s/\.pdf$//;  # remove trailing .pdf
    
    my ($name,$package) = split (/\./, $file);
    if ($name eq "") {
      warn ("$COPYRIGHT_PAPERS_FILE:$.: empty name in $_\n");
      next;
    }

    # many filenames lack package names; try to match up last names.
    $package = ".nameonly" if $package eq "";
    $ret{$year}->{$package}->{$name}++;
  }
    
  close (COPYRIGHT_PAPERS_FILE)
  || warn "close($COPYRIGHT_PAPERS_FILE) failed: $!";
  return %ret;
}



# Read doc-categories.txt file for info about SHORT_CAT, and return a
# reference to a two-element array.  The first element in the array is
# the long category name; the second element is a url to the category in
# the Free Software Directory.
# 
# If we ever need a third piece of information, should probably switch
# to a hash for the values.
# 
sub read_doc_categories {
  my ($short_cat) = @_;
  
  if (keys %doc_category == 0) {
    open (DOC_CATEGORIES_FILE) || die "open($DOC_CATEGORIES_FILE) failed: $!";
  
    while (<DOC_CATEGORIES_FILE>) {
      next if /^\s*#/;  # ignore comments
      next if /^\s*$/;  # ignore blank lines.
      chomp;
    
      my ($short,$fsd,$full) = split (" ", $_, 3);
      my $ret_full = $full || $short;
      my $ret_url = $fsd eq "-"
                    ? "" : "http://directory.fsf.org/category/$fsd/";
      $doc_category{$short} = [ $ret_full, $ret_url ];
           
    }

    close (DOC_CATEGORIES_FILE)
    || warn "close($DOC_CATEGORIES_FILE) failed: $!";
  }
  
  # now we have the hash, so look up SHORT_CAT.
  my $ret;
  if (exists ($doc_category{$short_cat})) {
    $ret = $doc_category{$short_cat};
  } else {
    warn "$DOC_CATEGORIES_FILE: no short category name $short_cat\n";
    $ret = ["no long name for $short_cat", "no url for $short_cat"];
  }
  
  return $ret;
}



# Read an rsync listing of ftp.gnu.org, with entries like this:
# 
# drwxr-xr-x        4096 2004/01/16 12:20:08 gnu/3dldf
# lrwxrwxrwx           5 2010/12/29 13:30:03 gnu/libc -> glibc
# 
# Return list of directories and symlinks under gnu/.
#
sub read_ftplisting {
  my @ret;

  open (FTPLISTING_FILE) || die "open($FTPLISTING_FILE) failed: $!";
  my %keys;
  while (<FTPLISTING_FILE>) {
    chomp;
    next unless /^[dl].*[0-9] gnu[^+]/;  # the 0-9 is the seconds
    my $orig = $_;
    s,^.* gnu/,,; # rm through the gnu/
    s,[ /].*$,,;  # rm all following components or symlink target
    push (@ret, $_) unless exists $keys{$_};
    $keys{$_} = 1;
    warn "keeping $_ from $orig\n" if /^drw/;
  }
  close (FTPLISTING_FILE) || warn "close($FTPLISTING_FILE) failed: $!";
  
  return @ret;
}



# Read the ftp-upload-email file, generated by the sysadmins.
# Return per-package hash of info, with keys being the package names and
# values the list of email addresses.
#
sub read_ftpupload_email {
  my %ret;

  open (FTPUPLOAD_EMAIL_FILE) || die "open($FTPUPLOAD_EMAIL_FILE) failed: $!";
  while (<FTPUPLOAD_EMAIL_FILE>) {
    chomp;
    my ($pkg,$emails) = split (" ");

    $pkg = "libc" if $pkg eq "glibc"; # name on ftp.gnu.org differs
    
    $ret{$pkg} = $emails;
  }
  close (FTPUPLOAD_EMAIL_FILE) || warn "close($FTPUPLOAD_EMAIL_FILE) failed: $!";
  
  return %ret;
}



# Read the gnupackages.txt file, return a hash of information, where
# the keys are package names and the values are hash references with the
# information.  If a key is given more than once (e.g., note), the
# values are separated by |.
# 
# A key "lineno" is synthesized with the line number of the blank line
# following the package.
# 
# Another key "human_label" is synthesized as a human-oriented package
# name: the mundane_name: field if it's present, else a prettified
# version of the package identifier.
# 
sub read_gnupackages {
  my %ret;
 
  open (GNUPACKAGES_FILE) || die "open($GNUPACKAGES_FILE) failed: $!";
  
  my %pkg;
  while (<GNUPACKAGES_FILE>) {
    next if /^#/;  # ignore comments
    next if /^%/;  # ignore Recutils annotation
    s/ +$//; # remove trailing spaces
    chomp;
    
    # at a blank line, save the info we've accumulated, if any.
    if (/^$/) {
      next unless keys %pkg;
      
      if (exists $pkg{"package"}) {
        my %copy = %pkg;  # do not save a pointer to what will be overwritten
        $copy{"lineno"} = $. - 1;  # save line number

        my $human_label = $pkg{"mundane_name"};
        if (! $human_label) {
          ($human_label = $pkg{"package"}) =~ s/^gnu/GNU/; # gnufoo -> GNUfoo
          $human_label = ucfirst ($human_label);    # bar -> Bar
        }
        $copy{"human_label"} = $human_label;

        # save the constructed hash.
        $ret{$pkg{"package"}} = \%copy;

      } else {
        warn "$GNUPACKAGES_FILE:$.: no package name for block ending here\n";
      }
      undef %pkg;  # clear out for next
      next;
    }

    # check for a line continuation
    if (/^\+ /){
	my $contval = substr $_, 2;
	$key{$val} = $key{$val} . $contval;
	next;
    }

    # key is everything before the first colon.
    # value is everything after the first colon and whitespace.
    my ($key,$val) = split (/:\s*/, $_, 2);
    if ($key eq $_) {
      warn "$GNUPACKAGES_FILE:$.: no colon in line\n";
    }
    
    if ($key eq "package" && $val =~ /[A-Z]/) {
      # as a convention for simplicitly, we want to eschew uppercase in
      # package identifiers.
      warn "$GNUPACKAGES_FILE:$.: forcing package name to lowercase\n";
      $val = lc ($val);
    }
    
    # if key already exists, use | to separate values.
    $val = "$pkg{$key}|$val" if exists $pkg{$key};
    
    $pkg{$key} = $val;
  }
  
  close (GNUPACKAGES_FILE) || warn "close($GNUPACKAGES_FILE) failed: $!";
  
  return %ret;
}


# Read the pkgblurbs.txt file, return a hash of information, where the
# keys are package names and the values are hash references with the
# information.  If a key is given more than once, an error is given.
# A key "lineno" is synthesized with the line number of the blank line
# following the package.
# 
sub read_pkgblurbs {
  my %ret;

  open (PKGBLURBS_FILE) || die "open($PKGBLURBS_FILE) failed: $!";

  my %pkg;
  while (<PKGBLURBS_FILE>) {
    next if /^#/;  # Ignore comments
    s/ +$//; # remove trailing spaces
    chomp;

    # at a blank line, save the info we've accumulated, if any.
    if (/^$/) {
      next unless keys %pkg;
      
      if (exists $pkg{"package"}) {
        my %copy = %pkg;  # do not save a pointer to what will be overwritten
        $copy{"lineno"} = $. - 1;  # save line number
        $ret{$pkg{"package"}} = \%copy;
      } else {
        warn "$PKGBLURBS_FILE:$.: no package name for block ending here\n";
      }
      undef %pkg;  # clear out for next
      next;
    }
    #
    # the idea is that we have
    #   blurb: text text text
    #   + continued text continued text ...
    #
    # Let's look for the continuation lines first.
    if (/^\+\s+/) {
      if (exists $pkg{"blurb"}) {
        # continued line, drop "+ " continuation and append.
        s,^\+\s+,,;
        $pkg{blurb} = "$pkg{blurb} $_";
      } else {
        warn "$PKGBLURBS_FILE:$.: +continuation without previous blurb:\n";
      }

    } elsif (/:/) {
      # not a continuation line, so it should be a normal
      # key: value line.
      my ($key,$val) = split (/:\s*/, $_, 2);
      # if key already exists, complain.
      warn "found second value $val for key $key (already have $pkg{$key})\n"
        if exists $pkg{$key};
    
      $pkg{$key} = $val;

    } else {
      warn "$PKGBLURBS_FILE:$.: no colon or plus in line\n";
    }
  }
  close (PKGBLURBS_FILE) || warn "close($PKGBLURBS_FILE) failed: $!";
  
  return %ret;
}



# Read htmlxref.cnf file for entries relating to PKGNAME.
# Return a hash where the keys are the manual identifiers and the values
# are the urls.
# 
# See the HTML Xref Configuration node in the Texinfo manual.
# The file is maintained in the util subdirectory of the Texinfo sources.
# 
sub read_htmlxref {
  my ($pkgname) = @_;
  my %ret;
    
  open (HTMLXREF_FILE) || die "open($HTMLXREF_FILE) failed: $!";

  my %variables;
  my %ret_type;  # record preferred xref type so far, for each manual
  while (<HTMLXREF_FILE>) {
    next if /^\s*#/;  # ignore comments
    next if /^\s*$/;  # ignore blank lines.
    chomp;
    
    # handle variable definitions:
    if (/^\s*(\w+)\s*=\s*(.*)\s*$/) {
      my ($var,$val) = ($1,$2);
      $variables{$var} = &expand_variables ($val);

    # look for manual entries relating to PKGNAME:
    } elsif (/^\s*(\S+)\s+(\w+)\s+(.*)\s*$/) {
      my ($manual,$type,$url) = ($1,$2,$3);

      # The manual name may be exactly the package name, but it may not
      # be, yet still part of the package.  To detect this, we see if
      # the entry uses the package name as a variable in the url.  (This
      # seemed the only other way we could extract the relevant manuals,
      # barring adding another field, which seemed redundant.)  Such a
      # variable name is always in all-uppercase, with - changed to _.
      # 
      (my $pkgname_as_var = $pkgname) =~ tr/a-z-/A-Z_/;
      if ($manual eq $pkgname || $url =~ /\$\{$pkgname_as_var\}/) {
        if (! exists $ret_type{$manual}
            || &prefer_xref_type ($type, $ret_type{$manual})) {
          $ret{$manual} = &expand_variables ($url);
          $ret_type{$manual} = $type;
        }
      }
      
    } else {
      warn "$HTMLXREF_FILE:$.: unexpected line: $_\n";
    }
  }
  
  close (HTMLXREF_FILE) || warn "close($HTMLXREF_FILE) failed: $!";

  return %ret;

  # using the %variables hash, expand ${varname} constructs until none
  # remain.  If a variable isn't defined, just replace it with the empty
  # string.  Would be better to give a warning, but let's not bother.
  sub expand_variables {
    my ($val) = @_;
    $val =~ s/\$\{(\w+)\}/$variables{$1}/eg  # expand variables
      until $val !~ /\$\{(\w+)\}/;           # until no more
    return $val;
  }

  # If TYPE1 is preferred to TYPE2, return 1, else 0.
  # When a given manual is available in multiple formats, we prefer the
  # "smallest" one (mono < chapter < section < node), since packages with
  # multiple manuals tend to be large, and in any event, we usually have a
  # generic url (/software/pkgname/manual/) linking to all available forms.
  sub prefer_xref_type {
    my ($type1,$type2) = @_;
    my %xref_types = (
      "node"    => 10,
      "section" => 20,
      "chapter" => 30,
      "mono"    => 40,
    );
    
    if (! exists $xref_types{$type1}) {
      warn "$HTMLXREF_FILE:$.: unexpected xref type: $type1\n";
    }
    if (! exists $xref_types{$type2}) {
      warn "$HTMLXREF_FILE:$.: unexpected xref type: $type2\n";
    }
    
    return $xref_types{$type1} < $xref_types{$type2};
  }
}



# Read $MAINTAINERS_FILE according to $HOW, either "by-package" or
# "by-maintainer".  We return a hash.  With "by-package", the keys are
# package names and the values are a list of maintainer hash references.
# With "by-maintainer", the keys are maintainer (real) names and the
# values are hash references with their information.
# If a field occurs more than once in a given maintainer's entry,
# the values are separated by a |.
# 
# Special maintainer keys we synthesize, i.e., that are not in the
# actual maintainers file:
# is_generic - whether it is an actual person or a generic address;
# best_email - uses privateemail where present, in preference to email;
# lineno - location in the file.
# 
sub read_maintainers {
  my ($how) = @_;
  my %ret;
  
  open (MAINTAINERS_FILE) || die "open($MAINTAINERS_FILE) failed: $!";

  # read the real information.
  my %maint;  # info we are accumulating for one maintainer
  while (<MAINTAINERS_FILE>) {
    next if /^#/;  # ignore comments
    next if /^%/;  # ignore Recutils annotation
    chomp;
    
    # at a blank line, save the maintainer info we've accumulated, if any.
    if (/^\s*$/) {
      &debug_hash ($., %maint);
      next unless keys %maint;
      
      # record whether this is a generic maintainer (starts with lowercase):
      $maint{"is_generic"} = $maint{"name"} =~ m/^[a-z]/;
      
      # record best email to use for the maintainer.
      $maint{"best_email"} = $maint{"privateemail"} || $maint{"email"};
      
      # record where we found it, more or less.  We are past the blank
      # line, and we can assume every entry has at least two lines.
      $maint{"lineno"} = $. - 4;
      
      if ($how eq "by-package") {
        # split apart the package value we've accumulated..
        if (exists $maint{"package"} || exists $maint{"uploader"}) {
          my @pkgs = split (/\|/, $maint{"package"});
	  my @uplds = split (/\|/, $maint{"uploader"});

          # append this maintainer to the list for each of his/her packages.
          my %copy = %maint;
	  $copy{"is_uploader"} = 0;
          for my $p (@pkgs) {
            my @x = exists $ret{$p} ? @{$ret{$p}} : ();
            push (@x, \%copy);
            $ret{$p} = \@x;
          }
	  my %copy2 = %maint;
	  $copy2{"is_uploader"} = 1;
          for my $p (@uplds) {
            my @x = exists $ret{$p} ? @{$ret{$p}} : ();
            push (@x, \%copy2);
            $ret{$p} = \@x;
          }
        } else {
          warn "no packages for $maint{name}";
        }

      } elsif ($how eq "by-maintainer") {
        if (! exists $maint{"name"}) {
          warn "no name for maintainer";
          next;
        }
        my $name = $maint{"name"};
        if (exists $ret{$name}) {
          warn "ignoring second entry for maintainer $name";
          next;
        }
        my %copy = %maint;
        $ret{$name} = \%copy;

      } else {
        die "can't read_maintainers($how)";
      }

      undef %maint;  # clear out for next maintainer.

      last if /^\f$/;  # form feed marks end of info.
      next;
    }

    # key is everything before the first colon.
    # value is everything after the first colon and whitespace.
    my ($key,$val) = split (/:\s*/, $_, 2);
    
    # check that an email value doesn't have two @'s.
    if ($key =~ /email/ && $val =~ /@@/) {
      warn "value for key $key has double \@: $val";
      next;
    }
    
    # if key already exists, use | to separate values.
    $val = "$maint{$key}|$val" if exists $maint{$key};
    
    # todo: parse key of address+ and append.
    $maint{$key} = $val;
  }
  
  # skip the rest.
  close (MAINTAINERS_FILE) || warn "close($MAINTAINERS_FILE) failed: $!";
  
  return %ret;
}



# Return list of entries in $OLDPACKAGES_FILE -- one per line, ignoring
# comments starting with # and blank lines.
# 
sub read_oldpackages {
  local $GNUPACKAGES_FILE = $OLDPACKAGES_FILE;
  my %ret = &read_gnupackages ();    # reuse routine via dynamic scoping
  return %ret;
}  



# Return list of entries in $RECENTREL_FILE -- one per line, ignoring
# comments starting with # and blank lines.
# 
sub read_recentrel {
  my @ret = ();
  
  open (RECENTREL_FILE) || die "open($RECENTREL_FILE) failed: $!";
  while (<RECENTREL_FILE>) {
    next if /^\s*#/;  # ignore comments
    next if /^\s*$/;  # ignore blank lines.
    chomp;
    push (@ret, $_);
  }
  close (RECENTREL_FILE) || warn "close($RECENTREL_FILE) failed: $!";
 
  return @ret;
}



# read the savannah groups.tsv file, return a hash of information, where
# the keys are project identifiers and the values are references to
# hashes with the information.  A key "lineno" is included in each value.
# 
sub read_savannah {
  my %ret;
  
  open (SAVANNAH_FILE) || die "open($SAVANNAH_FILE) failed: $!";
  <SAVANNAH_FILE>;  # ignore first line (with field names).
  
  # We want only the offical GNU packages, which, happily, come first (type=1).
  while (<SAVANNAH_FILE>) {
    last if /^2/;  # quit at first non-gnu
    chomp;
    my ($type_id,$name,$unix_group_name,$group_name) = split (/\t/);

    my %pkg;
    $pkg{"name"} = $group_name;
    $pkg{"lineno"} = $.;
    
    if (exists $ret{$unix_group_name}) {
      warn "$SAVANNAH_FILE:$.: already saw $unix_group_name?\n";
    } else {
      $ret{$unix_group_name} = \%pkg;
    }
  }
  
  close (SAVANNAH_FILE) || warn "close($SAVANNAH_FILE) failed: $!";
  
  return %ret;
}


1;

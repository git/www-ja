# $Id: gm-list.pl,v 1.3 2015/03/23 09:16:42 brandon Exp $
# The list actions for the gm script (see --help message).
# 
# Copyright 2007, 2008, 2009, 2010, 2012, 2013, 2015
# Free Software Foundation Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Originally written by Karl Berry.



# Return a list of strings: the (active) package names which the FSF is
# the copyright holder.  Or, if the NOTFSF argument is set, for which it
# is not the copyright holder.
# 
sub list_copyrightfsf_ {
  my ($notfsf,$nowarn) = @_;
  my @ret = ();

  my %fsf_pkgs = &read_copyright_list ("by-line");
  my %old_pkgs = &read_oldpackages ();
  my %maint_pkgs = &read_maintainers ("by-package");

  for my $fsf_pkg (sort keys %fsf_pkgs) {  # packages in copyright.list
    if (! exists $maint_pkgs{$fsf_pkg}) {  # if not in maintainers ...
      # warn about stray names unless known special case, or decommissioned.
      if (! &skip_fsf ($fsf_pkg) && ! exists $old_pkgs{$fsf_pkg}) {
        $fsf_line = $fsf_pkgs{$fsf_pkg};
        warn "$COPYRIGHT_LIST_FILE:$fsf_line: $fsf_pkg not in maintainers\n"
          unless $nowarn;
      }
      next;
    }
    
    if ($notfsf) {
      delete $maint_pkgs{$fsf_pkg};
    } else {
      push (@ret, $fsf_pkg) if ! &skip_pkg_p ($mp);
    }
  }
  
  
  if ($notfsf) {
    # if not fsf, then we want everything left in (not deleted from) maint.
    # The same few problem and non-packages to delete in this case.
    for my $mp (keys %maint_pkgs) {
      delete $maint_pkgs{$mp} if &skip_pkg_p ($mp);
    }
    push (@ret, sort keys %maint_pkgs);
  }
  
  return @ret;


  # Return 1 if we shouldn't worry about the name of this FSF assignment
  # not being a canonical package name.
  # 
  sub skip_fsf {
    my ($fsf) = @_;

    my @skip_fsf = qw(
      alloca art artwork asa
          at crontab atrm crond makeatfile
       autolib
          backupfile getversion
      banner blksize bsearch c2tex catalog cdlndir
          cgraph dfs
      checkaliases checker chkmalloc command configure crypto ctutorial cvs
       cvs-utils
      dcl debian dvc
      ebnf2ps ecc ecos edebug egcs elisp_manual elms emacstalk enc-dec
          ep gnust
       etags expect
      fcrypt fiasco file flex flymake flyspell fpr freeodbc fsflogo
      g77 g95 gamma garlic gc gcc-testsuite gconnectfour gellmu gfortran
       gfsd gm2 gnatdist gnoetry gnu_ocr gnulist gnussl go32 gomp grx gsmb
       gso guile-python guppi gyve
      initialize interp io isaac ispell
      je
      kaffe
      leif lesstif lib libiberty libstdc libwv linkcontroller lynx
      m2f mh-e mingw32 minpath misc mkinstalldirs mmalloc mpuz msort mtime
       mtrace mule mutt myme
      newlib newsticker nvi
      opcodes ox
      p2c pc pipobbs pips planner polyorb pptp profile psi publish    
      qsort quagga
      rcl readelf regex review riacs
          scandir srchdir
      send sim spim spline stm suit
          tcl tix tk expect
      texi2roff thethe tkwww trix tsp_solve tzset
      udi ul uncvt unexec
      viper web webpages win32api xemacs zlib
    );
    
    my %skip_fsf;
    @skip_fsf{@skip_fsf} = ();  # hash slice to make hash from list
    
    return exists $skip_fsf{$fsf};
  }
}  


# Return the packages for which the FSF is not the copyright holder.
# 
sub list_copyrightfsfnot_ {
  return list_copyrightfsf_ (1);
}



# Return copyright.list entries that don't have matching paperwork,
# and vice versa.
# 
sub list_copyrightpapers_ {
  my @ret = ();
  my %cl_pkgs = &read_copyright_list ("by-year");
  my %cp_pkgs = &read_copyright_papers ();
  
  $DEBUG = 1;
  
  for my $year (sort keys %cp_pkgs) {
    my $cp_year = $cp_pkgs{$year};
    my $cl_year = $cl_pkgs{$year};
  &debug_hash ("cp_year $year", $cp_year);
  &debug_hash ("cl_year $year", $cl_year);
    last;
  }
  
  return @ret;
}



# Return list of maintainers for whom we have no phone or address.
# 
sub list_maintainers_nophysical {
  my @maints = ();
  my %maints = &read_maintainers ("by-maintainer");

  for my $m (sort keys %maints) {
    my $m_ref = $maints{$m};
    my %m = %$m_ref;
    next if $m{"is_generic"};  # no contact info needed
    next if $m{"address"} || $m{"phone"};  # have contact info
    (my $packages = $m{"package"}) =~ tr/|/ /;
    push (@maints, "$m{best_email} ($m{name} - $packages)");
  }
  
  return @maints;
}



# Return all packages sorted by activity status, one package per line.
# 
sub list_packages_activity {
  my @ret = ();

  # sort activity statuses in this order.  If other strings are used,
  # they'll show up first so they can be easily fixed.
  my %activity_order = ("stale" => 1,
                        "moribund" => 2,
                        "ok" => 3,
                        "stable" => 4,
                        "subpkg" => 5,
                        "container" => 6,
                       );

  my %pkgs = &read_gnupackages ();
  for my $pkgname (sort by_activity keys %pkgs) {
    my %p = %{$pkgs{$pkgname}};
    my $activity = $p{"activity-status"};
    push (@ret, &gnupkgs_msg ($activity, %p));
  }
  
  return @ret;
  
  sub by_activity {
    (my $a_status = $pkgs{$a}->{"activity-status"}) =~ s/ .*//;
    (my $b_status = $pkgs{$b}->{"activity-status"}) =~ s/ .*//;
    $activity_order{$a_status} <=> $activity_order{$b_status}
    || $pkgs{$a}->{"activity-status"} cmp $pkgs{$b}->{"activity-status"}
    || $a cmp $b;
  }
}



# Return all packages whose GPLv3 status is not final.
# 
sub list_packages_gplv3 {
  my @ret = ();

  my %pkgs = &read_gnupackages ();
  for my $pkgname (sort by_gplv3 keys %pkgs) {
    my %p = %{$pkgs{$pkgname}};
    my $gplv3 = $p{"gplv3-status"};
    my $contact = $p{"last-contact"};
    next if $gplv3 =~ /^(done|doc|not-applicable|notgpl|ok|see)/;
    push (@ret, &gnupkgs_msg ($gplv3 . ($contact ? " [$contact]" : ""), %p));
  }
  
  return @ret;
  
  sub by_gplv3 {
    (my $a_status = $pkgs{$a}->{"gplv3-status"});# =~ s/ .*//;
    (my $b_status = $pkgs{$b}->{"gplv3-status"});# =~ s/ .*//;
    $pkgs{$a}->{"gplv3-status"} cmp $pkgs{$b}->{"gplv3-status"}
    || $a cmp $b;
  }
}



# Return list of packages for whom no maintainer has answered.
# 
sub list_packages_unanswered {
  my @recentrel = &read_recentrel ();
  my %activity = &read_activity ("by-package");
  my %pkgs = &read_maintainers ("by-package");
  my @ret = ();

  for my $p (sort { lc($a) cmp lc($b) } keys %pkgs) {
    #&debug_hash ($p, $pkgs{$p});

    if (grep { $p eq $_ } @recentrel) {
      &debug ("$p recently released, skipping");

    } elsif (exists $activity{$p}) {
      # todo: check back to some cutoff
      &debug ("$p got activity reply, skipping");

    } else {
      &debug ("$p no activity, returning");
      my @entries = ();
      for my $m (@{$pkgs{$p}}) {
        next if $m->{"is_generic"};
        my $entry = $m->{"name"};
        $entry .= " " if $entry;
        $entry .= "<$m->{best_email}>" if exists $m->{"best_email"};
        push (@entries, $entry);
      }
    
      # might not be anything in @entries.
      push (@ret, "$p - " . join (", ", @entries)) if @entries;
    }
  }
  
  return @ret;
}


1;

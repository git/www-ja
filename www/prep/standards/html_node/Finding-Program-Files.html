<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- The GNU coding standards, last updated May 26, 2024.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Free
Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Finding Program Files (GNU Coding Standards)</title>

<meta name="description" content="Finding Program Files (GNU Coding Standards)">
<meta name="keywords" content="Finding Program Files (GNU Coding Standards)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Program-Behavior.html#Program-Behavior" rel="up" title="Program Behavior">
<link href="Graphical-Interfaces.html#Graphical-Interfaces" rel="next" title="Graphical Interfaces">
<link href="User-Interfaces.html#User-Interfaces" rel="prev" title="User Interfaces">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="Finding-Program-Files"></a>
<div class="header">
<p>
Next: <a href="Graphical-Interfaces.html#Graphical-Interfaces" accesskey="n" rel="next">Graphical Interfaces</a>, Previous: <a href="User-Interfaces.html#User-Interfaces" accesskey="p" rel="prev">User Interfaces</a>, Up: <a href="Program-Behavior.html#Program-Behavior" accesskey="u" rel="up">Program Behavior</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Finding-the-Program_0027s-Executable-and-Associated-Files"></a>
<h3 class="section">4.6 Finding the Program&rsquo;s Executable and Associated Files</h3>

<p>A program may need to find the executable file it was started with, so
as to relaunch the same program.  It may need to find associated
files, either source files or files constructed by building, that
it uses at run time.
</p>
<p>The way to find them starts with looking at <code>argv[0]</code>.
</p>
<p>If that string contains a slash, it is by convention the file name of
the executable and its directory part is the directory that contained
the executable.  This is the case when the program was not found
through <code>PATH</code>, which normally means it was built but not
installed, and run from the build directory.  The program can use the
<code>argv[0]</code> file name to relaunch itself, and can look in its
directory part for associated files.  If that file name is not
absolute, then it is relative to the working directory in which the
program started.
</p>
<p>If <code>argv[0]</code> does not contain a slash, it is a command name whose
executable was found via <code>PATH</code>.  The program should search for
that name in the directories in <code>PATH</code>, interpreting <samp>.</samp> as
the working directory that was current when the program started.
</p>
<p>If this procedure finds the executable, we call the directory it was
found in the <em>invocation directory</em>.  The program should check
for the presence in that directory of the associated files it needs.
</p>
<p>If the program&rsquo;s executable is normally built in a subdirectory of the
main build directory, and the main build directory contains associated
files (perhaps including subdirectories), the program should look at
the parent of the invocation directory, checking for the associated
files and subdirectories the main build directory should contain.
</p>
<p>If the invocation directory doesn&rsquo;t contain what&rsquo;s needed, but the
executable file name is a symbolic link, the program should try using
the link target&rsquo;s containing directory as the invocation directory.
</p>
<p>If this procedure doesn&rsquo;t come up with an invocation directory that is
valid&mdash;normally the case for an installed program that was found via
<code>PATH</code>&mdash;the program should look for the associated files in the
directories where the program&rsquo;s makefile installs them.
See <a href="Directory-Variables.html#Directory-Variables">Directory Variables</a>.
</p>
<p>Providing valid information in <code>argv[0]</code> is a convention, not
guaranteed.  Well-behaved programs that launch other programs, such as
shells, follow the convention; your code should follow it too, when
launching other programs.  But it is always possible to launch the
program and give a nonsensical value in <code>argv[0]</code>.
</p>
<p>Therefore, any program that needs to know the location of its
executable, or that of of other associated files, should offer the
user environment variables to specify those locations explicitly.
</p>
<p><strong>Don&rsquo;t give special privilege, such as with the <code>setuid</code>
bit, to programs that will search heuristically for associated files
or for their own executables when invoked that way.</strong>  Limit that
privilege to programs that find associated files in hard-coded
installed locations such as under <samp>/usr</samp> and <samp>/etc</samp>.
</p>

<p>See <a href="http://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Variables.html#Bourne-Shell-Variables">Bourne Shell Variables</a> in <cite>Bash Reference Manual</cite>,
for more information about <code>PATH</code>.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Graphical-Interfaces.html#Graphical-Interfaces" accesskey="n" rel="next">Graphical Interfaces</a>, Previous: <a href="User-Interfaces.html#User-Interfaces" accesskey="p" rel="prev">User Interfaces</a>, Up: <a href="Program-Behavior.html#Program-Behavior" accesskey="u" rel="up">Program Behavior</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

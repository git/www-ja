<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- The GNU coding standards, last updated May 26, 2024.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Free
Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Change Logs (GNU Coding Standards)</title>

<meta name="description" content="Change Logs (GNU Coding Standards)">
<meta name="keywords" content="Change Logs (GNU Coding Standards)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Documentation.html#Documentation" rel="up" title="Documentation">
<link href="Change-Log-Concepts.html#Change-Log-Concepts" rel="next" title="Change Log Concepts">
<link href="NEWS-File.html#NEWS-File" rel="prev" title="NEWS File">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="Change-Logs"></a>
<div class="header">
<p>
Next: <a href="Man-Pages.html#Man-Pages" accesskey="n" rel="next">Man Pages</a>, Previous: <a href="NEWS-File.html#NEWS-File" accesskey="p" rel="prev">NEWS File</a>, Up: <a href="Documentation.html#Documentation" accesskey="u" rel="up">Documentation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Change-Logs-1"></a>
<h3 class="section">6.8 Change Logs</h3>
<a name="index-change-logs"></a>

<p>Keep a change log to describe all the changes made to program source
files.  The purpose of this is so that people investigating bugs in the
future will know about the changes that might have introduced the bug.
Often a new bug can be found by looking at what was recently changed.
More importantly, change logs can help you eliminate conceptual
inconsistencies between different parts of a program, by giving you a
history of how the conflicting concepts arose, who they came from, and
why the conflicting changes were made.
</p>
<a name="index-software-forensics_002c-and-change-logs"></a>
<p>Therefore, change logs should be detailed enough and accurate enough
to provide the information commonly required for such <em>software
forensics</em>.  Specifically, change logs should make finding answers to
the following questions easy:
</p>
<ul>
<li> What changes affected a particular source file?

</li><li> Was a particular source file renamed or moved, and if so, as part of
what change?

</li><li> What changes affected a given function or macro or definition of a
data structure?

</li><li> Was a function (or a macro or the definition of a data structure)
renamed or moved from another file, and if so, as part of which
change?

</li><li> What changes deleted a function (or macro or data structure)?

</li><li> What was the rationale for a given change, and what were its main
ideas?

</li><li> Is there any additional information regarding the change, and if so,
where can it be found?
</li></ul>

<a name="index-VCS"></a>
<a name="index-version-control-system_002c-for-keeping-change-logs"></a>
<p>Historically, change logs were maintained on specially formatted
files.  Nowadays, projects commonly keep their source files under a
<em>version control system</em> (VCS), such as Git,
Subversion, or Mercurial.  If the VCS repository is publicly
accessible, and changes are committed to it separately (one commit for
each logical changeset) and record the authors of each change, then
the information recorded by the VCS can be used to produce
the change logs out of VCS logs, and to answer the above
questions by using the suitable VCS commands.  (However, the
VCS log messages still need to provide some supporting
information, as described below.)  Projects that maintain such
VCS repositories can decide not to maintain separate change
log files, and instead rely on the VCS to keep the change
logs.
</p>
<p>If you decide not to maintain separate change log files, you should
still consider providing them in the release tarballs, for the benefit
of users who&rsquo;d like to review the change logs without accessing the
project&rsquo;s VCS repository.  Scripts exist that can produce
<samp>ChangeLog</samp> files from the VCS logs; for example, the
<samp>gitlog-to-changelog</samp> script, which is part of Gnulib, can do
that for Git repositories.  In Emacs, the command <kbd>C-x v a</kbd>
(<code>vc-update-change-log</code>) does the job of incrementally updating a
<samp>ChangeLog</samp> file from the VCS logs.
</p>
<p>If separate change log files <em>are</em> maintained, they are normally
called <samp>ChangeLog</samp>, and each such file covers an entire
directory.  Each directory can have its own change log file, or a
directory can use the change log of its parent directory&mdash;it&rsquo;s up to
you.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Change-Log-Concepts.html#Change-Log-Concepts" accesskey="1">Change Log Concepts</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Style-of-Change-Logs.html#Style-of-Change-Logs" accesskey="2">Style of Change Logs</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Simple-Changes.html#Simple-Changes" accesskey="3">Simple Changes</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Conditional-Changes.html#Conditional-Changes" accesskey="4">Conditional Changes</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Indicating-the-Part-Changed.html#Indicating-the-Part-Changed" accesskey="5">Indicating the Part Changed</a>:</td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Man-Pages.html#Man-Pages" accesskey="n" rel="next">Man Pages</a>, Previous: <a href="NEWS-File.html#NEWS-File" accesskey="p" rel="prev">NEWS File</a>, Up: <a href="Documentation.html#Documentation" accesskey="u" rel="up">Documentation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

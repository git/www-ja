<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- The GNU coding standards, last updated May 26, 2024.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Free
Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Change Log Concepts (GNU Coding Standards)</title>

<meta name="description" content="Change Log Concepts (GNU Coding Standards)">
<meta name="keywords" content="Change Log Concepts (GNU Coding Standards)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Change-Logs.html#Change-Logs" rel="up" title="Change Logs">
<link href="Style-of-Change-Logs.html#Style-of-Change-Logs" rel="next" title="Style of Change Logs">
<link href="Change-Logs.html#Change-Logs" rel="prev" title="Change Logs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="Change-Log-Concepts"></a>
<div class="header">
<p>
Next: <a href="Style-of-Change-Logs.html#Style-of-Change-Logs" accesskey="n" rel="next">Style of Change Logs</a>, Up: <a href="Change-Logs.html#Change-Logs" accesskey="u" rel="up">Change Logs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Change-Log-Concepts-and-Conventions"></a>
<h4 class="subsection">6.8.1 Change Log Concepts and Conventions</h4>

<a name="index-changeset_002c-in-a-change-log"></a>
<a name="index-batch-of-changes_002c-in-a-change-log"></a>
<p>You can think of the change log as a conceptual &ldquo;undo list&rdquo; which
states how earlier versions were different from the current version.
People can see the current version; they don&rsquo;t need the change log to
tell them what is in it.  What they want from a change log is a clear
explanation of how the earlier version differed.  Each <em>entry</em> in
a change log describes either an individual change or the smallest
batch of changes that belong together, also known as a <em>changeset</em>.
</p>
<a name="index-title_002c-change-log-entry"></a>
<a name="index-header-line_002c-change-log-entry"></a>
<p>It is a good idea to start the change log entry with a <em>header
line</em>: a single line that is a complete sentence which summarizes the
changeset.  If you keep the change log in a VCS, this
should be a requirement, as VCS commands that show the
change log in abbreviated form, such as <kbd>git log --oneline</kbd>, treat
the header line specially.  (In a <samp>ChangeLog</samp> file, the header
line follows a line that says who was the author of the change and
when it was installed.)
</p>
<a name="index-description_002c-change-log-entry"></a>
<p>Follow the change log entry&rsquo;s header line with a description of the
overall change.  This should be as long as needed to give a clear
description.  Pay special attention to aspects of the changeset not
easily gleaned from the diffs or from the names of modified files and
functions: the overall idea of the change and the need for it, and the
relations, if any, between changes made to different files/functions.
If the change or its reasons were discussed on some public forum, such
as the project&rsquo;s issue tracker or mailing list, it is a good idea to
summarize the main points of that discussion in the change&rsquo;s
description, and include a pointer to that discussion or the issue ID
for those who&rsquo;d like to read it in full.
</p>
<p>The best place to explain how parts of the new code work with other code
is in comments in the code, not in the change log.
</p>
<p>If you think that a change calls for explanation of <em>why</em> the
change was needed&mdash;that is, what problem the old code had such that
it required this change&mdash;you&rsquo;re probably right.  Please put the
explanation in comments in the code, where people will see it whenever
they see the code.  An example of such an explanation is, &ldquo;This
function used to be iterative, but that failed when MUMBLE was a
tree.&rdquo;  (Though such a simple reason would not need this kind of
explanation.)
</p>
<p>The best place for other kinds of explanation of the change is in the
change log entry.  In particular, comments usually will not say why
some code was deleted or moved to another place&mdash;that belongs to the
description of the change which did that.
</p>
<p>Following the free-text description of the change, it is a good idea
to give a list of names of the entities or definitions that you
changed, according to the files they are in, and what was changed in
each one.  See <a href="Style-of-Change-Logs.html#Style-of-Change-Logs">Style of Change Logs</a>.  If a project uses a modern
VCS to keep the change log information, as described in
<a href="Change-Logs.html#Change-Logs">Change Logs</a>, explicitly listing the files and functions that
were changed is not strictly necessary, and in some cases (like
identical mechanical changes in many places) even tedious.  It is up
to you to decide whether to allow your project&rsquo;s developers to omit
the list of changed files and functions from the log entries, and
whether to allow such omissions under some specific conditions.
However, while making this decision, please consider the following
benefits of providing the list of changed entities with each change:
</p>
<ul>
<li> Generation of useful <samp>ChangeLog</samp> files from VCS logs
becomes more difficult if the change log entries don&rsquo;t list the
modified functions/macros, because VCS commands cannot
reliably reproduce their names from the commit information alone.  For
example, when there is a change in the header part of a function
definition, the heading of the diff hunk as shown in the VCS log
commands will name the wrong function as being modified (usually, the
function defined before the one being modified), so using those diffs
to glean the names of the modified functions will produce inaccurate
results.  You will need to use specialized scripts, such as gnulib&rsquo;s
<samp>vcs-to-changelog.py</samp>, mentioned below, to solve these
difficulties, and make sure it supports the source languages used by
your project.

</li><li> While modern VCS commands, such as Git&rsquo;s <kbd>git log -L</kbd>
and <kbd>git log -G</kbd>, provide powerful means for finding changes that
affected a certain function or macro or data structure (and thus might
make <samp>ChangeLog</samp> files unnecessary if you have the repository
available), they can sometimes fail.  For example, <kbd>git log -L</kbd>
doesn&rsquo;t support syntax of some programming languages out of the box.
Mentioning the modified functions/macros explicitly allows finding the
related changes simply and reliably.

</li><li> Some VCS commands have difficulties or limitations when
tracking changes across file moves or renames.  Again, if the entities
are mentioned explicitly, those difficulties can be overcome.

</li><li> Users that review changes using the generated <samp>ChangeLog</samp> files
may not have the repository and the VCS commands available
to them.  Naming the modified entities alleviates that problem.
</li></ul>

<p>For these reasons, providing lists of modified files and functions
with each change makes the change logs more useful, and we therefore
recommend to include them whenever possible and practical.
</p>
<p>It is also possible to generate the lists naming the modified entities
by running a script.  One such script is <samp>mklog.py</samp> (written in
Python 3); it is used by the <code>GCC</code> project.  Gnulib provides
another variant of such a script, called <samp>vcs-to-changelog.py</samp>,
part of the <code>vcs-to-changelog</code> module.  Note that these scripts
currently support fewer programming languages than the manual commands
provided by Emacs (see <a href="Style-of-Change-Logs.html#Style-of-Change-Logs">Style of Change Logs</a>).  Therefore, the
above mentioned method of generating the <code>ChangeLog</code> file from
the VCS commit history, for instance via the
<code>gitlog-to-changelog</code> script, usually gives better
results&mdash;provided that the contributors stick to providing good
commit messages.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Style-of-Change-Logs.html#Style-of-Change-Logs" accesskey="n" rel="next">Style of Change Logs</a>, Up: <a href="Change-Logs.html#Change-Logs" accesskey="u" rel="up">Change Logs</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- The GNU coding standards, last updated May 26, 2024.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Free
Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>System Portability (GNU Coding Standards)</title>

<meta name="description" content="System Portability (GNU Coding Standards)">
<meta name="keywords" content="System Portability (GNU Coding Standards)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Writing-C.html#Writing-C" rel="up" title="Writing C">
<link href="CPU-Portability.html#CPU-Portability" rel="next" title="CPU Portability">
<link href="Names.html#Names" rel="prev" title="Names">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="System-Portability"></a>
<div class="header">
<p>
Next: <a href="CPU-Portability.html#CPU-Portability" accesskey="n" rel="next">CPU Portability</a>, Previous: <a href="Names.html#Names" accesskey="p" rel="prev">Names</a>, Up: <a href="Writing-C.html#Writing-C" accesskey="u" rel="up">Writing C</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Portability-between-System-Types"></a>
<h3 class="section">5.5 Portability between System Types</h3>
<a name="index-portability_002c-between-system-types"></a>

<p>In the Unix world, &ldquo;portability&rdquo; refers to porting to different Unix
versions.  For a GNU program, this kind of portability is desirable, but
not paramount.
</p>
<p>The primary purpose of GNU software is to run as part of the GNU
operating system, compiled with GNU compilers, on various types of
hardware.  So the kinds of portability that are absolutely necessary
are quite limited.  It is important to support Linux-based GNU
systems, since they are the form of GNU that people mainly use.
</p>
<p>Making a GNU program operate on operating systems other than the GNU
system is not part of the core goal of developing a GNU package.  You
don&rsquo;t ever have to do that.  However, users will ask you to do that,
and cooperating with those requests is useful&mdash;as long as you don&rsquo;t
let it dominate the project or impede the primary goal.
</p>
<p>It is good to support the other free or nearly free operating systems
(for instance, *BSD).  Supporting a variety of Unix-like systems is
desirable, although not paramount.  It is usually not too hard, so you
may as well do it.  But you don&rsquo;t have to consider it an obligation,
if it does turn out to be hard.
</p>
<p>For the most part it is good to port the program to more platforms,
but you should not let take up so much of your time that it hinders
you from improving the program in more central ways.  If it starts to
do that, please tell users that you don&rsquo;t want to spend any more
time on this&mdash;someone else must write that code, debug it, document
it, etc., and then you can install it.
</p>
<p>You can reject porting patches for technical reasons too, as with any
other patch that users submit.  It is up to you.
</p>
<a name="index-autoconf"></a>
<p>The easiest way to achieve portability to most Unix-like systems is to
use Autoconf.  It&rsquo;s unlikely that your program needs to know more
information about the host platform than Autoconf can provide, simply
because most of the programs that need such knowledge have already been
written.
</p>
<p>Avoid using the format of semi-internal data bases (e.g., directories)
when there is a higher-level alternative (<code>readdir</code>).
</p>
<a name="index-non_002dPOSIX-systems_002c-and-portability"></a>
<p>As for systems that are not like Unix, such as MS-DOS, Windows, VMS, MVS,
and older Macintosh systems, supporting them is often a lot of work.
When that is the case, it is better to spend your time adding features
that will be useful on GNU and GNU/Linux, rather than on supporting
other incompatible systems.
</p>
<p>If you do support Windows, please do not abbreviate it as &ldquo;win&rdquo;.
See <a href="Trademarks.html#Trademarks">Trademarks</a>.
</p>
<p>Usually we write the name &ldquo;Windows&rdquo; in full, but when brevity is
very important (as in file names and some symbol names), we abbreviate
it to &ldquo;w&rdquo;.  In GNU Emacs, for instance, we use &lsquo;<samp>w32</samp>&rsquo; in file
names of Windows-specific files, but the macro for Windows
conditionals is called <code>WINDOWSNT</code>.  In principle there could
also be &lsquo;<samp>w64</samp>&rsquo;.
</p>
<p>It is a good idea to define the &ldquo;feature test macro&rdquo;
<code>_GNU_SOURCE</code> when compiling your C files.  When you compile on GNU
or GNU/Linux, this will enable the declarations of GNU library extension
functions, and that will usually give you a compiler error message if
you define the same function names in some other way in your program.
(You don&rsquo;t have to actually <em>use</em> these functions, if you prefer
to make the program more portable to other systems.)
</p>
<p>But whether or not you use these GNU extensions, you should avoid
using their names for any other meanings.  Doing so would make it hard
to move your code into other GNU programs.
</p>
<hr>
<div class="header">
<p>
Next: <a href="CPU-Portability.html#CPU-Portability" accesskey="n" rel="next">CPU Portability</a>, Previous: <a href="Names.html#Names" accesskey="p" rel="prev">Names</a>, Up: <a href="Writing-C.html#Writing-C" accesskey="u" rel="up">Writing C</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

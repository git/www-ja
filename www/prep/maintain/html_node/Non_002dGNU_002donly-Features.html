<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Information for maintainers of GNU software, last updated June 8, 2023.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009,
2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2019, 2022
Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Non-GNU-only Features (Information for Maintainers of GNU Software)</title>

<meta name="description" content="Non-GNU-only Features (Information for Maintainers of GNU Software)">
<meta name="keywords" content="Non-GNU-only Features (Information for Maintainers of GNU Software)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Patches-Not-to-Accept.html#Patches-Not-to-Accept" rel="up" title="Patches Not to Accept">
<link href="Interoperation-with-Nonfree.html#Interoperation-with-Nonfree" rel="next" title="Interoperation with Nonfree">
<link href="Patches-Not-to-Accept.html#Patches-Not-to-Accept" rel="prev" title="Patches Not to Accept">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="Non_002dGNU_002donly-Features"></a>
<div class="header">
<p>
Next: <a href="Interoperation-with-Nonfree.html#Interoperation-with-Nonfree" accesskey="n" rel="next">Interoperation with Nonfree</a>, Up: <a href="Patches-Not-to-Accept.html#Patches-Not-to-Accept" accesskey="u" rel="up">Patches Not to Accept</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Don_0027t-Install-a-Feature-Till-It-Works-on-GNU"></a>
<h3 class="section">9.1 Don&rsquo;t Install a Feature Till It Works on GNU</h3>

<p>Please <em>don&rsquo;t</em> write or install code for features that would have
worse or less functionality (or none) on the GNU system than on some
non-GNU systems.
</p>
<p>The primary purpose of any GNU program is to enhance the capability of
the GNU system to give users freedom, so every feature of a GNU
package should be usable and useful on free distributions of the GNU
operating system (<a href="https://www.gnu.org/distros/">https://www.gnu.org/distros/</a>).  For this
purpose, a &ldquo;feature&rdquo; is an operation which does something
substantially useful for the user and not the technical details of an
implementation.  We explain this point further below.
</p>
<p>A feature that functions only on, or functions better on, some non-GNU
operating system would undermine that primary purpose; worse, it would
promote that non-GNU system at the expense of GNU.  Such a situation
would work directly against the goal of liberating users from those
systems, even though installing features that create such a situation
would be seen as desirable in terms of the &ldquo;open source&rdquo; philosophy.
</p>
<p>Since freedom in use of computing is the overall purpose, we need to
aim clearly for that freedom.  We need to show with our practical
decisions&mdash;and with our explanations of them&mdash;that we&rsquo;re working for
something deeper and more important than &ldquo;better software&rdquo; and
&ldquo;more convenience.&rdquo;  That will give users a chance to reflect about
our reasons for taking a path different from what most programmers
would do.  See
<a href="https://www.gnu.org/philosophy/open-source-misses-the-point.html">https://www.gnu.org/philosophy/open-source-misses-the-point.html</a>.
</p>
<p>Therefore, when you as a GNU maintainer receive contributions of
features that do not work on the GNU system, please explain this rule
and why it is necessary.  Explain that we need all features in GNU
packages to work properly on the GNU system, so that they potentiate
each other and make the GNU system better.  Thus, the change should
not be installed in its present form.
</p>
<p>That doesn&rsquo;t mean the change can&rsquo;t be installed <em>ever</em>.  It could
be installed later, if and when equally good support on the GNU system
for the same feature can be installed at the same time.  Explaining
this is a good opportunity to ask people to help write the code to
support the feature on GNU.  Also inform the contributor about
resources to learn about how to support this feature on GNU, so perse
could consider doing that job&mdash;or recruiting others to help.
</p>
<p>If parts of the code are system-independent, and will do part of the
job of supporting the feature on the GNU system, you can install them
right away.  Or you can put them in the package repo without actually
installing them, in a &lsquo;<samp>wip-<var>name</var></samp>&rsquo; branch.  Having them in
the repository may help and encourage people to finish implementing
the feature on GNU.
</p>
<p>If you think it is very important, you can implement the support for
that feature on the GNU system yourself.  If you think it is highly
desirable to have that feature on GNU someday, you can make special
arrangements to put the non-GNU system-specific code in the package
repo but not install it&mdash;see <a href="Uninstalled-Code-in-Repo.html#Uninstalled-Code-in-Repo">Uninstalled Code in Repo</a>.
</p>
<p>It is ok to implement or support a feature for non-GNU systems if the
feature works at least as well on GNU, even if it is implemented
differently on different systems, uses different system facilities in
its implementation, or looks different to the user in some details.
It is ok to implement those little details, on each system, in the way
that is convenient or conventional for making the features work.  The
point is that the program and the feature should &ldquo;run best on GNU.&rdquo;
</p>
<p>If system facilities on some other system provide, without any special
application code, a feature not available on GNU, there is no need
to do work to prevent it from functioning.  In that case, we should
work to implement that feature in GNU.
</p>
<p>We don&rsquo;t consider the little details of interfaces to control or
configure specific operations, or details of implementing operations,
as &ldquo;features.&rdquo;  Likewise, a system facility (including libraries
that come with the system) is a means for implementing features but
use of the facility is not in itself a feature.
</p>
<p>For instance, a programming platform often offers an interface to
control the computer or the operating system at a low level.  It is OK
to support the feature of low-level control on a non-GNU system
provided the package supports the same capabilities on the GNU system
also, even if the details of how to invoke the feature vary from
system to system.  But do undertake to make the invocation of this
feature consistent across systems, for the specific actions that are
supported on multiple systems.
</p>
<p>Features mainly for communicating with other users&rsquo; computers, or
between computers not set up for tightly-coupled use as a group, are a
different matter entirely.  A communication feature is truly &ldquo;the
same feature&rdquo; as on GNU only if it interoperates with a free distribution
of the GNU system&mdash;as, for instance, TCP/IP does.  Unportable,
system-specific communication facilities for non-GNU systems are abuse
of the community, so don&rsquo;t install support for them.  This point also
applies to file formats used for communication between programs, if
there is ever an occasion to move those files between unrelated
computers.  (Exception: it is admirable to write code to extract the user&rsquo;s
data from such a file, if you can do it.)
</p>
<p>Finally, please be careful not to let installing or supporting
system-specific code for non-GNU systems consume much of your own
time.  See <a href="http://www.gnu.org/prep/standards/html_node/System-Portability.html#System-Portability">GNU Coding Standards</a> in <cite>GNU Coding Standards</cite>.
</p>
<p>Suppose people ask for support for feature F on some non-GNU system,
and feature F does work on GNU.  You can say yes if you wish, but you
have no obligation to write or maintain that code.  You can tell them
that it&rsquo;s their responsibility to write it and maintain it.  If they
write good clean code for it, you can approve its installation, but
that doesn&rsquo;t mean you or anyone else is obliged to support it.  If
someday the code suffers from bitrot, you can delete it if users don&rsquo;t
fix it.
</p>
<p>See <a href="Suggested-Responses.html#Suggested-Responses">Suggested Responses</a>, for some text you can use or adapt, if you
like, to say no to these patches.  It aims to invite them to support
the GNU system equally well in the new feature.  If there is no hope
of that, just &ldquo;No thanks&rdquo; is enough.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Interoperation-with-Nonfree.html#Interoperation-with-Nonfree" accesskey="n" rel="next">Interoperation with Nonfree</a>, Up: <a href="Patches-Not-to-Accept.html#Patches-Not-to-Accept" accesskey="u" rel="up">Patches Not to Accept</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

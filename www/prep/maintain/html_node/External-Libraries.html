<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- Information for maintainers of GNU software, last updated June 8, 2023.

Copyright (C) 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009,
2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2019, 2022
Free Software Foundation, Inc.

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover
Texts.  A copy of the license is included in the section entitled
"GNU Free Documentation License". -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>External Libraries (Information for Maintainers of GNU Software)</title>

<meta name="description" content="External Libraries (Information for Maintainers of GNU Software)">
<meta name="keywords" content="External Libraries (Information for Maintainers of GNU Software)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Legal-Matters.html#Legal-Matters" rel="up" title="Legal Matters">
<link href="Crediting-Authors.html#Crediting-Authors" rel="next" title="Crediting Authors">
<link href="License-Notices-for-Other-Files.html#License-Notices-for-Other-Files" rel="prev" title="License Notices for Other Files">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="/software/gnulib/manual.css">


</head>

<body lang="en">
<a name="External-Libraries"></a>
<div class="header">
<p>
Next: <a href="Crediting-Authors.html#Crediting-Authors" accesskey="n" rel="next">Crediting Authors</a>, Previous: <a href="License-Notices.html#License-Notices" accesskey="p" rel="prev">License Notices</a>, Up: <a href="Legal-Matters.html#Legal-Matters" accesskey="u" rel="up">Legal Matters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="External-Libraries-1"></a>
<h3 class="section">6.7 External Libraries</h3>

<p>As maintainer of an FSF-copyrighted GNU package, how do you use
separately-published general-purpose free modules?  (We also call them
&ldquo;libraries&rdquo; because we are using them as libraries; it doesn&rsquo;t
matter whether they are packaged as libraries or not.)
</p>
<p>It would be unreasonable to ask their authors to assign copyright to
the FSF.  They didn&rsquo;t write those modules as contributions to GNU.  We
just happen to want to use them, as any developer might.  It would be
rude to ask developers, out of the blue, to give the FSF their
copyright.  Please don&rsquo;t ask for that in cases like these.
</p>
<p>The proper way to use those modules is to link your package with them
and say they are <em>not</em> part of your package.  See below for the
mechanics of this.
</p>
<p>To avoid present or future legal trouble, you must make sure the
license of the module is compatible with current <em>and future</em> GPL
versions.  &ldquo;GNU GPL version 3 or later&rdquo; is good, and so is anything
which includes permission for use under those GPL versions (including
&ldquo;GNU GPL version 2 or later&rdquo;, &ldquo;LGPL version <var>n</var> or later&rdquo;,
&ldquo;LGPL version 2.1&rdquo;, &ldquo;GNU Affero GPL version 3 or later&rdquo;).  Lax
permissive licenses are ok too, since they are compatible with all GPL
versions.
</p>
<p>&ldquo;GPL version 2 only&rdquo; is obviously unacceptable because it is
incompatible with GPL version 3.  &ldquo;GPL version 3 only&rdquo; and &ldquo;GPL
version 2 or 3 only&rdquo; have a subtler problem: they would be incompatible
with GPL version 4, if we ever make one, so the module would become an
obstacle to upgrading your package&rsquo;s license to &ldquo;GPL version 4 or
later&rdquo;.  Don&rsquo;t use such modules.
</p>
<p>One library you need to avoid is <code>goffice</code>, since it allows only
GPL versions 2 and 3.
</p>
<p>So, here are the mechanics of how to arrange your package to use the
unrelated free module.
</p>
<ol>
<li> Assume the module is already installed on the system, and link with it
when linking your program.  This is only reasonable if the module
really has the form of a library.

</li><li> Include the module in your package&rsquo;s distribution, putting the source
in a separate subdirectory whose <samp>README</samp> file says, &ldquo;This is
not part of the GNU FOO program, but is used with GNU FOO.&rdquo;  Then set
up your makefiles to build this module and link it into the
executable.

<p>With this method, it is not necessary to treat the module as a library
and make a &lsquo;<samp>.a</samp>&rsquo; file from it.  You can link directly with the
&lsquo;<samp>.o</samp>&rsquo; files in the usual manner.
</p></li></ol>

<p>Both of these methods create an irregularity, and our lawyers have
told us to minimize the amount of such irregularity.  So use these
methods only for general-purpose modules that were <em>not</em> written
for your package.  For anything that was written as a contribution to
your package, please get papers signed.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Crediting-Authors.html#Crediting-Authors" accesskey="n" rel="next">Crediting Authors</a>, Previous: <a href="License-Notices.html#License-Notices" accesskey="p" rel="prev">License Notices</a>, Up: <a href="Legal-Matters.html#Legal-Matters" accesskey="u" rel="up">Legal Matters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>

# Russian translation of http://www.gnu.org/software/recent-releases.html
# Copyright (C) 2018 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Ineiev <ineiev@gnu.org>, 2013, 2014, 2015, 2017, 2018
# this translation lacks appropriate review
#
msgid ""
msgstr ""
"Project-Id-Version: recent-releases.html\n"
"POT-Creation-Date: 2018-01-01 06:00+0000\n"
"PO-Revision-Date: 2018-01-01 17:51+0000\n"
"Last-Translator: Ineiev <ineiev@gnu.org>\n"
"Language-Team: Russian <www-ru-list@gnu.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Recent Software Releases - GNU Project - Free Software Foundation"
msgstr ""
"Последние выпуски программ - Проект GNU - Фонд свободного программного "
"обеспечения"

#. type: Content of: <h2>
msgid "Recent Software Releases"
msgstr "Последние выпуски программ"

#. type: Content of: <p>
msgid ""
"Here is a list of recent GNU releases, with links to their announcements on "
"the <a href=\"http://lists.gnu.org/mailman/listinfo/info-gnu\">info-gnu</a> "
"mailing list.  (Releases not announced are not listed here.)"
msgstr ""
"Вот список последних выпусков GNU со ссылками на их объявления в списке "
"рассылки <a href=\"http://lists.gnu.org/mailman/listinfo/info-gnu\"> info-"
"gnu</a> (неанонсированные выпуски здесь не перечисляются)."

#. type: Content of: <p>
msgid ""
"Anyone is welcome to subscribe to the info-gnu list.  Other sources of "
"announcements are the <a href=\"http://planet.gnu.org\"> planet.gnu.org</a> "
"RSS feed and the monthly <a href=\"http://www.fsf.org/free-software-"
"supporter/\"> Free Software Supporter</a> newsletter."
msgstr ""
"Подписаться на список рассылки info-gnu может каждый. Другие источники "
"объявлений&nbsp;&mdash; лента RSS <a href=\"http://planet.gnu.org\"> planet."
"gnu.org</a> и ежемесячник <a href=\"http://www.fsf.org/free-software-"
"supporter/\"> Free Software Supporter</a>."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Пожалуйста, присылайте общие запросы фонду и GNU по адресу <a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Есть также <a href=\"/contact/"
"\">другие способы связаться</a> с фондом. Отчеты о неработающих ссылках и "
"другие поправки или предложения можно присылать по адресу <a href=\"mailto:"
"webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Мы старались сделать этот перевод точным и качественным, но исключить "
"возможность ошибки мы не можем. Присылайте, пожалуйста, свои замечания и "
"предложения по переводу по адресу <a href=\"mailto:web-translators@gnu.org"
"\">&lt;web-translators@gnu.org&gt;</a>. </p><p>Сведения по координации и "
"предложениям переводов наших статей см. в <a href=\"/server/standards/README."
"translations.html\">&ldquo;Руководстве по переводам&rdquo;</a>."

# type: Content of: <div><p>
#. type: Content of: <div><p>
msgid "Copyright &copy; 2017, 2018 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2017, 2018 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Это произведение доступно по <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.ru\">лицензии Creative Commons "
"Attribution-NoDerivs (<em>Атрибуция&nbsp;&mdash; Без производных "
"произведений</em>) 4.0 Всемирная</a>."

# type: Content of: <div><div>
#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<em>Внимание! В подготовке этого перевода участвовал только один человек. Вы "
"можете существенно улучшить перевод, если проверите его и расскажете о "
"найденных ошибках в <a href=\"http://savannah.gnu.org/projects/www-ru"
"\">русской группе переводов gnu.org</a>.</em>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Обновлено:"

# type: Content of: <div><p>
#~ msgid "Copyright &copy; 2017 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2017 Free Software Foundation, Inc."

# type: Content of: <div><p>
#~ msgid "Copyright &copy; 2014 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014 Free Software Foundation, Inc."

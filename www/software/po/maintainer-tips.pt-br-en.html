<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.86 -->
<title>Tips for new GNU maintainers
- GNU Project - Free Software Foundation</title>
<style type="text/css" media="print,screen"><!--
.all { color: red }
.new { color: purple }
.adopted { color: green }
--></style>
 <!--#include virtual="/software/po/maintainer-tips.translist" -->
<!--#include virtual="/server/banner.html" -->

<h2>Tips for new GNU maintainers</h2>

<p>If you are new to maintaining a GNU package, whether one that you
have offered to GNU or an existing one that you have adopted, it can be
overwhelming to know where to start.  The official email notice you
received when you became a GNU maintainer has lots of details; this
document is not a replacement for that email, but rather a complement,
aiming to provide some tips on getting started.</p>

<p>Of course, what's listed here just skims the surface of GNU
maintainership.  Please be sure to read the <a
href="/prep/maintain/">GNU Maintainers Guide</a> and the <a
href="/prep/standards/">GNU Coding Standards</a>.  Indeed, you should
have read them already, but they are densely enough written that careful
re-readings are useful.  In addition, a few experienced GNU contributors
have volunteered to answer questions about GNU maintenance via
&lt;mentors@gnu.org&gt; as well as &lt;maintainers@gnu.org&gt;.</p>


<h3>First steps for new maintainers</h3>

<p>These tasks are listed in order of priority. The labels
(<span class="all">[All]</span>, <span class="new">[New]</span>, and
<span class="adopted">[Adopted]</span>) indicate the category of packages
each task mostly applies to.</p>

<ol>
<li><span class="all">[All]</span> <b>Update project information on
 Savannah.</b>  Go to your project page
(<code>https://sv.gnu.org/projects/PKG</code>, where PKG is the name of your
package), log in with your Savannah ID, and check under &ldquo;Update public
info&rdquo; in the &ldquo;Main&rdquo; menu.  Here, you should set the project's
full name and, if needed, write both a short and a long description for
it (please also send those descriptions to &lt;maintainers@gnu.org&gt; for use
in the lists of <a href="/software/software.html#allgnupkgs">all GNU
packages</a>).  You should also set its development status to reflect
the maturity of the code.  It is essential to do this if you have
adopted a package, since it will be marked as &ldquo;Orphan&rdquo;.</li>

<li><span class="all">[All]</span> <b>Turn to the mailing lists.</b>  If you
have a new GNU package, you should set up at least one mailing list for the
package (&ldquo;Select features&rdquo; in the Main menu).  It is strongly
recommended to have one called &lt;bug-PKG@gnu.org&gt;;
others can wait until traffic warrants.  If
you have adopted an existing package, send an email introducing
yourself.  Finally, whether lists are newly created or already existing,
don't forget to subscribe yourself, as this is not automatically done.
(<a href="/prep/maintain/html_node/Mail.html">About dealing with
mail</a>.)</li>

<li><span class="new">[New]</span> <b>Set up a repository for your source
code</b> (&ldquo;Select features&rdquo; in the Main menu), and import
whatever is available (look up the procedures under &ldquo;Source Code
Manager&rdquo; in the Development Tools section). Experience has shown
that self-hosting is unreliable. (<a
href="/prep/maintain/html_node/Old-Versions.html#Old-Versions">About back-up
files</a>.)</li>

<li><span class="new">[New]</span> <b>Upload your package to the GNU FTP
site</b> if it is ready for public release. (<a
href="/prep/maintain/html_node/Automated-FTP-Uploads.html">About the upload
procedure</a>.)</li>

<li><span class="new">[New]</span> <b>Create a GNU home page</b>, to replace
the <code>PKG.html</code> file that webmasters have placed in your web repo
(keep the same name). If you want to make this quick and easy, use our
standard <a href="/server/standards/boilerplate.html">template</a>. Comment
out the irrelevant parts, such as Downloading and Documentation if the
project has barely started; you'll complete them later on. Also put any
 documentation you already have in a subdirectory called
&ldquo;manual&rdquo;. (<a
href="/prep/maintain/html_node/Web-Pages.html">About web pages</a>.)</li>

<li><span class="adopted">[Adopted]</span> <b>Check for existing bug
reports</b> if you have adopted an existing package.  These may
be on any or all of a Savannah bug tracker, a <a
href="https://lists.gnu.org/">mailing list</a> (mbox archives can be
downloaded by <a
href="https://lists.gnu.org/archive/mbox/">HTTPS</a>), or the <a
href="https://debbugs.gnu.org/">GNU debbugs server</a>.  (<a
href="/prep/maintain/html_node/Replying-to-Mail.html">About replying to
bug reports</a>.)</li>

<li><span class="adopted">[Adopted]</span> <b>Contact distro packagers.</b>
If you have adopted
an existing package and it is available in downstream distros, get in
contact with the packagers.  It is likely that they have unresolved bug
reports to be addressed, and perhaps even patches that should be
applied.  You may also like to get involved with the <a
href="/software/guix/">GNU Guix</a> package manager.  (<a
href="/distros/distros.html">About distros</a>.)</li>

<li><span class="adopted">[Adopted]</span> <b>Use the software.</b>
It should go without saying that if you
have adopted a GNU package, you should use it to get a feel for its
current status and to discover what might need to be fixed.</li>

<li><span class="all">[All]</span> <b>Pick some tasks and start hacking!</b>
There's no substitute for spending time doing the actual work.</li>
</ol>


<h3>General tips on maintaining GNU software</h3>

<ul>
<li><b>Aim for a quick first release.</b>  Whether your package is
new or you've adopted an older one, your number one priority should be
making a first release as soon as possible.  If you've adopted a
package, it's sufficient to fix a few bugs and push that out.  In fact,
just updating the infrastructure files (e.g., Autoconf/Automake, gnulib
modules, etc.) is enough to justify a new release when you've first
taken over a package.  If your package is new, don't be afraid to put
out an early version that is not yet feature complete.  In any case, a
release (with <a
href="/prep/maintain/html_node/Announcements.html">proper
announcements</a>) will draw attention to your package in the form of
potential users or even other developers.  It is the best way to let
people know that an old package has been revived or that a new package
exists at all.  (<a
href="/prep/maintain/html_node/Distributions.html">About making
releases</a>.)</li>

<li><b>Focus on fixing outstanding bugs first.</b>  This is an excellent
way to start learning a new codebase, and get a
feel for how the system works.  Besides, new features shouldn't be given
much focus if notable problems already exist.  (<a
href="/software/devel.html">About resources for GNU
developers</a>.)</li>

<li><b>Don't try to rewrite the entire thing.</b>  If you've adopted
a package, we strongly recommend that you not set out to rewrite the
whole program from scratch.  While always tempting, and an easy thing to
start, completing it successfully requires a colossal effort, and
experience has shown that it is an almost sure-fire way to become
demotivated and lead to eventual (re-)stagnation of the project.
Instead, focus on incremental improvements.  Once you've become
intimately familiar with the package, you will be in a better position
to consider more radical changes.</li>

<li><b>You're in charge!</b>  As a maintainer, you look after a
package as part of the overall GNU project.  GNU depends on you to take
care of <a href="/prep/maintain/html_node/Legal-Matters.html">legal
matters</a>, make new releases, keep the <a
href="/prep/maintain/html_node/Web-Pages.html">web pages</a> updated,
reply to bug reports and otherwise communicate with users, <a
href="/prep/maintain/html_node/Clean-Ups.html">handle patches
appropriately</a>, and all else.  This is your privilege and your
responsibility.  Please help us maintain an active and stable collection
of software.  If you have questions or run into problems, do not
hesitate to get in touch via &lt;maintainers@gnu.org&gt;.  (<a
href="/prep/maintain/html_node/Recruiting-Developers.html">About
recruiting developers</a>.)</li>

<li><b>Get involved.</b>  GNU consists entirely of volunteers and
your participation in the organization is what you make of it!  As a
maintainer, the communication received from us (read: pestering) will be
low-volume.  Being active within GNU is a great way to increase your
contact and exposure to other like-minded volunteers.  If you find an
aspect of GNU in which you would like to be involved, there is almost
certainly room for contribution.  (<a href="/help/help.html">About
helping GNU and free software</a>.)</li>

</ul>

<p>To conclude this list with one final reiteration: the information and
links above are just a sampling.  Please refer to and (re)read the full
<a href="/prep/maintain/">GNU Maintainer Information</a> and <a
href="/prep/standards/">GNU Coding Standards</a> documents for plenty
more.</p>


<h3>GNU Philosophy</h3>

<p>This also seems like an appropriate page on which to give some links
to the basic ideas of GNU and free software:</p>

<ul>
<li><a href="/gnu/the-gnu-project.html">The GNU Project</a></li>

<li><a href="/philosophy/free-sw.html">What is free software?</a>
(the free software definition)</li>

<li><a href="/philosophy/categories.html">Categories of free and nonfree
software</a></li>

<li><a href="/philosophy/compromise.html">Avoiding ruinous compromises</a></li>

<li><a href="/philosophy/words-to-avoid.html">Words to avoid (or use
with care) because they are loaded or confusing</a></li>

<li><a href="/gnu/linux-and-gnu.html">Linux and the GNU System</a></li>

<li><a href="/gnu/gnu-linux-faq.html">GNU/Linux FAQ</a></li>

<li><a href="/philosophy/open-source-misses-the-point.html">Why
Open Source misses the point of Free Software</a></li>

</ul>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" --> <div id="footer"> <div
class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and submitting translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and submitting translations
of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2014, 2017, 2018, 2019 Free Software Foundation, Inc.</p>

<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2020/03/01 22:59:56 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>

#! /usr/bin/awk -f

# List targets for malware items (specific id's on <h?> or <div>) for
# each page of proprietary/

# Copyright (C) 2018 Free Software Foundation, Inc.

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty. 

# Written by Ineiev <ineiev@gnu.org>.


BEGIN { target_no = 0; have_list = 0 }
/<h[23456]/ || /<div[ \t]*class[ \t]*=[ \t]*"column-limit"/ {
  target_no++
  idx = match($0, /id[ \t]*=[ \t]*('[^>]*'|"[^>]*")/)
  if (idx)
    {
      sub(/.*<(div|h[23456])[^>]*id[ \t]*=[ \t]*/, "")
      q = substr($0, 1, 1)
      name = substr($0, 2)
      idx = index(name, q)
      name = substr(name, 1, idx - 1)
      target_name = name
    }
  have_list = 0
}
/^<ul class[ \t]*=[ \t]*"blurbs"/ {
  if (target_name == "")
    # Unnamed target, identified by number.
    target_name = "@" target_no
  if (have_list)
    print "Multiple blurb lists in target No " target_no \
          " '" target_name "'" >> "/dev/stderr"
  target_names = target_names " " target_name
  have_list = 1
}
END { print substr(target_names, 2) }

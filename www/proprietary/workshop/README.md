Copyright (C) 2018, 2019, 2022 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
--------------------------------------------------------------------------

#                              MALWARE HOWTO


## What's in this directory

  It contains ingredients and tools for adding new malware items to
  proprietary/, and regenerating malware lists.
  You are more than welcome to improve them.

 *Scripts*

  malgen has 3 main functions:
     * regenerating malware lists in malware-xx.html and
       proprietary-xx.html (with the help of list-targets.awk);
     * listing 5 recently added items in proprietary.html;
     * updating malware statistics in proprietary.html and
       free-software-even-more-important.html (with the help of
       update-item-count).

  item-create reformats a new item, and calls malgen to add it to the
     relevant pages.

  list-targets.awk update-item-count are malgen's helpers.

 *Text files*

  mal.rec has the text of each item and meta-information about it
     (see detailed description below), in Recutils format.

  targets.rec records all the possible targets for item addition. This
     file is used by malgen, and is updated automatically at each run
     by list-targets.awk.

  item-start is the template for writing a new item.

  item-pending contains the RT references and dates for items that should
     not be published yet. This file should not contain any actual
     malware descriptions. Its only purpose is to easily retrieve the
     corresponding tickets.


## How to create a malware item

  1. Copy "item-start" to "item".

  2. Complete the Id, RT, PubDate (one for each reference), Target (one
     for each page the item is supposed to go to), and optionally Keywords
     fields, as explained below.

  3. Write the HTML text in the Blurb field.

  4. Run "item-create" without argument.

  5. If for some reason addition fails, either totally or partially, edit
     "item", and run item-create again.

  **Note**: item-create can't be used to edit items that have been
  successfully added.


## How to complete the top fields in "item"

  *Note*: top fields in mal.rec are inserted automatically by item-create.

 *Added*

     Date of addition of the item, inserted automatically by item-create.

 *PubDate* (one or more)

     Publication or last-modification date for each article. If this date
     is not on the page itself, it can usually be found in the source
     code.

 *Id* (exactly one)

     A 9-digit integer, made from the publication date of the latest
     reference in the  blurb and a disambiguation digit. For instance,
     "Id: 201805300" if the latest reference was published on
     May 30th, 2018.

     Another blurb on a different topic with the same latest-publication
     date would have "Id: 201805301", and a third one "Id: 201805302".

     Variants of these 3 blurbs (e.g., different wording in malware-* and
     proprietary-*) would have "Id: 201805300.1", "Id: 201805301.1", and
     "Id: 201805302.1" respectively.

 *RT*

     Ticket numbers and other references for this item, *on one line*.

 *Target* (one or more)

     This field determines which page and which page section (if any)
     the item will go to. All the possible targets are listed in
     targets.rec (automatically regenerated at each run of item-create).

     - If the page doesn't have sections, it has only one list of malware
       items. The Target field only needs to contains the name of the HTML
       file. For example:
                 Target: malware-adobe.html
                 Target: proprietary-drm.html

       The target id will be added by item-create:
                 Target: malware-adobe.html malware-adobe
                 Target: proprietary-drm.html proprietary-drm

     - If the page has sections (i.e. several lists), the Target field
       contains 2 words: the name of the HTML file, and the target id.
       A list of these id's is in target.rec. For example:
                 Target: malware-microsoft.html back-doors
                 Target: proprietary-back-doors spy

 *Keywords*

     Any keywords that might be useful some day. This field can be left
     empty.


## How to edit an already added item

  1. Edit its entry in mal.rec.

  2. Regenerate all pages by running malgen without argument. This has
     the additional effect of re-sorting the items, updating targets.rec,
     and removing old entries from the Latest Additions list in
     proprietary.html.


## How to split a page into new sections

  1. *Leave the items where they are*.

  2. Create new sections with the proper id's. The cosmetic div that is
     labeled with the old page id will become the first header, and the
     first section will contain all the items. The other sections will have
     empty lists.

     For example, to split malware-appliances into sections (say 
     back-doors, drm, etc.):

     - replace 
       <div class="column-limit" id="malware-appliances"></div>
       with
       <h3 id="back-doors">Back Doors</h3>
       <p>explanations, if any</p>

     - create the other sections:
       <h3 id="drm">DRM</h3>
       <p>explanations, if any</p>
       <ul class="blurbs">
       </ul>
       etc.
       *Note:* <ul class="blurbs"> and </ul> should be on different lines.

  3. In mal.rec, search for the items that belong to the page being split,
     then change the corresponding target according to the Blurb and other
     Target fields.

     In the preceding example, search for "malware-appliances". This
     will select the lines:
                 Target: malware-appliances.html malware-appliances

     If the item belongs to back-doors, replace this with
                 Target: malware-appliances.html back-doors

     If the item belongs to DRM, replace this with
                 Target: malware-appliances.html drm

  4. Regenerate the page (or all pages) with malgen.


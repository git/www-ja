# German translation of https://gnu.org/fry/happy-birthday-to-gnu.html.
# Copyright (C) 2008, 2014, 2018, 2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Jоегg Kоhпе <joeko (AT) online [PUNKT] de>, 2011, 2013, 2014, 2018, 2019.
# April 2021: unfuzzify (T. Godefroy).
#
msgid ""
msgstr ""
"Project-Id-Version: happy-birthday-to-gnu.html\n"
"Report-Msgid-Bugs-To: Webmasters <webmasters@gnu.org>\n"
"POT-Creation-Date: 2022-04-15 08:27+0000\n"
"PO-Revision-Date: 2019-01-19 22:00+0100\n"
"Last-Translator: Jоегg Kоhпе <joeko (AT) online [PUNKT] de>\n"
"Language-Team: German <www-de-translators@gnu.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2022-04-15 08:27+0000\n"

#. type: Content of: <title>
msgid ""
"Stephen Fry - Happy birthday to GNU - GNU Project - Free Software Foundation"
msgstr ""
"Stephen Fry: „Alles Gute zum Geburtstag, GNU!“ - GNU-Projekt - Free Software "
"Foundation"

#. type: Attribute 'content' of: <meta>
msgid ""
"GNU, FSF, Free Software Foundation, Linux, Emacs, GCC, Unix, Free Software, "
"Operating System, GNU Kernel, HURD, GNU HURD, Hurd"
msgstr " "

#. type: Attribute 'content' of: <meta>
msgid ""
"Since 1983, developing the free Unix style operating system GNU, so that "
"computer users can have the freedom to share and improve the software they "
"use."
msgstr " "

#. type: Content of: <h2>
msgid "Freedom Fry &mdash; &ldquo;Happy birthday to GNU&rdquo;"
msgstr "Stephen Fry: „Alles Gute zum Geburtstag, GNU!“"

#. type: Content of: <p>
msgid ""
"Mr. Stephen Fry introduces you to free software, and reminds you of a very "
"special birthday."
msgstr ""
"Stephen Fry stellt Freie Software vor und erinnert an einen ganz besonderen "
"Geburtstag."

#. type: Content of: <ul><li>
# | <a [-href=\"happy-birthday-to-gnu-download.html\">Download-]
# | {+href=\"/fry/happy-birthday-to-gnu-download.html\">Download+} video</a>
#, fuzzy
#| msgid "<a href=\"happy-birthday-to-gnu-download.html\">Download video</a>"
msgid "<a href=\"/fry/happy-birthday-to-gnu-download.html\">Download video</a>"
msgstr "<a href=\"./happy-birthday-to-gnu-download\">Film herunterladen</a>"

#. type: Content of: <ul><li>
# | <a [-href=\"http://www.gnu.org/distros/free-distros.html\">Download-]
# | {+href=\"/distros/free-distros.html\">Download+} GNU</a>
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.gnu.org/distros/free-distros.html\">Download GNU</a>"
msgid "<a href=\"/distros/free-distros.html\">Download GNU</a>"
msgstr "<a href=\"/distros/free-distros\">GNU herunterladen</a>"

#. type: Content of: <ul><li>
# | <a href=\"http{+s+}://www.fsf.org/news/freedom-fry/\">Press release</a>
#, fuzzy
#| msgid "<a href=\"http://www.fsf.org/news/freedom-fry/\">Press release</a>"
msgid "<a href=\"https://www.fsf.org/news/freedom-fry/\">Press release</a>"
msgstr "<a href=\"https://www.fsf.org/news/freedom-fry/\">Pressemitteilung</a>"

#. type: Content of: <p>
# | [-<a
# | href=\"happy-birthday-to-gnu-in-your-language.html\">Over-]{+<strong><a
# | href=\"/fry/happy-birthday-to-gnu-in-your-language.html\">Over+} 20
# | translations of the film now [-available</a>-] {+available</a></strong>+}
#, fuzzy
#| msgid ""
#| "<a href=\"happy-birthday-to-gnu-in-your-language.html\">Over 20 "
#| "translations of the film now available</a>"
msgid ""
"<strong><a href=\"/fry/happy-birthday-to-gnu-in-your-language.html\">Over 20 "
"translations of the film now available</a></strong>"
msgstr ""
"<a href=\"./happy-birthday-to-gnu-in-your-language\">Über 20 Übersetzungen "
"des Films sind verfügbar</a>"

#. type: Content of: <p>
# | <a [-href=\"happy-birthday-to-gnu-translation.html\">Help-]
# | {+href=\"/fry/happy-birthday-to-gnu-translation.html\">Help+} translate
# | the video into your own language</a> and <a
# | [-href=\"happy-birthday-to-gnu-credits.html\">full-]
# | {+href=\"/fry/happy-birthday-to-gnu-credits.html\">full+} credits</a>
#, fuzzy
#| msgid ""
#| "<a href=\"happy-birthday-to-gnu-translation.html\">Help translate the "
#| "video into your own language</a> and <a href=\"happy-birthday-to-gnu-"
#| "credits.html\">full credits</a>"
msgid ""
"<a href=\"/fry/happy-birthday-to-gnu-translation.html\">Help translate the "
"video into your own language</a> and <a href=\"/fry/happy-birthday-to-gnu-"
"credits.html\">full credits</a>"
msgstr ""
"(Bei der Übersetzung des <a href=\"./happy-birthday-to-gnu-translation"
"\">Films</a> und des gesamten <a href=\"./happy-birthday-to-gnu-credits"
"\">Nachspanns</a> in die eigene Sprache mithelfen.)"

#. type: Content of: <p>
# | <small>Licensed under a <a rel=\"license\"
# | href=\"http{+s+}://creativecommons.org/licenses/by-nd/3.0/us/\">Creative
# | Commons Attribution-No Derivative Works 3.0 United States
# | License</a>.</small>
#, fuzzy
#| msgid ""
#| "<small>Licensed under a <a rel=\"license\" href=\"http://creativecommons."
#| "org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-No Derivative "
#| "Works 3.0 United States License</a>.</small>"
msgid ""
"<small>Licensed under a <a rel=\"license\" href=\"https://creativecommons."
"org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-No Derivative "
"Works 3.0 United States License</a>.</small>"
msgstr ""
"<small>Lizenz: <a rel=\"license\" href=\"//creativecommons.org/licenses/by-"
"nd/3.0/us/deed.de\">Creative Commons Namensnennung-Keine Bearbeitung 3.0 "
"Vereinigte Staaten von Amerika</a>.</small>"

#. type: Content of: <p>
# | <a [-style=\"font-weight: bold;\"-] href=\"#signup\"><span [-style=\"
# | padding: 5px;\"-] {+style=\"padding: .3em;\"+} class=\"highlight\">Sign up
# | for the Free Software Supporter, a monthly update on GNU and the
# | FSF</span></a>
#, fuzzy
#| msgid ""
#| "<a style=\"font-weight: bold;\" href=\"#signup\"><span style=\" padding: "
#| "5px;\" class=\"highlight\">Sign up for the Free Software Supporter, a "
#| "monthly update on GNU and the FSF</span></a>"
msgid ""
"<a href=\"#signup\"><span style=\"padding: .3em;\" class=\"highlight\">Sign "
"up for the Free Software Supporter, a monthly update on GNU and the FSF</"
"span></a>"
msgstr ""
"Den <a style=\"font-weight: bold;\" href=\"#signup\"><span style=\" padding: "
"5px;\" class=\"highlight\">Free Software Supporter</span></a> abonnieren, "
"ein monatlicher Newsletter Rund um GNU und FSF."

#. type: Content of: <table><tr><td><h3>
msgid "Get involved with GNU!"
msgstr "Bei GNU mitwirken!"

#. type: Content of: <table><tr><td><p>
# | We started the GNU Project with a specific overall goal: to create a free
# | software operating system, the GNU System.  The scope of GNU is
# | far-reaching: any job that computer users want to do <a
# | [-href=\"http://www.gnu.org/gnu/gnu-history.html\">should-]
# | {+href=\"/gnu/gnu-history.html\">should+} be doable by free software</a>,
# | and is thus a potential part of GNU.  For instance, any program found in a
# | typical <a [-href=\"/distros\">GNU/Linux-]
# | {+href=\"/distros/distros.html\">GNU/Linux+} distribution</a> is a
# | candidate.
#, fuzzy
#| msgid ""
#| "We started the GNU Project with a specific overall goal: to create a free "
#| "software operating system, the GNU System.  The scope of GNU is far-"
#| "reaching: any job that computer users want to do <a href=\"http://www.gnu."
#| "org/gnu/gnu-history.html\">should be doable by free software</a>, and is "
#| "thus a potential part of GNU.  For instance, any program found in a "
#| "typical <a href=\"/distros\">GNU/Linux distribution</a> is a candidate."
msgid ""
"We started the GNU Project with a specific overall goal: to create a free "
"software operating system, the GNU System.  The scope of GNU is far-"
"reaching: any job that computer users want to do <a href=\"/gnu/gnu-history."
"html\">should be doable by free software</a>, and is thus a potential part "
"of GNU.  For instance, any program found in a typical <a href=\"/distros/"
"distros.html\">GNU/Linux distribution</a> is a candidate."
msgstr ""
"Wir starteten das GNU-Projekt mit einem bestimmten Ziel: ein Freie-Software-"
"Betriebssystem zu erstellen, das GNU-System. Der Umfang von GNU ist "
"weitreichend: jedwede Aufgabe, die Rechnernutzer ausführen wollen, sollte <a "
"href=\"/gnu/gnu-history\">mit freier Software machbar</a> sein, und somit "
"Teil von GNU. Beispielsweise ist jedes in einer typischen <a href=\"/distros/"
"\">GNU/Linux-Verteilung</a> gefundene Programm ein potentieller Kandidat."

#. type: Content of: <table><tr><td><p>
# | So we welcome new packages in GNU, to further our common cause of free
# | software.  If you want to join in and may have a suitable package, please
# | fill out this <a href=\"/help/evaluation.html\">short questionnaire</a> to
# | get started, and see the background information there.  The <a
# | [-href=\"http://directory.fsf.org/\">Free-]
# | {+href=\"https://directory.fsf.org/wiki/Main_Page\">Free+} Software
# | Directory</a> holds the <a
# | href=\"http{+s+}://directory.fsf.org/wiki/GNU/\">list of all current GNU
# | packages</a>.
#, fuzzy
#| msgid ""
#| "So we welcome new packages in GNU, to further our common cause of free "
#| "software.  If you want to join in and may have a suitable package, please "
#| "fill out this <a href=\"/help/evaluation.html\">short questionnaire</a> "
#| "to get started, and see the background information there.  The <a href="
#| "\"http://directory.fsf.org/\">Free Software Directory</a> holds the <a "
#| "href=\"http://directory.fsf.org/wiki/GNU/\">list of all current GNU "
#| "packages</a>."
msgid ""
"So we welcome new packages in GNU, to further our common cause of free "
"software.  If you want to join in and may have a suitable package, please "
"fill out this <a href=\"/help/evaluation.html\">short questionnaire</a> to "
"get started, and see the background information there.  The <a href="
"\"https://directory.fsf.org/wiki/Main_Page\">Free Software Directory</a> "
"holds the <a href=\"https://directory.fsf.org/wiki/GNU/\">list of all "
"current GNU packages</a>."
msgstr ""
"Um unsere gemeinsame Sache, Freie Software, zu fördern, begrüßen wir neue "
"Pakete in GNU. Sie möchten mitmachen und haben vielleicht ein passendes "
"Paket? Um loszulegen, siehe die Informationen zur<a href=\"/help/evaluation"
"\">Softwareevaluierung</a> sowie für einen Überblick das <a href=\"https://"
"directory.fsf.org/\">Freie Software-Verzeichnis</a> mit <a href=\"https://"
"directory.fsf.org/wiki/GNU/\">allen aktuellen GNU-Paketen</a>."

#. type: Content of: <table><tr><td><p>
# | Another important way to help GNU is by contributing to the existing GNU
# | projects. The <a
# | href=\"http{+s+}://savannah.gnu.org/people/?type_id=1\">GNU Help
# | Wanted</a> list is the general task list for GNU software.  You might also
# | consider taking over one of the <a
# | href=\"/server/takeaction.html#unmaint\">unmaintained GNU packages</a>. 
# | The general <a href=\"/help/help.html\">Help GNU</a> page includes many
# | non-technical ways to contribute, too.
#, fuzzy
#| msgid ""
#| "Another important way to help GNU is by contributing to the existing GNU "
#| "projects. The <a href=\"http://savannah.gnu.org/people/?type_id=1\">GNU "
#| "Help Wanted</a> list is the general task list for GNU software.  You "
#| "might also consider taking over one of the <a href=\"/server/takeaction."
#| "html#unmaint\">unmaintained GNU packages</a>.  The general <a href=\"/"
#| "help/help.html\">Help GNU</a> page includes many non-technical ways to "
#| "contribute, too."
msgid ""
"Another important way to help GNU is by contributing to the existing GNU "
"projects. The <a href=\"https://savannah.gnu.org/people/?type_id=1\">GNU "
"Help Wanted</a> list is the general task list for GNU software.  You might "
"also consider taking over one of the <a href=\"/server/takeaction."
"html#unmaint\">unmaintained GNU packages</a>.  The general <a href=\"/help/"
"help.html\">Help GNU</a> page includes many non-technical ways to "
"contribute, too."
msgstr ""
"Eine weitere wichtige Möglichkeit zur Unterstützung ist die Beteiligung an "
"einzelnen, bestehenden GNU-Projekten. <a href=\"https://savannah.gnu.org/"
"people/?type_id=1\">GNU sucht Hilfe</a> ist eine allgemeine Aufgabenliste "
"für GNU-Software, möglicherweise auch eines der <a href=\"/server/takeaction."
"html#unmaint\">unbetreuten GNU-Pakete</a>? <a href=\"/help/help.html\">GNU "
"unterstützen</a> enthält ebenfalls viele allgemeine nicht-technische "
"Möglichkeiten, uns etwas beizutragen."

#. type: Content of: <table><tr><td><p>
# | If you possess the skills of a webmaster, you might like to consider
# | helping on this [-website &mdash; <a-] {+website&mdash;<a+}
# | href=\"/server/standards/webmaster-quiz.html\">please take our webmaster
# | quiz</a>.
#, fuzzy
#| msgid ""
#| "If you possess the skills of a webmaster, you might like to consider "
#| "helping on this website &mdash; <a href=\"/server/standards/webmaster-"
#| "quiz.html\">please take our webmaster quiz</a>."
msgid ""
"If you possess the skills of a webmaster, you might like to consider helping "
"on this website&mdash;<a href=\"/server/standards/webmaster-quiz.html"
"\">please take our webmaster quiz</a>."
msgstr ""
"Besitzen Sie die Fähigkeiten eines Webmasters, sollten Sie erwägen sich für "
"diese Präsenz zu engagieren: beantworten Sie bitte unserem <a href=\"/server/"
"standards/webmaster-quiz\">Webmaster-Quiz</a>."

#. type: Content of: <table><tr><td><p>
# | Our server <a href=\"http{+s+}://savannah.gnu.org/\">savannah.gnu.org</a>
# | is a central point for development and maintenance of free software, and
# | many <a
# | href=\"https://savannah.nongnu.org/search/?type_of_search=soft&amp;words=%%%&amp;type=1\">GNU
# | projects are hosted at savannah</a>.  Savannah also welcomes and supports
# | free software projects that are not official GNU packages, so feel free to
# | use it for any of your free software work; unlike other hosting sites, <a
# | href=\"http{+s+}://www.fsf.org/blogs/community/savannah\">savannah is
# | firmly based on free software ideals</a>.
#, fuzzy
#| msgid ""
#| "Our server <a href=\"http://savannah.gnu.org/\">savannah.gnu.org</a> is a "
#| "central point for development and maintenance of free software, and many "
#| "<a href=\"https://savannah.nongnu.org/search/?type_of_search=soft&amp;"
#| "words=%%%&amp;type=1\">GNU projects are hosted at savannah</a>.  Savannah "
#| "also welcomes and supports free software projects that are not official "
#| "GNU packages, so feel free to use it for any of your free software work; "
#| "unlike other hosting sites, <a href=\"http://www.fsf.org/blogs/community/"
#| "savannah\">savannah is firmly based on free software ideals</a>."
msgid ""
"Our server <a href=\"https://savannah.gnu.org/\">savannah.gnu.org</a> is a "
"central point for development and maintenance of free software, and many <a "
"href=\"https://savannah.nongnu.org/search/?type_of_search=soft&amp;words=%%"
"%&amp;type=1\">GNU projects are hosted at savannah</a>.  Savannah also "
"welcomes and supports free software projects that are not official GNU "
"packages, so feel free to use it for any of your free software work; unlike "
"other hosting sites, <a href=\"https://www.fsf.org/blogs/community/savannah"
"\">savannah is firmly based on free software ideals</a>."
msgstr ""
"Unser Server unter <a href=\"https://savannah.gnu.org/\">https://savannah."
"gnu.org/</a> ist zentrale Anlaufstelle für Entwicklung und Wartung freier "
"Software, und viele <a href=\"https://savannah.nongnu.org/search/?"
"type_of_search=soft&amp;words=%%%&amp;type=1\">GNU-Projekte werden bei "
"Savannah gehostet</a>. Savannah begrüßt und unterstützt auch Freie-Software-"
"Projekte, die keine offiziellen GNU-Pakete sind&#160;&#8209;&#160;und kann "
"daher gern für die eigene Freie-Software-Arbeit eingesetzt werden. Im "
"Gegensatz zu anderen Hosting-Präsenzen <a href=\"https://www.fsf.org/blogs/"
"community/savannah\">basiert Savannah fest auf Freie-Software-Idealen</a>."

#. type: Content of: <table><tr><td><h3>
msgid "Support the FSF"
msgstr "Die FSF unterstützen"

#. type: Attribute 'alt' of: <table><tr><td><p><img>
msgid "&nbsp;[FSF logo]&nbsp;"
msgstr "&nbsp;[FSF]&nbsp;"

#. type: Content of: <table><tr><td><p>
msgid ""
"Your <a href=\"https://www.fsf.org/associate/support_freedom?\">charitable "
"donation</a> to the FSF helps to support, promote, and develop free software:"
msgstr ""
"Ihre <a href=\"https://www.fsf.org/associate/support_freedom?\">karitative "
"Spende</a> an die FSF hilft, Freie Software zu unterstützen, fördern und zu "
"entwickeln:"

#. type: Content of: <table><tr><td><ul><li>
# | <a [-href=\"http://www.gnu.org/\">GNU-] {+href=\"/\">GNU+} Project</a>
# | &mdash; Developing a complete free software operating system
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.gnu.org/\">GNU Project</a> &mdash; Developing a "
#| "complete free software operating system"
msgid ""
"<a href=\"/\">GNU Project</a> &mdash; Developing a complete free software "
"operating system"
msgstr ""
"<a href=\"/\">GNU-Projekt</a>: Entwickeln eines völlig freien Software-"
"Betriebssystems,"

#. type: Content of: <table><tr><td><ul><li>
# | <a href=\"http{+s+}://www.fsf.org/licensing/\">Licensing &amp; Compliance
# | Lab</a> &mdash; Stewardship and enforcement of the GNU General Public
# | License
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.fsf.org/licensing/\">Licensing &amp; Compliance Lab</"
#| "a> &mdash; Stewardship and enforcement of the GNU General Public License"
msgid ""
"<a href=\"https://www.fsf.org/licensing/\">Licensing &amp; Compliance Lab</"
"a> &mdash; Stewardship and enforcement of the GNU General Public License"
msgstr ""
"<a href=\"https://www.fsf.org/licensing/\">Licensing &amp; Compliance Lab</"
"a>: Verantwortlichkeit für und Durchsetzung der <span xml:lang=\"en\">GNU "
"General Public License</span>,"

#. type: Content of: <table><tr><td><ul><li>
# | <a [-href=\"http://directory.fsf.org/\">Free-]
# | {+href=\"https://directory.fsf.org/wiki/Main_Page\">Free+} Software
# | Directory</a> &mdash; Cataloging the world of free software
#, fuzzy
#| msgid ""
#| "<a href=\"http://directory.fsf.org/\">Free Software Directory</a> &mdash; "
#| "Cataloging the world of free software"
msgid ""
"<a href=\"https://directory.fsf.org/wiki/Main_Page\">Free Software "
"Directory</a> &mdash; Cataloging the world of free software"
msgstr ""
"<a href=\"http://directory.fsf.org/\">Freie Software-Verzeichnis</a>: "
"Katalogisieren der Freie-Software-Welt,"

#. type: Content of: <table><tr><td><ul><li>
# | <a href=\"http{+s+}://savannah.gnu.org/\">Savannah Community</a> &mdash;
# | Supporting free software developers
#, fuzzy
#| msgid ""
#| "<a href=\"http://savannah.gnu.org/\">Savannah Community</a> &mdash; "
#| "Supporting free software developers"
msgid ""
"<a href=\"https://savannah.gnu.org/\">Savannah Community</a> &mdash; "
"Supporting free software developers"
msgstr ""
"<a href=\"https://savannah.gnu.org/\">Savannah-Gemeinschaft</a>: "
"Unterstützen von Freie-Software-Entwicklern,"

#. type: Content of: <table><tr><td><ul><li>
# | <a href=\"http{+s+}://www.fsf.org/campaigns/\">Campaigns for Freedom</a>
# | &mdash; Earning mindshare in support of free software
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.fsf.org/campaigns/\">Campaigns for Freedom</a> "
#| "&mdash; Earning mindshare in support of free software"
msgid ""
"<a href=\"https://www.fsf.org/campaigns/\">Campaigns for Freedom</a> &mdash; "
"Earning mindshare in support of free software"
msgstr ""
"<a href=\"https://www.fsf.org/campaigns/\">Kampagnen für die Freiheit</a>: "
"Fördern des Gedankenaustauschs zur Unterstützung freier Software."

#. type: Content of: <p>
# | [-<a
# | href=\"http://www.fsf.org/register_form?referrer=2442\">Join-]{+<strong><a
# | href=\"https://www.fsf.org/register_form?referrer=2442\">Join+} the FSF
# | [-today!</a>-] {+today!</a></strong>+} or [-<a-] {+<strong><a+}
# | href=\"https://www.fsf.org/associate/support_freedom/donate\">Make a
# | [-donation</a>-] {+donation</a></strong>+}
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.fsf.org/register_form?referrer=2442\">Join the FSF "
#| "today!</a> or <a href=\"https://www.fsf.org/associate/support_freedom/"
#| "donate\">Make a donation</a>"
msgid ""
"<strong><a href=\"https://www.fsf.org/register_form?referrer=2442\">Join the "
"FSF today!</a></strong> or <strong><a href=\"https://www.fsf.org/associate/"
"support_freedom/donate\">Make a donation</a></strong>"
msgstr ""
"Noch heute <a href=\"https://www.fsf.org/register_form?referrer=2442\">der "
"FSF anschließen</a> oder <a href=\"https://www.fsf.org/associate/"
"support_freedom/donate\">Spenden</a>!"

#. type: Content of: <table><tr><td><h3>
msgid "Philosophy"
msgstr "Philosophie"

#. type: Content of: <table><tr><td><p>
msgid "Learn more about the philosophy behind free software."
msgstr "Mehr über die zugrunde liegende Philosophie erfahren."

#. type: Content of: <table><tr><td><ul><li>
msgid "<a href=\"/philosophy/free-sw.html\">What is free software?</a>"
msgstr "<a href=\"/philosophy/free-sw\">Freie Software. Was ist das?</a>"

#. type: Content of: <table><tr><td><ul><li>
# | <a href=\"/philosophy/open-source-misses-the-point.html\">Why [-\"open
# | source\"-] {+&ldquo;open source&rdquo;+} misses the point</a>
#, fuzzy
#| msgid ""
#| "<a href=\"/philosophy/open-source-misses-the-point.html\">Why \"open "
#| "source\" misses the point</a>"
msgid ""
"<a href=\"/philosophy/open-source-misses-the-point.html\">Why &ldquo;open "
"source&rdquo; misses the point</a>"
msgstr ""
"<a href=\"/philosophy/open-source-misses-the-point\">Warum „Open Source“ das "
"Ziel <em>Freie Software</em> verfehlt</a>"

#. type: Content of: <table><tr><td><p>
# | <strong>Sign up for the [-<em>Free-] {+<cite>Free+} Software
# | [-Supporter</em></strong>-] {+Supporter</cite></strong>+}
#, fuzzy
#| msgid "<strong>Sign up for the <em>Free Software Supporter</em></strong>"
msgid "<strong>Sign up for the <cite>Free Software Supporter</cite></strong>"
msgstr ""
"Den <strong><cite><span xml:lang=\"en\">Free Software Supporter</span></"
"cite></strong> abonnieren"

#. type: Content of: <table><tr><td><p>
msgid "A monthly update on GNU and the Free Software Foundation"
msgstr ""
"Ein monatlicher Newsletter mit Neuigkeiten rund um GNU und Free Software "
"Foundation."

#. type: Content of: <table><tr><td><form><p>
# | <input type=\"text\" name=\"email\" size=\"[-3-]{+2+}0\" maxlength=\"80\"
# | value=\"you@example.com\" onfocus=\"this.value=''\" />&nbsp;<input
# | type=\"submit\" value=\"Subscribe me\" />
#, fuzzy
#| msgid ""
#| "<input type=\"text\" name=\"email\" size=\"30\" maxlength=\"80\" value="
#| "\"you@example.com\" onfocus=\"this.value=''\" />&nbsp;<input type=\"submit"
#| "\" value=\"Subscribe me\" />"
msgid ""
"<input type=\"text\" name=\"email\" size=\"20\" maxlength=\"80\" value="
"\"you@example.com\" onfocus=\"this.value=''\" />&nbsp;<input type=\"submit\" "
"value=\"Subscribe me\" />"
msgstr ""
"<input type=\"text\" name=\"email\" size=\"30\" maxlength=\"80\" value="
"\"name@example.com\" onfocus=\"this.value=''\" />&nbsp;<input type=\"submit"
"\" value=\"Abonnieren\" />"

#. type: Content of: <table><tr><td><h3>
msgid "Download"
msgstr "Herunterladen"

#. type: Content of: <table><tr><td><ul><li>
# | <a [-href=\"http://www.gnewsense.org/\">gNewSense</a>,-]
# | {+href=\"/distros/free-distros.html\">gNewSense</a>,+} a GNU/Linux
# | distribution based on Debian and Ubuntu, with sponsorship from the FSF.
#, fuzzy
#| msgid ""
#| "<a href=\"http://www.gnewsense.org/\">gNewSense</a>, a GNU/Linux "
#| "distribution based on Debian and Ubuntu, with sponsorship from the FSF."
msgid ""
"<a href=\"/distros/free-distros.html\">gNewSense</a>, a GNU/Linux "
"distribution based on Debian and Ubuntu, with sponsorship from the FSF."
msgstr ""
"<b><a href=\"//www.gnewsense.org/\">gNewSense</a></b>, eine auf Debian und "
"Ubuntu basierende GNU/Linux-Verteilung, mit Unterstützung der FSF."

#. type: Content of: <table><tr><td><ul><li>
msgid ""
"<a href=\"https://web.archive.org/web/20200620205430/http://www.blagblagblag."
"org/\">BLAG</a>, BLAG Linux and GNU, a GNU/Linux distribution based on "
"Fedora."
msgstr ""
"<b><a href=\"https://web.archive.org/web/20200620205430/http://www."
"blagblagblag.org/\">BLAG</a></b>, eine auf Fedora basierende GNU/Linux-"
"Verteilung "

#. type: Content of: <table><tr><td><p>
# | <a [-href=\"/distros\">More-] {+href=\"/distros/distros.html\">More+} GNU
# | distributions</a> and a <a
# | href=\"http{+s+}://directory.fsf.org/wiki/GNU/\">complete list of GNU
# | packages</a>
#, fuzzy
#| msgid ""
#| "<a href=\"/distros\">More GNU distributions</a> and a <a href=\"http://"
#| "directory.fsf.org/wiki/GNU/\">complete list of GNU packages</a>"
msgid ""
"<a href=\"/distros/distros.html\">More GNU distributions</a> and a <a href="
"\"https://directory.fsf.org/wiki/GNU/\">complete list of GNU packages</a>"
msgstr ""
"<a href=\"/distros/\">Mehr GNU-Verteilungen</a> sowie eine <a href=\"http://"
"directory.fsf.org/wiki/GNU/\">Übersicht aller GNU-Pakete</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Bitte senden Sie allgemeine Fragen zur FSF &amp; GNU an <a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Sie können auch die <a href=\"/"
"contact/\"><span xml:lang=\"en\" lang=\"en\">Free Software Foundation</span> "
"kontaktieren</a>. Ungültige Verweise und andere Korrekturen oder Vorschläge "
"können an <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</"
"a> gesendet werden."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
# || No change detected.  The change might only be in amounts of spaces.
#, fuzzy
#| msgid ""
#| "Please see the <a href=\"/server/standards/README.translations.html"
#| "\">Translations README</a> for information on coordinating and "
#| "contributing translations of this article."
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Bei der Übersetzung dieses Werkes wurde mit größter Sorgfalt vorgegangen. "
"Trotzdem können Fehler nicht völlig ausgeschlossen werden. Sollten Sie "
"Fehler bemerken oder Vorschläge, Kommentare oder Fragen zu diesem Dokument "
"haben, wenden Sie sich bitte an unser Übersetzungsteam <a href=\"mailto:web-"
"translators@gnu.org?cc=www-de-translators@gnu.org\">&lt;web-translators@gnu."
"org&gt;</a>.</p>\n"
"<p>Weitere Informationen über die Koordinierung und Einsendung von "
"Übersetzungen unserer Internetpräsenz finden Sie in der <a href=\"/server/"
"standards/README.translations\">LIESMICH für Übersetzungen</a>."

#. type: Content of: <div><p>
# | Copyright &copy; 2008, [-2014-] {+2022+} Free Software Foundation, Inc.
#, fuzzy
#| msgid "Copyright &copy; 2008, 2014 Free Software Foundation, Inc."
msgid "Copyright &copy; 2008, 2022 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2008, 2014 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Dieses Werk ist lizenziert unter einer <a rel=\"license\" href=\"//"
"creativecommons.org/licenses/by-nd/4.0/deed.de\">Creative Commons "
"Namensnennung-Keine Bearbeitungen 4.0 International</a>-Lizenz."

#. type: Content of: <div><p>
# | &ldquo;<a href=\"/fry/happy-birthday-to-gnu.html\">Happy Birthday to
# | GNU</a>&rdquo; is licensed under a <a rel=\"license\"
# | href=\"http{+s+}://creativecommons.org/licenses/by-nd/3.0/us/\">Creative
# | Commons Attribution-No Derivative Works 3.0 United States License</a>.
#, fuzzy
#| msgid ""
#| "&ldquo;<a href=\"/fry/happy-birthday-to-gnu.html\">Happy Birthday to GNU</"
#| "a>&rdquo; is licensed under a <a rel=\"license\" href=\"http://"
#| "creativecommons.org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-"
#| "No Derivative Works 3.0 United States License</a>."
msgid ""
"&ldquo;<a href=\"/fry/happy-birthday-to-gnu.html\">Happy Birthday to GNU</"
"a>&rdquo; is licensed under a <a rel=\"license\" href=\"https://"
"creativecommons.org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-No "
"Derivative Works 3.0 United States License</a>."
msgstr ""
"<cite><a href=\"/fry/happy-birthday-to-gnu\">Happy Birthday to GNU</a></"
"cite> ist lizenziert unter einer <a rel=\"license\" href=\"//creativecommons."
"org/licenses/by-nd/3.0/us/deed.de\">Creative Commons Namensnennung-Keine "
"Bearbeitung 3.0 Vereinigte Staaten von Amerika</a>-Lizenz."

#. type: Content of: <div><p>
# | Please <a href=\"http{+s+}://www.fsf.org/about/contact.html\">contact
# | us</a> for further permissions, including derived works.
#, fuzzy
#| msgid ""
#| "Please <a href=\"http://www.fsf.org/about/contact.html\">contact us</a> "
#| "for further permissions, including derived works."
msgid ""
"Please <a href=\"https://www.fsf.org/about/contact.html\">contact us</a> for "
"further permissions, including derived works."
msgstr ""
"<a href=\"https://www.fsf.org/about/contact.html\">Kontaktieren Sie uns</a> "
"bitte für weitere Erlaubnisse, einschließlich abgeleiteter Werke."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"<strong>Übersetzung:</strong> <!--Jоегg Kоhпе 2011, 2013, 2014, 2018, 2019. "
"--><a href=\"//savannah.gnu.org/projects/www-de\">&lt;www-de&gt;</a>, 2011, "
"2013, 2014, 2018, 2019."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Letzte Änderung:"

#~ msgid ""
#~ ".inline {text-align: center; line-height: 4em; padding: 0; margin: 0 !"
#~ "important;  margin-bottom: 1em !important;}\n"
#~ "\n"
#~ ".inline li{list-style: none; display: inline; }\n"
#~ "\n"
#~ "#download li a{background-color:#1ea410; padding: 1em 3em 1em 3em; -moz-"
#~ "border-radius: 8px; color: white; font-weight: bold; margin: 0.4em; text-"
#~ "decoration: none; width: 23%; text-align: center;}\n"
#~ "\n"
#~ "#download a:hover{background-color: #b1ef0b; color: black;}\n"
#~ "\n"
#~ ".title{font-size: 180%;}\n"
#~ "\n"
#~ "td p{padding-left: 1em; padding-right: 1em;}\n"
#~ "\n"
#~ "td li, td p{font-size: 0.8em;}\n"
#~ "\n"
#~ ".formHelp{text-align: center; color: #666; font-size: 90%;}\n"
#~ "\n"
#~ "table{margin-bottom: 0.8em !important;}\n"
#~ "\n"
#~ "#sjf{width: 600px; margin: 0 auto 1em auto;}\n"
#~ "\n"
#~ msgstr ""
#~ ".inline {text-align: center; line-height: 4em; padding: 0; margin: 0 !"
#~ "important;  margin-bottom: 1em !important;}\n"
#~ "\n"
#~ ".inline li{list-style: none; display: inline; }\n"
#~ "\n"
#~ "#download li a{background-color:#1ea410; padding: 1em 3em 1em 3em; -moz-"
#~ "border-radius: 8px; color: white; font-weight: bold; margin: 0.4em; text-"
#~ "decoration: none; width: 23%; text-align: center;}\n"
#~ "\n"
#~ "#download a:hover{background-color: #b1ef0b; color: black;}\n"
#~ "\n"
#~ ".title{font-size: 180%;}\n"
#~ "\n"
#~ "td p{padding-left: 1em; padding-right: 1em;}\n"
#~ "\n"
#~ "td li, td p{font-size: 0.8em;}\n"
#~ "\n"
#~ ".formHelp{text-align: center; color: #666; font-size: 90%;}\n"
#~ "\n"
#~ "table{margin-bottom: 0.8em !important;}\n"
#~ "\n"
#~ "#sjf{width: 600px; margin: 0 auto 1em auto;}\n"
#~ "\n"

#~ msgid ""
#~ "Copyright &copy; 2008, 2014, 2018, 2019, 2020 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2008, 2014, 2018-2020 Free Software Foundation, Inc."

#~ msgid ""
#~ "Copyright &copy; 2008, 2014, 2018, 2019, 2020, 2022 Free Software "
#~ "Foundation, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2008, 2014, 2018-2022 Free Software Foundation, Inc."

#~ msgid "Copyright &copy; 2008, 2014, 2018 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2008, 2014, 2018 Free Software Foundation, Inc."

#~ msgid ""
#~ "<a href=\"http://onebigtorrent.org/torrents/3899/Happy-Birthday-to-GNU"
#~ "\">Help share this movie by BitTorrent</a>."
#~ msgstr ""
#~ "Diesen Film <a href=\"//onebigtorrent.org/torrents/3899/Happy-Birthday-to-"
#~ "GNU\">via BitTorrent</a> mit anderen teilen."

#~ msgid ""
#~ ".inline {text-align: center; line-height: 4em; padding: 0; margin: 0 !"
#~ "important; margin-bottom: 1em !important;} .inline li{list-style: none; "
#~ "display: inline; } #download li a{background-color:#1ea410; padding: 1em "
#~ "3em 1em 3em; -moz-border-radius: 8px; color: white; font-weight: bold; "
#~ "margin: 0.4em; text-decoration: none; width: 23%; text-align: center;} "
#~ "#download a:hover{background-color: #b1ef0b; color: black;} .title{font-"
#~ "size: 180%;} td p{padding-left: 1em; padding-right: 1em;} td li, td p"
#~ "{font-size: 0.8em;} .formHelp{text-align: center; color: #666; font-size: "
#~ "90%;} table{margin-bottom: 0.8em !important;} #sjf{width: 600px; margin: "
#~ "0 auto 1em auto;}"
#~ msgstr ""
#~ ".inline {text-align: center; line-height: 4em; padding: 0; margin: 0 !"
#~ "important; margin-bottom: 1em !important;} .inline li{list-style: none; "
#~ "display: inline; } #download li a{background-color:#1ea410; padding: 1em "
#~ "3em 1em 3em; -moz-border-radius: 8px; color: white; font-weight: bold; "
#~ "margin: 0.4em; text-decoration: none; width: 23%; text-align: center;} "
#~ "#download a:hover{background-color: #b1ef0b; color: black;} .title{font-"
#~ "size: 180%;} td p{padding-left: 1em; padding-right: 1em;} td li, td p"
#~ "{font-size: 0.8em;} .formHelp{text-align: center; color: #666; font-size: "
#~ "90%;} table{margin-bottom: 0.8em !important;} #sjf{width: 600px; margin: "
#~ "0 auto 1em auto;}"

#~ msgid "Copyright &copy; 2008 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2008 Free Software Foundation, Inc."

#~ msgid "What's New"
#~ msgstr "Was gibt&#8217;s Neues"

#~ msgid ""
#~ "Stephen Fry &mdash; Happy birthday to GNU &mdash; The GNU Operating System"
#~ msgstr ""
#~ "Stephen Fry - Alles Gute zum Geburtstag GNU - Das Betriebssystem GNU"

#~ msgid ""
#~ "Copyright &copy; 2008 <a href=\"http://www.fsf.org\">Free Software "
#~ "Foundation</a>, Inc."
#~ msgstr ""
#~ "Copyright &copy; 2008 <a href=\"http://www.fsf.org/\">Free Software "
#~ "Foundation</a>, Inc."

#~ msgid "Translations of this page"
#~ msgstr "Übersetzungen dieser Seite"

# LANGUAGE translation of https://www.gnu.org/fun/jokes/eternal-flame.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: eternal-flame.html\n"
"POT-Creation-Date: 2021-07-12 16:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Eternal Flame - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <p><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <p><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <p>
msgid ""
"</a>&nbsp;/ <a href=\"/fun/humor.html#content\">GNU humor</a>&nbsp;/ <a "
"href=\"/music/music.html#content\">Music</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "Eternal Flame"
msgstr ""

#. type: Content of: <div><address>
msgid "by Bob Kanefsky and Julia Ecklar"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This is a parody of <cite>God Lives on Terra</cite> by Julia&nbsp;Ecklar.  "
"For more information, visit <a "
"href=\"http://www.songworm.com/db/songworm-parody/EternalFlame.html\"> "
"Songworm</a>."
msgstr ""

#. type: Content of: <div><div><p>
msgid "I was taught assembler"
msgstr ""

#. type: Content of: <div><div><p>
msgid "in my second year of school."
msgstr ""

#. type: Content of: <div><div><p>
msgid "It's kinda like construction work"
msgstr ""

#. type: Content of: <div><div><p>
msgid "with a toothpick for a tool."
msgstr ""

#. type: Content of: <div><div><p>
msgid "So when I made my senior year,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "I threw my code away,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "And learned the way to program"
msgstr ""

#. type: Content of: <div><div><p>
msgid "that I still prefer today."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Now, some folks on the Internet"
msgstr ""

#. type: Content of: <div><div><p>
msgid "put their faith in C++."
msgstr ""

#. type: Content of: <div><div><p>
msgid "They swear that it's so powerful,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "it's what God used for us."
msgstr ""

#. type: Content of: <div><div><p>
msgid "And maybe it lets mortals"
msgstr ""

#. type: Content of: <div><div><p>
msgid "dredge their objects from the C."
msgstr ""

#. type: Content of: <div><div><p>
msgid "But I think that explains"
msgstr ""

#. type: Content of: <div><div><p>
msgid "why only God can make a tree."
msgstr ""

#. type: Content of: <div><div><p>
msgid "For God wrote in Lisp code"
msgstr ""

#. type: Content of: <div><div><p>
msgid "when he filled the leaves with green."
msgstr ""

#. type: Content of: <div><div><p>
msgid "The fractal flowers and recursive roots:"
msgstr ""

#. type: Content of: <div><div><p>
msgid "the most lovely hack I've seen."
msgstr ""

#. type: Content of: <div><div><p>
msgid "And when I ponder snowflakes,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "never finding two the same,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "I know God likes a language"
msgstr ""

#. type: Content of: <div><div><p>
msgid "with its own four-letter name."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Now, I've used a SUN under Unix,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "so I've seen what C can hold."
msgstr ""

#. type: Content of: <div><div><p>
msgid "I've surfed for Perls, found what Fortran's for,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "got that Java stuff down cold."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Though the chance that I'd write COBOL code"
msgstr ""

#. type: Content of: <div><div><p>
msgid "is a SNOBOL's chance in Hell."
msgstr ""

#. type: Content of: <div><div><p>
msgid "And I basically hate hieroglyphs,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "so I won't use APL."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Now, God must know all these languages,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "and a few I haven't named."
msgstr ""

#. type: Content of: <div><div><p>
msgid "But the Lord made sure, when each sparrow falls,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "that its flesh will be reclaimed."
msgstr ""

#. type: Content of: <div><div><p>
msgid "And the Lord could not count grains of sand"
msgstr ""

#. type: Content of: <div><div><p>
msgid "with a 32-bit word."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Who knows where we would go to"
msgstr ""

#. type: Content of: <div><div><p>
msgid "if Lisp weren't what he preferred?"
msgstr ""

#. type: Content of: <div><div><p>
msgid "And God wrote in Lisp code"
msgstr ""

#. type: Content of: <div><div><p>
msgid "every creature great and small."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Don't search the disk drive for <code>man.c</code>,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "when the listing's on the wall."
msgstr ""

#. type: Content of: <div><div><p>
msgid "And when I watch the lightning"
msgstr ""

#. type: Content of: <div><div><p>
msgid "burn unbelievers to a crisp,"
msgstr ""

#. type: Content of: <div><div><p>
msgid "I know God had six days to work."
msgstr ""

#. type: Content of: <div><div><p>
msgid "So he wrote it all in Lisp."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Yes, God had a deadline."
msgstr ""

#. type: Content of: <div><p>
msgid "<cite>Eternal Flame</cite> was performed by Julia Ecklar:"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"<a href=\"/fun/jokes/eternal-flame.ogg\">Audio recording</a> "
"<small>(OGG&nbsp;Vorbis&nbsp;format,&nbsp;5.5MB)</small>"
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; Julia Ecklar 1984 (original song: lyrics and music)"
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; Bob Kanefsky 1996 (parody)"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Non-commercial duplication and redistribution of the audio recording of "
"<cite>Eternal Flame</cite>, by Julia Ecklar and Bob Kanefsky, is permitted "
"provided the entire recording is preserved, including the entire notice."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

# LANGUAGE translation of https://www.gnu.org/fun/jokes/gnu-overflow.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnu-overflow.html\n"
"POT-Creation-Date: 2021-07-14 16:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "GNU-Overflow - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <div><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <div><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <div>
msgid ""
"</a>&nbsp;/ <a href=\"/fun/humor.html#content\">GNU humor</a>&nbsp;/ <a "
"href=\"/fun/humor.html#Software\">Software</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "GNU-Overflow"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"The recursive acronym &ldquo;GNU's Not Unix&rdquo; harbors a stack overflow "
"bug that can cause the English language to crash and may allow arbitrary "
"linguistic commands to be executed, according to a message posted on "
"<code>gnu.acronym.bug</code> this morning.  All sites running GNU software "
"are urged to apply a temporary patch which changes the expansion of the "
"acronym to &ldquo;GNU Needs Users&rdquo;, until a permanent patch is "
"available.  GNU project founder Richard M. Stallman is currently hunting the "
"error in the acronym he created over a decade ago."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"&ldquo;Linguistic bugs are notoriously difficult to track down,&rdquo; "
"Stallman told <code>segfault.org</code> via email.  &ldquo;The capacity of "
"the stack depends on the memory of the person reading the buggy text.  In "
"addition, there is not yet any English interface to GDB, which means "
"searching manually through coredumps to find the problem.&rdquo;"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Most people experience the stack overflow at around 600 expansions of the "
"acronym.  In practice, few people have cause to carry the expansion this "
"far, so the main concern lies with the security risk posed by the bug.  "
"Although no exploit has yet been discovered, a malicious user could "
"theoretically embed commands into the same section of text as the acronym "
"expansion, allowing them to change the syntax of the language, redefine "
"words, and create new figures of speech with arbitrary meanings."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Many on the net saw the bug as a chance to reopen old holy wars.  &ldquo;The "
"stack problems that are endemic in the computer industry today are a direct "
"result of the widespread adoption of English as the language of "
"choice,&rdquo; said one Dothead.  &ldquo;English is a fine tool for "
"low-level descriptions and expository writing, but it offers too many "
"inconsistencies and is far too unstable to use in production environments.  "
"It's time to move to languages like Esperanto that feature built-in stack "
"protection.&rdquo; When it was pointed out that he had written his comment "
"in English, the poster went into an incoherent rant, finishing with "
"&ldquo;La cina industrio, kun fama milijara tradicio, pli kaj pli largskale "
"produktas ankau komputilon! Sed kiel aspekta la cina komputil-merkato el la "
"vidpunko de la aplikanto? Mi provos respondi al tiu demando lau personaj "
"spertoj en la plej granda cina urbo, Sanhajo!&rdquo;"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"FUD Week magazine was quick to cash in on the incident, as well.  &ldquo;It "
"is clear that freeware cannot be relied upon to keep the English language "
"secure,&rdquo; says an online editorial.  &ldquo;We suggest that these "
"&lsquo;computer hippies&rsquo; get their acts together before attempting "
"hippopotamus nap delta foley snurk tin possibility.&rdquo;"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Meanwhile, an anxious public waits for the restoration of the GNU acronym.  "
"Until the bug is fixed, we urge you to download the temporary patch from "
"your nearest mirror site and keep in mind that this process of continuous "
"revision is what has made both free software and human language into forces "
"to be reckoned with."
msgstr ""

#. type: Content of: <div><p>
msgid "<i>Jake Berendes contributed to this report.</i>"
msgstr ""

#. type: Content of: <div><div><h3>
msgid "Disclaimer"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"The joke on this page was obtained from the FSF's email archives of the GNU "
"Project. The Free Software Foundation claims no copyright on it."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

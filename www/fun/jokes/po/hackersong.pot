# LANGUAGE translation of https://www.gnu.org/fun/jokes/hackersong.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: hackersong.html\n"
"POT-Creation-Date: 2021-07-12 16:56+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "If the Beatles were hackers&hellip; - GNU Project - Free Software Foundation"
msgstr ""

#
#. h3 small { font-size: .75em; font-weight: normal; }
#
#. type: Content of: <style>
#, no-wrap
msgid ".lyrics { width: 32em; }\n"
msgstr ""

#. type: Content of: <p><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <p><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <p>
msgid ""
"</a>&nbsp;/ <a href=\"/fun/humor.html#content\">GNU humor</a>&nbsp;/ <a "
"href=\"/music/music.html#content\">Music</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "If the Beatles were hackers&hellip;"
msgstr ""

#. type: Content of: <div><h3>
msgid "Eleanor Rigby <small>(with apologies to the Beatles)</small>"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Eleanor Rigby"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Sits at the keyboard and waits for a line on the screen"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Lives in a dream"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Waits for a signal"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Writing some code that will make the machine do some more"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Who is it for?"
msgstr ""

#. type: Content of: <div><div><p>
msgid "All these lonely users, where do they all come from"
msgstr ""

#. type: Content of: <div><div><p>
msgid "All these lonely users, where do they all belong"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Hacker Mackenzy"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Writing the code for a program that no one will run"
msgstr ""

#. type: Content of: <div><div><p>
msgid "Its nearly done."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Look at him working."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Fixing the bugs late at night when there's nobody there"
msgstr ""

#. type: Content of: <div><div><p>
msgid "nobody cares."
msgstr ""

#. type: Content of: <div><div><p>
msgid "Ahhhh&hellip; look at all those lonely users&hellip;"
msgstr ""

#. type: Content of: <div><h3>
msgid "Always a Hacker <small>(with apologies to Billy Joel)</small>"
msgstr ""

#. type: Content of: <div><div><p>
msgid "She can kill all your files"
msgstr ""

#. type: Content of: <div><div><p>
msgid "She can freeze with a frown"
msgstr ""

#. type: Content of: <div><div><p>
msgid "And a wave of her hand bring your whole system down"
msgstr ""

#. type: Content of: <div><div><p>
msgid "And she works on her code until twelve after three"
msgstr ""

#. type: Content of: <div><div><p>
msgid "She lives like a bat but she's always a hacker to me."
msgstr ""

#. type: Content of: <div><div><h3>
msgid "Disclaimer"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"The lyrics on this page were obtained from the FSF's email archives of the "
"GNU Project. The Free Software Foundation does not claim copyright on them."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

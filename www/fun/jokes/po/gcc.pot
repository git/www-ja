# LANGUAGE translation of https://www.gnu.org/fun/jokes/gcc.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gcc.html\n"
"POT-Creation-Date: 2021-07-14 16:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Some suggested future GCC options - GNU Project - Free Software Foundation"
msgstr ""

#. type: Content of: <div><a>
msgid "<a href=\"/\">"
msgstr ""

#. type: Attribute 'title' of: <div><a><img>
msgid "GNU Home"
msgstr ""

#. type: Content of: <div>
msgid ""
"</a>&nbsp;/ <a href=\"/fun/humor.html#content\">GNU humor</a>&nbsp;/ <a "
"href=\"/fun/humor.html#Software\">Software</a>&nbsp;/"
msgstr ""

#. type: Content of: <div><h2>
msgid "Some suggested future GCC options"
msgstr ""

#. type: Content of: <div><p>
msgid "From: Noah Friedman"
msgstr ""

#. type: Content of: <div><p>
msgid "To: Roland McGrath, Richard Stallman, Jim Blandy, Michael Bushnell"
msgstr ""

#. type: Content of: <div><p>
msgid "Cc: chris williams, Christian Longshore Claiborn"
msgstr ""

#. type: Content of: <div><p>
msgid "Subject: Some gcc options we'd like to see."
msgstr ""

#. type: Content of: <div><p>
msgid "Date: Mon, 28 Jun 93 00:45:09 EST"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Waggravate-return"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Wcast-spell"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Wcaste-align"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Win"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Wmissing-protons"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-Wredundant-repetitions"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-antsy"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fbungee-jump"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fexpensive-operations"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fextra-strength"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fjesus-saves"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fkeep-programmers-inline"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fno-peeping-toms"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fruit-roll-ups"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-fshort-enough"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-mno-dialogue"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-pedophile"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "-vomit-frame-pointer"
msgstr ""

#. type: Content of: <div><p>
msgid "From: Alexandre Oliva"
msgstr ""

#. type: Content of: <div><p>
msgid "Date: 06 Jan 2002 17:37:07 -0200"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"On Jan 2, 2002, in a long, heated thread concerning the interpretation of "
"certain passages of the C standard, jtv wrote:"
msgstr ""

#. type: Content of: <div><blockquote><p>
msgid ""
"(Yes, I'm a pedant.  I'm pining for the day when gcc will support the "
"options &lsquo;-ffascist&nbsp;-Wanal&rsquo;)"
msgstr ""

#. type: Content of: <div><p>
msgid "How about introducing the options &lsquo;-flame&nbsp;-War&rsquo; :-)"
msgstr ""

#. type: Content of: <div><div><h3>
msgid "Disclaimer"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"The joke on this page was submitted to the GNU Project in 2003. The Free "
"Software Foundation claims no copyright on it."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

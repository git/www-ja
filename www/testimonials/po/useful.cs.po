# Czech translation of http://www.gnu.org/testimonials/useful.html
# Copyright (C) 2001 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Saved from the web
# Eva Frantova, 2001.
# Jan. 2017: GNUNify.
#
msgid ""
msgstr ""
"Project-Id-Version: useful.html\n"
"POT-Creation-Date: 2014-04-05 00:03+0000\n"
"PO-Revision-Date: \n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: web-translators <web-translators@gnu.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Free Software is Useful - GNU Project - Free Software Foundation"
msgstr ""
"Svobodný software je užitečný – Projekt GNU – Nadace pro svobodný software"

#. type: Content of: <h2>
msgid "Free Software is Useful"
msgstr "Svobodný software je užitečný"

#. type: Content of: <p>
msgid ""
"Free software is both versatile and effective in a wide variety of "
"applications.  Because the source code is given, it can be tailored "
"specifically to an individuals needs&hellip; Something not easily "
"accomplished with proprietary software."
msgstr ""
"Svobodný software je jak všestranný, tak užitečný pro širokou paletu "
"aplikací. Díky tomu, že zdrojový kód je přístupný, může být přizpůsoben "
"každému na míru podle jeho vlastních potřeb&hellip; Něco, čeho s "
"proprietárním software nedosáhnete."

#. type: Content of: <blockquote><p>
msgid ""
"&ldquo;As you well know, GNU tools are all over the place for the various "
"space missions.  They are used extensively throughout the mission "
"development, test and operations phases.  I wrote the DARTS dynamics "
"simulator for Cassini (spacecraft) several years ago and relied heavily on "
"tools such as GCC, GDB, Emacs, RCS etc.  These and others are in use by many "
"other spacecraft missions including Galileo, Mars Pathfinder, New Millennium "
"etc.  It is my personal belief that the FSF activities and software have "
"been a tremendous source of high quality tools which are readily accessible "
"and usable by the community.&rdquo;"
msgstr ""
"„Jak dobře víte, GNU nástroje jsou všude na vesmírných misích. Jsou často "
"používány všude ve vývoji misí, testovací a operační fázi. Před několika "
"lety jsem napsal dynamický simulátor DARTS pro Cassini (kosmická loď) a plně "
"se spoléhám na nástroje jako jsou GCC, GDB, Emacs, RCS atd. Tyto a jiné se "
"používají v mnoha dalších vesmírných misích včetně Galilea, Mars "
"Pathfinderu, New Millennia atd. Domnívám se, že činnost a software FSF jsou "
"ohromným zdrojem pro vysoce kvalitní nástroje, komunitě ihned přístupné a "
"použitelné.”"

#. type: Content of: <p><strong>
msgid "<strong>Abhinandan Jain"
msgstr "<strong>Abhinandan Jain"

#. type: Content of: <p>
msgid "NASA engineer</strong>"
msgstr "inženýr NASA</strong>"

#. type: Content of: <p>
msgid ""
"Regarding graphics development for the &ldquo;Titanic&rdquo; movie "
"production:"
msgstr "Ocenění od grafiků podílejících se na produkci filmu „Titanic”:"

#. type: Content of: <blockquote><p>
msgid ""
"&ldquo;Using 200 DEC Alpha-based systems running the Red Hat 4.1 "
"distribution of GNU/Linux, after upgrading the kernel to support the PC164 "
"mainboard, Digital Domain found a performance increase of three to four over "
"SGI systems.  The combination of the GNU/Linux OS and Alpha CPUs also "
"delivered the most cost-effective solution to time and processing demands."
"&rdquo;"
msgstr ""
"„Používali jsme 200 DEC systém na bázi Alpha, běžící na Red Hat 4.1 GNU/"
"Linux distribuci, po upgrade kernelu, aby podporoval základní desku počítače "
"PC164. Digital Domain zjistil, že se výkon se SGI systémy zvýšil o čtvrtinu. "
"Kombinace systému GNU/Linux OS a Alphy CPUs přinesla nejefektivnější řešení "
"požadavků na zpracování v krátkém čase.”"

#. type: Content of: <p><strong>
msgid "<strong>Daryll Strauss"
msgstr "<strong>Daryll Strauss"

#. type: Content of: <p>
msgid "Digital Domain</strong>"
msgstr "Digital Domain</strong>"

#. type: Content of: <blockquote><p>
msgid ""
"&ldquo;The proper care of our cancer patients would not be what it is today "
"without [GNU/]Linux &hellip; The tools that we have been able to deploy from "
"free software channels have enabled us to write and develop innovative "
"applications which &hellip; do not exist through commercial avenues.&rdquo;"
msgstr ""
"„Naše péče o nemocné s rakovinou by nebyla taková, jako je nyní bez [GNU/]"
"Linuxu &hellip; Nástroje, které jsme mohli čerpat z kanálů svobodného "
"software, nám umožnily psát a vyvíjet inovační aplikace které &hellip; "
"neexistují a nejsou dostupné v obchodech.”"

#. type: Content of: <p><strong>
msgid "<strong>Dr. G.W. Wettstein"
msgstr "<strong>Dr. G.W. Wettstein"

#. type: Content of: <p><strong>
msgid "Cancer Center"
msgstr "Centrum pro výzkum rakoviny"

#. type: Content of: <p>
msgid "Fargo, North Dakota</strong>"
msgstr "Fargo, North Dakota</strong>"

#. type: Content of: <blockquote><p>
msgid ""
"&ldquo;Within three months, we demonstrated a product developed entirely on "
"[free] software, the core of which were the programming tools (GCC and GDB) "
"from the Free Software Foundation.  Solid code, it was cross-platform "
"compatible with either Motif or Lesstif.  Our satisfied customers extended "
"the contract and we won some corporate visibility in a very high tech market "
"&hellip;&rdquo;"
msgstr ""
"„Během tří měsíců jsme vyvinuli produkt na [svobodném] software, jehož "
"jádrem byly programovací nástroje (GCC a GDB) od Nadace pro svobodný "
"software. Pevný kód, cross-platform byla kompatibilní buď s Motif nebo s "
"LessTif. Naši spokojení zákazníci rozšířili smlouvy a na trhu techniky jsme "
"se výrazně zviditelnili&hellip;”"

#. type: Content of: <p><strong>
msgid "<strong>Ron Broberg"
msgstr "<strong>Ron Broberg"

#. type: Content of: <p><strong>
msgid "Systems Engineer"
msgstr "systémový inženýr"

#. type: Content of: <p>
msgid "Lockheed-Martin</strong>"
msgstr "Lockheed-Martin</strong>"

#. type: Content of: <blockquote><p>
msgid ""
"&ldquo;[The Free Software Foundation's] high quality software makes our work "
"easier, and we value it greatly &hellip; Recently we have received some "
"prizes and monetary awards for our work. We believe we would not have "
"received these without your software.&rdquo;"
msgstr ""
"„Vysoce kvalitní software [od Nadace pro svobodný software] nám usnadňuje "
"práci. Velmi ho oceňujeme &hellip; nedávno jsme obdrželi nějaké ceny a "
"finanční odměny za naši práci. Domníváme se, že bez vašeho software by se "
"tak nikdy nestalo.”"

#. type: Content of: <p><strong>
msgid "<strong>VSC Research and Development group"
msgstr "<strong>VSC výzkumná a vývojová skupina"

#. type: Content of: <p>
msgid "Toyota Motor Corporation</strong>"
msgstr "Toyota Motor Corporation</strong>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Dotazy ohledně FSF a GNU prosím posílejte na <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  Jsou tu i <a href=\"/contact/\">další možnosti "
"jak kontaktovat</a> nadaci FSF. Ohledně nefunkčních odkazů a dalších návrhů "
"nebo oprav se prosím obracejte na <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Pracujeme tvrdě a děláme to nejlepší, abychom vám přinesli přesné a kvalitní "
"překlady. Nicméně i my můžeme udělat chybu. Vaše komentáře a návrhy na "
"vylepšení vítáme na adrese <a href=\"mailto:web-translators@gnu.org\"> &lt;"
"web-translators@gnu.org&gt;</a>.</p> <p>Přečtěte si prosím  <a href=\"/"
"server/standards/README.translations.html\">Příručku překladatele</a>, kde "
"se dozvíte, jak koordinovat svoji práci a posílat překlady tohoto článku."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1998, 1999, 2000, 2001, 2005, 2006, 2008, 2012, 2013, 2014 "
"Free Software Foundation, Inc."
msgstr ""
"Copyright &copy; 1998, 1999, 2000, 2001, 2005, 2006, 2008, 2012, 2013, 2014 "
"Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/3.0/us/\">Creative Commons Attribution-"
"NoDerivs 3.0 United States License</a>."
msgstr ""
"Tato stránka je vydána pod licencí <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/3.0/us/deed.cs\">Creative Commons Uveďte "
"původ-Nezpracovávejte 3.0 Spojené státy americké</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr " "

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Aktualizováno:"

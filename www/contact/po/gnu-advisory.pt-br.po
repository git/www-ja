# Brazilian Portuguese translation of https://www.gnu.org/contact/gnu-advisory.html
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cassiano Reinert Novais dos Santos <caco@posteo.net>, 2020.
# Rafael Fontenelle <rafaelff@gnome.org>, 2020-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnu-advisory.html\n"
"POT-Creation-Date: 2022-05-27 00:55+0000\n"
"PO-Revision-Date: 2021-04-22 09:49-0300\n"
"Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>\n"
"Language-Team: Brazilian Portuguese <www-pt-br-general@gnu.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Outdated-Since: 2022-05-27 00:55+0000\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"X-Generator: Gtranslator 40.0\n"

#. type: Content of: <title>
msgid "GNU Advisory Committee - GNU Project - Free Software Foundation"
msgstr "Comitê Consultivo do GNU - Projeto GNU - Free Software Foundation"

#. type: Content of: <div><h2>
msgid "GNU Advisory Committee"
msgstr "Comitê Consultivo do GNU"

#. type: Content of: <div><h3>
msgid "Overview"
msgstr "Visão geral"

#. type: Content of: <div><p>
msgid ""
"The GNU Advisory Committee exists to facilitate coordination within the GNU "
"project.  RMS (Richard Stallman), the founder of GNU and the FSF, remains "
"the Chief GNUisance with overall responsibility for and authority over the "
"GNU Project."
msgstr ""
"O Comitê Consultivo do GNU existe para facilitar a coordenação dentro do "
"projeto GNU. RMS (Richard Stallman), o fundador do GNU e da FSF, permanece o "
"<em>Cheif GNUisance</em> com responsabilidade e autoridade gerais sobre o "
"projeto GNU."

#. type: Content of: <div><p>
msgid ""
"The committee's responsibilities deal with the project as a whole.  It "
"provides an initial point of contact for questions from maintainers, FSF and "
"others.  Members of the committee are appointed by RMS and meet by phone "
"each month to discuss current issues."
msgstr ""
"As responsabilidades do comitê estão relacionadas ao projeto como um todo. "
"Ele fornece um ponto de contato inicial para perguntas de mantenedores, FSF "
"e outros. Os membros do comitê são nomeados pelo RMS e se reúnem por "
"telefone todos os meses para discutir as questões atuais."

#. type: Content of: <div><p>
msgid ""
"The committee can be contacted directly at <a href=\"mailto:gnu-advisory@gnu."
"org\">&lt;gnu-advisory@gnu.org&gt;</a>."
msgstr ""
"O comitê pode ser contatado diretamente em <a href=\"mailto:gnu-advisory@gnu."
"org\">&lt;gnu-advisory@gnu.org&gt;</a>."

#. type: Content of: <div><h3>
msgid "Current committee members"
msgstr "Membros atuais do comitê"

#. type: Content of: <div><ul><li>
msgid "Alexandre Oliva"
msgstr "Alexandre Oliva"

#. type: Content of: <div><ul><li>
msgid "Amin Bandali"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "Christian Grothoff"
msgstr "Christian Grothoff"

#. type: Content of: <div><ul><li>
msgid "Jason Self"
msgstr "Jason Self"

#. type: Content of: <div><ul><li>
msgid "Jim Meyering"
msgstr "Jim Meyering"

#. type: Content of: <div><ul><li>
msgid "Jose Marchesi"
msgstr "Jose Marchesi"

#. type: Content of: <div><ul><li>
msgid "Mike Gerwitz"
msgstr "Mike Gerwitz"

#. type: Content of: <div><ul><li>
msgid "Simon Josefsson"
msgstr "Simon Josefsson"

#. type: Content of: <div><h3>
msgid "Responsibilities"
msgstr "Responsabilidades"

#. type: Content of: <div><p>
msgid "The main responsibilities of the committee are to:"
msgstr "As responsabilidades principais do comitê são:"

#. type: Content of: <div><ul><li>
msgid ""
"monitor the health of the project, identifying problems and possible "
"solutions on behalf of RMS."
msgstr ""
"monitorar a saúde do projeto, identificando problemas e possíveis soluções "
"em nome do RMS."

#. type: Content of: <div><ul><li>
msgid ""
"improve communication within the project, by taking an active role in "
"talking to maintainers, FSF and others."
msgstr ""
"melhorar a comunicação dentro do projeto, participando ativamente da "
"conversação com mantenedores, FSF e outros."

#. type: Content of: <div><ul><li>
msgid ""
"be an initial contact point for issues concerning the whole project, both "
"internally and externally."
msgstr ""
"ser um ponto de contato inicial para questões relativas a todo o projeto, "
"interna e externamente."

#. type: Content of: <div><ul><li>
msgid "work with the FSF on maintaining the High Priority Projects list."
msgstr ""
"trabalhar com a FSF na manutenção da lista de Projetos de Alta Prioridade <i "
"lang=\"en\">(High Priority Projects)</i>."

#. type: Content of: <div><ul><li>
msgid ""
"review GNU-related activities regularly, to avoid duplicate efforts and "
"promote existing ones, and to discuss the adoption of prospective GNU "
"packages."
msgstr ""
"revisar as atividades relacionadas ao GNU regularmente, para evitar esforços "
"duplicados e promover os existentes, e para discutir a adoção de possíveis "
"pacotes GNU."

#. type: Content of: <div><ul><li>
msgid ""
"be available to RMS to discuss the content of public announcements as needed."
msgstr ""
"estar disponível ao RMS para discutir o conteúdo de anúncios públicos, "
"conforme necessário."

#. type: Content of: <div><h3>
msgid "For GNU maintainers"
msgstr "Para mantenedores do GNU"

#. type: Content of: <div><p>
msgid ""
"More information about the Advisory Committee, intended for GNU software "
"maintainers, is in the <a href=\"/prep/maintain/maintain.html#Getting-Help"
"\">Getting Help</a> section of the GNU Maintainers' Information document."
msgstr ""
"Mais informações sobre o Comitê Consultivo, destinado aos mantenedores de "
"<em>software</em> GNU, estão na seção <a href=\"/prep/maintain/maintain."
"html#Getting-Help\"><cite>Getting Help</cite></a> do documento <cite>GNU "
"Maintainers' Information</cite>."

#. type: Content of: <div><h3>
msgid "History"
msgstr "Histórico"

#. type: Content of: <div><p>
msgid "The committee held its first meeting in December 2009."
msgstr "O comitê realizou sua primeira reunião em dezembro de 2009."

#. type: Content of: <div><p>
msgid "Brian Gough was a member until January 2012."
msgstr "Brian Gough era membro até janeiro de 2012."

#. type: Content of: <div><p>
msgid "John Sullivan was a member until April 2021."
msgstr "John Sullivan era membro até abril de 2021."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:gnu@gnu."
"org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/\">outros "
"meios de contatar</a> a FSF. Links quebrados e outras correções ou sugestões "
"podem ser enviadas para <a href=\"mailto:webmasters@gnu.org\">&lt;"
"webmasters@gnu.org&gt;</a>."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"A equipe de traduções para o português brasileiro se esforça para oferecer "
"traduções precisas e de boa qualidade, mas não estamos isentos de erros. Por "
"favor, envie seus comentários e sugestões em geral sobre as traduções para "
"<a href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</"
"a>. </p><p>Consulte o <a href=\"/server/standards/README.translations.html"
"\">Guia para as traduções</a> para mais informações sobre a coordenação e a "
"contribuição com traduções das páginas deste site."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2014, 2015, 2019, 2020, 2021, 2022 Free Software "
"Foundation, Inc."
msgstr ""
"Copyright &copy; 2014, 2015, 2019, 2020, 2021, 2022 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Esta página está licenciada sob uma licença <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/deed.pt_BR\">Creative Commons "
"Atribuição-SemDerivações 4.0 Internacional</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduzido por: Rafael Fontenelle <a href=\"mailto:rafaelff@gnome.org\">&lt;"
"rafaelff@gnome.org&gt;</a>, 2020-2021.<br/>\n"
"Revisado por: Cassiano Reinert Novais dos Santos <a href=\"mailto:"
"caco@posteo.net\">&lt;caco@posteo.net&gt;</a>, 2020."

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Última atualização:"

#~ msgid ""
#~ "Copyright &copy; 2014, 2015, 2019, 2020, 2021 Free Software Foundation, "
#~ "Inc."
#~ msgstr ""
#~ "Copyright &copy; 2014, 2015, 2019, 2020, 2021 Free Software Foundation, "
#~ "Inc."

#~ msgid "John Sullivan (FSF)"
#~ msgstr "John Sullivan (FSF)"

#~ msgid "Brandon Invergo"
#~ msgstr "Brandon Invergo"

#, fuzzy
#~| msgid ""
#~| "Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu."
#~| "org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/"
#~| "\">other ways to contact</a> the FSF.  Broken links and other "
#~| "corrections or suggestions can be sent to <a href=\"mailto:"
#~| "webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
#~ msgid ""
#~ "Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu."
#~ "org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/"
#~ "\">other ways to contact</a> the FSF.  Broken links and other corrections "
#~ "or suggestions can be sent to <a href=\"mailto:webmsters@gnu.org\">&lt;"
#~ "webmasters@gnu.org&gt;</a>."
#~ msgstr ""
#~ "Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:"
#~ "gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/"
#~ "\">outros meios de contatar</a> a FSF. Links quebrados e outras correções "
#~ "ou sugestões podem ser enviadas para <a href=\"mailto:webmasters@gnu.org"
#~ "\">&lt;webmasters@gnu.org&gt;</a>."

#~ msgid ""
#~ "Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu."
#~ "org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/"
#~ "\">other ways to contact</a> the FSF.  Broken links and other corrections "
#~ "or suggestions can be sent to <a href=\"mailto:gnu-advisory@gnu.org\">&lt;"
#~ "gnu-advisory@gnu.org&gt;</a>."
#~ msgstr ""
#~ "Envie perguntas em geral sobre a FSF e o GNU para <a href=\"mailto:"
#~ "gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. Também existem <a href=\"/contact/"
#~ "\">outros meios de contatar</a> a FSF. Links quebrados e outras correções "
#~ "ou sugestões podem ser enviadas para <a href=\"mailto:gnu-advisory@gnu.org"
#~ "\">&lt;gnu-advisory@gnu.org&gt;</a>."

#~ msgid "Copyright &copy; 2014, 2015, 2019 Free Software Foundation, Inc."
#~ msgstr "Copyright &copy; 2014, 2015, 2019 Free Software Foundation, Inc."

# LANGUAGE translation of https://www.gnu.org/help/help.html
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: help.html\n"
"POT-Creation-Date: 2024-06-29 10:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Helping the GNU Project and the Free Software Movement - GNU Project - Free "
"Software Foundation"
msgstr ""

#. type: Content of: <h2>
msgid "Helping the GNU Project and the Free Software Movement"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Saying no to the use of a nonfree program or an online disservice <a "
"href=\"/philosophy/saying-no-even-once.html\">on even one occasion</a> helps "
"the cause of software freedom.  Saying no to using it <em>with others</em> "
"helps even more.  And if you tell them it is for defense of your freedom and "
"theirs, that helps even more."
msgstr ""

#. type: Content of: <p>
msgid ""
"Beyond that, you can also help by volunteering to do work.  This page lists "
"many kinds of work we need."
msgstr ""

#. type: Content of: <address>
msgid ""
"For general questions about the GNU project, mail <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a> and for questions and "
"suggestions about this web site, mail <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#. type: Content of: <div><h3>
msgid "Table of Contents"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#develop\">Help develop the GNU operating system</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#smallprograms\">Important new small-to-medium programs needed</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#helpgnu\">Help support GNU development and use</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"#awareness\">Spread awareness about GNU and the Free Software "
"Movement</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid "<a href=\"#hnode\">Help improve h-node.org</a>"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"#fsf\">Volunteer with and donate to the Free Software "
"Foundation</a>"
msgstr ""

#. type: Content of: <div><h3>
msgid "Help develop the GNU operating system"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Write free <a href=\"/philosophy/free-doc.html\"> manuals and other "
"documentation</a> for GNU software."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Propose your useful software packages as GNU packages.  See the <a "
"href=\"/help/evaluation.html\">GNU software evaluation</a> information."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Write a Firefox extension that will replace the nonfree Javascript code of "
"some useful web site (when that nonfree code is blocked by LibreJS).  Please "
"see our <a href=\"/help/help-javascript.html\"> guidelines and "
"suggestions</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Do one of the <a href=\"/help/priority-projects.html\">GNU High Priority "
"Enhancement Projects</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"The <a href=\"//savannah.gnu.org/people/?type_id=1\">GNU Help Wanted</a> "
"list is the general task list for GNU software packages.  You might also "
"consider taking over one of the <a "
"href=\"/server/takeaction.html#unmaint\">unmaintained GNU packages</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"There are other <a "
"href=\"//savannah.gnu.org/people/?group=tasklist\">projects that would be "
"good to do</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Work on a project on the <a "
"href=\"https://www.fsf.org/campaigns/priority.html\">FSF's list of very "
"important free software projects</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Improve <a href=\"/accessibility/accessibility.html\">accessibility</a> of "
"free software and web pages to meet the needs of all users regardless of "
"disability."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"If you are a student, and you must do a software development project, "
"contribute it to GNU. See <a href=\"/philosophy/university.html\">how to "
"make the university let you release it as free software</a>."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Please let the GNU Volunteer Coordinators <a "
"href=\"mailto:gvc@gnu.org\">&lt;gvc@gnu.org&gt;</a> know if you start a new "
"package that you found in those lists.  We want to keep track of what tasks "
"are being worked on."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"When writing software for GNU, please follow the <a "
"href=\"/prep/standards/\">GNU Coding Standards</a> and <a "
"href=\"/prep/maintain/\">Information for Maintainers of GNU Software</a> "
"documents."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"We are sometimes offered software which already does substantially the same "
"task as an existing GNU package.  Although of course we appreciate all "
"offers, we'd naturally like to encourage programmers to spend their time "
"writing free software to do new jobs, not already-solved ones.  So, before "
"starting a new program, please check the <a "
"href=\"https://directory.fsf.org/\">Free Software Directory</a> for free "
"software that does the job already."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"We can offer <a href=\"/software/devel.html\">some resources</a> to help GNU "
"software developers."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"The GNU Volunteer Coordinators <a "
"href=\"mailto:gvc@gnu.org\">&lt;gvc@gnu.org&gt;</a> can assist you if you "
"would like to help developing GNU software. They will be able to put you in "
"touch with other people interested in or working on similar projects. When "
"you have selected a task from our task lists, please let them know you're "
"interested in working on it."
msgstr ""

#. type: Content of: <div><h3>
msgid "Important new small-to-medium programs needed"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/help/music-subtraction.html\">Free program that can subtract "
"background music</a>"
msgstr ""

#. type: Content of: <div><h3>
msgid "Help support GNU development and use"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This list is ordered roughly with the more urgent items near the top.  "
"Please note that many things on this list link to larger, expanded lists."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Help with <a href=\"//savannah.gnu.org\">Savannah</a>. We are especially "
"looking for technical sysadmin volunteers to help with underlying "
"infrastructure support.  Volunteers to help with pending project submissions "
"are also very welcome.  Please see this <a "
"href=\"//savannah.gnu.org/maintenance/HowToBecomeASavannahHacker/\">general "
"information on how to become a savannah hacker</a>.  Please communicate with "
"us on the <a "
"href=\"https://lists.gnu.org/mailman/listinfo/savannah-hackers-public\">savannah-hackers-public</a> "
"mailing list."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Organize a new <a href=\"https://libreplanet.org/wiki/Group_list\">GNU/Linux "
"User Group</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Volunteer as a GNU Webmaster.  Start by completing the <a "
"href=\"/server/standards/webmaster-quiz.html\"> webmaster quiz</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Translate the GNU Web site into other languages.  Each translation team "
"needs several members that are native speakers of the target language (and "
"fluent in English), but it also needs at least one member that is a native "
"speaker of English (and fluent in the target language.)  More information "
"about the issue can be found at the <a "
"href=\"/server/standards/README.translations.html\">Guide to Translating the "
"www.gnu.org Web Pages</a>.  Write to <a "
"href=\"mailto:web-translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a> "
"if you want to help."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"We need native English speakers with a good command of the language to "
"proofread English text written by GNU package maintainers and other "
"volunteers. These texts can be articles on various subjects related to free "
"software, documentation, or sometimes GNU manuals that need to be prepared "
"for printing. To help with this task, please subscribe to the low-traffic <a "
"href=\"https://lists.gnu.org/mailman/listinfo/proofreaders\">GNU "
"documentation proofreaders list</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"When you are talking with people that don't value freedom and community, you "
"can show them the many practical advantages of free software (see <a "
"href=\"//dwheeler.com/oss_fs_why.html\">Why Open Source / Free Software? "
"Look at the Numbers!</a> for some useful evidence).  But keep mentioning the "
"ethical issues too! Don't let their pressure change your voice into an "
"open-source voice."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Make sure that essays from our <a "
"href=\"/philosophy/philosophy.html\">philosophy section</a> and other GNU "
"URLs are linked to often in the appropriate categories.  If you'd like to "
"help us with this task, please contact the GNU Volunteer Coordinators <a "
"href=\"mailto:gvc@gnu.org\">&lt;gvc@gnu.org&gt;</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid "Donate <a href=\"/help/help-hardware.html\">hardware</a> to the FSF."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"If you or your company work supporting or developing free software in some "
"way, you can list yourself (or your company) in the <a "
"href=\"/prep/service.html\"> Service Directory</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"If you run a company that needs to hire people to work with free software, "
"you can advertise on our <a "
"href=\"https://www.fsf.org/resources/jobs/\">Free Software Job Page</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Volunteer to contact companies and suggest that they use our <a "
"href=\"https://www.fsf.org/resources/jobs/\">Free Software Job Page</a> to "
"publish their job postings.  If you would be interested in this, please "
"contact <a href=\"mailto:job-page@fsf.org\">&lt;job-page@fsf.org&gt;</a>."
msgstr ""

#. type: Content of: <div><h3>
msgid "Spread awareness about GNU and the Free Software Movement"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Inform your acquaintances about the GNU <a "
"href=\"/philosophy/philosophy.html\">philosophy</a> and <a "
"href=\"/software/software.html\">software</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"When you refer to the operating system that started as GNU with Linux added, "
"call it <a href=\"/gnu/linux-and-gnu.html\"> GNU/Linux</a>, and don't follow "
"those who call it &ldquo;Linux&rdquo;.  Once people are aware of what we "
"have already done, rather than attributing it to others, they will <a "
"href=\"/gnu/why-gnu-linux.html\">support our present and future efforts "
"more</a>.  This help takes very little of your time once you have unlearned "
"the old habit."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Show your support for the free software movement and our <a "
"href=\"/philosophy/free-sw.html\">ideas of freedom for users of "
"computing</a>, by saying &ldquo;free software&rdquo;, &ldquo;libre "
"software&rdquo; or &ldquo;free/libre software&rdquo;."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/philosophy/open-source-misses-the-point.html\"> Avoid the term "
"&ldquo;open source&rdquo;</a> which stands for rejection of our ideals."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/help/linking-gnu.html\">Add a link to this web site</a> to your "
"home pages."
msgstr ""

#. type: Content of: <div><ul><li>
msgid "Suggest that others do these things."
msgstr ""

#. type: Content of: <div><ul><li>
msgid "Offer GNU/Linux Installation or support in your local time banks."
msgstr ""

#. type: Content of: <div><h3>
msgid "Help improve h-node.org"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"h-node.org is a repository that contains information about how well "
"particular hardware works with free software."
msgstr ""

#. type: Content of: <div><p>
msgid "Help is needed in:"
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://h-node.org/wiki/page/en/Main-Page\"> Maintaining and "
"improving the h-node.org wiki</a>."
msgstr ""

#. type: Content of: <div><ul><li>
msgid ""
"Improving <a "
"href=\"https://h-node.org/wiki/page/en/h-source-code\">h-source</a> that "
"powers h-node.org and <a "
"href=\"https://h-node.org/wiki/page/en/client-for-h-node-org\">h-client</a> "
"a desktop program for submitting device information to h-node.org."
msgstr ""

#. type: Content of: <div><ul><li>
msgid "Suggesting new hardware that should be added to the database."
msgstr ""

#. type: Content of: <div>
msgid ""
"For other ways to help, see <a "
"href=\"https://h-node.org/wiki/page/en/Main-Page\"> "
"https://h-node.org/wiki/page/en/Main-Page</a>.  For communicating with "
"h-node.org users/developers, use their <a "
"href=\"https://h-node.org/wiki/page/en/mailing-lists\">mailing lists</a>."
msgstr ""

#. type: Content of: <div><h3>
msgid "Volunteer with and donate to the Free Software Foundation"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"The Free Software Foundation is the principal organizational sponsor of the "
"GNU Operating System.  The FSF also helps to spread awareness of the ethical "
"and political issues of software freedom."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Like GNU, the FSF also gets a lot of its strength from volunteers.  It's a "
"great place to volunteer and a great community to join, especially if you "
"don't have the technical background to contribute directly to free software "
"development.  Check out the FSF's <a "
"href=\"https://fsf.org/volunteer\">volunteering page</a> to get started or "
"the <a href=\"https://www.fsf.org\">homepage</a> to learn more about the "
"organization."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Support the FSF and the GNU Project financially by becoming an FSF <a "
"href=\"https://my.fsf.org/join\">associate member</a>, <a "
"href=\"https://www.fsf.org/about/ways-to-donate/\">donating</a> to the FSF, "
"<a href=\"https://shop.fsf.org\">purchasing</a> manuals, t-shirts, stickers, "
"and gear from the FSF, or by <a href=\"/philosophy/selling.html\">selling "
"free software</a> and donating some of the proceeds to the FSF or another "
"free software organization.  By funding development, you can advance the "
"world of free software."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a "
"href=\"mailto:gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>.  There are also <a "
"href=\"/contact/\">other ways to contact</a> the FSF.  Broken links and "
"other corrections or suggestions can be sent to <a "
"href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a "
"href=\"/server/standards/README.translations.html\">Translations README</a> "
"for information on coordinating and contributing translations of this "
"article."
msgstr ""

#. type: Content of: <div><p>
msgid "Copyright &copy; 1996-2008, 2013-2024 Free Software Foundation, Inc."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" "
"href=\"http://creativecommons.org/licenses/by-nd/4.0/\">Creative Commons "
"Attribution-NoDerivatives 4.0 International License</a>."
msgstr ""

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr ""

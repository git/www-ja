/*
layout.css -- css stylesheet used on www.gnu.org
(This version should be used with reset.css. A few basic parameters that used
to be set in combo.css are now defined here.)

Copyright (C) 2006-2011, 2013-2017, 2019-2021 Free Software Foundation

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.
*/

/*
   NOTE: Changes to this file will affect the entire site, often in
         unexpected ways. Please mail patches to www-discuss@gnu.org rather
         than commit changes directly.

   NOTE: After modifying this file, please generate the corresponding minified
         version, otherwise the changes won't be visible.

         To generate the minified version of this file, we currently use a free
         program called YUI Compressor and run this command:

         yuicompressor layout.css > layout.min.css
*/


/* CONTRAST

   source: http://juicystudio.com/services/luminositycontrastratio.php

   Color1       Color 2       Contrast      Passing
                               ratio         level
Black-gray
   #000         white          21.30          AAA
   #222         white          15.91          AAA
   #333         white          12.63          AAA
   #333         #eee           10.89          AAA


Red
   #a32d2a      white           7.08

Blue (non-visited links)
   #049         white           9.18          AAA
                #eee            7.91          AAA
                #ddd            6.76          AA
                #333            1.38
                #222            1.73
                black           2.29

Purple (visited links)
   #503         white          14.64          AAA
                #f3f3f3        13.20
                #222            1.09
                black           1.43
                #049            1.59
*/


/* PAGE STRUCTURE

From top to bottom:
   skiplink, #fsf-frame (#join-fsf, #fssbox),
   #header (#gnu-banner, #switches)              [body-include-1.html]
   #navigation                                   [body-include-2.html]
   (#edu-navigation / #navlinks in a few pages)  [<page>.html]
   #content                                      [<page>.html]
   #languages                                    [footer-text.html]
   #mission-statement                            [footer-text.html]
   #footer                                       [<page>.html]
*/


/* TABLE OF CONTENTS

   * MAIN DIVS & CONSTANT FEATURES
     - html, body, .inner
     - header for CSS3-unaware browsers
         #gnu-banner
         searcher, language & menu icons
     - site navigation (layout, style)
     - languages for CSS3-unaware browsers
     - media queries for header, navbars & languages
         #fsf-frame (> 57em)
     - #content
         navigation (breadcrumb)
         byline
         text of the article (reduced-width, columns, infobox)
         for translations (trans-disclaimer, #outdated, translators-notes)
         for the Education section (edu-cases)
         for the Malware section (about-dir, about-page)
     - #mission-statement
     - #footer

   * BUILDING BLOCKS
     - simple text styling (small, strong, em, etc.)
     - basic classes, mostly replacing inline elements like small or em
     - blocks of text (p, blockquote, etc.)
     - h* headers
     - lists
     - separators
     - tables

   * IMAGES & EMPHASIZED TEXT
     - images (imgleft/imgright, pict)
     - text with no background or border (comment)
     - text with background and/or border (emph-box, highlight-para,
       highlight, lyrics, announcement, important)
     - notes (note & edu-note)
     - table of contents (toc, summary & toc-inline)
     - media queries for note & summary

   * INTERACTIVE ELEMENTS
     - links (special links, style)
     - forms & buttons (layout, style)

   * FONT FAMILY
*/

/*  OVERVIEW OF TEXT CLASSES
 ____________________________________________________________________________
|                |                                                           |
|     Class      | Applies to   Background     Border    Width   Floats on   |
|                |                                              wide screens |
|________________|___________________________________________________________|
|                                                                            |
| ** General use                                                             |
| emph-box       |  div p pre   light gray    complete     -         -       |
| important      |    block         -       left, orange   -         -       |
| highlight      |    p span      orange         -         -         -       |
|----------------------------------------------------------------------------|
| ** Special parts of the article                                            |
| comment        |    block         -            -         -         -       |
| lyrics         |     id.      light gray    complete     -         -       |
|----------------------------------------------------------------------------|
| ** Asides                                                                  |
| highlight-para |    div p       orange    top & bottom   -         -       |
| announcement   |    block         -       left, green    -         -       |
| note           | div blockquote   -          green       +         +       |
| edu-note       |     id.          -          green       +         -       |
|----------------------------------------------------------------------------|
| ** Tables of contents                                                      |
| toc            |     div      light gray       -         +         -       |
| summary        |     div      light gray       -         +         +       |
| toc-inline     |     div            button-like          -         -       |
|____________________________________________________________________________|
*/



/*====================================================================*/
/*                   MAIN DIVS & CONSTANT FEATURES                    */
/*====================================================================*/


html, body {
   font-size: 1em;  /* 16px */
   text-align: left;
   text-decoration: none;
   color: #222;
}
html { background: #e4e4e4; }

/* This specifies the basic width of our web pages.  Don't change it
   without discussion on www-discuss.  The magic 74.92 is for
   consistency with fsf.org.  */
body {
   max-width: 74.92em;   /* About 1200px */
   padding: 0;
   margin: 0 auto;
   background: white;
   border: .1em solid #bbb;
   border-top: 0;
   -moz-box-shadow: 0 0 5px 5px #bbb;
   -webkit-box-shadow: 0 0 5px 5px #bbb;
   -icab-box-shadow: 0 0 5px 5px #bbb;
   -o-box-shadow: 0 0 5px 5px #bbb;
   box-shadow: 0 0 5px 5px #bbb;
}
div {
   padding: 0;
   margin: 0;
}
.inner { overflow: hidden; }


/**************************************/
/*  HEADER FOR CSS3-UNAWARE BROWSERS  */
/**************************************/

/*** TOP LINKS ***/

#top { border-top: 3px solid #a32d2a; }

.skip, #fsf-frame {
   position: fixed;
   top: -1000px;
}

/*** BANNER and SWITCHES ***/

#header {
   text-align: center;
   display: table;
   width: 100%;
}
#gnu-banner {
   line-height: 1em;
   padding: .6em 0;
}
#gnu-banner, #switches {
   display: table-cell;
   text-align: center;
   vertical-align: middle;
   padding: .6em;
}

#gnu-banner img {
   vertical-align: middle;
   height: 3em; width: auto;
   margin-right: .15em;
}
#gnu-banner strong {
   vertical-align: middle;
   font-weight: normal;
   font-size: 2em;
}
#gnu-banner .hide, #fsf-support {
   display: none;
}

.switch {
   display: inline-block;
   vertical-align: middle;
   line-height: 1.1em;
   padding: .2em 8%;
}
.switch img {
   height: 1.875em;
   width: auto;
}
@media (max-width:20em) {
   .switch { padding: .2em; }
}


/*********************/
/*  SITE NAVIGATION  */
/*********************/

/** LAYOUT **/

/* 'submenu' would be a more appropriate name than 'edu-navigation'. */
#navigation, #edu-navigation {
   font-size: .94em;
   font-weight: bold;
}
#navigation ul { margin: 0; }
#edu-navigation { margin: 0 -3.2%; }

#navigation ul, #edu-navigation {
   line-height: 2.125em;
   padding: 0 2% ;
}
#navigation li, #edu-navigation li {
   white-space: nowrap;
   display: inline;
   line-height: 2.125em;
   margin: 0 .2em;
}
#navigation li a, #edu-navigation a {
   display: inline-block;
   line-height: 2em;
   padding: 0 .6em;
   margin-top: .125em;
}
#more-links, #less-links { display: none; }

/** STYLE **/

#navigation a, #edu-navigation a {
   text-decoration: none;
}
#navigation     { background: #a32d2a; }
#edu-navigation { background: #eee; }

#navigation a     { color: white; }
#edu-navigation a { color: #333; }

#navigation li a:hover,
#navigation li.active a     { background: #800300; }
#edu-navigation li a:hover,
#edu-navigation li.active a { background: white; }

#edu-navigation li.active a,
#navigation li.active a {
   font-style: italic;
   cursor: default;
}


/*****************************************/
/*  LANGUAGES FOR CSS3-UNAWARE BROWSERS  */
/*****************************************/

#language-container .backtotop {
   margin: 0 3%;
}
#language-container .backtotop a {
   margin-top: .5em;
}
#languages .close { display: none; }
#languages {
   font-size: .94em;  /* 15px */
   line-height: 1.2em;
   text-align: left;
   padding: .6em 3%;
   margin: 0;
   background: #f4f4f4;
   border-top: 3px solid #ddd;
}
#set-language { margin-bottom: .2em; }
#set-language + p { 
   display: inline-block;
   font-size: 1em;  /* 15px */
   margin: .45em 0 0;
}

#translations { padding: .4em 0; }
#translations p { margin: 0; }
#translations span {
   display: inline-block;
   width: 10.5em;
   line-height: 2em;
}
#translations span a {
   line-height: 1.9em;
}
#translations span.original { font-weight: bold; }


/***************************************************/
/*  MEDIA QUERIES FOR BANNER, NAVBARS & LANGUAGES  */
/***************************************************/

@media (min-width: 0) {
   html { font-size: .94em; }

/* Display/hide language list */

   #language-container { display: none; }
   #language-container:target { display: block; }
   .backtotop b { display: none; }

   body { position: relative; }
   #language-container {
      position: absolute;
      top: -3px; left: 0;
      width: 100%;
      height: 100%;
   }
   #language-container .backtotop a {
      position: absolute;
      width: 100%;
      height: 100%;
      background: transparent;
      border: none;
      margin-top: 0;
   }
   #languages .close {
      float: right;
      display: block;
   }
   #languages .close span {
      display: none;
   }
   #languages .close:after {
      content: "\2A09";
      float: right;
      position: relative; bottom: .1em;
      cursor: pointer;
   }
   #languages {
      position: relative;
      margin-top: 4em;
      float: right;
      overflow: auto;
      max-width: 21em;
      text-align: left;
      padding: .5em 1em;
      background: white;
      border: 2px solid #bbb;
      box-shadow: 0 0 1em 1em #9999;
   }
   #translations {
      column-width: 10em;
      column-count: 3;
      column-gap: 0;
   }
   #translations span { display: block; }
   #translations span a { display: inline-block; }

/* Expand/shrink navbar */

   #navigation, #edu-navigation { font-size: 1em; }
   #navigation {
      height: 2.25em;
      overflow: hidden;
   }
   #more-links { display: block; }
   #navigation:target { height: auto; }
   #navigation:target #more-links { display: none; }
   #navigation:target #less-links { display: block; }
   #more-links, #less-links {
      float: right;
      line-height: 2em;
      padding: 0 .6em;
      background: #800300;
      margin: .125em 2% 0;
      overflow: hidden; /* Removes extra clickable space below the button. */
   }
   #more-links span, #less-links b { display: none; }
   #more-links:after {
      content: "\226B";
      position: relative; bottom: .05em;
   }
   #less-links:after {
      content: "\2A09";
      font-weight: bold;
      position: relative; bottom: .1em;
   }
}

@media (min-width: 34em) {
   #languages { max-width: 36em; }
}

@media (min-width: 45em) {                           /* 720px */
   html { font-size: 1em; }

   #gnu-banner .hide {
      display: inline;
      vertical-align: middle;
      font-size: 2em;
   }
   #fsf-support {
      display: block;
      font-size: .8em;  /* 14px */
   }
   #fsf-support a {
      font-weight: bold;
   }

   #languages { margin-top: 5.5em; }
}

@media (min-width: 57em) {                           /* 912px */
   #fsf-frame {
      position: static;
      display: table;
      font-size: .875em; /* 14px */
      width: 94%;
      padding: .3em 3%;
   }
   #fssbox, #join-fsf {
      display: table-cell;
      vertical-align: middle;
   }
   #join-fsf { text-align: right; padding-left: 2em; }
   #fssbox a { font-weight: bold; }
   #fssbox div {
      display: inline;
      white-space: nowrap;
   }
   #languages { margin-top: 8.5em; }
}


/*************/
/*  CONTENT  */
/*************/

#content {
   padding: 1px 3% 0;
   margin-bottom: 3em;
}


/** Navigation **/

.breadcrumb {
   display: inline-block;
   vertical-align: top;
   line-height: 1.6em;
   margin: .5em 1.5em 0 0;
   text-align: left;
}
.breadcrumb img {
   height: 1.4em; width: auto;
   padding: 0;
}
.breadcrumb a { white-space: nowrap; }


/** Author line after the main heading **/

address.byline {
   font-size: 1.06rem;  /* 17px */
   font-style: italic;
   margin-bottom: 1.5em;
}


/** Text of the article **/

.article { font-size: 1.06em; }  /* 17px */

.reduced-width { max-width: 100%; }
@media (min-width: 51em) {
   .reduced-width, .reduced-width + .translators-notes {
      width: 48em;
      margin: 1.5em auto;
   }
   .article.reduced-width { width: 45em; }
}


/** Footnotes **/

h3.footnote + ol, h3.footnote + ul { font-size: 1rem; }


/** Back links **/
/* ('forward' would be more appropriate) */

.back {
   line-height: 1.7em;
   text-align: right;
   font-weight: bold;
   margin: 2em 2%;
}
.back a { display: inline-block; }


/** Metadata, notes, etc. **/

.infobox {
   font-size: .94em;
   line-height: 1.3em;
   color: #444;
}
.infobox p {
   margin: .4em 0 0;
}
.infobox hr {
   display: block;
   width: 15em; max-width: 100%;
   height: 1px;
   border: none;
   margin: 0 0 1em;
}
.infobox hr.full-width {
   width: 100%;
   margin: 1.5em 0;
}
div.infobox { margin: 2.5em 0 0; }


/* Note about Free Software Free Society */
blockquote#fsfs p {
   padding: .3em 0;
   font-size: 1.2em;
}
p#fsfs { margin: 2.5em 0; }


/** Translations **/

/* For the note saying the page is a translation */
.trans-disclaimer {
   clear: both;
   text-align: center;
   font-style: italic;
   position: relative;
   top: .4em;
   margin: .2em 0 0;
}
@media (max-width: 30em) {
   .breadcrumb ~ .trans-disclaimer,
   .nav ~ .trans-disclaimer { text-align: right; }
}

/* For outdated translations */
#outdated {
   clear: both;
   font-size: .94em;  /* 15px */
   width: 48em;
   max-width: 85%;
   padding: .5em .8em .7em;
   border: .2em solid #fc7;
   margin: .8em auto;
}
#outdated p {
   display: inline;
   margin: 0;
   color: black;
}
#outdated a.original-link {
   font-weight: bold;
   color: brown;
}

#outdated p.hide { display: none; }
@media (min-width: 50em) {
   #outdated p.hide { display: inline; }
}

/* For translators notes */
.translators-notes {
   font-size: 1rem;
   clear: left;
   line-height: 1.5em;
   margin-top: 2em;
}
.translators-notes hr {
   height: 1px;
   margin: 1.7em 0 1.2em;
   border: none;
}
.translators-notes p, .translators-notes ul, .translators-notes ol {
   margin-bottom: 0;
}
.translators-notes ol li { margin: .5em 1.1em 0; }


/** Subsections of /education "Case Studies" **/

.edu-cases {
   font-size: 1.06em;                        /* 17 px */
   width: 44.3em; max-width: 100%;
   margin: 2em auto;
}
.edu-cases ul, .edu-cases ol { margin: 1em 4%; }
.edu-cases h3 { font-size: 1.3em; }          /* 22 px */


/***********************/
/*  MISSION STATEMENT  */
/***********************/

#mission-statement {
   padding: .5em 3% 2em;
   background: white;
   border-top: 3px solid #999;
}
#mission-statement blockquote {
   font-size: .94em;  /* 15px */
   font-weight: bold;
   font-style: italic;
   margin: .5em 3% .8em;
}
#mission-statement p { margin: 0; }
#mission-statement img {
   float: left;
   height: 2em;
   margin: .5em;
}
#support-the-fsf {
   clear: both;
   text-align: center;
}


/************/
/*  FOOTER  */
/************/

#footer {
   font-size: .875em;  /* 14px */
   padding: 1.5em 3%;
   color: #333;
   background: #f4f4f4;
   border-top: 3px solid #999;
}
#footer p, #bottom-notes {
   line-height: 1.3em;
   margin: 0;
}
.unprintable { margin-bottom: .7em; }

.translators-credits { margin: .7em 0; }

#footer p.unprintable, #generic p { margin-top: .7em; }



/*====================================================================*/
/*                          BUILDING BLOCKS                           */
/*====================================================================*/


/*************************/
/*  SIMPLE TEXT STYLING  */
/*************************/

small { font-size: .875em; }  /* 16px -> 14px */
strong { font-style: inherit; font-weight: bold; }
em, cite, var, dfn { font-style: italic; font-weight: inherit; }
em i, em cite, cite i, cite em, i em, i cite, i i { font-style: normal; }
acronym, abbr {
   font-variant: normal;
   text-decoration: none;
   border-bottom: 1px dotted #000;
   cursor: help;
}
code, samp, kbd {
   font-family: monospace;
   padding: 0 .2em;
}
code, kbd { background: #f2f2f2; }
kbd { color: #752b1b; }

/* Rarely used */
ins { text-decoration: none; }
del { text-decoration: line-through; }
sup { vertical-align: super; }
sub { vertical-align: sub; }


/*******************/
/*  BASIC CLASSES  */
/*******************/

.center, .c { text-align: center; }
.nocenter { text-align: left; }
.right-align { text-align: right; margin: 2em 0 2em 10%; }
.inline-list li { display: inline; }
.no-bullet li { list-style: none; }
.no-display { display: none; }
.clear { clear: both; }


/********************/
/*  BLOCKS OF TEXT  */
/********************/

/* The top margin should be sufficient. The bottom margin is only useful
   if the element that follows is an anonymous box. */
p, pre, address {
   line-height: 1.5em;
   margin: 1em 0;
}
blockquote { margin: 1em 3%; }

p, blockquote { padding: 0; }
pre { padding: 0 0 .3em; overflow: auto; }


/*************/
/*  HEADERS  */
/*************/

h1 { font-size: 2.375em; margin: .7em 0; }                       /* 38px */
h2 { font-size: 2em;     margin: .7em 0; }                       /* 32px */
.article h2 { font-size: 1.89em; }                               /* 32px */
h3 { font-size: 1.5em;   margin: 1.2em 0  .8em; }                /* 24px */
.article h3 { font-size: 1.41em; }                               /* 24px */
h4 { font-size: 1.25em;  margin: 1.2em 0  .85em; }               /* 20px */
.article h4 { font-size: 1.18em; }                               /* 20px */
h5 { font-size: 1.125em; margin: 1.3em 0  .9em; }                /* 18px */ 
.article h5 { font-size: 1.06em; }                               /* 18px */
h6 { font-size: 1em;     margin: 1.3em 0 1.0em; }                /* 16px */ 

h2 { clear: both; }

h1, h2, h3, h4 { font-weight: bold; }
h1, h2, h3  { padding: .2em 0; }
h3 { color: #333; }
h4 { color: #505050; }
h5, h6 { font-weight: normal; font-style: italic; } 

.infobox h3, h3.footnote,
.translators-notes h3, .translators-notes > b, .translators-notes > strong {
   font-size: 1.19em;
   font-size: 1.19rem;  /* 19px */
}
.translators-notes > b, .translators-notes > strong {
   margin: 1.2em 0 .8em;
   color: #333;
}


/** BIG SECTION HEADERS **/

/* For browsers that don't support inline-block */
.big-section {
   clear: left;
   float: left;
}
.big-section h3 {
   display: inline-block;
   font-size: 1.75em;  /* 28px */
   margin: 1em .3em .5em 0;
   color: black;
   border-top: .12em solid #e74c3c;
   border-bottom: .12em solid #e74c3c;
}
.article .big-section h3 { font-size: 1.65em; }   /* 28px */

.big-subsection { margin: 1.5em 0; }
.big-subsection h4 {
   display: inline;
   font-size: 1.5em;   /* 24px */
   margin-right: .3em;
   color: black;
}
.article .big-subsection h4 { font-size: 1.41em; }    /* 24px */

/* The next two statements adjust line spacing in Netsurf 1.2 */
.big-subsection {
   line-height: 2.0em;
}
.big-subsection h4 {
   line-height: 1.2em;
}

/* Anchor - This is used in pages of lists, such as gnu-linux.faq.html,
   to give readers a hint that they can link directly to a given item.
   We make it less obtrusive than the item heading it follows.  */
.anchor-reference-id { font-weight: normal; font-size: .8125em; } /* 13px */


/** HEADERS ON NARROW SCREENS **/

@media (max-width: 30em) {    /* 480 px - 1em = 15px */
   h1 { font-size: 2em; }                              /* 30px */
   h2 { font-size: 1.73em; }                           /* 26px */
   .article h2 { font-size: 1.63em; }                  /* 26px */
   h3 { font-size: 1.4em; }                            /* 21px */
   .article h3 { font-size: 1.32em; }                  /* 21px */
   h4 { font-size: 1.2em; }                            /* 18px */
   .article h4 { font-size: 1.13em; }                  /* 18px */
   h5 { font-size: 1.07em; }                           /* 16px */

   .big-section h3    { font-size: 1.53em; }           /* 23px */
   .article .big-section h3    { font-size: 1.44em; }  /* 23px */
   .big-subsection h4 { font-size: 1.4em; }            /* 21px */
   .article .big-subsection h4 { font-size: 1.32em; }  /* 21px */
}


/***********/
/*  LISTS  */
/***********/

ul, ol, li, dl, dd, dt { padding: 0; }
li, dt, dd { line-height: 1.5em; }

ul li { list-style: square outside; }
ul ul li, ol ul li { list-style: circle; }
ol li, #content ul li ol li { list-style: decimal outside; }

ul, ol { margin: 1em 1.5%; }

/* Lists of underlined links are difficult to read. The top margin
   gives a little more spacing between entries. */
ol li { margin: 1em 0 0 1em; }
ul li, ul ol li { margin: .5em 0 0 1em; }
ul ul li { margin-top: .3em; }
ul li p, ul li pre, ul li blockquote, table li {
   margin-top: .3em; margin-bottom: .3em;
}
ul ul, ol ul, table ul { margin: 0 1.5%; }

/* For items that are whole paragraphs (e.g., gnu-website-guidelines) */
ul.para > li { margin-top: 1em; }

/* For dated items  (e.g., malware, essays-and-articles)*/
.date-tag {
   padding: .2em .5em;
   color: #505050;
   background: #f2f2f2;
   border-radius: .25em;
}

/* For malware items */
ul.blurbs li { margin-top: 1em; }
ul.blurbs ul li, ul.blurbs ol li, ul.blurbs dl li {
   margin-top: .5em;
}
ul.blurbs > li { list-style: none; }

.blurbs .date-tag { position: relative; right: 2em; }
@media (max-width:30em) {
   .blurbs .date-tag { right: 1.5em; }
}

/* Separate description lists from preceding text */
dl { margin: 1.5em 0 0; }
/* separate the "term" from subsequent "description" */
dt {
   font-weight: bold; 
   color: #333;
   margin: 1em 0;
}
/* separate the "description" from subsequent list item
   when the final <dd> child is an anonymous box */
dl dd { margin: 1em 3% 1.5em; }

dl.compact { margin: .5em 0; }
dl.compact dt {
   font-weight: normal;
   font-style: italic;
   margin: .5em 0 0;
}
dl.compact dd { margin: .3em 3% 0; }
dl.compact dd p { margin: .3em 0 0; }
dl.compact dd ul { margin: .3em 1.5% 0; }


/****************/
/*  SEPARATORS  */
/****************/

hr {
   display: block;
   margin: 1.2em 0;
   color: #999;
   background: #999;
}

/* For license-list.html, but could be used elsewhere. */
hr.separator {
   height: .3em;
   border: none;
}
/* Can be applied to div or hr. */
.thin {
   clear: both;
   height: 2px;
   margin: 1.5em 0;
   border: none;
   background: #bbb;
}
/* Can be applied to div or hr. */
.column-limit {
   height: .4em;
   width: 15%; min-width: 7em;
   margin: 2em auto;
   border: none;
   background: #bbb; /* default */
   background-image: linear-gradient(to right, white, #bbb, white);
}


/************/
/*  TABLES  */
/************/

table {
   border-collapse: collapse;
   border-spacing: 0;
   margin: 1.5em 0;
}
caption {
   font-style: inherit;
   font-weight: inherit;
   text-align: center;
   margin-bottom: .5em;
}
th, td {
   border: 1px solid #bbb; /* default */
   padding: .5em;
   margin: 0;
}
th { font-weight: bold; text-align: center; }

th, td, td p, td li { line-height: 1.3em; }

.listing {
   /* The default table for document listings. Contains name, document
   types, modification times etc in a file-browser-like fashion */
   width: 100%;
   display: block;
   overflow-x: auto;
   border: .2em solid #ccc;
}
.listing th {
   font-weight: normal;
   padding: .7em;
   color: black;
   background: #fff1c0;
   border: 1px solid #999;
   /* for browsers that don't know about border-collapse: */
   border-bottom: 0;
}
.listing td {
   padding: 1em;
   text-align: center;
   border: 1px solid #999;
}
.listing img {
   vertical-align: middle;
}
.listing .odd {
   /*every second line should be shaded */
   background: transparent;
}
.listing .even {
   background: #f4f4f4;
}


/*************/
/*  COLUMNS  */
/*************/

.columns p.inline-block {
   display: inline-block;
   margin: 0;
}
.columns > *:first-child { margin-top: 0 }
.columns > *:last-child { margin-bottom: 0; }

@media (min-width: 55em) {
   .columns {
      -webkit-column-count: 2;
      -webkit-column-gap: 1.5em;
      -moz-column-count: 2;
      -moz-column-gap: 1.5em;
      column-count: 2;
      column-gap: 1.5em;
   }
}



/*====================================================================*/
/*                      IMAGES & EMPHASIZED TEXT                      */
/*====================================================================*/


/************/
/*  IMAGES  */
/************/

img {
   vertical-align: top;
   border: 0;
}

/** SMALL FLOATING IMAGES **

/* The floating direction will be reversed for rtl languages. */
.imgright, .imgleft { max-width: 100%; }
.imgright { float: right; margin: .3em 0 1em 2em; }
.imgleft  { float: left;  margin: .3em 2em 1em 0; }


/** ALL IMAGES, with or without legends **/

/* 'narrow', 'medium' and 'wide' define both image width and breakpoint
   between centered and floating image. If the preset combination doesn't
   work, it is easier to redefine width than breakpoint. */
/* 'pict' without 'narrow', 'medium' or 'wide' will always be centered
   and resize to fit the screen. Image width needs to be defined in a
   style element or attribute. */
.narrow { width: 15em; }
.medium { width: 20em; }
.wide   { width: 27em; }
.pict { max-width: 100%; margin: 1em auto; }
.pict img { width: 100%; height: auto; }
.pict p {
   text-align: center;
   font-style: italic;
   font-size: .875em;  /* 14px */
   margin-top: .5em;
}

@media (min-width: 45em) {
   .pict.narrow {
       max-width: 45%;
       float:right; margin: .3em 0 1em 2em;
   }
}
@media (min-width: 50em) {
   .pict.medium {
       max-width: 45%;
       float:right; margin: .3em 0 1em 2em;
   }
}
@media (min-width: 57em) {
   .pict.wide {
       max-width: 45%;
       float:right; margin: .3em 0 1em 2em;
   }
}


/***************************************/
/*  TEXT WITH NO BACKGROUND OR BORDER  */
/***************************************/

/* This replaces <blockquote> for paragraphs that are not quotations. */
.indent-para { margin-left: 3%; margin-right: 3%; }

/* Watch out, 'comment' is suppressed by the reading mode of Firefox. */
.comment, .introduction {
   font-style: italic;
   margin: 1.5em 6%;
}
.article .comment { font-size: .94em; }
.comment cite, .introduction cite { font-style: normal; }

.epigraph { margin: 2em 0 2em 10%; color: #444; }

@media (max-width: 30em) {
   .comment, .introduction { margin: 1.5em 0; }
}

/****************************************/
/*  TEXT WITH BACKGROUND AND/OR BORDER  */
/****************************************/

/* Strip bottom margins of children. */

.emph-box *, .highlight-para *, .lyrics *,
.announcement *, .important * { margin-bottom: 0; }


/** announcement, important **/

.announcement, .important {
   padding: .4em 1em;
   margin-top: 1.5em;
   margin-bottom: 1.5em;
}
.announcement blockquote { margin: 0; }
.announcement p, .important p {
   padding-top: .4em;
   padding-bottom: .5em;
   margin: 0;
}
.announcement > ul, .important > ul { margin: 0; }
.announcement li, .important li {
   padding-top: .2em;
   padding-bottom: .3em;
   margin-top: 0;
}
.announcement h3, .important h3 {
   font-size: 1.25em;  /* 20px */
   margin: .35em 0 .4em;
}
.announcement { border-left: .4em solid #5c5; }
.important    { border-left: .4em solid #fc7; }

@media (min-width: 48em) {
   .announcement, .important { padding: .4em 1.5em; }
}


/** emph-box, highlight-para, highlight **/

blockquote.highlight-para, blockquote.emph-box,
div.highlight-para, div.emph-box {
   padding: 0 1.2em 1.1em;
   margin: 2em 0;
}
p.highlight-para, p.highlight,
p.emph-box, pre.emph-box { padding: .7em 1.2em .8em; }

.emph-box h3, .highlight-para h3, .lyrics h3 {
   font-size: 1.25em;  /* 20px */
   margin: .8em 0;
}
.emph-box h4, .highlight-para h4, .lyrics h4 {
   font-size: 1em;
}
pre.emph-box code, pre.emph-box kbd, pre.emph-box samp,
p.emph-box code, p.emph-box kbd, p.emph-box samp { padding: 0; }

.highlight-para, .highlight { background: #fff5d4; }
span.highlight { background: #fff1c0; }
.highlight-para {
   border-top: .1em solid #fc7;
   border-bottom: .1em solid #fc7;
}

.emph-box {
   background: #f2f2f2;     /* to match <code> and <kbd> */
   border: .1em solid #bbb;
}


/** lyrics **/

.lyrics {
   display: inline-block;
   max-width: 80%;
   font-style: italic;
   padding: .5em 2em 1.7em;
   background: #f7f7f7;
   border: .1em solid #bbb;
   border-radius: .5em;
   margin: .5em 0 .5em 6%;
}
@media (max-width: 45em) {
   .lyrics {
      box-sizing: border-box;
      max-width: 100%;
      padding: 0 1em 1.2em;
      margin-left: 0;
   }
}


/***********/
/*  NOTES  */
/***********/

/** LAYOUT **/

.note, .edu-note {
   margin: 2em auto;
}

/* Single <p> */
.edu-note {
   width: 37.7em;
   max-width: 100%;
}
.edu-note p {
   font-style: italic;
   padding: .7em 1em .8em;
   margin: 0;
}

/* Several <p>'s */
.note {
   width: 45em;
   max-width: 82%;
   padding: .3em 3% .4em;
   background: white;
}
.note p {
   padding: .4em;
   margin: 0;
}

/** STYLE **/

.note, .edu-note p { border: .15em solid #0aa; }
.edu-note p { border-radius: .5em; }


/************************/
/*  TABLES OF CONTENTS  */
/************************/

/** LAYOUT **/

.summary, .toc {
   width: 45em;
   text-align: center;
   padding: 1.3em 3%;
   margin: 2em auto;
   background: #f4f4f4;
}
.summary { max-width: 82%; }
.toc { max-width: 94%; }
.summary > ul, .toc > ul {
   text-align: left;
   font-size: .94em; font-size: .94rem;
   display: inline-block;
   margin: 0 .5em 0 0;
}
.summary ul ul, .toc ul ul { margin: 0; }
.summary li, .toc li {
   margin: 0 0 0 1.5em; padding: .1em 0;
}

.summary h3, .toc h3 {
   font-size: 1.125em;   /* 18px */
   margin-top: .3em;
}
.summary h4, .toc h4 {
   text-align: left;
   font-size: 1em;
}
.summary a, .toc a {
   display: block;
   padding: .2em 0;
}

.toc-inline {
   font-size: .94em;
   text-align: center;
   padding: 0 3%;
   margin: 2em auto;
}
.toc-inline a {
   display: inline-block;
   padding: .1em .4em;
   margin: .3em;
   background: #f2f2f2;
   border: .1em solid #bbb;
}
.toc-inline h3 {
   display: none;
}
.toc-inline h4 {
   font-size: 1em;
   margin: 1em 0 .2em;
}
.toc-inline h4 a {
   border: none;
}
.toc-inline ul, .toc-inline li {
   display: inline;
   margin: 0;
}


/**************************************/
/*  MEDIA QUERIES FOR NOTE & SUMMARY  */
/**************************************/

/* Baby NetSurf (1.2) would hurt itself using these definitions.
   Place them out of reach. Too bad for its older brother (2.9). */
@media (max-width: 30em) {
   .note, .summary { max-width: 92%; }
   /* Override margin definition in free-sw.html. */
   #content .note { margin-left: auto; margin-right: auto; }
}
@media (min-width: 48em) {
   .note, .summary {
      clear: right;
      float: right;
      width: 20em;
      max-width: 40%;
      margin: .5em 0 1em 2em;
   }
   #content .note { margin-right: 0; margin-left: 2em; }
   .summary { padding: 1em .8em; }
   .note { padding: .3em .8em .4em; }
}

/* ~~~~~~~~~~~~~~~~~~~ For outdated translations ~~~~~~~~~~~~~~~~~~~ */

/* proprietary.html */
div.toc .malfunctions h3, div.toc .companies h3 {
   padding: 0;
   margin: 0 0 .5em;
}
#content .malfunctions, #content .companies {
   display: inline-block;
   padding: .5em .5em 0; margin: 0;
}
#content .toc .malfunctions { max-width: 29em; }
#content .toc .companies { max-width: 14em; }
#content .malfunctions ul, #content .companies ul {
   padding: 0; margin: 0;
}
#content div.toc .malfunctions li, #content div.toc .companies li {
   padding: 0 0 .5em; margin: 0;
}

/* proprietary.de.html & malware-*.de.html */
.malfunctions a + br, .summary a + br { display: none; }

/* malware-amazon.(de|it).html */
#content div.toc.c { background: none; }
#content div.toc.c h3 { display: none; }
#content div.toc.c a {
   display: inline-block;
   padding: .1em .4em;
   margin: .3em;
   background: #f2f2f2;
   border: .1em solid #bbb;
}
#content div.toc.c a:hover { background: white; }

/* education.html */
#content #video-container { width: 37.7em; margin: 2em auto; }
#video-container .emph-box { font-size: smaller; margin-top: 0; }

/* software.html */
div.package-list a, div.package-list a:visited {
   margin-bottom: .1em;
}
@media (min-width: 48em) { 
   #content #dynamic-duo { 
      width: 19.2em; max-width: 38%;
      margin-bottom: 1.5em;
   }
   #dynamic-duo p { font-size: .875em; }
}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */



/*====================================================================*/
/*                        INTERACTIVE ELEMENTS                        */
/*====================================================================*/


/***********/
/*  LINKS  */
/***********/

/* Inline links */

a[href] {
   text-decoration-color: #bbb;
}
a[href]:link {
   color: #049;
}
a[href]:visited {
   color: #503;
}
a[href]:active { text-decoration: none; }

/* Navigation links */

#gnu-banner > a {
   color: #333;
   text-decoration: none;
}
#gnu-banner > a strong {
   color: #a32d2a;
}
#fsf-support,
#fsf-support         a[href],
#fssbox              a[href],
.breadcrumb          a[href],
.back                a[href],
.anchor-reference-id a[href] {
   color: #4b4b4b;
}
#translations        a[href],
.trans-disclaimer    a[href],
.toc                 a[href],
.toc-inline          a[href],
.summary             a[href],
.package-list        a[href] {
   color: #049;
   text-decoration: none;
}

/* :hover */

a[href]:hover,
#fsf-support         a[href]:hover,
#fssbox              a[href]:hover {
   color: black;
   text-decoration: none;
}
#translations        a[href]:hover {
   background: #ebebff;
}
.toc                 a[href]:hover,
.summary             a[href]:hover,
.toc-inline          a[href]:hover,
.package-list        a[href]:hover {
   background: white;
}


/*********************/
/*  FORMS & BUTTONS  */
/*********************/

/** LAYOUT **/

form, legend, fieldset, textarea { padding: 0; }
form, legend, button, textarea, input { margin: 0; }
fieldset {
   border: 0;
   margin: 1em 0;
}
input, button, textarea, select, optgroup, option {
   font-family: inherit;
   font-size: inherit;
   font-style: normal;
   font-weight: normal;
}
button, select, input {
   padding: .2em .3em;
}
.button a, .switch a {
   display: block;
   display: inline-block;
}
#fssbox input, .button a {
   line-height: 1.1em;
   padding: .4em .6em;
}
#fssbox input[type="text"] { height: 1.1em; }
#fssbox input[type="submit"], .button a, .switch a {
   font-weight: bold;
   margin: 0;
}

#support-the-fsf a {
   padding: .6em 1em;
   margin: 1em 1.5em 0;
}

.backtotop a {
   float: right;
   position: relative; bottom: 1em;
   font-size: 1.2em;
   font-weight: bold;
   line-height: 1em;
   padding: .2em .6em .5em;
   margin: 0 1em;
}
.backtotop a span { display: none; }
.inner > .backtotop { margin-right: 3%; }


/** STYLE **/

.button    a[href],
.backtotop a[href] {
   text-decoration: none;
}
.button    a[href],
.backtotop a[href] {
   color: #4040bb;
   background: white;
   border: .1em solid #999;
   border: .1rem solid #999;
}
.button    a[href]:hover,
.backtotop a[href]:hover {
   color: #33c;
   background: #ebebff;
   border-color: #55b;
}
.switch a:hover {
   opacity: 80%;
}
#join-fsf a, #support-the-fsf a.join {
   color: #a32d2a;
   border-color: #a32d2a;
}
#join-fsf a:hover, #support-the-fsf a.join:hover {
   color: #960400;
   background: #f6e5e1;
}
#support-the-fsf a.donate {
   color: #4040bb;
   border-color: #55b;
}
#support-the-fsf a.donate:hover {
   color: #33c;
   background: #ebebff;
}
#support-the-fsf a.shop {
   color: #006363;
   border-color: #088;
}
#support-the-fsf a.shop:hover {
   color: #005f5f;
   background: #dff;
}

#fssbox input {
   background: white;
}
#fssbox input[type="text"] {
   color: #555;
   border: .1em solid #bbb;
}
#fssbox input[type="text"]:focus {
   color: #333;
   border-color: #088;
}
#fssbox input[type="submit"] {
   color: #006363;
   border: .1em solid #088;
   cursor: pointer;
}
#fssbox input[type="text"]:focus ~ input[type="submit"] {
   color: #005f5f;
   background: #dff;
}

.rounded-corners, .button a, .backtotop a, #fssbox input {
   border-radius: .4em;
   -moz-border-radius: .4em;
   -khtml-border-radius: .4em;
   -webkit-border-radius: .4em;
   -opera-border-radius: .4em;
}



/*====================================================================*/
/*                            FONT FAMILY                             */
/*====================================================================*/


a[href] { font-family: sans-serif; }
 
#fsf-frame a[href], form, .button a[href],
#header a[href], #fsf-support, #languages, #languages a[href],
.breadcrumb, .breadcrumb a, .trans-disclaimer, .trans-disclaimer a[href],
#outdated, #outdated a[href] {
   font-family: "Noto Sans Display", "Noto Sans", "Liberation Sans",
                sans-serif;
}
#gnu-banner strong {
   font-family: "Noto Sans", "Liberation Sans", sans-serif;
}

#navigation a[href], #edu-navigation a[href] {
   font-family: "Dosis", "Noto Sans Display", "Noto Sans", "Liberation Sans",
                sans-serif;
}
